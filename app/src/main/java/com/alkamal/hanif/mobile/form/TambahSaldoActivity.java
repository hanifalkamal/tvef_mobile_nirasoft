package com.alkamal.hanif.mobile.form;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.chat.ChatActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 19/02/2018.
 */

public class TambahSaldoActivity extends BaseActivity {
    String  myID,pin,tanggal,idtrx,Server;
    private ArrayList<String> array_bank = new ArrayList<String>() ;
    @BindView(R.id.ed_nominalSaldo)
    EditText nominal;
    @BindView(R.id.tv_statSaldo)
    TextView blsn;

    @BindView(R.id.sp_bank)
    Spinner pilihBank;

    @BindView(R.id.tv_Tiket) TextView tvTiket;

    int CekCounting,waktutimeout;
    NotificationCompat.Builder notification;
    Calendar calendar;
    SimpleDateFormat dateformat;
    private static final int uniqueID = 45612;
    private static SharedPreferences prefs;
    ProgressDialog prd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tambah_saldo_layout);
        getSupportActionBar().setElevation(0);
        //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ButterKnife.bind(this);

        prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
        myID = prefs.getString("MY_USERID","");
        Intent intent = getIntent();

        String iblsnSal = intent.getStringExtra("CekSal");

        blsn.setText(iblsnSal);

        array_bank.add("BCA");
        array_bank.add("BNI");
        //pilih = (Spinner) findViewById(R.id.sp_kodeBrg);
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.tambahsaldo_spinner_item, array_bank);
        pilihBank.setAdapter(adapter);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);

        tvTiket.setText(prefs.getString("TIKET", ""));
    }

    @OnClick(R.id.bt_tambahSaldo) public void onSend(){

        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();

        prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
        Server = prefs.getString("MY_SERVER", "");
        pin = prefs.getString("MY_PIN_TRX", "");

        //tv = prefs.getString("TIKET", "");

        String Format = "tiket "+pin+" "+nominal.getText().toString()+" "+pilihBank.getSelectedItem().toString();
        sendReqSal(Format);

    }

    @OnClick(R.id.bt_intruksiTambahSald) public void OnintruksiTambahsado(){

    }

    @OnClick(R.id.bt_tambahSaldo_wa) public void onSendWa(){

        Intent i = new Intent(TambahSaldoActivity.this,ChatActivity.class);
        startActivity(i);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.rightmenu_topup, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final Intent intent;
        switch (item.getItemId()){
            case R.id.m_konfTopup:
                //setContentView(R.layout.setting_format);
                intent = new Intent(TambahSaldoActivity.this, konfirmasiTopUpActivity.class);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }

    public void sendReqSal(String format){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;

        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx, Server, "");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(TambahSaldoActivity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        getBalsan();

                        //finish();
                        log("Berhasil");

                    } else {
                        prd.dismiss();
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(TambahSaldoActivity.this, "Gagal Load Data Cek Koneksi Internet", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void getBalsan(){
        Call<MsgResponse> call = getApi().GetMSg(myID,idtrx, Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {
                            prd.dismiss();
                            Intent intent = new Intent(TambahSaldoActivity.this, BalasanActivity.class);
                            intent.putExtra("CekSal",ges.getPesan());
                            Notif(ges.getPesan());

                            SharedPreferences.Editor editor = prefs.edit();

                            editor.putString("TIKET", ges.getPesan());
                            editor.apply();

                            tvTiket.setText(ges.getPesan());
                            startActivity(intent);

                        //Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_LONG).show();


                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                         Handler handler = new Handler();
                         handler.postDelayed(new Runnable() {
                         public void run() {
                         // yourMethod();
                         }
                         }, 1000);   //5 seconds


                        //finish();
                        //tvSaldo.setText(ges.getPesan());

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < waktutimeout){
                            getBalsan();
                            CekCounting = CekCounting+1;
                        }else{
                            prd.dismiss();
                            Notif("Time Out / Tidak ada balasan masuk");
                            Toast.makeText(TambahSaldoActivity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }

    public void Notif(String notifBody){
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker(notifBody);
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("TVEF");
        notification.setContentText(notifBody);

        Context context = getApplicationContext();
        Intent intent = new Intent(context, BalasanActivity.class);
        intent.putExtra("CekSal",notifBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());

    }

    public boolean contactExists(Context context, String number) {
/// number is the phone number
        Uri lookupUri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));
        String[] mPhoneNumberProjection = { ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME };
        Cursor cur = context.getContentResolver().query(lookupUri,mPhoneNumberProjection, null, null, null);
        try {
            if (cur.moveToFirst()) {
                return true;
            }
        }
        finally {
            if (cur != null)
                cur.close();
        }
        return false;
    }
}
