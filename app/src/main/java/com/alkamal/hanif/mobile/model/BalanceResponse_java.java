package com.alkamal.hanif.mobile.model;

public class BalanceResponse_java {
    private String saldo;
    private String nama;
    private String kdplgnmaster;
    private String kdplgn;
    private String hp;
    private String tglanggota;
    private String tglpoint;
    private String statusharga;
    private String maxpiutang;
    private String statusplgn;
    private String uperkodeplgn;
    private String kodeplgn_induk;
    private String namaupline;
    private String bankrekening;
    private String banknama;
    private String pointawal;
    private String komisi;
    private String idutama;
    private String NamaUpline_pelanggan;
    private String cekpoint;
    private String pengumuman;
    private String respon;
    private boolean success;

    public BalanceResponse_java() {
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKdplgnmaster() {
        return kdplgnmaster;
    }

    public void setKdplgnmaster(String kdplgnmaster) {
        this.kdplgnmaster = kdplgnmaster;
    }

    public String getKdplgn() {
        return kdplgn;
    }

    public void setKdplgn(String kdplgn) {
        this.kdplgn = kdplgn;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getTglanggota() {
        return tglanggota;
    }

    public void setTglanggota(String tglanggota) {
        this.tglanggota = tglanggota;
    }

    public String getTglpoint() {
        return tglpoint;
    }

    public void setTglpoint(String tglpoint) {
        this.tglpoint = tglpoint;
    }

    public String getStatusharga() {
        return statusharga;
    }

    public void setStatusharga(String statusharga) {
        this.statusharga = statusharga;
    }

    public String getMaxpiutang() {
        return maxpiutang;
    }

    public void setMaxpiutang(String maxpiutang) {
        this.maxpiutang = maxpiutang;
    }

    public String getStatusplgn() {
        return statusplgn;
    }

    public void setStatusplgn(String statusplgn) {
        this.statusplgn = statusplgn;
    }

    public String getUperkodeplgn() {
        return uperkodeplgn;
    }

    public void setUperkodeplgn(String uperkodeplgn) {
        this.uperkodeplgn = uperkodeplgn;
    }

    public String getKodeplgn_induk() {
        return kodeplgn_induk;
    }

    public void setKodeplgn_induk(String kodeplgn_induk) {
        this.kodeplgn_induk = kodeplgn_induk;
    }

    public String getNamaupline() {
        return namaupline;
    }

    public void setNamaupline(String namaupline) {
        this.namaupline = namaupline;
    }

    public String getBankrekening() {
        return bankrekening;
    }

    public void setBankrekening(String bankrekening) {
        this.bankrekening = bankrekening;
    }

    public String getBanknama() {
        return banknama;
    }

    public void setBanknama(String banknama) {
        this.banknama = banknama;
    }

    public String getPointawal() {
        return pointawal;
    }

    public void setPointawal(String pointawal) {
        this.pointawal = pointawal;
    }

    public String getKomisi() {
        return komisi;
    }

    public void setKomisi(String komisi) {
        this.komisi = komisi;
    }

    public String getIdutama() {
        return idutama;
    }

    public void setIdutama(String idutama) {
        this.idutama = idutama;
    }

    public String getNamaUpline_pelanggan() {
        return NamaUpline_pelanggan;
    }

    public void setNamaUpline_pelanggan(String namaUpline_pelanggan) {
        NamaUpline_pelanggan = namaUpline_pelanggan;
    }

    public String getCekpoint() {
        return cekpoint;
    }

    public void setCekpoint(String cekpoint) {
        this.cekpoint = cekpoint;
    }

    public String getPengumuman() {
        return pengumuman;
    }

    public void setPengumuman(String pengumuman) {
        this.pengumuman = pengumuman;
    }

    public String getRespon() {
        return respon;
    }

    public void setRespon(String respon) {
        this.respon = respon;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
