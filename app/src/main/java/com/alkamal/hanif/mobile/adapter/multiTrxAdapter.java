package com.alkamal.hanif.mobile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alkamal.hanif.mobile.R;

import java.util.List;

public class multiTrxAdapter extends BaseAdapter {
    private Context mContext;
    private List<MultiTrx> mMultiTrx;

    public multiTrxAdapter(Context mContext, List<MultiTrx> mMultiTrx) {
        this.mContext = mContext;
        this.mMultiTrx = mMultiTrx;
    }

    @Override
    public int getCount() {
        return mMultiTrx.size();
    }

    @Override
    public Object getItem(int position) {
        return mMultiTrx.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View v = View.inflate(mContext, R.layout.list_multitrx_item, null);
        TextView tvName = (TextView)v.findViewById(R.id.tv_Name);

        tvName.setText(mMultiTrx.get(position).getNama_trx());

        v.setTag(mMultiTrx.get(position).getId());

        return v;
    }
}
