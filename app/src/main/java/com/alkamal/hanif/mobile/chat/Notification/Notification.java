package com.alkamal.hanif.mobile.chat.Notification;

public class Notification {

        public String body ;
        public String title ;
        public String sound ;

        public Notification(String body, String title, String sound)  {
            this.body = body;
            this.title = title;
            this.sound = sound;
        }
}
