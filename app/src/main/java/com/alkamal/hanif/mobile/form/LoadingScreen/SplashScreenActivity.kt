package com.alkamal.hanif.mobile.form.LoadingScreen

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.form.Login.LoginActivity
import com.alkamal.hanif.mobile.form.MainHome.HomeActivity
import com.alkamal.hanif.mobile.form.Transaksi.InputPinActivity
import com.alkamal.hanif.mobile.model.BalanceResponse
import com.alkamal.hanif.mobile.utils.CacheManager.balance
import com.alkamal.hanif.mobile.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SplashScreenActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        //Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_LONG).show();


        //intent.putExtra("password",res.getPassword());
        //intent.putExtra("email",res.getEmail());


        if (!hasPermissions(this, Manifest.permission.READ_CONTACTS)){
            runtime_permissions()
            System.exit(1)
        }

        val handler = Handler()
        handler.postDelayed({
            if (preferences.getBoolean(Constant.STATE_HAS_LOGGED_IN, false)) {
                var intent = Intent(applicationContext, InputPinActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                var intent = Intent(applicationContext, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }, 1500)




//        cekSaldo("1234")
    }

    fun cekSaldo(pin: String) {
        val balanceResponse = arrayOf<BalanceResponse?>(null)
        val call = api.balance(kodePlgn, pin)
        call.enqueue(object : Callback<BalanceResponse?> {
            override fun onResponse(call: Call<BalanceResponse?>, response: Response<BalanceResponse?>) {
                balanceResponse[0] = response.body()
                if (balanceResponse[0] != null) {
                    if (balanceResponse[0]!!.success) {
                        balance = "Rp. " + currencyFormatter(balanceResponse[0]!!.saldo)
                        var intent = Intent(applicationContext, HomeActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
            }

            override fun onFailure(call: Call<BalanceResponse?>, t: Throwable) {
                log("Fail to check balance, message : " + t.message)
                makeErrorDialog("Tidak ada jaringan")
                balanceResponse[0]!!.success = false
                balanceResponse[0]!!.respon = t.message!!
            }
        })
    }


}