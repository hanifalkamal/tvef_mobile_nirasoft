package com.alkamal.hanif.mobile.form;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.MainActivity;
import com.alkamal.hanif.mobile.Response.CekSessionRespone;
import com.alkamal.hanif.mobile.Response.SetSessionRespone;
import com.alkamal.hanif.mobile.fungsi.md5;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.LoginRespone;
import com.alkamal.hanif.mobile.fungsi.session;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 01/02/2018.
 */

public class LoginActivity extends BaseActivity {

    @BindView(R.id.ed_psw)
    EditText edPsw;
    private static String myID;
    md5 toMd5;
    public static session Session;
    private SharedPreferences prefs;
    @BindView(R.id.ed_noTelp_login)
    EditText edNotelp;
    static String status = "";
    int keyDel = 0;
    ProgressDialog prd;

    //String myID;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        //   //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        //    Toast.makeText(LoginActivity.this, myImei, Toast.LENGTH_LONG).show();
        edNotelp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                edNotelp.setOnKeyListener(new View.OnKeyListener() {
//                    @Override
//                    public boolean onKey(View v, int keyCode, KeyEvent event) {
//                        if (keyCode == KeyEvent.KEYCODE_DEL)
//                            keyDel = 1;
//                        return false;
//                    }
//                });
//                if (keyDel == 0) {
//                    int len = edNotelp.getText().length();
//                    if(len > 1) {
//                        edNotelp.setText("62"+edNotelp.getText());
//                        edNotelp.setSelection(edNotelp.getText().length());
//                    }
//                } else {
//                    keyDel = 0;
//                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (("" + edNotelp.getText().charAt(0)).equals("0")) {
                        edNotelp.setText(edNotelp.getText().toString().substring(1));
                        edNotelp.append("62");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @OnClick(R.id.tv_GantiPsw)
    public void onGantiPsw() {
        Intent i = new Intent(LoginActivity.this, GantiPswActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.bt_daftar_baru)
    public void onDaftarBaru() {

        Intent i = new Intent(LoginActivity.this, opsiActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.bt_login)
    public void onLogon() {
        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
        String psw = edPsw.getText().toString();
//        Log.d("MD5 ", psw);
//        String StatusSession = cekSession(edNotelp.getText().toString(),psw);
        //cekSession(edNotelp.getText().toString(),psw);
        Login(psw);


    }

    public void setSession(String status, String notelp) {

        Call<SetSessionRespone> call = getApi().SetSession(notelp, status, "curdate()", "curtime()");
        call.enqueue(new Callback<SetSessionRespone>() {
            @Override
            public void onResponse(Call<SetSessionRespone> call, Response<SetSessionRespone> response) {
                if (response.isSuccessful()) {
                    SetSessionRespone res = response.body();
                    if (res.IsSuccess()) {

                        //finish();
                        log("Berhasil");
                        //SAMPE SINI
                    } else {
                        //Toast.makeText(session.this, "Anda telah terdaftar sebelumnya hubungi admin untuk daftar ulang", Toast.LENGTH_LONG).show();
                        //prd.dismiss();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SetSessionRespone> call, Throwable t) {
                log(t.toString());
            }
        });


    }

    public void cekSession(String notelp, String psw) {


        Call<CekSessionRespone> call = getApi().CekSession(notelp);
        call.enqueue(new Callback<CekSessionRespone>() {
            @Override
            public void onResponse(Call<CekSessionRespone> call, Response<CekSessionRespone> response) {
                if (response.isSuccessful()) {
                    CekSessionRespone res = response.body();
                    Log.e("Status", "Suucces");
                    if (res.isSuccess()) {
                        status = res.getStatus();
                        if (status.equals("OUT")){
                            Login(psw);
                        } else {
                            Toast.makeText(LoginActivity.this, "Anda sedang login di handphone lain, Silakan hubungi Customer Service !!!", Toast.LENGTH_LONG).show();
                        }
//                        status = "IN";
                        Log.e("Status Session", res.getStatus());
//                        Log.e("Status Session", StatusSession);
                        prd.dismiss();
                        //finish();
                        log("Berhasil");
                        //SAMPE SINI
                    } else {
                        status = "OUT";
                        Log.e("Status", "Force Out");
                        //Toast.makeText(session.this, "Anda telah terdaftar sebelumnya hubungi admin untuk daftar ulang", Toast.LENGTH_LONG).show();
                        //prd.dismiss();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<CekSessionRespone> call, Throwable t) {
                log(t.toString());
            }
        });
//        return status;
    }

    public void Login(String psw) {
        Call<LoginRespone> call = getApi().GetMSgLogin(edNotelp.getText().toString(), psw);
        call.enqueue(new Callback<LoginRespone>() {
            @Override
            public void onResponse(Call<LoginRespone> call, Response<LoginRespone> response) {
                if (response.isSuccessful()) {
                    LoginRespone res = response.body();
                    if (res.isSuccess()) {
                        prd.dismiss();
//                        Toast.makeText(LoginActivity.this, "Login Berhasil", Toast.LENGTH_LONG).show();
//
//                        myID = "AND" + edNotelp.getText().toString();
//                        prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putString("MY_USERID", myID);
//                        editor.putString("MY_NUMBER", edNotelp.getText().toString());
//                        editor.apply();
//                        Log.e("LOGIN Activity", "Saved ID :" + myID);
//
//
//                        setSession("IN", edNotelp.getText().toString());


//                        Intent intent = new Intent(LoginActivity.this, SetPinActivity.class);
                        Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
                        intent.putExtra("NoTelp", edNotelp.getText().toString());
                        startActivity(intent);
                        finish();
                        log("Berhasil");

//                        getSharedPreferences("PREFERENCE", MODE_PRIVATE)
//                                .edit()
//                                .putBoolean("isFirstRun", false)
//                                .apply();
                        status = "IN";
                    } else {
                        Toast.makeText(LoginActivity.this, "No Telepon atau Password tidak dikenal  ", Toast.LENGTH_LONG).show();
                        prd.dismiss();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRespone> call, Throwable t) {
                log(t.toString());
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (FirebaseAuth.getInstance().getCurrentUser() != null){
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            //finish();
        }
    }
}
