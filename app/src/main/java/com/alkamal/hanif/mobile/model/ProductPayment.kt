package com.alkamal.hanif.mobile.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
@Parcelize
data class ProductPayment (
        @SerializedName("kategori") var kategori: String,
        @SerializedName("item1") var item1 : String,
        @SerializedName("kode_item1") var kode_item1: String,
        @SerializedName("img_item1") var img_item1: String,
        @SerializedName("hint1") var hint1: String,
        @SerializedName("item2") var item2 : String,
        @SerializedName("kode_item2") var kode_item2: String,
        @SerializedName("img_item2") var img_item2: String,
        @SerializedName("hint2") var hint2: String,
        @SerializedName("item3") var item3 : String,
        @SerializedName("kode_item3") var kode_item3: String,
        @SerializedName("img_item3") var img_item3: String,
        @SerializedName("hint3") var hint3: String,
        @SerializedName("item4") var item4 : String,
        @SerializedName("kode_item4") var kode_item4: String,
        @SerializedName("img_item4") var img_item4: String,
        @SerializedName("hint4") var hint4: String
) : Parcelable