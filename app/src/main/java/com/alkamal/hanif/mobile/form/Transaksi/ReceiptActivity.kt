package com.alkamal.hanif.mobile.form.Transaksi

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.alkamal.hanif.mobile.BackgroundService.PrinterService
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.form.Konfigurasi.SettingReceiptFormat
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.activity_receipt.*
import kotlinx.android.synthetic.main.activity_receipt.button_back
import kotlinx.android.synthetic.main.activity_receipt.layout_tambah_harga
import kotlinx.android.synthetic.main.activity_receipt.text_harga
import kotlinx.android.synthetic.main.activity_transaksi.*
import kotlinx.android.synthetic.main.layout_tambah_harga_jual.*
import kotlinx.android.synthetic.main.layout_tambah_harga_jual.view.*
import kotlinx.android.synthetic.main.layout_tambah_harga_jual.view.text_batalkan

class ReceiptActivity : BaseActivity() {

    var harga = 0;
    var laba = 0;

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receipt)
        hideKeyboard(this@ReceiptActivity)
        animate("HIDE", layout_tambah_harga)

        if (intent.getBooleanExtra(Constant.RECEIPT_STATUS, true)) {

            if (intent.getStringExtra(Constant.RECEIPT_HEADER).toString().toUpperCase().equals("BERHASIL")) {
                text_header_receipt.setText("TRANSAKSI BERHASIL")
            } else {
                text_header_receipt.setText(intent.getStringExtra(Constant.RECEIPT_HEADER).toString().toUpperCase() + " BERHASIL")
            }

            text_pesan_receipt.setText(intent.getStringExtra(Constant.RECEIPT_PESAN))
            text_produk_receipt.setText(intent.getStringExtra(Constant.RECEIPT_PRODUK))
            text_tgl_receipt.setText(intent.getStringExtra(Constant.RECEIPT_TANGGAL))
            text_jam_receipt.setText(intent.getStringExtra(Constant.RECEIPT_WAKTU))
            text_no_tujuan_receipt.setText(intent.getStringExtra(Constant.RECEIPT_NOMOR_TUJUAN))
            text_id.setText(intent.getStringExtra(Constant.RECEIPT_ID))
            if (intent.getStringExtra(Constant.RECEIPT_HARGA).toString().contains(".")) {
                text_harga.setText("Rp. " + (intent.getStringExtra(Constant.RECEIPT_HARGA)))
            } else {
                text_harga.setText("Rp. " + currencyFormatter(intent.getStringExtra(Constant.RECEIPT_HARGA)))
            }

            var StringHarga = intent.getStringExtra(Constant.RECEIPT_HARGA).toString().replace(".","")
            harga = StringHarga.toInt()

        } else {
            // Filter Ganti Icon
            text_header_receipt.setTextColor(ContextCompat.getColor(applicationContext, R.color.NewUI_Red))
            image_header_receipt.background = ContextCompat.getDrawable(applicationContext, R.drawable.ic_fail)

            text_header_receipt.setText("TRANSAKSI GAGAL")
            text_pesan_receipt.setText(intent.getStringExtra(Constant.RECEIPT_PESAN).toString().toUpperCase())
            text_no_tujuan_receipt.setText(intent.getStringExtra(Constant.RECEIPT_NOMOR_TUJUAN))
            if (intent.getStringExtra(Constant.RECEIPT_PESAN).toString().contains("GAGAL")) {

                text_produk_receipt.setText(intent.getStringExtra(Constant.RECEIPT_PRODUK))
                text_tgl_receipt.setText(intent.getStringExtra(Constant.RECEIPT_TANGGAL))
                text_jam_receipt.setText(intent.getStringExtra(Constant.RECEIPT_WAKTU))
                text_id.setText(intent.getStringExtra(Constant.RECEIPT_ID))
                if (intent.getStringExtra(Constant.RECEIPT_HARGA) != null && intent.getStringExtra(Constant.RECEIPT_HARGA).toString().contains(".")) {
                    text_harga.setText("Rp. " + (intent.getStringExtra(Constant.RECEIPT_HARGA)))
                } else {
                    text_harga.setText("Rp. " + currencyFormatter(intent.getStringExtra(Constant.RECEIPT_HARGA)))
                }

            }
        }

        button_back.setOnClickListener {
            onBackPressed()
        }

        button_preview.setOnClickListener {
            var intent = Intent(applicationContext, SettingReceiptFormat::class.java)
            intent.putExtra(Constant.RECEIPT_HEADER, text_header_receipt.text)
            intent.putExtra(Constant.RECEIPT_PESAN, text_pesan_receipt.text)
            intent.putExtra(Constant.RECEIPT_PRODUK, text_produk_receipt.text)
            intent.putExtra(Constant.RECEIPT_TANGGAL, text_tgl_receipt.text)
            intent.putExtra(Constant.RECEIPT_WAKTU, text_jam_receipt.text)
            intent.putExtra(Constant.RECEIPT_NOMOR_TUJUAN, text_no_tujuan_receipt.text)
            intent.putExtra(Constant.RECEIPT_ID, text_id.text)
            intent.putExtra(Constant.RECEIPT_HARGA, text_harga.text)
            startActivity(intent)
        }

        button_print.setOnClickListener {
            print()
        }

        text_tambah_laba.setOnClickListener {
            animate("SHOW", layout_tambah_harga)
        }

        layout_tambah_harga.text_batalkan.setOnClickListener {
            animate("HIDE", layout_tambah_harga)
        }

        text_reset_laba.setOnClickListener {
            var StringHarga = intent.getStringExtra(Constant.RECEIPT_HARGA).toString().replace(".","")
            harga = StringHarga.toInt()
            if (intent.getStringExtra(Constant.RECEIPT_HARGA).toString().contains(".")) {
                text_harga.setText("Rp. " + (intent.getStringExtra(Constant.RECEIPT_HARGA)))
            } else {
                text_harga.setText("Rp. " + currencyFormatter(intent.getStringExtra(Constant.RECEIPT_HARGA)))
            }
        }

        layout_tambah_harga.ed_nominal.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                try {
                    laba = ed_nominal.text.toString().replace(".","").toInt()
                    currencyFormatterOnEdiittextListener(s.toString(), ed_nominal, this)
                } catch (E: java.lang.Exception) {
                    E.printStackTrace()
                }
            }

            override fun afterTextChanged(p0: Editable?) {

            }
        })

        layout_tambah_harga.button_terapkan.setOnClickListener {
            harga = harga + laba
            text_harga.setText("Rp. " + currencyFormatter(harga.toString()))
            animate("HIDE", layout_tambah_harga)
        }

    }

    fun print (){
        var header = preferences.getString(Constant.CONFIG_HEADER_PRINTER,"")
        var footer = preferences.getString(Constant.CONFIG_FOOTER_PRINTER,"")
        var body =
                "ID Transaksi : " + intent.getStringExtra(Constant.RECEIPT_ID) + "\n" +
                "Tanggal : " + intent.getStringExtra(Constant.RECEIPT_TANGGAL) + "\n" +
                "Jam : " + intent.getStringExtra(Constant.RECEIPT_WAKTU) + "\n" +
                "Produk : " + intent.getStringExtra(Constant.RECEIPT_PRODUK) + "\n" +
                "Harga : " + text_harga.text.toString() + "\n" +
                "Nomor Tujuan : " + intent.getStringExtra(Constant.RECEIPT_NOMOR_TUJUAN) + "\n"
        var dataToPrint = header +"\n\n" + body + "\n\n" + footer
        PrinterService.printData(dataToPrint,applicationContext)
    }

    var isAnimatedHeader = false
    fun animate(arrgument: String, view: View) {
        //Animate Header Show
        if (arrgument.equals("SHOW")) {
            if (!isAnimatedHeader) {
                isAnimatedHeader = true
                view.visibility = View.VISIBLE
                view.animate()
                        .translationY(0.toFloat()) //
                        .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                        .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_FADE_TO_SHOW)
                        .setListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator) {
                                super.onAnimationEnd(animation)
                                isAnimatedHeader = false
                            }

                        })
            }
        } else {
            //Animate Header Hide
            view.animate()
                    .translationY(view.height.toFloat()) //
                    .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                    .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_SHOW_TO_FADE)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)
                            isAnimatedHeader = false

                        }
                    })
        }
    }




}