package com.alkamal.hanif.mobile.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HistoryResponse(
        @SerializedName("idTrx") var idTrx : String,
        @SerializedName("tanggal") var tanggal : String,
        @SerializedName("jam") var jam : String,
        @SerializedName("status") var status : String,
        @SerializedName("kodeProduk") var kodeProduk : String,
        @SerializedName("namaProduk") var namaProduk : String,
        @SerializedName("nomorTujuan") var nomorTujuan : String,
        @SerializedName("pesan") var pesan : String,
        @SerializedName("hargaJual") var hargaJual : String
): Parcelable