package com.alkamal.hanif.mobile.utils

import com.alkamal.hanif.mobile.model.HistoryResponse

internal fun List<HistoryResponse>.filterEmptyData() : List<HistoryResponse>{
    val filteredList: ArrayList<HistoryResponse> = ArrayList<HistoryResponse>()
    for (histori in this){
        if (!histori.namaProduk.equals("-")){
            filteredList.add(histori)
        }
    }
    return filteredList

}