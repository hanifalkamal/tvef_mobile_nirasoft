package com.alkamal.hanif.mobile.BackgroundService;

import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
//import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.alkamal.hanif.mobile.APIService;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.NewsResponse;

import java.util.ArrayList;

import butterknife.BindView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by HanifAlKamal on 04/02/2018.
 */

public class beritaService extends Service  {

    private static ArrayList<String> array_pengumuman = new ArrayList<String>(3) ;


    NotificationCompat.Builder notification;
    private static final int uniqueID = 45612;
    @BindView(R.id.bt_news)
    Button news;
    String Server;
    SharedPreferences prefs;
    Integer QueueNews;
    public static Runnable runnable = null;
    public Context context = this;
    public Handler handler = null;

    APIService api ;

    @Override
    public void onCreate() {
        QueueNews = 0;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient().newBuilder();
        client.addInterceptor(interceptor);
        Retrofit base = new Retrofit.Builder()
                .baseUrl("http://www.tvef.xyz/tvefKirimPesan/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();
        api = base.create(APIService.class);

        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);

        Server = prefs.getString("MY_SERVER", "");
//        handler = new Handler();
//        runnable = new Runnable() {
//            @Override
//            public void run() {
//                getBalasan();
//                handler.postDelayed(runnable, 10000);
//
//            }
//        };
       // getBerita.run();
        //Intent i = new Intent(this , FirebaseInstanceService.class);
        //startService(i);

        //getBalasan();


    }

    public APIService getApi() {
        return api;
    }

    public void getBalasan(){


        Call<NewsResponse> call = getApi().GetNews("3",Server);
        call.enqueue(new Callback<NewsResponse>() {

            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                if (response.isSuccessful()) {
                    NewsResponse ges = response.body();
                    if (ges.IsSuccess()) {
                        array_pengumuman.add(0, ges.getPengumuman1());
                        array_pengumuman.add(1, ges.getPengumuman2());
                        array_pengumuman.add(2, ges.getPengumuman3());
                        if (QueueNews == 3){
                            QueueNews  = 0;
                        }

                        Log.e("here","ss");
                        prefs=getSharedPreferences("MY_NEWS", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("MY_NEWS1",array_pengumuman.get(QueueNews));
                        editor.apply();
                        if (array_pengumuman.get(QueueNews).equals(null)){
                            array_pengumuman.set(QueueNews, "Belum ada");
                            Toast.makeText(beritaService.this, "kosong", Toast.LENGTH_LONG).show();
                        }
                        Intent i = new Intent("NewsUpdate");
                        // i.putExtra("HotNews",ges.getPengumuman1());
                        sendBroadcast(i);

                        log("Berhasil");
                        QueueNews = QueueNews +1;
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {public void run() {
                            getBalasan();
                            // yourMethod();
                        }
                    }, 5000);   //5 seconds

                       // Thread.sleep(5000);// sleeps 1 second

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();

                        log("GAGAL");
                        getBalasan();
                    }
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {log(t.toString());}
        });

    }

    public void makeErrorDialog(String msg) {
        AlertDialog.Builder alertDBuilder = new AlertDialog.Builder(this);
        alertDBuilder.setMessage(msg);
        alertDBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = alertDBuilder.create();
        alertDialog.show();
    }

    public void log(String log) {
        if (true) {
            Log.d("Hanif" , log);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
