package com.alkamal.hanif.mobile.model

import com.google.gson.annotations.SerializedName

class ProductBanner(
        @SerializedName("id") var id : Int,
        @SerializedName("product_name") var product_name : String,
        @SerializedName("product_code") var product_code : String,
        @SerializedName("urutan") var urutan : Int,
        @SerializedName("banner") var banner : String
)