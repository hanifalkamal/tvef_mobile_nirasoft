package com.alkamal.hanif.mobile.Response;

/**
 * Created by HanifAlKamal on 10/12/2017.
 */

public class MsgResponse {
    private String kodeplgn,pesan,idtrx,namaserver,response_code,response_message;
    private Boolean success ;

    public String getResponse_code() {
        return response_code;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    public String getResponse_message() {
        return response_message;
    }

    public void setResponse_message(String response_message) {
        this.response_message = response_message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getNamaserver() {
        return namaserver;
    }

    public void setNamaserver(String namaserver) {
        this.namaserver = namaserver;
    }

    public String getIdtrx() {
        return idtrx;
    }

    public void setIdtrx(String idtrx) {
        this.idtrx = idtrx;
    }

    public String getKodeplgn() {
        return kodeplgn;
    }

    public void setKodeplgn(String kodeplgn) {
        this.kodeplgn = kodeplgn;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public Boolean IsSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
