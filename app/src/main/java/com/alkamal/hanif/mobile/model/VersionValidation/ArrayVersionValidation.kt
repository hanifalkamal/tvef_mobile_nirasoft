package com.itenas.sikpsipil.data.model.VersionValidation

import com.google.gson.annotations.SerializedName

class ArrayVersionValidation (
    @SerializedName("data") var data : List<VersionValidationResponse>
)