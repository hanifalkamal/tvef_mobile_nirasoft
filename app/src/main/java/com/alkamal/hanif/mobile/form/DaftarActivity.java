package com.alkamal.hanif.mobile.form;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.widget.EditText;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.DaftarRespone;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.fungsi.md5;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 09/02/2018.
 */

public class DaftarActivity extends BaseActivity {

    String myPswConf, myTlp, myPsw, myNamaUsaha;

    //String myID;
    md5 toMd5;
    public static String myID;
    Calendar calendar;
    SimpleDateFormat dateformat;
    String tanggal,idtrx;
    NotificationCompat.Builder notification;
    private static final int uniqueID = 45612;
    int CekCounting,waktutimeout;
    private SharedPreferences prefs;
    String jenisDaftar;
    @BindView(R.id.ed_nama_usaha) EditText edNamaUsaha;

    @BindView(R.id.ed_psw_conf)
    EditText EdPswConf;

    @BindView(R.id.ed_noTelp_daftar)
    EditText EdTlp;

    @BindView(R.id.ed_psw)
    EditText EdPsw;

    @BindView(R.id.ed_nama_server) EditText EdNamaServer;

    @BindView(R.id.ed_nama_pemilik) EditText edNamaPemilik;
    @BindView(R.id.ed_alamat) EditText edAlamat;
    ProgressDialog prd;
    String Nama,Alamat,Notelp,Server;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.daftar_layout);
        ButterKnife.bind(this);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);

        Intent intent = getIntent();
        jenisDaftar = intent.getStringExtra("jenisDaftar");
        ////myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
      //  //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));

       // myImei = myImei + "xxxx";
    }

    @OnClick(R.id.bt_intruksinamaserver) public void opsiintruksinamaserver(){
        Intent i = new Intent(DaftarActivity.this,intruksiPendaftaran_Activity.class);
        startActivity(i);
    }

    @OnClick(R.id.bt_Send_Daftar) public void onSendDaftar(){

        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
        prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("MY_SERVER","free_cell"); // ISI NAMA SERVER
//        editor.putString("MY_SERVER","free_cell"); // ISI NAMA SERVER
        editor.apply();
        myNamaUsaha = edNamaUsaha.getText().toString();
        myPswConf = EdPswConf.getText().toString();
        myTlp = "62"+EdTlp.getText().toString();
        myPsw =(EdPsw.getText().toString());


      //  myNoJoin = EdNoJoin.getText().toString();
       // myAlamat = EdAlamat.getText().toString();

        if (myPswConf.matches("")||myTlp.matches("")||myPsw.matches("")||myNamaUsaha.matches("")){
            Toast.makeText(DaftarActivity.this, "Wajib di isi Semua", Toast.LENGTH_LONG).show();
        } else {

             if (myPsw.matches(myPswConf)){

                 SendDataDaftarAplikasi();

             } else {
                 Toast.makeText(DaftarActivity.this, "Password tidak sama", Toast.LENGTH_LONG).show();
                 prd.dismiss();
             }

        }
    }

    public void SendDataDaftarAplikasi(){

        myID = "AND"+myTlp;

        Call<DaftarRespone> call = getApi().GetMSgDaftar(myTlp, myPsw, myNamaUsaha);
        call.enqueue(new Callback<DaftarRespone>() {
            @Override
            public void onResponse(Call<DaftarRespone> call, Response<DaftarRespone> response) {
                if(response.isSuccessful()){
                    DaftarRespone res = response.body();
                    if(res.IsSuccess()){
                        prd.dismiss();
                   //     Toast.makeText(DaftarActivity.this, "Daftar Berhasil", Toast.LENGTH_LONG).show();
                        if (jenisDaftar.equals("join")){
                            Intent i = new Intent(DaftarActivity.this,join_actity.class);
                            i.putExtra("nama",edNamaPemilik.getText().toString());
                            i.putExtra("alamat",edAlamat.getText().toString());
                            i.putExtra("notelp", EdTlp.getText().toString());

                            startActivity(i);
                        } else if (jenisDaftar.equals("kaitkan")){
                            Intent i = new Intent(DaftarActivity.this,kaitkanActivity.class);
                            startActivity(i);
                        } else  {
                            Nama = edNamaPemilik.getText().toString();
                            Alamat = edAlamat.getText().toString();
                            Notelp = "62"+EdTlp.getText().toString();
//                            Server = "free_cell"; //Ganti Nama Server Disini
                            Server = "free_cell"; //Ganti Nama Server Disini

                            String Format = "daftar " +
                                    "\""+Nama +"\""+ " " +
                                    "\""+Alamat+"\"" + " "+
                                    "\""+Notelp+"\"" + " " ;
                            Toast.makeText(DaftarActivity.this, Format,Toast.LENGTH_LONG).show();

                            sendReqSal(Format);
                        }

                        finish();
                        log("Berhasil");
                        //SAMPE SINI
                    } else {Toast.makeText(DaftarActivity.this, "Anda telah terdaftar sebelumnya hubungi admin untuk daftar ulang", Toast.LENGTH_LONG).show();
                        prd.dismiss();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<DaftarRespone> call, Throwable t) {
                log(t.toString());
            }
        });

    }

    public void sendReqSal(String format){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;

        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx, Server, "");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(DaftarActivity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();

                        getBalsan();

                        //finish();
                         log("Berhasil");

                    } else {
                        //tvMsg.setText(res.getMessage());
                        //prd.dismiss();
                        Toast.makeText(DaftarActivity.this, "Cek Koneksi Internet", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void getBalsan(){

        Call<MsgResponse> call = getApi().GetMSg(myID,idtrx, Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {
                        Toast.makeText(DaftarActivity.this, "Daftar Berhasil", Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(DaftarActivity.this, BalasanActivity.class);
                        intent.putExtra("CekSal",ges.getPesan());


                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                // yourMethod();
                            }
                        }, 1000);   //5 seconds*/
                        prd.dismiss();
                        //Notif(ges.getPesan()); -> notifs
                        startActivity(intent);
                        finish();
                        //tvSaldo.setText(ges.getPesan());

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < waktutimeout){
                            getBalsan();
                            CekCounting = CekCounting+1;
                        }else{
                            prd.dismiss();
                            //("Time Out / Tidak ada balasan masuk");
                            Toast.makeText(DaftarActivity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }

    public void Notif(String notifBody){
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker("This is the ticker");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("TVEF");
        notification.setContentText(notifBody);

        Context context = getApplicationContext();
        Intent intent = new Intent(context, BalasanActivity.class);
        intent.putExtra("CekSal",notifBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());

    }




}
