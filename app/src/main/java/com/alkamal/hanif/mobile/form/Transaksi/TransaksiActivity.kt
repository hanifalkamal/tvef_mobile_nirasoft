package com.alkamal.hanif.mobile.form.Transaksi

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.model.Provider
import com.alkamal.hanif.mobile.utils.CacheManager
import com.alkamal.hanif.mobile.utils.Constant
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_transaksi.*
import kotlinx.android.synthetic.main.activity_transaksi.bt_trxKirim
import kotlinx.android.synthetic.main.activity_transaksi.ed_noTelp
import kotlinx.android.synthetic.main.activity_transaksi.relative_layout_img
import kotlinx.android.synthetic.main.layout_konfirmasi_transaksi.*
import kotlinx.android.synthetic.main.layout_konfirmasi_transaksi.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TransaksiActivity : BaseActivity() {


    var ProvideTeridentifikasi = false
    var isAnimatedImg = false
    var isAnimatedHeader = false
    var kodeBarang = ""
    var hargaBarang = ""
    var selectedBank = ""
    lateinit var notujuan: String
    lateinit var providerName: String
    private val PICK_CONTACT = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaksi)

        if (!CacheManager.balance.isNullOrEmpty()) {
            text_saldo_trx.setText(CacheManager.balance)
        }

        initHeader(header_telp)

        if (intent.hasExtra(Constant.HOME_TRX_PULSA) || intent.hasExtra(Constant.HOME_KODE_BRG_EXTRA)) {
            InitProduk()
            initHeader(layout_konfirm)
            initHeader(header_pilih_produk)
            header_title.setText("Produk")
            relative_layout_img.visibility = View.VISIBLE
            layout_product_trx.visibility = View.VISIBLE
            layout_input_nominal.visibility = View.GONE
            layout_kode_transaksi.visibility = View.GONE
            checkbox_ralat.visibility = View.GONE
            layout_button_bca.visibility = View.GONE
            layout_button_bri.visibility = View.GONE
            layout_no_telp_trx.visibility = View.VISIBLE
            bt_trxKirim.setText("Beli Produk")
            text_instruksi.visibility = View.GONE
            ed_prefix.visibility = View.GONE

        } else if (intent.hasExtra(Constant.HOME_TRANSFER_SALDO)) {
            animate("HIDE", layout_konfirm)
            initHeader(header_input_nominal)
            initHeader(header_kode_transaksi)
            header_title.setText("Transfer Saldo")
            bt_trxKirim.setText("Transfer Saldo")
            checkbox_ralat.visibility = View.VISIBLE
            layout_input_nominal.visibility = View.VISIBLE
            layout_no_telp_trx.visibility = View.VISIBLE
            layout_button_bca.visibility = View.GONE
            layout_button_bri.visibility = View.GONE
            text_instruksi.visibility = View.GONE
            ed_prefix.visibility = View.GONE
            ed_noTelp.inputType = InputType.TYPE_CLASS_TEXT
            ed_noTelp.hint ="Masukan nomor ponsel / kode pelangan"
        } else {
            initHeader(header_input_nominal)
            header_title.setText("Tambah Saldo")
            bt_trxKirim.setText("Tambah Saldo")
            layout_button_bca.visibility = View.VISIBLE
            layout_button_bri.visibility = View.VISIBLE
            layout_input_nominal.visibility = View.VISIBLE
            layout_no_telp_trx.visibility = View.GONE
            checkbox_ralat.visibility = View.GONE
            text_instruksi.visibility = View.VISIBLE
        }

        text_instruksi.setOnClickListener {
            var intent = Intent(applicationContext, TopUpInstructionActivity::class.java)
            startActivity(intent)
        }

        checkbox_ralat.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                bt_trxKirim.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ui_buttonskin_deactive_shape))
                layout_kode_transaksi.visibility = View.VISIBLE
            } else {
                layout_kode_transaksi.visibility = View.GONE
            }

        }

        ed_input_nominal.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    currencyFormatterOnEdiittextListener(s.toString(), ed_input_nominal, this)
                } catch (E: java.lang.Exception) {
                    E.printStackTrace()
                }
            }
        })

        layout_button_bca.setOnClickListener {
            SelectedBank(layout_button_bca, radiobutton_bca)
            selectedBank = getString(R.string.TOPUP_SELECTED_BCA)
        }

        layout_button_bri.setOnClickListener {
            SelectedBank(layout_button_bri, radiobutton_bri)
            selectedBank = getString(R.string.TOPUP_SELECTED_BRI)
        }

        button_back.setOnClickListener {
            onBackPressed()
        }

        image_contact.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
            startActivityForResult(intent, PICK_CONTACT)
        }

        layout_konfirm.text_batalkan_trf.setOnClickListener {
//            layout_konfirm_trf.visibility = View.GONE
            animate("HIDE", layout_konfirm)
        }

        layout_konfirm.button_confirm_transaksi.setOnClickListener {

            if (intent.hasExtra(Constant.HOME_TRX_PULSA) || intent.hasExtra(Constant.HOME_KODE_BRG_EXTRA)) {

                var intent = Intent(this, InputPinActivity::class.java)
                intent.putExtra(Constant.SELECTED_PRODUK_KODE_EXTRA, kodeBarang)
                intent.putExtra(Constant.SELECTED_PRODUK_NAMA_EXTRA, text_barang.text.toString())
                intent.putExtra(Constant.SELECTED_PRODUK_HARGA_EXTRA, hargaBarang)
                intent.putExtra("nomorHp", ed_noTelp.text.toString())
                startActivity(intent)
                finish()

            } else {
                var intent = Intent(applicationContext, InputPinActivity::class.java)
                intent.putExtra(Constant.HOME_TRANSFER_SALDO, intent.getStringExtra(Constant.HOME_TRANSFER_SALDO))
                intent.putExtra("nomorHp", ed_noTelp.text.toString())
                intent.putExtra("nominal", ed_input_nominal.text.toString().replace(".", ""))
                if (checkbox_ralat.isChecked) {
                    intent.putExtra("kodeTrf", ed_kode_transaksi.text.toString())
                }
                startActivity(intent)
                finish()

            }

        }

        bt_trxKirim.setOnClickListener {

            if (intent.hasExtra(Constant.HOME_TRX_PULSA) || intent.hasExtra(Constant.HOME_KODE_BRG_EXTRA)) {
                if (ed_noTelp.text.isEmpty()) {

                    showErrMessage("Silakan isi nomor tujuan")

                } else if (!kodeBarang.isNullOrEmpty()) {

                    layout_konfirm.header_konfirm_transaksi.setText("Konfirmasi Transaksi")

                    layout_konfirm.text_no_tujuan_transaksi.setText(ed_noTelp.text.toString())

                    layout_konfirm.header_kode_konfirm_transaksi.setText("Produk")
                    layout_konfirm.text_kode_transaksi.setText(text_barang.text.toString())

                    layout_konfirm.text_header_harga.setText("Harga")
                    layout_konfirm.text_nominal_transaksi.setText("Rp. "+currencyFormatter(hargaBarang))

                    layout_konfirm.text_header_keterangan.visibility = View.GONE
                    layout_konfirm.text_konfirm_keterangan.visibility = View.GONE

                    button_confirm_transaksi.setText("SUBMIT")

                    animate("SHOW", layout_konfirm)

                } else {

                    showErrMessage("Produk belum dipilih !")

                }
            } else if (intent.hasExtra(Constant.HOME_TRANSFER_SALDO)) {


                if (ed_noTelp.text.isEmpty()) {

                    showErrMessage("Lengkapi Nomor Tujuan !")

                } else if (ed_input_nominal.text.isEmpty()) {
                    showErrMessage("Lengkapi Nominal !")
                } else {

                    layout_konfirm.text_no_tujuan_transaksi.setText(ed_noTelp.text.toString())
                    layout_konfirm.text_nominal_transaksi.setText("Rp. " + ed_input_nominal.text.toString())

                    if (checkbox_ralat.isChecked) {
                        if (ed_kode_transaksi.text.isNullOrEmpty()) {

                            showErrMessage("Lengkapi Kode Transaksi !")

                        } else {
                            // RALAT
                            layout_konfirm.text_kode_transaksi.visibility = View.VISIBLE
                            layout_konfirm.header_kode_konfirm_transaksi.visibility = View.VISIBLE
                            layout_konfirm.text_kode_transaksi.setText(ed_kode_transaksi.text.toString())
                            layout_konfirm.text_konfirm_keterangan.setText("Ralat Transfer Saldo")
                            animate("SHOW", layout_konfirm)
                        }
                    } else {
                        // TRF BiASA
                        layout_konfirm.text_konfirm_keterangan.setText("Transfer Saldo")
                        layout_konfirm.text_kode_transaksi.visibility = View.GONE
                        layout_konfirm.header_kode_konfirm_transaksi.visibility = View.GONE

                        animate("SHOW", layout_konfirm)
                    }
                }


            } else if (intent.hasExtra(Constant.HOME_TAMBAH_SALDO)) {

                if (!ed_input_nominal.text.isNullOrEmpty()) {
                    if (!selectedBank.isNullOrEmpty()) {

                        var intent = Intent(applicationContext, InputPinActivity::class.java)
                        intent.putExtra(Constant.HOME_TAMBAH_SALDO, intent.getStringExtra(Constant.HOME_TAMBAH_SALDO))
                        intent.putExtra("nominal", ed_input_nominal.text.toString().replace(".", ""))
                        intent.putExtra("bank", selectedBank)
                        startActivity(intent)
                        finish()

                    } else {
                        showErrMessage("Pilih Salah Satu BANK !")
                    }
                } else {
                    showErrMessage("Lengkapi Nominal !")
                }


            }

        }

        ed_produk.setOnClickListener {

            if (ProvideTeridentifikasi) {
                ed_produk.setText("")
                var i = Intent(this, ListProdukActivity::class.java)
                if (intent.hasExtra(Constant.HOME_TRX_PULSA)) {
                    i.putExtra("NoTelp", notujuan)
                    i.putExtra("Jenis", intent.getStringExtra(Constant.HOME_TRX_PULSA))
                    i.putExtra("KodeBrg", "")
                } else if (intent.hasExtra(Constant.HOME_KODE_BRG_EXTRA)) {
                    i.putExtra("NoTelp", "")
                    i.putExtra("Jenis", "")
                    i.putExtra("KodeBrg", intent.getStringExtra(Constant.HOME_KODE_BRG_EXTRA))
                }
                startActivityForResult(i, 1)

            } else {

                showErrMessage("Lengkapi Nomor Handphone !")

            }
        }

        ed_noTelp.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

                try {
                    if (ed_noTelp.text.isNotBlank()) {


                        if (!intent.hasExtra(Constant.HOME_KODE_BRG_EXTRA)) {

                            if (!intent.hasExtra(Constant.HOME_TRANSFER_SALDO)) {
                                //TRX PULSA or DATA

                                //no 62 or 0 in first latter
//                                if ("" + ed_noTelp.getText().get(0) == "0") {
//                                    ed_noTelp.setText(ed_noTelp.getText().toString().substring(1))
//                                    //edNotelp.append("");
//                                } else if ("" + ed_noTelp.getText().get(0) + "" + ed_noTelp.getText().get(1) == "62") {
//                                    ed_noTelp.setText(ed_noTelp.getText().toString().substring(2))
//                                }

                                notujuan = ed_prefix.text.toString() + ed_noTelp.getText().toString().substring(1)

                            } else {
                                //Auto 62 if 0 input on first latter
                                if ("" + ed_noTelp.text.toString().get(0) == "0") {

                                    notujuan = "62"+ed_noTelp.getText().toString().substring(1)
//                                    ed_noTelp.setText()
//                                    ed_noTelp.append("62")
//                                    notujuan = ed_noTelp.text.toString()
                                }


                            }

                        } else {
                            notujuan = ed_noTelp.text.toString()
                        }

                        if (intent.hasExtra(Constant.HOME_TRX_PULSA)) {


                            if (ed_noTelp.text.length >= 5) {

                                if (!ProvideTeridentifikasi) {
                                    loadImage()
                                }

                            } else if (ed_noTelp.text.length == 4) {
                                ProvideTeridentifikasi = false
                                animate("HIDE", relative_layout_img)
                            }

                        } else {
//                            notujuan = ed_noTelp.text.toString()
                            ProvideTeridentifikasi = true
                        }

                        animate("SHOW", header_telp)
                    } else {

                        animate("HIDE", header_telp)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


        ed_input_nominal.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

                try {
                    if (ed_input_nominal.text.isNotBlank()) {
                        animate("SHOW", header_input_nominal)
                    } else {
                        animate("HIDE", header_input_nominal)
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        ed_kode_transaksi.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                try {
                    if (ed_kode_transaksi.text.isNotBlank()) {
                        animate("SHOW", header_kode_transaksi)
                    } else {
                        animate("HIDE", header_kode_transaksi)
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

    }

    fun animate(arrgument: String, view: View) {


        when (view) {

            header_telp, header_pilih_produk, header_input_nominal, header_kode_transaksi -> {
                //Animate Header Show
                if (arrgument.equals("SHOW")) {
                    if (!isAnimatedHeader) {
                        isAnimatedHeader = true
                        view.visibility = View.VISIBLE
                        view.animate()
                                .translationY(0.toFloat()) //
                                .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                                .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_FADE_TO_SHOW)
                                .setListener(object : AnimatorListenerAdapter() {
                                    override fun onAnimationEnd(animation: Animator) {
                                        super.onAnimationEnd(animation)
                                        isAnimatedHeader = false
                                    }

                                })
                    }
                } else {
                    //Animate Header Hide
                    view.animate()
                            .translationY(view.height.toFloat()) //
                            .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                            .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_SHOW_TO_FADE)
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    super.onAnimationEnd(animation)
                                    isAnimatedHeader = false

                                }
                            })
//                    }
                }


            }

            relative_layout_img -> {
                //Animate Header Show
                if (arrgument.equals("SHOW")) {
                    if (!isAnimatedImg) {
                        isAnimatedImg = true
                        view.visibility = View.VISIBLE
                        view.animate()
                                .translationY(0.toFloat()) //
                                .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                                .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_FADE_TO_SHOW)
                                .setListener(object : AnimatorListenerAdapter() {
                                    override fun onAnimationEnd(animation: Animator) {
                                        super.onAnimationEnd(animation)
                                        isAnimatedImg = false
                                    }

                                })
                    }
                } else {
                    //Animate Header Hide
                    view.animate()
                            .translationY(view.height.toFloat()) //
                            .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                            .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_SHOW_TO_FADE)
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    super.onAnimationEnd(animation)
                                    isAnimatedImg = false

                                }
                            })
//                    }
                }
            }

            layout_konfirm -> {
                if (arrgument.equals("SHOW")) {
                    if (!isAnimatedHeader) {
                        isAnimatedHeader = true
                        view.visibility = View.VISIBLE
                        view.animate()
                                .translationY(0.toFloat()) //
                                .setDuration(Constant.KONFIRM_TRF_DURATION.toLong())
                                .alpha(Constant.KONFIRM_TRF_TRANSALTION_FADE_TO_SHOW)
                                .setListener(object : AnimatorListenerAdapter() {
                                    override fun onAnimationEnd(animation: Animator) {
                                        super.onAnimationEnd(animation)
                                        isAnimatedHeader = false
                                    }

                                })
                    }
                } else {
                    //Animate Header Hide
                    view.animate()
                            .translationY(view.height.toFloat()) //
                            .setDuration(Constant.KONFIRM_TRF_DURATION.toLong())
                            .alpha(Constant.KONFIRM_TRF_TRANSALTION_SHOW_TO_FADE)
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    super.onAnimationEnd(animation)
                                    isAnimatedHeader = false

                                }
                            })
//                    }
                }
            }
        }

    }


    fun initHeader(view: View) {
        view.visibility = View.INVISIBLE
        view.animate()
                .translationY(Constant.HEADER_EDITTEXT_TRANSALTION_TO_BOTTOM.toFloat()) //
                .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_SHOW_TO_FADE)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)

                    }
                })

    }

    fun initConfirmTrfSaldo(view: View) {
        view.visibility = View.INVISIBLE
        view.animate()
                .translationY(Constant.KONFIRM_TRF_TRANSALTION_TO_BOTTOM.toFloat()) //
                .setDuration(Constant.KONFIRM_TRF_DURATION.toLong())
                .alpha(Constant.KONFIRM_TRF_TRANSALTION_SHOW_TO_FADE)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)

                    }
                })

    }

    fun InitProduk() {

        if (!intent.hasExtra(Constant.HOME_TRX_PULSA)) {
            relative_layout_img.visibility = View.VISIBLE
            ed_prefix.visibility = View.GONE


            if (intent.getStringExtra(Constant.HOME_KODE_BRG_EXTRA).equals("PLN")) {

                Glide.with(applicationContext)
                        .load(ContextCompat.getDrawable(applicationContext, R.drawable.ic_pln))
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable?>, isFirstResource: Boolean): Boolean {
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any, target: Target<Drawable?>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                                return false
                            }
                        })
                        .into(img_product_trx)
            } else {
                showImage(
                        intent.getStringExtra(Constant.HOME_IMAGE_EXTRA),
                        img_product_trx,
                        this
                )
            }

            ed_noTelp.hint = intent.getStringExtra(Constant.HOME_HINT_EXTRA)
            text_product_name_trx.setText(intent.getStringExtra(Constant.HOME_NAMA_PRODUK_EXTRA))
            providerName = intent.getStringExtra(Constant.HOME_NAMA_PRODUK_EXTRA).toString()
        } else {
            ed_noTelp.inputType = InputType.TYPE_CLASS_NUMBER
            ed_prefix.visibility = View.VISIBLE
            initHeader(relative_layout_img)
        }


    }

    fun showErrMessage(msg: String) {
        text_error_message.setText(msg)
        text_error_message.visibility = View.VISIBLE
    }

    fun hideErrMessage() {
        text_error_message.visibility = View.GONE
    }

    fun loadImage() {
        var call = api.getImageByPrefix(notujuan)
        call.enqueue(object : Callback<Provider> {

            override fun onFailure(call: Call<Provider>, t: Throwable) {

                showErrMessage("Connection Error, Try again later")
            }

            override fun onResponse(call: Call<Provider>, response: Response<Provider>) {
                var provider = response.body()

                if (provider != null) {
                    if (provider.responseMessage != null) {

                        showErrMessage(provider.responseMessage)

                    } else {
                        hideErrMessage()
                        animate("SHOW", relative_layout_img)
                        ProvideTeridentifikasi = true
//                        relative_layout_img.visibility = View.VISIBLE
                        showImage(provider.imageUrl, img_product_trx, applicationContext)
                        text_product_name_trx.text = provider.nama_provider
                        providerName = provider.nama_provider

                    }
                }
            }
        })
    }

    fun SelectedBank(layout: RelativeLayout, rdButton: ImageView) {

        UnSelectAllBank()
        layout.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ui_buttonskin_active_transparent_shape))
        rdButton.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ic_radiobutton_selected))

    }

    fun UnSelectAllBank() {

        var listLayout: MutableList<RelativeLayout> = ArrayList()
        listLayout.add(layout_button_bca)
        listLayout.add(layout_button_bri)

        var listRadioButton: MutableList<ImageView> = ArrayList()
        listRadioButton.add(radiobutton_bca)
        listRadioButton.add(radiobutton_bri)

        for (item in listLayout) {
            item.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ui_buttonskin_deactive_transparent_shape))
        }

        for (item in listRadioButton) {
            item.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ic_radiobutton_unselected))
        }

    }

    override fun dispatchTouchEvent(event: MotionEvent?): Boolean {

        if (intent.hasExtra(Constant.HOME_TRANSFER_SALDO)) {
            bt_trxKirim.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ui_buttonskin_deactive_shape))
            if (!ed_noTelp.text.isEmpty()) {
                if (!ed_input_nominal.text.isEmpty()) {
                    if (checkbox_ralat.isChecked) {
                        if (!ed_kode_transaksi.text.isNullOrEmpty()) {
                            bt_trxKirim.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ui_buttonskin_active_shape))
                        }
                    } else {
                        bt_trxKirim.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ui_buttonskin_active_shape))
                    }
                }
            }
        } else if (intent.hasExtra(Constant.HOME_TAMBAH_SALDO)) {
            bt_trxKirim.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ui_buttonskin_deactive_shape))
            if (!ed_input_nominal.text.isNullOrEmpty()) {
                if (!selectedBank.isNullOrEmpty()) {
                    bt_trxKirim.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ui_buttonskin_active_shape))
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            hideErrMessage()
            val selectedProduk = data!!.getStringExtra(Constant.SELECTED_PRODUK_NAMA_EXTRA) + "\n" +
                    currencyFormatter(data.getStringExtra(Constant.SELECTED_PRODUK_HARGA_EXTRA))
            ed_produk.setText(selectedProduk)
            animate("SHOW", header_pilih_produk)
            hargaBarang = data.getStringExtra(Constant.SELECTED_PRODUK_HARGA_EXTRA).toString()
            text_harga.setText("Rp. " + currencyFormatter(data.getStringExtra(Constant.SELECTED_PRODUK_HARGA_EXTRA)))
            text_barang.setText(data!!.getStringExtra(Constant.SELECTED_PRODUK_NAMA_EXTRA))
            kodeBarang = data.getStringExtra(Constant.SELECTED_PRODUK_KODE_EXTRA).toString()
            text_provider.setText(providerName)


            relative_layout_detail_pembayaran.visibility = View.VISIBLE
            bt_trxKirim.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ui_buttonskin_active_shape))

        } else if (requestCode == PICK_CONTACT) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                val ContactData = data!!.data
                val C = ContactData?.let { contentResolver.query(it, null, null, null, null) }
                if (C != null) {
                    if (C.moveToFirst()) {
                        val name = C.getString(C.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME))
                        val cr = contentResolver
                        val cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                                "DISPLAY_NAME = '$name'", null, null)
                        if (cursor!!.moveToFirst()) {
                            val contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                            val phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null)
                            while (phones!!.moveToNext()) {
                                val number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                                val type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE))
                                when (type) {
                                    ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE -> {
                                        // do something with the Mobile number here...
                                        val plusEmanDua = "" + number[0] + number[1] + number[2]
                                        val EnamDua = "" + number[0] + number[1]
                                        val nol = "" + number[0]
                                        var noTelpTrx = ""
        //                                    if (nol == "0") {
                                        noTelpTrx = number
        //                                    } else
                                        if (EnamDua == "62") {
                                            noTelpTrx = number.substring(2)
                                            noTelpTrx = "0"+noTelpTrx
                                        } else if (plusEmanDua == "+62") {
                                            noTelpTrx = number.substring(3)
                                            noTelpTrx = "0"+noTelpTrx
                                        }
                                        ed_noTelp.setText(noTelpTrx)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
