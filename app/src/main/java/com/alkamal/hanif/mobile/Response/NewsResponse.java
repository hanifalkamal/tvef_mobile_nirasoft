package com.alkamal.hanif.mobile.Response;

/**
 * Created by HanifAlKamal on 23/02/2018.
 */

public class NewsResponse {
    private String versi_android,pengumuman1,pengumuman2,pengumuman3,namaserver;
    private Boolean success;

    public NewsResponse(String versi_android, String pengumuman1, String pengumuman2, String pengumuman3, Boolean success) {
        this.versi_android = versi_android;
        this.pengumuman1 = pengumuman1;
        this.pengumuman2 = pengumuman2;
        this.pengumuman3 = pengumuman3;
        this.success = success;
    }

    public String getNamaserver() {
        return namaserver;
    }

    public void setNamaserver(String namaserver) {
        this.namaserver = namaserver;
    }

    public String getVersi_android() {
        return versi_android;
    }

    public void setVersi_android(String versi_android) {
        this.versi_android = versi_android;
    }

    public String getPengumuman1() {
        return pengumuman1;
    }

    public void setPengumuman1(String pengumuman1) {
        this.pengumuman1 = pengumuman1;
    }

    public String getPengumuman2() {
        return pengumuman2;
    }

    public void setPengumuman2(String pengumuman2) {
        this.pengumuman2 = pengumuman2;
    }

    public String getPengumuman3() {
        return pengumuman3;
    }

    public void setPengumuman3(String pengumuman3) {
        this.pengumuman3 = pengumuman3;
    }

    public Boolean IsSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
