package com.alkamal.hanif.mobile.form;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.MainActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 01/03/2018.
 */

public class kaitkanActivity extends BaseActivity{
    static String pin,tanggal,idtrx,Server;//,myID;
    int CekCounting,waktutimeout;
    NotificationCompat.Builder notification;
    Calendar calendar;
    SimpleDateFormat dateformat;
    private static final int uniqueID = 45612;
    ProgressDialog prd;
    DaftarActivity daftarAct;

    @BindView(R.id.ed_kaitkan_nomor)
    EditText edNo;

    @OnClick(R.id.bt_kaitkan) public void onKaitkan(){
        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu ...");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
        String Format = "prakaitkan 62"+ edNo.getText().toString();
        //myID = "AND62"+ edNo.getText().toString();
        sendReqSal(Format);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.rightmenu_kaitkan, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final Intent intent;
        switch (item.getItemId()){
            case R.id.m_konfKaitkan:
                //setContentView(R.layout.setting_format);
                intent = new Intent(kaitkanActivity.this, VerifikasiKaitkanActivity.class);
                intent.putExtra("nomor",edNo.getText().toString());
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kaitkan_layout);
        ButterKnife.bind(this);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);
        ////myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
        pin = prefs.getString("MY_PIN_TRX", "");
        Server = prefs.getString("MY_SERVER", "");

        String Format = "laptrx "+pin ;


    }

    public void sendReqSal(String format){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;

        Call<SendMsgRespone> call = getApi().SendMSg(daftarAct.myID, format, idtrx,Server,"");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(kaitkanActivity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        getBalsan();

                        //finish();
                        log("Berhasil");

                    } else {
                        prd.dismiss();
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(kaitkanActivity.this, "Gagal Load Data Cek Koneksi Internet", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void getBalsan(){
        Call<MsgResponse> call = getApi().GetMSg(daftarAct.myID,idtrx,Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {

                        Intent intentx = new Intent(kaitkanActivity.this, VerifikasiKaitkanActivity.class);
                        intentx.putExtra("nomor",edNo.getText().toString());
                        startActivity(intentx);

                        Intent intent = new Intent(kaitkanActivity.this, BalasanActivity.class);
                        intent.putExtra("CekSal",ges.getPesan());
                        //  Notif(ges.getPesan());
                        startActivity(intent);
                        prd.dismiss();
                        //Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_LONG).show();
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                // yourMethod();
                                //5 seconds


                                //finish();
                                //tvSaldo.setText(ges.getPesan());

                                //finish();
                                log("Berhasil");
                            }
                        }, 1000);

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < waktutimeout){
                            getBalsan();
                            CekCounting = CekCounting+1;
                        }else{
                            prd.dismiss();
                            // Notif("Time Out / Tidak ada balasan masuk");
                            Toast.makeText(kaitkanActivity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }

    @OnClick(R.id.tv_BacktoLogin2) public void onBacktoLogin2(){
        Intent intent = new Intent(kaitkanActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
