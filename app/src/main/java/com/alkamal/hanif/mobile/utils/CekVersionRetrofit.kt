package com.alkamal.hanif.mobile.utils

import com.alkamal.hanif.mobile.APIService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class CekVersionRetrofit {

    var api: APIService? = null

    fun CekVersionRetrofit() {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient().newBuilder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(40, TimeUnit.SECONDS)
        client.addInterceptor(interceptor)
        val base = Retrofit.Builder()
                .baseUrl("http://tekniksipil.my.id/tsipil/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build()
        api = base.create(APIService::class.java)
    }


}