package com.alkamal.hanif.mobile.form;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BackgroundService.PrinterService;
import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by HanifAlKamal on 12/12/2017.
 */

public class BalasanActivity extends BaseActivity {

    @BindView(R.id.tv_blsn_saldo) TextView blsn;
    Integer data;
    private SharedPreferences prefs;
    private static String iblsnSal;
    OutputStream outputStream;
    private ArrayList<String> ListData;
    String body;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.balasan_layout);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.6));

        ButterKnife.bind(this);
        final TextView blsnSaldo = (TextView) findViewById(R.id.tv_blsn_saldo);



        Intent intent = getIntent();

        iblsnSal = intent.getStringExtra("CekSal");

        blsnSaldo.setText(iblsnSal);

       // ListData = new String[500];
        //ListData = prefs.get("MY_FORMAT_CEKSALDO","");


        //finish();

    }

    public ArrayList<String> getListData() {
        return ListData;
    }

    @OnClick(R.id.bt_print) public void onPrint(){
        try{
            prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
            String header = prefs.getString("HEADER","   TVEF   \n");
            String footer = prefs.getString("FOOTER","   Terima Kasih   \n");

            try{
                if ((iblsnSal.substring(0,2)+"").equals("No")){
                    String noBukti = iblsnSal.substring(iblsnSal.indexOf("Bukti") + 5);
                    noBukti = noBukti.substring(0, noBukti.indexOf(", "));

                    String NamaUsaha = iblsnSal.substring(iblsnSal.indexOf(", ") + 2);
                    NamaUsaha = NamaUsaha.substring(0, NamaUsaha.indexOf(" Transfer"));

                    String NamaTujuan = iblsnSal.substring(iblsnSal.indexOf("ke") + 2);
                    NamaTujuan = NamaTujuan.substring(0, NamaTujuan.indexOf(" No:"));

                    String noTujuan = iblsnSal.substring(iblsnSal.indexOf("No:") + 3);
                    noTujuan = noTujuan.substring(0, noTujuan.indexOf(" sebesar"));

                    String nominal = iblsnSal.substring(iblsnSal.indexOf("sebesar :") + 9);
                    nominal = nominal.substring(0, nominal.indexOf(" berhasil"));

                    String saldoDownline = iblsnSal.substring(iblsnSal.indexOf("downline :") + 10);
                    saldoDownline = saldoDownline.substring(0, saldoDownline.indexOf(" trims"));

                    body = "No Bukti : " + noBukti + "\n" +
                            "Nama Usaha  : " + NamaUsaha + "\n" +
                            "Agen : " + NamaTujuan + "\n" +
                            "Nomor Agen : " + noTujuan + "\n" +
                            "Nominal : Rp." + nominal + "\n" +
                            "Sisa Saldo : " + saldoDownline + "\n" +
                            "Status : Berhasil";

                } else {
                    String SN = iblsnSal.substring(iblsnSal.indexOf("SN:") + 3);
                    SN = SN.substring(0, SN.indexOf(". "));

                    String Brg = iblsnSal.substring(iblsnSal.indexOf("SUKSES ") + 6);
                    Brg = Brg.substring(0, Brg.indexOf(" ke"));

                    String noTelp = iblsnSal.substring(iblsnSal.indexOf("ke ") + 3);
                    noTelp = noTelp.substring(0, noTelp.indexOf("SN:"));

                    String harga = iblsnSal.substring(iblsnSal.indexOf("- ") + 2);
                    harga = harga.substring(0, harga.indexOf(" ="));

                    String tgl = iblsnSal.substring(iblsnSal.indexOf("pd ") + 3);
                    tgl = tgl.substring(0, tgl.indexOf(" @"));

                    String jam = iblsnSal.substring(iblsnSal.indexOf("@") + 1);
                    jam = jam.substring(0, jam.indexOf(" "));

                    String namaUsaha = iblsnSal.substring(0, iblsnSal.indexOf(" "));

                    body = "Nama Usaha : " + namaUsaha + "\n" +
                            "SN  : " + SN + "\n" +
                            "Tlp : " + noTelp + "\n" +
                            "Brg : " + Brg + "\n" +
                            "Harga : Rp." + harga + "\n" +
                            "Tgl : " + tgl + "\n" +
                            "Jam : " + jam + "\n"+
                            "Status : Berhasil";
                }

            }catch (Exception e){
                e.printStackTrace();
                body = iblsnSal;
                Toast.makeText(this, (iblsnSal.charAt(0)+""+iblsnSal.charAt(1)+"") + " = "+iblsnSal.substring(0,3), Toast.LENGTH_SHORT).show();

            }
            String text = header;
            text+="\n"+body;
            text+="\n"+footer;
            text+="\n";
            text+="\n";
            text+="\n";
            PrinterService.printData(text,this.getApplicationContext());
            //Toast.makeText(this, "Printing. . .", Toast.LENGTH_SHORT).show();;
        } catch (Exception e){
            Log.e("Print Balasan Activity", ""+e);
            //Toast.makeText(this, "Printer Tidak terhubung", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.bt_preview) public void  onPreview(){
        Intent intent = new Intent(this, PrintPreviewActivity.class);
        intent.putExtra("Balasan",iblsnSal);
        startActivity(intent);
        Toast.makeText(this, (iblsnSal.charAt(0)+iblsnSal.charAt(1)+""), Toast.LENGTH_SHORT).show();
    }

    void printData() throws IOException {
        try{


            String msg = iblsnSal;
            msg+="\n";
            outputStream.write(msg.getBytes());
            Toast.makeText(this, "Printing. . .", Toast.LENGTH_SHORT).show();;
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /*@OnClick(R.id.bt_refreshCekSaldo) public void onRefreshNotif(){
        Pattern p = Pattern.compile("\"(.*?)\" ");
        //blsn.setText("\"XL50 \"XL 50.000\" XL \" 50000\"");
        Matcher m = p.matcher(blsn.getText().toString());
        data = 0;
        ListData = new ArrayList<String>();

       while (m.find()){
           //for (int i=1;i<=m.groupCount();i++) {
            ListData.add(m.group(1).replaceAll("\"",""));

           // ListData = ListData.replaceAll("\"","");


            Toast.makeText(BalasanActivity.this, ListData.get(data) , Toast.LENGTH_LONG).show();
            //blsn.setText(m.group(1));
            //prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
//            SharedPreferences.Editor editor =prefs.edit();
            data = data +1 ;


            //}
        }
         /*
        prefs =  getSharedPreferences("preferencename", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("ListData_Size",ListData.length);
        int i = 1;
        while(i < 10) {
            if (ListData[i].isEmpty()){
                i = 1000;
            }
            editor.putString("ListData_" + i, ListData[i]);
            i = 1;
            editor.apply();
        }*/
       // Toast.makeText(BalasanActivity.this, "Beres", Toast.LENGTH_LONG).show();
//    } /*


    //on Back Pressedd
    /*
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
       // Intent setIntent = new Intent(Intent.ACTION_MAIN);
       // setIntent.addCategory(Intent.CATEGORY_HOME);
       // setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       // startActivity(setIntent);

        final TextView blsnSaldo = (TextView) findViewById(R.id.tv_blsn_saldo);
        blsnSaldo.setText("");


        Intent intent = new Intent(BalasanActivity.this, MainActivity.class);
        startActivity(intent);
    }*/




}
