package com.alkamal.hanif.mobile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alkamal.hanif.mobile.R;

import java.util.List;

/**
 * Created by HanifAlKamal on 03/03/2018.
 */

public class HistoriTrxVpsListAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mBrg;
    private List<String> mJam;
    private List<String> mTgl;
    private List<String> mStatus;

    public HistoriTrxVpsListAdapter(Context mContext, List<String> mBrg, List<String> mJam, List<String> mTgl, List<String> mStatus) {
        this.mContext = mContext;
        this.mBrg = mBrg;
        this.mJam = mJam;
        this.mTgl = mTgl;
        this.mStatus = mStatus;
    }

    @Override
    public int getCount() {
        return mBrg.size();
    }

    @Override
    public Object getItem(int position) {
        return mBrg.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View v = View.inflate(mContext, R.layout.listhistoritrx_item_layout, null);
        TextView lbBrg = (TextView)v.findViewById(R.id.lb_brg);
        TextView lbJam = (TextView)v.findViewById(R.id.lb_jam);
        TextView lbTgl = (TextView)v.findViewById(R.id.lb_tgl);
        TextView lbStatus = (TextView)v.findViewById(R.id.lb_status);

        lbBrg.setText(mBrg.get(position));
//        lbJam.setText(mJam.get(position));
        lbTgl.setText(mTgl.get(position));
        lbStatus.setText(mStatus.get(position));

        if (mStatus.get(position).equals("SUKSES")){
            lbStatus.setBackgroundResource(R.color.DarkGreen);
        } else if (mStatus.get(position).equals("GAGAL")){
            lbStatus.setBackgroundResource(R.color.DarkRed);
        } else if (mStatus.get(position).equals("RALAT")){
            lbStatus.setBackgroundResource(R.color.DarkOrange   );
        } else if (mStatus.get(position).equals("+SALDO")){
            lbStatus.setBackgroundResource(R.color.DarkBlue );
        } else if (mStatus.get(position).equals("RALAT SALDO")){
            lbStatus.setBackgroundResource(R.color.DarkGoldenrod );
        }

        v.setTag(position);

        return v;
    }
}
