package com.alkamal.hanif.mobile.form

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.adapter.ListProductAdapter
import com.alkamal.hanif.mobile.model.ListProduct
import com.alkamal.hanif.mobile.model.Product
import com.alkamal.hanif.mobile.model.ProductBanner
import com.alkamal.hanif.mobile.utils.SpringBaseActivity
import kotlinx.android.synthetic.main.activity_produk.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ProdukActivity : BaseActivity() {

    var listProduk : ListProduct? = null
    var adapter : ListProductAdapter? = null
    var calendar : Calendar? = null
    var dateformat : SimpleDateFormat? = null
    var tanggal: String = ""
    var idtrx : String = ""
    var myID : String = ""
    var Server : String = ""
    var myNumber : String = ""
    var CekCounting : Int = 0
    var format : String = ""
    var count : Int = 0

    var prefs : SharedPreferences? = null

    var listKodeProduk: ArrayList<String>? = null
    var listShortDesc: ArrayList<String>? = null
    var listBanner: ArrayList<String>? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_produk)
        listKodeProduk = ArrayList<String>()
        listShortDesc = ArrayList<String>()
        listBanner = ArrayList<String>()


        //String Token = FirebaseInstanceService
//            Log.e("FIS",FirebaseInstanceId.getInstance().getToken());
        prefs = getSharedPreferences("MY_DATA", Context.MODE_PRIVATE)
        var getPinTrx = prefs!!.getString("MY_PIN_TRX", "1234")
        prefs = getSharedPreferences("MY_DATA", Context.MODE_PRIVATE)
        myID = prefs!!.getString("MY_USERID", "").toString()
        myNumber = prefs!!.getString("MY_NUMBER", "").toString()
        Server = prefs!!.getString("MY_SERVER", "").toString()
        format = "databrg " + getPinTrx
        count = prefs!!.getString("MY_TRX_TIME_OUT", "")!!.toInt()



        GetListProduct()
//        println("Response : "+intent.getStringExtra("ListProductBanner"))


//        var product  = Product("XIAOMI","Hp Xiaomi","")
////        listProduk!!.data.add(product)
//
//        var product2  = Product("Simpat","Dua","")
////        listProduk!!.data.add(product2)
//
//        var product3  = Product("Simpat","Tiga","")
////        listProduk!!.data.add(product)
//
//        var getAllProduct : ArrayList<Product> = ArrayList<Product>()
//        getAllProduct.add(product)
//        getAllProduct.add(product2)
//        getAllProduct.add(product3)
//
//        listProduk = ListProduct(getAllProduct)
//
//        adapter = applicationContext!!.let { ListProductAdapter(
//                it,
//                listProduk as ListProduct,
//                "Produk",
//                api,
//                myID,
//                Server,
//                myNumber,
//                count,
//                format
//        ) }
//
//        listview_product.setAdapter(adapter)


    }

    fun GetListProduct() {
        var getAllProduct : ArrayList<Product> = ArrayList<Product>()
        val springBaseActivity = SpringBaseActivity()
        val call = springBaseActivity.api.productCategory
        call.enqueue(object : Callback<List<ProductBanner>> {
            override fun onResponse(call: Call<List<ProductBanner>>, response: Response<List<ProductBanner>>) {
                if (response.code() == 200) {
                    var listProductBanner = response.body()
                    var getAllProduct : ArrayList<Product> = ArrayList<Product>()
                    for (item in listProductBanner!!.iterator()){
                        var product = Product(
                                item.product_code,
                                item.product_name,
                                item.banner
                        )
                        getAllProduct.add(product)

                        listProduk = ListProduct(getAllProduct)

                        adapter = applicationContext!!.let { ListProductAdapter(
                                it,
                                listProduk as ListProduct,
                                "Produk",
                                api,
                                myID,
                                Server,
                                myNumber,
                                count,
                                format,
                                progressBar
                        ) }

                        listview_product.setAdapter(adapter)
                        progressBar.visibility = View.GONE

                    }

                } else {
                    Toast.makeText(applicationContext, "Error Code : " + response.code(), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<List<ProductBanner>>, t: Throwable) {
                Toast.makeText(applicationContext, "Failure", Toast.LENGTH_SHORT).show()
            }
        })
    }





}
