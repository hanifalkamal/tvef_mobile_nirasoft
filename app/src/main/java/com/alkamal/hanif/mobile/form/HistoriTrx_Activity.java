package com.alkamal.hanif.mobile.form;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.adapter.HistoriTrxVpsListAdapter;
import com.alkamal.hanif.mobile.adapter.Histori;
import com.alkamal.hanif.mobile.adapter.HistoriTrxListAdapter;
import com.alkamal.hanif.mobile.model.HistoriTrxVps;
import com.alkamal.hanif.mobile.utils.SpringBaseActivity;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 25/02/2018.
 */

public class HistoriTrx_Activity extends BaseActivity {

    private ListView lvHistori;
    private ListView tvhistoriItem;
    private HistoriTrxListAdapter adapter;
    private HistoriTrxVpsListAdapter adapterVps;
    private static List<Histori> mHistoList;
    private ArrayList<String> array_historiTrx = new ArrayList<String>() ;
    private SharedPreferences prefs;

    String  myID,pin,tanggal,idtrx,Server;
    int CekCounting,waktutimeout;
    NotificationCompat.Builder notification;
    Calendar calendar;
    SimpleDateFormat dateformat;
    private static final int uniqueID = 45612;
    ProgressDialog prd;

//    @BindView(R.id.tv_garis) TextView garis;

//    @BindView(R.id.tv_HistoriTrx)
//    TextView tvHistoriTrx;

    @BindView(R.id.edittext_search)
    EditText edSearch;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullRefresh;

    private List<String> mBrg;
    private List<String> mJam;
    private List<String> mTgl;
    private List<String> mStatus;

    private String phoneNumber = "";

    Intent in ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listhistoritrx_layout);
        ButterKnife.bind(this);

        in = getIntent();

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);
        //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
        pin = prefs.getString("MY_PIN_TRX", "");
        Server = prefs.getString("MY_SERVER", "");
        myID = prefs.getString("MY_USERID","");

        String Format = "laptrx "+pin ;
        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu ...");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
        getHistoryFromVPS(myID,Server,false);

        mBrg = new ArrayList<>();
        mJam = new ArrayList<>();
        mTgl = new ArrayList<>();
        mStatus = new ArrayList<>();

//        sendReqSal(Format);
        //tvhistoriItem = (TextView) findViewById(R.id.tv_ListhistoriTrx);
        lvHistori = (ListView)findViewById(R.id.lv_historitrx);
        lvHistori.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), "Clicked ID = "+view.getTag(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(HistoriTrx_Activity.this, BalasanActivity.class);
//                intent.putExtra("CekSal",array_historiTrx.get((Integer.parseInt(view.getTag().toString())-1)));
                intent.putExtra("CekSal",array_historiTrx.get(position));
                startActivity(intent);

            }
        });

        edSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    performSearch();
//                    prd = new ProgressDialog(getApplicationContext());
//                    prd.setMessage("Mohon Tunggu ...");
//                    prd.setCancelable(false);
//                    prd.setCanceledOnTouchOutside(false);
//
//                    prd.show();
                    phoneNumber = edSearch.getText().toString();
                    edSearch.setText("");
                    getHistoryFromVPS(myID,Server,true);

                    return true;
                }
                return false;
            }
        });

        pullRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHistoryFromVPS(myID,Server,false);

            }
        });


    }

    public void sendReqSal(String format){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;

        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx, Server, "nonotif");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(HistoriTrx_Activity.this, res.getMessage(), Toast.LENGTH_LONG).show();

                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        getBalsan();

                        //finish();
                        log("Berhasil");

                    } else {
                        prd.dismiss();
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(HistoriTrx_Activity.this, "Gagal Load Data Cek Koneksi Internet", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void getBalsan(){
        Call<MsgResponse> call = getApi().GetMSg(myID,idtrx,Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {
                        prefs=getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt("NOTIF", 0);
                        editor.putString("LAST_ID",idtrx);
                        editor.apply();
                        Pattern p = Pattern.compile("\"(.*?)\" ");
                        Matcher m = p.matcher(ges.getPesan());
                        int data = 0;
                        int indexListView = 1;
                        lvHistori = (ListView)findViewById(R.id.lv_historitrx);

                        mHistoList = new ArrayList<>();

                        while (m.find()){
                            array_historiTrx.add(m.group(1));
                            mHistoList.add(new Histori(indexListView, m.group(1).toString()));
                            indexListView = indexListView +1;
                        }


                        adapter = new HistoriTrxListAdapter(getApplicationContext(), mHistoList);
                        lvHistori.setAdapter(adapter);

                        prd.dismiss();

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                // yourMethod();
                                //5 seconds


                                //finish();
                                //tvSaldo.setText(ges.getPesan());

                                //finish();
                                log("Berhasil");
                            }
                        }, 1000);

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < waktutimeout){
                            getBalsan();
                            CekCounting = CekCounting+1;
                        }else{
                            prd.dismiss();
                            // Notif("Time Out / Tidak ada balasan masuk");
                            Toast.makeText(HistoriTrx_Activity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }

    void getHistoryFromVPS(String kodeplgn, String namaserver, Boolean isSearch){
        Call<List<HistoriTrxVps>> call = null;
        SpringBaseActivity springBaseActivity = new SpringBaseActivity();
        if (isSearch){

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("kodeplgn", kodeplgn);
            jsonObject.addProperty("namaserver", namaserver);
            jsonObject.addProperty("pesan", phoneNumber);
            call = springBaseActivity.getApi().getHistoryTrxVpsByPhoneNumber(jsonObject);
        } else {

            if (in.hasExtra("tanggal1")){

                call = springBaseActivity.getApi().getHistoryCustomDate(
                        kodeplgn,
                        namaserver,
                        in.getStringExtra("tanggal1"),
                        in.getStringExtra("tanggal2"));

            } else {

                call = springBaseActivity.getApi().getHistoryCurrentDate(kodeplgn, namaserver);

            }

//            call = getApi().getHistoryTrxVps(kodeplgn,namaserver);
        }
//        Call<List<HistoriTrxVps>> call = getApi().getHistoryTrxVps("AND6282120008000",namaserver);
        call.enqueue(new Callback<List<HistoriTrxVps>>() {
            @Override
            public void onResponse(Call<List<HistoriTrxVps>> call, Response<List<HistoriTrxVps>> response) {
                List<HistoriTrxVps> listTrx = response.body();
                if (listTrx.size() > 0){
                    array_historiTrx.clear();
                    mBrg.clear();
                    mTgl.clear();
                    mJam.clear();
                    mStatus.clear();
                    for (HistoriTrxVps historiTrxVps : listTrx){

                        array_historiTrx.add(historiTrxVps.getPesan());

                        if(historiTrxVps.getPesan().contains("SUKSES")){
                            String Brg = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("SUKSES ") + 6);
                            Brg = Brg.substring(0, Brg.indexOf(" ke"));

                            String nohp = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("ke ") + 3);
                            nohp = nohp.substring(0, nohp.indexOf(" "));

                            String tgl = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("pd ") + 3);
                            tgl = tgl.substring(0, tgl.indexOf(" @"));

                            String jam = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("@") + 1);
                            jam = jam.substring(0, jam.indexOf(" "));

                            mBrg.add(Brg +"\n "+nohp);
                            mTgl.add(jam +"\n"+ tgl);
//                            mJam.add(jam);
                            mStatus.add("SUKSES");
                        } else if (historiTrxVps.getPesan().contains("pernah dilakukan")){

                            mBrg.add("  -");
                            mTgl.add("-");
                            mJam.add("-");
                            mStatus.add("ULANG");
                        } else if (historiTrxVps.getPesan().contains("Ralat")){
                            String newChar = historiTrxVps.getPesan();
//                            newChar.replaceAll("Dikirim : ", "");
                            if (newChar.contains("Dikirim")){
//                                newChar.replace("Dikirim : ", "");
                                newChar = newChar.substring(10);
                            }

                            String Brg = newChar.substring(newChar.indexOf(" ") + 1);
                            Brg = Brg.substring(0, Brg.indexOf(" Ralat"));

//                            String nohp = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("") + 0);
                            String nohp = historiTrxVps.getPesan().substring(0, historiTrxVps.getPesan().indexOf(Brg));

                            String tgl = newChar.substring(newChar.indexOf("Resi:") + 5);
                            tgl = tgl.substring(tgl.length() -10);

//                            String jam = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf(tgl+" ") + 11);
//                            jam = jam.substring(0, jam.indexOf(" "));

                            mBrg.add(" "+Brg +"\n "+nohp);
                            mTgl.add("-\n"+tgl);
                            mJam.add("-");
                            mStatus.add("RALAT");
                        } else if (historiTrxVps.getPesan().contains("GAGAL")){

                            String Brg = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("GAGAL ") + 6);
                            Brg = Brg.substring(0, Brg.indexOf(" ke"));

                            String nohp = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("ke ") + 3);
                            nohp = nohp.substring(0, nohp.indexOf("."));

                            String tgl = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("pd ") + 3);
                            tgl = tgl.substring(0, tgl.indexOf(" "));

                            String jam = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf(tgl+" ") + 11);
                            jam = jam.substring(0, jam.indexOf(" "));

                            mBrg.add(Brg +"\n "+nohp);
//                            mBrg.add(Brg);
//                            mTgl.add(tgl);
//                            mJam.add(jam);
                            mTgl.add(jam +"\n"+ tgl);
                            mStatus.add("GAGAL");
                        } else if (historiTrxVps.getPesan().toUpperCase().contains("TRANSFER")){

                            if (historiTrxVps.getPesan().toUpperCase().contains("RALAT")){

                                String Brg = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("sebesar : ") + 10);
                                Brg = Brg.substring(0, Brg.indexOf(" "));

                                String nohp = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("No : ") + 5);
                                nohp = nohp.substring(0, nohp.indexOf(" "));

                                String tgl = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf(".pd ") + 4);
                                tgl = tgl.substring(0, tgl.indexOf(" "));

                                String jam = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf(tgl + " @") + 12);
                                jam = jam.substring(0, jam.indexOf(" "));

                                mBrg.add(" " + Brg + "\n " + nohp);
//                            mTgl.add(tgl);
//                            mJam.add(jam);
                                mTgl.add(jam + "\n" + tgl);
                                mStatus.add("RALAT SALDO");

                            } else {
                                String Brg = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("sebesar : ") + 10);
                                Brg = Brg.substring(0, Brg.indexOf(" "));

                                String nohp = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf("No:") + 3);
                                nohp = nohp.substring(0, nohp.indexOf(" "));

                                String tgl = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf(".pd ") + 4);
                                tgl = tgl.substring(0, tgl.indexOf(" "));

                                String jam = historiTrxVps.getPesan().substring(historiTrxVps.getPesan().indexOf(tgl + " @") + 12);
                                jam = jam.substring(0, jam.indexOf(" "));

                                mBrg.add(" " + Brg + "\n " + nohp);
//                            mTgl.add(tgl);
//                            mJam.add(jam);
                                mTgl.add(jam + "\n" + tgl);
                                mStatus.add("+SALDO");
                            }
                        } else {
                            mBrg.add("  -");
                            mTgl.add("-");
                            mJam.add("-");
                            mStatus.add("Lain - Lain");
                        }
                    }

                    log("GET PESAN HISTORI : "+listTrx.get(0).getPesan());

//                    lvHistori = (ListView)findViewById(R.id.lv_historitrx);

                    adapterVps = new HistoriTrxVpsListAdapter(getApplicationContext(), mBrg,mJam,mTgl,mStatus);
                    lvHistori.setAdapter(adapterVps);
                    hideKeyboard(HistoriTrx_Activity.this);
                    if (pullRefresh.isRefreshing()){
                        pullRefresh.setRefreshing(false);
                    }
                    prd.dismiss();
                } else {
                    prd.dismiss();
                    Toast.makeText(HistoriTrx_Activity.this, "Tidak ada transaksi !", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<HistoriTrxVps>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private static final String TAG = "HistoriTrx_Activity";
}
