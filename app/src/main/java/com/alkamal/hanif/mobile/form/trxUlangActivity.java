package com.alkamal.hanif.mobile.form;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.widget.EditText;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.adapter.KirimTerimaBalasan;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by Hanif on 11/05/2018.
 */

public class trxUlangActivity extends BaseActivity {
    @BindView(R.id.ed_trx_ulang)
    EditText edTrxulang;

    String format,myID,Server;
    int waktutimeout,timeout;
    ProgressDialog prd;
    Calendar calendar;
    SimpleDateFormat dateformat;
    String tanggal,idtrx,formatfinal,pesan;
    KirimTerimaBalasan kirim = new KirimTerimaBalasan();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trxulang_layout);
        ButterKnife.bind(this);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.6));

        Intent intent = getIntent();

        format = intent.getStringExtra("format");

        //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
        Server = prefs.getString("MY_SERVER", "");
        myID = prefs.getString("MY_USERID","");
    }

    @OnClick(R.id.bt_trx_ulang) public void ontrxUlang(){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;
        formatfinal = format+" "+"u"+edTrxulang.getText().toString();
        Call<SendMsgRespone> call = getApi().SendMSg(myID, formatfinal, idtrx,Server,"");
        Call<MsgResponse> call2 = getApi().GetMSg(myID,idtrx,Server);
        kirim.sendReqSal(waktutimeout,call,call2);
        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
        Toast.makeText(this, "Permintaan telah dikirim silahkan tunggu !",Toast.LENGTH_LONG).show();
        timeout = 0;
        dapetPesan();
    }
      //  this.finish();
    public void dapetPesan(){
        pesan = kirim.pesan();
        if (pesan.isEmpty()){
            if (timeout == waktutimeout){
                prd.hide();
                finish();
                Toast.makeText(this, "Koneksi Internet tidak stabil", Toast.LENGTH_LONG).show();
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    // yourMethod();
                    timeout = timeout+1;
                    dapetPesan();

                }
            }, 1000);



        } else {
            prd.hide();
            Intent intent = new Intent(trxUlangActivity.this, BalasanActivity.class);
            intent.putExtra("CekSal",pesan);
            startActivity(intent);
        }

    }


    }

