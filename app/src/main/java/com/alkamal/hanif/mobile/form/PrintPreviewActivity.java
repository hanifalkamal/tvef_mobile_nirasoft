package com.alkamal.hanif.mobile.form;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BackgroundService.PrinterService;
import com.alkamal.hanif.mobile.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PrintPreviewActivity extends AppCompatActivity {

    @BindView(R.id.edHeader) EditText edHeader ;
    @BindView(R.id.edBody) EditText edBody ;
    @BindView(R.id.edFooter) EditText edFooter ;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_preview);
        ButterKnife.bind(this);

        prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
        String header = prefs.getString("HEADER","   TVEF   \n");
        String footer = prefs.getString("FOOTER","   Terima Kasih   \n");

        Intent intent = getIntent();
        String balasan = intent.getStringExtra("Balasan");

        try {
            String SN = balasan.substring(balasan.indexOf("SN:") + 3);
            SN = SN.substring(0, SN.indexOf(". "));

            String Brg = balasan.substring(balasan.indexOf("SUKSES ") + 6);
            Brg = Brg.substring(0, Brg.indexOf(" ke"));

            String noTelp = balasan.substring(balasan.indexOf("ke ") + 3);
            noTelp = noTelp.substring(0, noTelp.indexOf("SN:"));

            String harga = balasan.substring(balasan.indexOf("- ") + 2);
            harga = harga.substring(0, harga.indexOf(" ="));

            String tgl = balasan.substring(balasan.indexOf("pd ") + 3);
            tgl = tgl.substring(0, tgl.indexOf(" @"));

            String jam = balasan.substring(balasan.indexOf("@") + 1);
            jam = jam.substring(0, jam.indexOf(" "));

            String namaUsaha = balasan.substring(0, balasan.indexOf(" "));

            edBody.setText( "Nama Usaha : "+namaUsaha+"\n"+
                    "SN  : "+ SN +"\n"+
                    "Tlp : "+ noTelp+"\n"+
                    "Brg : "+ Brg+"\n"+
                    "Harga : "+ harga+"\n"+
                    "Tgl : " + tgl + "\n"+
                    "Jam : " + jam + "\n");
        } catch (Exception e){
            e.printStackTrace();
            edBody.setText(balasan);
            Toast.makeText(this, "Tidak tersedia untuk print !", Toast.LENGTH_SHORT).show();
        }
        edHeader.setText(header);

        edFooter.setText(footer);

    }

    @OnClick(R.id.bt_printPreview) public void onPrintPreview(){
        try{
            String text = edHeader.getText().toString();
            text+="\n"+edBody.getText().toString();
            text+="\n"+edFooter.getText().toString();
            text+="\n";
            text+="\n";
            text+="\n";
            PrinterService.printData(text,this.getApplicationContext());

            SharedPreferences.Editor editor = prefs.edit();

            editor.putString("HEADER",edHeader.getText().toString());
            editor.putString("FOOTER",edFooter.getText().toString());
            editor.apply();
            //Toast.makeText(this, "Printing. . .", Toast.LENGTH_SHORT).show();;
        } catch (Exception e){
            Log.e("Print Balasan Activity", ""+e);
            Toast.makeText(this, "Printer Tidak terhubung", Toast.LENGTH_SHORT).show();
        }
    }
}
