package com.alkamal.hanif.mobile.model

import com.google.gson.annotations.SerializedName

data class Provider (
        @SerializedName("namaProvider")var nama_provider : String,
        @SerializedName("imageUrl") var imageUrl : String,
        @SerializedName("responseMessage") var responseMessage : String
)