package com.alkamal.hanif.mobile.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.model.BannerResponse
import com.alkamal.hanif.mobile.utils.Constant
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target

class HomeHorizontalBannerAdapter(
        var context: Context,
        var listBanner : List<BannerResponse>
) : RecyclerView.Adapter<HomeHorizontalBannerAdapter.ViewHolder>(){

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        var image : ImageView = itemView.findViewById(R.id.imageview_banner)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        var view = LayoutInflater.from(p0.context).inflate(R.layout.info_product_item, p0, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listBanner.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
       ShowImage(listBanner.get(p1).url, p0.image)
    }

    fun ShowImage(bannerUrl: String, imageView: ImageView) {
        Glide.with(context)
                .load(Constant.BaseUrlPic + bannerUrl)
                .apply(RequestOptions().override(568, 237))
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
//                        view.progressBar.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
//                        view.progressBar .visibility = View.GONE
                        return false
                    }

                })
                .into(imageView)
    }
}