package com.alkamal.hanif.mobile.form;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.adapter.Gambar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CobaGambarActivity extends AppCompatActivity {

    DatabaseReference reference;
    public static final ArrayList<String> ImageUrl = new ArrayList<String>() ;
    @BindView(R.id.tv_getDataFirebase)
    TextView edGetData;
    @BindView(R.id.iv_gambar)
    ImageView ivGambar;
    private static int x =0;
    private Boolean BeresAmbilData = false;
    private static Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coba_gambar);
        ButterKnife.bind(this);

                    Query query = FirebaseDatabase.getInstance().getReference("gambar");
                    //reference = FirebaseDatabase.getInstance().getReference("gambar").child("B00"+i);
                    query.addValueEventListener(new ValueEventListener() {
                        int i =0;
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            ImageUrl.clear();
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                Gambar gambar = snapshot.getValue(Gambar.class);
                                Log.d("cobaGambar", "getData");
                                if (gambar.getJenis().equals("Iklan")) {
                                    edGetData.append(gambar.getKode() + "\n");
                                    edGetData.append(gambar.getJenis() + "\n");
                                    edGetData.append(gambar.getNama_Server() + "\n");
                                    edGetData.append(gambar.getImageUrl() + "\n");
                                    ImageUrl.add(gambar.getImageUrl().toString());
                                    Log.d("Task", ImageUrl.get(i) + "");
                                }
                                i++;
                            }
                            final RequestOptions requestOptions = new RequestOptions();
                            requestOptions.skipMemoryCache(true);
                            requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

                            final Handler handler = new Handler();
                            final Runnable runnable = new Runnable() {
                                public void run() {

                                    if (x == ImageUrl.size()){

                                        x = 0;
                                    }
                                    Glide.with(getApplicationContext())
                                                    .load(ImageUrl.get(x))
                                                    .transition(DrawableTransitionOptions.withCrossFade(1000))
                                                    .apply(requestOptions)
                                                    .into(ivGambar);
                                    Log.d("loop", "jalan");

                                    if (x++ < 3) {

                                        handler.postDelayed(this, 5000);
                                    }

                                }
                            };

                            // trigger first time
                            handler.post(runnable);
//
                       }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Log.d("cobaGambar","Error");
                        }
                    });




    }
}
