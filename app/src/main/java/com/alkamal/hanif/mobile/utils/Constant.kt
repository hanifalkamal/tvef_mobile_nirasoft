package com.alkamal.hanif.mobile.utils

object Constant {


    const val ANDROID_ID = "11"
    const val DATE_FORMAT = "dd MM yyyy HH:mm:ss"

    var protocol : String = "http://"
    @kotlin.jvm.JvmField
    var ip : String = "193.168.195.139"
//    var ip : String = "192.168.1.25"
//    var ip : String = "172.20.30.169"
    @kotlin.jvm.JvmField
    //Prod
//    var BaseUrl: String = "http://193.168.195.139"
    //UAT / Develop
    var BaseUrl: String = protocol+ ip
    @kotlin.jvm.JvmField
    var IntBasePort: String = "8081"
    @kotlin.jvm.JvmField
    var BasePort: String = ":"+IntBasePort+"/"
    @kotlin.jvm.JvmField
    var BaseUrlPic: String = "http://193.168.195.139"

    val PREFERENCES_TRX_ULANG = "trx_ulang"
    @kotlin.jvm.JvmField
    val PREFERENCES_TRX = "trx"
    val PREFERENCES_PRODUK_KETERANGAN = "produkKeterangan"
    val PREFERENCES_PRODUK_HARGA = "produkHarga"
    val PREFERENCES_PRODUK_KODE = "produkKode"
    val PREFERENCES_SELECTED_ITEM_POSITION = "selectedItemPosition"
    val BUNDLE_TRX_ISI_PULSA = "isiPulsa"


    val PREFERENCES_NAME_IS_YESTERDAY = "IS_YESTERDAY"
    val PREFERENCES_DATE_RESET_COUTER_TRX_ULANG = "SavedDate"

    val BUNDLE_KODE_PRODUK = "kodeProduk"
    val BUNDLE_PRODUK_KATEGORI = "kategoriProduk"
    val BUNDLE_PRODUK_DESKRIPSI = "descProduk"
    val BUNDLE_PRODUK_PIC1 = "picProduk1"
    val BUNDLE_PRODUK_PIC2 = "picProduk2"
    val BUNDLE_PRODUK_PIC3 = "picProduk3"
    val BUNDLE_PRODUK_PIC4 = "picProduk4"
    val BUNDLE_PRODUK_PIC5 = "picProduk5"
    val BUNDLE_PRODUK_TITLE = "picTitle"
    val BUNDLE_PRODUK_HARGA = "hargaProduk"

    //Home
    val HOME_KATEGORI_EXTRA = "kategoriExtra"
    val HOME_IMAGE_EXTRA = "ImgExtra"
    val HOME_KODE_BRG_EXTRA = "KodeBrgExtra"
    val HOME_NAMA_PRODUK_EXTRA = "NamaBrgExtra"
    val HOME_HINT_EXTRA = "hintExtra"
    val HOME_TRX_PULSA = "homeJenisTrx"
    val HOME_TRANSFER_SALDO = "homeTrfSaldo"
    val HOME_TAMBAH_SALDO = "homeTambahSaldo"

    //Produk List
    val SELECTED_PRODUK_NAMA_EXTRA = "produkNamaExtra"
    val SELECTED_PRODUK_KODE_EXTRA = "produkKodeExtra"
    val SELECTED_PRODUK_HARGA_EXTRA = "produkHargaExtra"

    //Animation Param
    val HEADER_EDITTEXT_DURATION = 200
    val HEADER_EDITTEXT_TRANSALTION_TO_BOTTOM = 20
    val HEADER_EDITTEXT_TRANSALTION_TO_TOP = -20
    val HEADER_EDITTEXT_TRANSALTION_SHOW_TO_FADE = 0.0f
    val HEADER_EDITTEXT_TRANSALTION_FADE_TO_SHOW = 1.0f

    val KONFIRM_TRF_DURATION = 200
    val KONFIRM_TRF_TRANSALTION_TO_BOTTOM = 20
    val KONFIRM_TRF_TRANSALTION_TO_TOP = -20
    val KONFIRM_TRF_TRANSALTION_SHOW_TO_FADE = 0.0f
    val KONFIRM_TRF_TRANSALTION_FADE_TO_SHOW = 1.0f

    //Receipt
    val RECEIPT_HEADER = "keteranganReceipt"
    val RECEIPT_PESAN = "pesanReceipt"
    val RECEIPT_PRODUK = "produkReceipt"
    val RECEIPT_TANGGAL = "tglReceipt"
    val RECEIPT_WAKTU = "waktuReceipt"
    val RECEIPT_NOMOR_TUJUAN = "nomorTujuanReceipt"
    val RECEIPT_STATUS = "statusReceipt"
    val RECEIPT_ID = "idReceipt"
    val RECEIPT_HARGA = "hargaReceipt"

    val STATE_TRX = "TRX"
    val STATE_NON_TRX = "NONTRX"
    val STATE_HAS_LOGGED_IN = "LOGIN"

    val OPSI_REGIS_KAITKAN = "KAITKAN"
    val OPSI_REGIS_PRAKAITKAN = "PRAKAITKAN"
    val OPSI_REGIS_NEW = "NEW"
    val OPSI_REGIS_JOIN = "JOIN"
    val OPSI_REGIS = "OPSI"

    @kotlin.jvm.JvmField
    val WARNING_MESSAGE = "warningMessage"
    @kotlin.jvm.JvmField
    val WARNING_HEADER = "warningHeader"
    @kotlin.jvm.JvmField
    val WARNING_PIC = "warningPic"
    @kotlin.jvm.JvmField
    val WARNING_CUSTOM_LINK = "warningLink"

    //config
    val CONFIG_JENIS_PRINTER = "JENIS_PRINTER"
    val CONFIG_HEADER_PRINTER = "HEADER_PRINTER"
    val CONFIG_FOOTER_PRINTER = "FOOTER_PRINTER"

    //Account
    @kotlin.jvm.JvmField
    val ACCOUNT_NAMA_USAHA = "NAMA_USAHA"
    @kotlin.jvm.JvmField
    val ACCOUNT_KODE_PLGN = "KODE_PLGN"
    @kotlin.jvm.JvmField
    val ACCOUNT_ID_PLGN = "ID_PLGN"
    @kotlin.jvm.JvmField
    val ACCOUNT_NOMOR_TELEPON = "NOTELPACCOUNT"

    val TOPUP_NO_REK = "TOPUP_NO_REK"
    val TOPUP_TIKET = "TOPUP_TIKET"
    val TOPUP_NOMINAL = "TOPUP_NOMINAL"
    val TOPUP_SELECTED_BANK = "TOPUP_SELECTED_BANK"
    
    val REGISTER_NAMA_PEMILIK = "REGIS_NAMA_PEMILIK"
    val REGISTER_NO_TELP = "REGIS_NO_TELP"
    val REGISTER_NAMA_USAHA = "REGIS_NAMA_USAHA"
    val REGISTER_ALAMAT = "REGIS_ALAMAT"
    val REGISTER_PSW = "REGIS_PSW"
    val REGISTER_PIN = "REGIS_PIN"

    @kotlin.jvm.JvmField
    val FROM_NOTIF = "FROM_NOTIF"






}