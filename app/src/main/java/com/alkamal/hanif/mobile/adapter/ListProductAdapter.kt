package com.alkamal.hanif.mobile.adapter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.util.Base64
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import com.alkamal.hanif.mobile.APIService
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.Response.SendMsgRespone
import com.alkamal.hanif.mobile.Response.MsgResponse
import com.alkamal.hanif.mobile.form.ProductCategoryActivity
import com.alkamal.hanif.mobile.form.ProductDetilActivity
import com.alkamal.hanif.mobile.model.ListProduct
import com.alkamal.hanif.mobile.model.ProductCategory
import com.alkamal.hanif.mobile.model.barangresponse.Data
import com.alkamal.hanif.mobile.utils.Constant
import com.alkamal.hanif.mobile.utils.SpringBaseActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.list_produk.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class ListProductAdapter(
        context: Context,
        listProduct: ListProduct,
        list : String,
        api: APIService,
        myID : String,
        Server : String,
        myNumber : String,
        waktuTimeOut : Int,
        format: String,
        progressBar: ProgressBar

) : BaseAdapter() {
    var listProduct : ListProduct = listProduct
    var mContext : Context = context
    var list  = list
    var calendar : Calendar? = null
    var dateformat : SimpleDateFormat? = null
    var tanggal: String = ""
    var idtrx : String = ""
    var myID : String = myID
    var Server : String = Server
    var myNumber : String = myNumber
    var CekCounting : Int = 0
    var api = api
    var waktuTimeOut = waktuTimeOut
    var format = format
    var progressBar : ProgressBar = progressBar

    var prefs : SharedPreferences? = null



    var productCategory = ""

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var v : View = View.inflate(mContext, R.layout.list_produk, null)

        v.tv_short_desc_produk.setText(listProduct.data.get(position).shortDesc)

        // decode base 64 pic
//        if (listProduct.data.get(position).produkBanner.length > 1) {
//            base64ToImage(listProduct.data.get(position).produkBanner, v.image_banner)
//        }

        //Put Image
        if (listProduct.data.get(position).produkBanner.length > 1) {
            ShowImage(listProduct.data.get(position).produkBanner, v, v.image_banner)
        }

        v.layout_product.setOnClickListener { v : View? ->

            if(list.equals("Produk")) {
                productCategory = listProduct.data.get(position).kodeProduk
//                sendReqSal(format+" "+productCategory)
                getProductVps(productCategory)

                //DUMMY
//                var intent = Intent()
//                intent.setClass(mContext, ProductCategoryActivity::class.java)
//                intent.putExtra(Constant.BUNDLE_KODE_PRODUK, xiaomiDummy)
//                intent.putExtra(Constant.BUNDLE_PRODUK_KATEGORI, productCategory)
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                ContextCompat.startActivity(mContext, intent, null)
                //----



//                Toast.makeText(mContext,"Harap Tunggu",Toast.LENGTH_LONG).show()

            } else if(list.equals("ProdukType")){
//                var kodedbrg = listProduct.data.get(0).kodeProduk
//                var intent = Intent()
//                intent.setClass(mContext, ProductDetilActivity::class.java)
//                var bundle = Bundle()
//                bundle.putString(Constant.BUNDLE_KODE_PRODUK, kodedbrg)
//                ContextCompat.startActivity(mContext, intent, bundle)
                getProductDetil(listProduct.data.get(position).kodeProduk, listProduct.data.get(position).shortDesc)

            }
        }



        return v

    }

    override fun getItem(position: Int): Any {
        return listProduct.data.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return listProduct.data.size
    }

    fun base64ToImage(encodedImage: String?, ib: ImageView) {
        try {
            val decodedString = Base64.decode(encodedImage, Base64.DEFAULT)

            Glide.with(mContext!!)
                    .load(decodedString)
                    .into(ib)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun sendReqSal(format: String?) {
        progressBar!!.visibility = View.VISIBLE
        val sf = settingFormat()
        calendar = Calendar.getInstance()
        dateformat = SimpleDateFormat("ddMMyyyyHHmmss")
        tanggal = dateformat!!.format(calendar!!.getTime())
        idtrx = tanggal
        val call = api.SendMSg(myID, format, idtrx, Server, "nonotif,JSON")
        call.enqueue(object : Callback<SendMsgRespone?> {
            override fun onResponse(call: Call<SendMsgRespone?>, response: Response<SendMsgRespone?>) {
                if (response.isSuccessful) {
                    val res = response.body()
                    if (res!!.isSuccess) {
                        CekCounting = 0
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        getBalsan()
                        //finish();
                        //log("Berhasil")
                    } else { //tvMsg.setText(res.getMessage());
                        progressBar!!.visibility = View.GONE
                        Toast.makeText(mContext, "Gagal Cek Koneksi Internet", Toast.LENGTH_LONG).show()
                        //log("Gagal")
                    }
                }
            }

            override fun onFailure(call: Call<SendMsgRespone?>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                println(t.toString())
//                prd.dismiss()
            }
        })
    }

    fun getBalsan() {

        val call: Call<MsgResponse> = api.GetMSg(myID, idtrx, Server)
        call.enqueue(object : Callback<MsgResponse?> {
            override fun onResponse(call: Call<MsgResponse?>, response: Response<MsgResponse?>) {
                if (response.isSuccessful) {
                    val ges = response.body()
                    if (ges!!.IsSuccess()) {
//                        prd.dismiss()
                        var intent = Intent()
                        intent.setClass(mContext, ProductCategoryActivity::class.java)
                        intent.putExtra(Constant.BUNDLE_KODE_PRODUK, ges.pesan)
                        intent.putExtra(Constant.BUNDLE_PRODUK_KATEGORI, productCategory)
//                        var bundle = Bundle()
//                        bundle.putString(Constant.BUNDLE_KODE_PRODUK, ges.pesan)
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        ContextCompat.startActivity(mContext, intent, null)
                        progressBar!!.visibility = View.GONE
                    } else {
//                        progressBar!!.visibility = View.GONE
                        println("Gagal")
                        if (CekCounting < waktuTimeOut) {
                            getBalsan()
                            CekCounting = CekCounting + 1
                        } else { //                                prd.dismiss();
//                            btRefreshSaldo.setVisibility(View.VISIBLE)
//                            loadingAnim.setVisibility(View.GONE)
//                            Notif("Time Out / Tidak ada balasan masuk")
                            Toast.makeText(mContext, "Time Out", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<MsgResponse?>, t: Throwable) {
                println(t.toString())
                progressBar!!.visibility = View.GONE

            }
        })
    }

    fun getProductVps(productCategory : String){
        progressBar!!.visibility = View.VISIBLE
        val springBaseActivity = SpringBaseActivity()
        val call = springBaseActivity.api.getDataBarang(myID, "", "PULSA", productCategory, "1234")
        call.enqueue(object : Callback<List<Data>>{
            override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                progressBar!!.visibility = View.GONE
                throw t
            }

            override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                var listData = response.body()
                var intent = Intent()
                intent.setClass(mContext, ProductCategoryActivity::class.java)
                intent.putExtra(Constant.BUNDLE_KODE_PRODUK, Gson().toJson(listData))
                intent.putExtra(Constant.BUNDLE_PRODUK_KATEGORI, productCategory)
//                        var bundle = Bundle()
//                        bundle.putString(Constant.BUNDLE_KODE_PRODUK, ges.pesan)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                ContextCompat.startActivity(mContext, intent, null)
                progressBar!!.visibility = View.GONE

            }

        })
    }

    fun getProductDetil(kodeProduk : String, titleProduk : String){
        var baseActivity = SpringBaseActivity()
//        Toast.makeText(mContext,"Harap Tunggu",Toast.LENGTH_SHORT).show()
        progressBar!!.visibility = View.VISIBLE
        var jsonObject = JsonObject();
        jsonObject.addProperty("kode_produk", kodeProduk)
        val call: Call<List<ProductCategory>> = baseActivity.api.getProductDetil(jsonObject)
        call.enqueue(object : Callback<List<ProductCategory>?>{
            override fun onFailure(call: Call<List<ProductCategory>?>, t: Throwable) {
                Toast.makeText(mContext,"Failure",Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<List<ProductCategory>?>, response: Response<List<ProductCategory>?>) {
                if(response.code() == 200){
                    var listRespProduct = response.body();
                    var kodeProduk = listRespProduct!!.get(0).kode_produk
                    var produk = listRespProduct!!.get(0).produk
                    var deskripsi = listRespProduct!!.get(0).deskrispi
                    var pic1 = listRespProduct!!.get(0).pic1
                    var pic2 = listRespProduct!!.get(0).pic2
                    var pic3 = listRespProduct!!.get(0).pic3
                    var pic4 = listRespProduct!!.get(0).pic4
                    var pic5 = listRespProduct!!.get(0).pic5
                    var intent = Intent()

                    intent.setClass(mContext, ProductDetilActivity::class.java)
//                    var bundle = Bundle()
                    intent.putExtra(Constant.BUNDLE_KODE_PRODUK, kodeProduk)
                    intent.putExtra(Constant.BUNDLE_PRODUK_KATEGORI, produk)
                    intent.putExtra(Constant.BUNDLE_PRODUK_DESKRIPSI, deskripsi)
                    intent.putExtra(Constant.BUNDLE_PRODUK_PIC1, pic1)
                    intent.putExtra(Constant.BUNDLE_PRODUK_PIC2, pic2)
                    intent.putExtra(Constant.BUNDLE_PRODUK_PIC3, pic3)
                    intent.putExtra(Constant.BUNDLE_PRODUK_PIC4, pic4)
                    intent.putExtra(Constant.BUNDLE_PRODUK_PIC5, pic5)
                    intent.putExtra(Constant.BUNDLE_PRODUK_TITLE, titleProduk)
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    ContextCompat.startActivity(mContext, intent, null)
                    progressBar!!.visibility = View.GONE
                } else {
                    progressBar!!.visibility = View.GONE
                    Toast.makeText(mContext,response.code().toString(),Toast.LENGTH_SHORT).show()
                }
            }

        })
    }

    fun ShowImage(bannerUrl: String, view:View, imageView: ImageView){
        Glide.with(view)
                .load(Constant.BaseUrl + bannerUrl)
                .listener(object :RequestListener<Drawable>{
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        view.progress.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        view.progress.visibility = View.GONE
                        return false
                    }

                })
                .into(imageView)

    }

    var xiaomiDummy = "\"\"REDMI6A\" \"XIAOMI REDMI 6A 2GB RAM 16 ROM\" \"XIAOMI\" \"1\" \"1200000\"\" \"\"REDMI7A\" \"XIAOMI REDMI 7A GB RAM 32 ROM\" \"XIAOMI\" \"2\" \"1380000\"\" \"\"REDMI7\" \"XIAOMI REDMI 7 3GB RAM 32 ROM\" \"XIAOMI\" \"3\" \"1750000\"\" \"\"REDMINOTE7\" \"XIAOMI REDMI NOTE 7 3GB RAM 32 ROM\" \"XIAOMI\" \"4\" \"1750000\"\" \"\"REDMI8\" \"XIAOMI REDMI 8 4GB RAM 64 ROM\" \"XIAOMI\" \"5\" \"1875000\"\" \"\"REDMINOTE8\" \"XIAOMI REDMI NOTE 8 3GB RAM 32 ROM\" \"XIAOMI\" \"6\" \"2050000\"\" "
}