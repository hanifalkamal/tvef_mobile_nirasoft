package com.alkamal.hanif.mobile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alkamal.hanif.mobile.form.Histori.HistoriActivity;
import com.alkamal.hanif.mobile.form.LoadingScreen.CustomWarningActivity;
import com.alkamal.hanif.mobile.form.VersionValidationActivity;
import com.alkamal.hanif.mobile.model.BalanceResponse;
import com.alkamal.hanif.mobile.model.ParamConfigResponse;
import com.alkamal.hanif.mobile.utils.CacheManager;
import com.alkamal.hanif.mobile.utils.CekVersionRetrofit;
import com.alkamal.hanif.mobile.utils.Constant;
import com.alkamal.hanif.mobile.utils.DateUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.JsonObject;
import com.itenas.sikpsipil.data.model.VersionValidation.ArrayVersionValidation;
import com.itenas.sikpsipil.data.model.VersionValidation.VersionValidationResponse;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by HanifAlKamal on 07/12/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public String androidId;
    public String deviceModel;
    APIService api;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        createNotificationChannel();
        preferences = getSharedPreferences(Constant.PREFERENCES_TRX, Context.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient().newBuilder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            client.addInterceptor(logging);
        }

//        client.addInterceptor(interceptor);
        Retrofit base = new Retrofit.Builder()
                //.baseUrl("http://www.tvef.xyz/tvefKirimPesan/")
//                .baseUrl("http://193.168.195.139/tvefKirimPesan/")
                .baseUrl(Constant.BaseUrl + Constant.BasePort)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();
        api = base.create(APIService.class);

        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        resetTrxUlangCounter(); // Resset counter trx ulang

        cekVersi();
        checkMaintenance();

        getUniqueID();
        deviceModel = getDeviceName();


        try {
            if (!isNetworkAvailable()) {
                Intent intent = new Intent(this, CustomWarningActivity.class);
                intent.putExtra(Constant.WARNING_MESSAGE, "Periksa kembali koneksi internet anda !");
                intent.putExtra(Constant.WARNING_HEADER, "NO INTERNET CONNECTION !");
                intent.putExtra(Constant.WARNING_CUSTOM_LINK, "");
                intent.putExtra(Constant.WARNING_PIC, "");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
//            else if (!InetAddress.getByName(Constant.ip).isReachable(2000)) {
            else if (!isReachable()) {
                Intent intent = new Intent(this, CustomWarningActivity.class);
                intent.putExtra(Constant.WARNING_MESSAGE, "Opps.. Ada kesalahan pada server\nMohon tunggu beberapa saat lagi");
                intent.putExtra(Constant.WARNING_HEADER, "INTERNAL SERVER ERROR !");
                intent.putExtra(Constant.WARNING_CUSTOM_LINK, "");
                intent.putExtra(Constant.WARNING_PIC, "");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        } catch (Exception E) {
            E.printStackTrace();
        }


    }

    private static String getAndroidId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @SuppressLint("HardwareIds")
    void getUniqueID() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            androidId = getAndroidId(this);
        } else {
            String imei = getImei(getApplicationContext());
            if (imei.trim().length() < 15) {
                androidId = imei + DateUtil.getTodaysDate() + DateUtil.getCurrentTime() + Constant.ANDROID_ID;
                androidId = androidId.toUpperCase();
            } else {
                androidId = imei + Constant.ANDROID_ID;
                androidId = androidId.toUpperCase();
            }
        }
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    public static String getImei(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = null;
        try {
            imei = manager.getDeviceId();
        } catch (Exception E) {
            if (imei == null || imei.trim().length() == 0) {
                imei = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            }

        }
        return imei;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public boolean isReachable() {
        boolean exists = false;

        try {
            SocketAddress sockaddr = new InetSocketAddress(Constant.ip, Integer.parseInt(Constant.IntBasePort));
            // Create an unbound socket
            Socket sock = new Socket();

            // This method will block no more than timeoutMs.
            // If the timeout occurs, SocketTimeoutException is thrown.
            int timeoutMs = 10000;   // 2 seconds
            sock.connect(sockaddr, timeoutMs);
            exists = true;
        } catch (IOException e) {
            // Handle exception
            e.printStackTrace();
        }
        return exists;
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public APIService getApi() {
        return api;
    }

    public void makeErrorDialog(String msg) {
        AlertDialog.Builder alertDBuilder = new AlertDialog.Builder(this);
        alertDBuilder.setMessage(msg);
        alertDBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = alertDBuilder.create();
        alertDialog.show();
    }

    public void log(String log) {
        if (true) {
            Log.d("BaseActivity", log);
        }
    }

    public void currencyFormatter(String p0, TextView tv) {
        try {
            String givenstring = p0.toString();
            Long longval;
            if (givenstring.contains(",")) {
                givenstring = givenstring.replace(",", "");
            }
            longval = java.lang.Long.parseLong(givenstring);
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String formattedString = formatter.format(longval);
            formattedString = formattedString.replace(",", ".");
            tv.setText("Rp. " + formattedString);
//            tv!!.setSelection(edOmset!!.getText().length)
            // to place the cursor at the end of text
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String currencyFormatter(String p0) {

        String formattedString = "";
        try {
            String givenstring = p0.toString();
            Long longval;
            if (givenstring.contains(",")) {
                givenstring = givenstring.replace(",", "");
            }
            longval = java.lang.Long.parseLong(givenstring);
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            formattedString = formatter.format(longval);
            formattedString = formattedString.replace(",", ".");
//            tv!!.setSelection(edOmset!!.getText().length)
            // to place the cursor at the end of text
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedString;
    }

    public void currencyFormatterOnEdiittextListener(String p0, EditText editText, TextWatcher watcher) {
        editText.removeTextChangedListener(watcher);
        try {

            String givenstring = p0.toString();
            Long longval;

            if (givenstring.contains(".")) {
                givenstring = givenstring.replace(".", "");
            }

            if (givenstring.contains(",")) {
                givenstring = givenstring.replace(",", "");
            }
            longval = java.lang.Long.parseLong(givenstring);
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String formattedString = formatter.format(longval);
            formattedString = formattedString.replace(",", ".");
            editText.setText(formattedString);
            editText.setSelection(editText.getText().length());
        } catch (Exception nfe) {
            nfe.printStackTrace();
        }
        editText.addTextChangedListener(watcher);
    }

    public SharedPreferences getPreferences() {
        return preferences;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    public void showImage(String url, ImageView imageView, Context context) {

        Glide.with(context)
                .load(Constant.BaseUrlPic + url)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);

    }

    public String generateIdTrx() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        return dateformat.format(calendar.getTime());
    }


    public BalanceResponse checkBalance(String pin, TextView textView) {

        final BalanceResponse[] balanceResponse = {null};
        Call<BalanceResponse> call = api.balance(getKodePlgn(), pin);
        call.enqueue(new Callback<BalanceResponse>() {
            @Override
            public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                balanceResponse[0] = response.body();
                if (balanceResponse[0] != null) {
                    if (balanceResponse[0].getSuccess()) {
                        textView.setText("Rp. " + currencyFormatter(balanceResponse[0].getSaldo()));
                        CacheManager.setBalance("Rp. " + currencyFormatter(balanceResponse[0].getSaldo()));
                    }
                }


            }

            @Override
            public void onFailure(Call<BalanceResponse> call, Throwable t) {
                log("Fail to check balance, message : " + t.getMessage());
                makeErrorDialog("Gagal Cek Saldo !");
                balanceResponse[0].setSuccess(false);
                balanceResponse[0].setRespon(t.getMessage());
            }
        });
        return balanceResponse[0];
    }

    public void checkMaintenance() {
        Call<ParamConfigResponse> call = api.getMaintenanceStatus();
        call.enqueue(new Callback<ParamConfigResponse>() {
            @Override
            public void onResponse(Call<ParamConfigResponse> call, Response<ParamConfigResponse> response) {
                ParamConfigResponse result = response.body();

                if (result != null) {
                    if (result.getValue().equals("1")) {
                        Intent intent = new Intent(getApplicationContext(), CustomWarningActivity.class);
                        intent.putExtra(Constant.WARNING_MESSAGE, "Kami sedang melakukan pemeliharaan sistem\nterima kasih atas pengertiannya");
                        intent.putExtra(Constant.WARNING_HEADER, "MAINTENANCE");
                        intent.putExtra(Constant.WARNING_CUSTOM_LINK, "");
                        intent.putExtra(Constant.WARNING_PIC, "MAINTENANCE");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }

            }

            @Override
            public void onFailure(Call<ParamConfigResponse> call, Throwable t) {

            }
        });
    }

    public void cekVersi() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("dbname", "tekniksi_dbglobal");
        jsonObject.addProperty("sqltext", "select * from tdaftaraplikasi where nama_app = 'TVEF_ANDROID'");
        jsonObject.addProperty("user", "admin");
        jsonObject.addProperty("pass", "admin1234");
        CekVersionRetrofit cekVersionRetrofit = new CekVersionRetrofit();
        cekVersionRetrofit.CekVersionRetrofit();
        Call<ArrayVersionValidation> call = Objects.requireNonNull(cekVersionRetrofit.getApi()).ExecuteQueryCheckVersi(jsonObject);
        call.enqueue(new Callback<ArrayVersionValidation>() {
            @Override
            public void onResponse(@NotNull Call<ArrayVersionValidation> call, @NotNull Response<ArrayVersionValidation> response) {

                ArrayVersionValidation result = response.body();
                if (result != null) {
                    VersionValidationResponse data = result.getData().get(0);
//                Log.d(TAG, "onResponse: "+data.getVersi());
                    String versionCode = (String.valueOf(BuildConfig.VERSION_CODE));
                    if (!versionCode.equals(data.getVersi())) {
                        Intent i = new Intent(getApplicationContext(), VersionValidationActivity.class);
                        i.putExtra("info_aktif", data.getInfo_aktif());
                        i.putExtra("link", data.getLink());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
//                    finish();

                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayVersionValidation> call, Throwable t) {

            }
        });

    }

    public BalanceResponse checkBalance(String pin, TextView textView, SwipeRefreshLayout pulltoRefresh) {

        final BalanceResponse[] balanceResponse = {null};
        Call<BalanceResponse> call = api.balance(getKodePlgn(), pin);
        call.enqueue(new Callback<BalanceResponse>() {
            @Override
            public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                balanceResponse[0] = response.body();
                if (balanceResponse[0] != null) {
                    if (balanceResponse[0].getSuccess()) {
                        textView.setText("Rp. " + currencyFormatter(balanceResponse[0].getSaldo()));
                        CacheManager.setBalance("Rp. " + currencyFormatter(balanceResponse[0].getSaldo()));
                        if (pulltoRefresh.isRefreshing()) {
                            pulltoRefresh.setRefreshing(false);
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<BalanceResponse> call, Throwable t) {
                log("Fail to check balance, message : " + t.getMessage());
                makeErrorDialog("Gagal Cek Saldo !");
                balanceResponse[0].setSuccess(false);
                balanceResponse[0].setRespon(t.getMessage());
                if (pulltoRefresh.isRefreshing()) {
                    pulltoRefresh.setRefreshing(false);
                }
            }
        });
        return balanceResponse[0];
    }

    public BalanceResponse checkBalance(String pin) {

        final BalanceResponse[] balanceResponse = {null};
        Call<BalanceResponse> call = api.balance(getKodePlgn(), pin);
        call.enqueue(new Callback<BalanceResponse>() {
            @Override
            public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                balanceResponse[0] = response.body();
                if (balanceResponse[0] != null) {
                    if (balanceResponse[0].getSuccess()) {
                        CacheManager.setBalance("Rp. " + currencyFormatter(balanceResponse[0].getSaldo()));
                    }
                }


            }

            @Override
            public void onFailure(Call<BalanceResponse> call, Throwable t) {
                log("Fail to check balance, message : " + t.getMessage());
                makeErrorDialog("Gagal Cek Saldo !");
                balanceResponse[0].setSuccess(false);
                balanceResponse[0].setRespon(t.getMessage());
            }
        });
        return balanceResponse[0];
    }

    public void openSoftKeyboard(Context context, View view) {
        view.requestFocus();
        // open the soft keyboard
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    public Boolean showDialog(Context context, String title, String message) {
        final Boolean[] result = {false};
        AlertDialog alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog)).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        result[0] = true;
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        result[0] = false;
                    }
                });
        alertDialog.show();

        return result[0];
    }

    public void resetTrxUlangCounter() {
        Date c = Calendar.getInstance().getTime();
//        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);

        String yesterday = getSharedPreferences(Constant.INSTANCE.getPREFERENCES_NAME_IS_YESTERDAY(), MODE_PRIVATE)
                .getString(Constant.INSTANCE.getPREFERENCES_DATE_RESET_COUTER_TRX_ULANG(), "-");

        if (!formattedDate.equals(yesterday)) {
            getSharedPreferences(Constant.INSTANCE.getPREFERENCES_NAME_IS_YESTERDAY(), MODE_PRIVATE)
                    .edit()
                    .putString(Constant.INSTANCE.getPREFERENCES_DATE_RESET_COUTER_TRX_ULANG(), formattedDate)
                    .apply();

            getSharedPreferences(Constant.INSTANCE.getPREFERENCES_TRX_ULANG(), MODE_PRIVATE)
                    .edit()
                    .clear()
                    .apply();
        }
    }

    public String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));

            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getIdPlgn() {
        return preferences.getString(Constant.ACCOUNT_ID_PLGN, "");
    }

    public String getNamaUsaha() {
        return preferences.getString(Constant.ACCOUNT_NAMA_USAHA, "");
    }

    public String getKodePlgn() {
        return encodeAndReverse(preferences.getString(Constant.ACCOUNT_KODE_PLGN, ""));
    }

    public void clearUserInfo() {
        preferences.edit().remove(Constant.ACCOUNT_ID_PLGN).apply();
        preferences.edit().remove(Constant.ACCOUNT_NAMA_USAHA).apply();
        preferences.edit().remove(Constant.ACCOUNT_KODE_PLGN).apply();
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(getString(R.string.CHANNEL_ID), name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void showNotif(String title, String body) {
        // Create an explicit intent for an Activity in your app
//        String title = data.get("title").toString();
        //String body = data.get("body").toString();
//        String body = data.get("message").toString();

        NotificationManager notificationmanager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "com.alkamal.hanif.kirimdatalisensi.test";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification",
                        NotificationManager.IMPORTANCE_DEFAULT);

                notificationChannel.setDescription("Notif");
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.BLUE);
                notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                notificationChannel.enableLights(true);
                notificationmanager.createNotificationChannel(notificationChannel);
            } catch (Exception e) {
                Log.e("FirebaseInstaceService", "" + e);
            }
        }

        NotificationCompat.Builder notificationbuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

        notificationbuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setContentInfo("Info");

        Context context = getApplicationContext();
//        Intent intent = new Intent(context, BalasanActivity.class);
        Intent intent = new Intent(context, HistoriActivity.class);
        intent.putExtra("CekSal", body);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationbuilder.setContentIntent(pendingIntent);

        notificationmanager.notify(new Random().nextInt(), notificationbuilder.build());
    }

    public boolean runtime_permissions() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, Manifest.permission
                .READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 100);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // enable_button();
                StringBuilder sb = new StringBuilder();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, 1);
                //     TelephonyManager telMan = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                //     sb.append(telMan.getDeviceId());
                //     myImei = sb.toString();

            } else {
                runtime_permissions();
            }

        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void copyToClipBoard(String label, String text) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(label, text);
        clipboard.setPrimaryClip(clip);
    }

    public String reverseString(String str) {
        StringBuilder sb = new StringBuilder(str);
        return sb.reverse().toString();
    }

    public String encode(String str) {
        try {
            byte[] data = str.getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.NO_WRAP);
            return base64;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String encodeAndReverse(String str) {
        return reverseString(encode(str));
    }

}
