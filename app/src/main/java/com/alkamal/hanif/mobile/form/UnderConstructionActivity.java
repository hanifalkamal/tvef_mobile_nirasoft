package com.alkamal.hanif.mobile.form;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.alkamal.hanif.mobile.R;

/**
 * Created by HanifAlKamal on 25/02/2018.
 */

public class UnderConstructionActivity extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.under_constraction_layout);
    }
}
