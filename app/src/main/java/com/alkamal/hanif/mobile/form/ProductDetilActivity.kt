package com.alkamal.hanif.mobile.form

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.RelativeLayout
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.form.Transaksi.InputPinActivity
import com.alkamal.hanif.mobile.adapter.ProductDetilPictureAdapter
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.activity_product_detil.*
import kotlinx.android.synthetic.main.layout_input_no_telp.view.*

class ProductDetilActivity : BaseActivity() {

    var kodeBrg = ""
    var produk = ""
    var deskripsi = ""
    var pic1 = ""
    var pic2 = ""
    var pic3 = ""
    var pic4 = ""
    var pic5 = ""
    var titleProduk = ""

    var mListPic : MutableList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detil)


        kodeBrg = intent.getStringExtra(Constant.BUNDLE_KODE_PRODUK)!!
        produk = intent.getStringExtra(Constant.BUNDLE_PRODUK_KATEGORI)!!
        deskripsi = intent.getStringExtra(Constant.BUNDLE_PRODUK_DESKRIPSI)!!
        pic1 = intent.getStringExtra(Constant.BUNDLE_PRODUK_PIC1)!!
        pic2 = intent.getStringExtra(Constant.BUNDLE_PRODUK_PIC2)!!
        pic3 = intent.getStringExtra(Constant.BUNDLE_PRODUK_PIC3)!!
        pic4 = intent.getStringExtra(Constant.BUNDLE_PRODUK_PIC4)!!
        pic5 = intent.getStringExtra(Constant.BUNDLE_PRODUK_PIC5)!!
        titleProduk = intent.getStringExtra(Constant.BUNDLE_PRODUK_TITLE)!!
        lb_desc.setText(deskripsi)
        jasaTitle.setText(titleProduk)
        initRecyclerView()

        lb_descReadmore.setOnClickListener {
            if (lb_descReadmore.text.equals("Read More")) {
                val lp = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
                lp.setMargins(0, 50, 0, 0)
                lb_desc.setLayoutParams(lp)
                lb_descReadmore.setText("Hide")
            } else {
                lb_desc.layoutParams.height = resources!!.getDimension(R.dimen.desc_height).toInt()
                lb_descReadmore.setText("Read More")
            }
        }

        bt_beli.setOnClickListener {
            layout_input_no_telp.visibility = View.VISIBLE

        }

        button_back.setOnClickListener {
            onBackPressed()
        }

        layout_input_no_telp.text_batalkan.setOnClickListener {
            layout_input_no_telp.visibility = View.GONE
        }

        layout_input_no_telp.button_submit.setOnClickListener {
            var intent = Intent()
            intent.setClass(this, InputPinActivity::class.java)
            intent.putExtra(Constant.SELECTED_PRODUK_KODE_EXTRA, kodeBrg)
            intent.putExtra(Constant.SELECTED_PRODUK_NAMA_EXTRA, title)
            intent.putExtra("nomorHp", layout_input_no_telp.edittext_nomor_telepon.text.toString())
            startActivity(intent)
            finish()
        }
    }

    fun initRecyclerView(){
        mListPic = ArrayList<String>()
        mListPic!!.add(pic1)
        mListPic!!.add(pic2)
        mListPic!!.add(pic3)
        mListPic!!.add(pic4)
        mListPic!!.add(pic5)

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        var recyclerView = recycyler_view_pic
        recyclerView.setFocusable(true)
        recyclerView.layoutManager = layoutManager
        var Adapter = ProductDetilPictureAdapter(this, mListPic!!)
        recyclerView.adapter = Adapter


    }
}
