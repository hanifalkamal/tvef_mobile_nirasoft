package com.alkamal.hanif.mobile.chat.Fragment;

import com.alkamal.hanif.mobile.chat.Notification.MyResponse;
import com.alkamal.hanif.mobile.chat.Notification.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {

    @Headers(
            {
                    "Content-type:application/json",
                    "Authorization:key=AAAANLb8_yw:APA91bFDvtkq3567Mlg7k6IMw6R6ma_bTvGYnll4KdIHDkB71Lq4Dtfpih5D_mXW5GK1ooBIOOBYxVrQN5zjM59iiYkNKfD0kLEjogb_-nG-HZE6NaBK2a5qmjspQ-HEyV4E7k-EsdYY"
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
