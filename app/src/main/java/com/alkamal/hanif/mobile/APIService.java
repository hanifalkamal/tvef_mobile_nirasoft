package com.alkamal.hanif.mobile;

import com.alkamal.hanif.mobile.Response.DaftarRespone;
import com.alkamal.hanif.mobile.Response.LoginRespone;
import com.alkamal.hanif.mobile.Response.MultiTrxRespone;
import com.alkamal.hanif.mobile.Response.NewsResponse;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.CekSessionRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.Response.NamaUsahaRespone;
import com.alkamal.hanif.mobile.Response.SetSessionRespone;
import com.alkamal.hanif.mobile.adapter.ProdukTransaksi;
import com.alkamal.hanif.mobile.adapter.Session;
import com.alkamal.hanif.mobile.model.BalanceResponse;
import com.alkamal.hanif.mobile.model.BannerResponse;
import com.alkamal.hanif.mobile.model.ChangePasswordResponse;
import com.alkamal.hanif.mobile.model.CreateAccountResponse;
import com.alkamal.hanif.mobile.model.HistoriTrxVps;
import com.alkamal.hanif.mobile.model.HistoryResponse;
import com.alkamal.hanif.mobile.model.ParamConfigResponse;
import com.alkamal.hanif.mobile.model.PendingResponse;
import com.alkamal.hanif.mobile.model.ProductBanner;
import com.alkamal.hanif.mobile.model.ProductCategory;
import com.alkamal.hanif.mobile.model.ProductPayment;
import com.alkamal.hanif.mobile.model.ProductPaymentExpand;
import com.alkamal.hanif.mobile.model.Provider;
import com.alkamal.hanif.mobile.model.TopUpResponse;
import com.alkamal.hanif.mobile.model.TransactionResponse;
import com.alkamal.hanif.mobile.model.TransferSaldoResponse;
import com.alkamal.hanif.mobile.model.UserLoginResponse;
import com.alkamal.hanif.mobile.model.barangresponse.Data;
import com.google.gson.JsonObject;
import com.itenas.sikpsipil.data.model.VersionValidation.ArrayVersionValidation;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by HanifAlKamal on 07/12/2017.
 */

public interface APIService {
//    @Headers({
//            "Content-Type: application/json",
//            "Authorization: key=AAAANLb8_yw:APA91bFDvtkq3567Mlg7k6IMw6R6ma_bTvGYnll4KdIHDkB71Lq4Dtfpih5D_mXW5GK1ooBIOOBYxVrQN5zjM59iiYkNKfD0kLEjogb_-nG-HZE6NaBK2a5qmjspQ-HEyV4E7k-EsdYY"
//    })
//    @POST("fcm/send")
//    Call<MyRespone> sendNotification(@Body Sender body);

    @FormUrlEncoded
    @POST("kirimPesanB.php")
    Call<SendMsgRespone> SendMSg (
                                @Field("kodeplgn") String kodePlg,
                                @Field("pesan") String Pesan,
                                @Field("idtrx") String idtTrx,
                                @Field("namaserver") String namaServer,
                                @Field("tipedata") String tipedata);

    @FormUrlEncoded
    @POST("getBalasan.php")
    Call<MsgResponse> GetMSg (
                                @Field("kodeplgn") String kodePlg,
                                @Field("idtrx") String idtTrx,
                                @Field("namaserver") String namaServer);
    @FormUrlEncoded
    @POST("serviceGetBalasan.php")
    Call<MsgResponse> ServiceGetMSg (
            @Field("kodeplgn") String kodePlg,
            @Field("namaserver") String namaServer);

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginRespone> GetMSgLogin (
            @Field("no_telp") String no_telp,
            @Field("psw") String psw);


    @FormUrlEncoded
    @POST("daftar.php")
    Call<DaftarRespone> GetMSgDaftar (
            @Field("no_telp") String no_telp,
            @Field("psw") String psw,
            @Field("namaUsaha") String namaUsaha);

    @FormUrlEncoded
    @POST("GantiPsw.php")
    Call<DaftarRespone> GetGantiPsw (
            @Field("no_telp") String no_telp,
            @Field("psw_baru") String psw_baru);

    @FormUrlEncoded
    @POST("news.php")
    Call<NewsResponse> GetNews (
            @Field("versi_android") String versi_android,
            @Field("namaserver") String namaServer);
            //@Field("pengumuman1") String pengumuman1,
            //@Field("pengumuman2") String pengumuman2,
            //@Field("pengumuman3") String pengumuman3);

    @FormUrlEncoded
    @POST("setSession.php")
    Call<SetSessionRespone> SetSession (
            @Field("notelp") String notelp,
            @Field("status") String status,
            @Field("tanggal") String tanggal,
            @Field("waktu") String waktu);

    @FormUrlEncoded
    @POST("cekSessionStatus.php")
    Call<CekSessionRespone> CekSession (
            @Field("notelp") String notelp);

    @FormUrlEncoded
    @POST("getJenisTrx.php")
    Call<MultiTrxRespone> GetJenisTrx(
            @Field("id") String id);


    @FormUrlEncoded
    @POST("getNamaUsaha.php")
    Call<NamaUsahaRespone> GetNamaUsaha(
            @Field("no_telp") String no_telp
    );

    @FormUrlEncoded
    @POST("cekSessionStatus.php")
    Call<Session> getSession(
            @Field("notelp") String no_telp
    );

    @GET("getListProduk.php")
    Call<List<ProdukTransaksi>> getProduk();

    @FormUrlEncoded
    @POST("getHistoriTrx.php")
    Call<List<HistoriTrxVps>> getHistoryTrxVps(
            @Field("kodeplgn") String kodeplgn,
            @Field("namaserver") String namaserver
    );



    @Headers("Content-Type: application/json")
    @POST("opendata2.py")
    Call<ArrayVersionValidation> ExecuteQueryCheckVersi(@Body JsonObject body);

    //Spring
    @Headers("Content-Type: application/json")
    @POST("product/loadproduk")
    Call<List<ProductCategory>> getProductByProductCategory(@Body JsonObject body);

    @FormUrlEncoded
    @POST("product/loadproduk")
    Call<List<ProductCategory>> loadProdukbyKategori(
            @Field("produk") String produk,
            @Field("kodeplgn") String kodeplgn
    );

    @Headers("Content-Type: application/json")
    @GET("productcategory/loadproductcategory")
    Call<List<ProductBanner>> getProductCategory();

    @Headers("Content-Type: application/json")
    @POST("product/getprodukdetil")
    Call<List<ProductCategory>> getProductDetil(@Body JsonObject body);

    @Headers("Content-Type: application/json")
    @POST("historitrx/getPesanByPhoneNumber")
    Call<List<HistoriTrxVps>> getHistoryTrxVpsByPhoneNumber(
            @Body JsonObject body
    );

    @FormUrlEncoded
    @POST("historitrx/getHistoryCurrentDate")
    Call<List<HistoriTrxVps>> getHistoryCurrentDate (
            @Field("kodeplgn") String kodeplgn,
            @Field("namaserver") String namaserver);

    @FormUrlEncoded
    @POST("historitrx/getHistoryByDate")
    Call<List<HistoriTrxVps>> getHistoryCustomDate (
            @Field("kodeplgn") String kodeplgn,
            @Field("namaserver") String namaserver,
            @Field("tanggal1") String tanggal1,
            @Field("tanggal2") String tanggal2);

    @FormUrlEncoded
    @POST("barang/brgharga")
    Call<List<Data>> getDataBarang (
            @Field("kodeplgn") String kodeplgn,
            @Field("no_tujuan") String no_tujuan,
            @Field("jenis") String jenis,
            @Field("kodebrg") String kodebrg,
            @Field("pin") String pin);

    @FormUrlEncoded
    @POST("provider/getimagebyprefix")
    Call<Provider> getImageByPrefix(
            @Field("no_tujuan") String no_tujuan
    );

    @FormUrlEncoded
    @POST("tvef/trx")
    Call<TransactionResponse> transaction (
            @Field("kodeplgn") String kodePlg,
            @Field("kodebrg") String kodeBrg,
            @Field("noTujuan") String noTujuan,
            @Field("pin") String pin,
            @Field("repeatCountTrx") Integer repeatCountTrx,
            @Field("harga") String harga,
            @Field("namabrg") String namaBrg
    );

    @FormUrlEncoded
    @POST("tvef/balanceInquiry")
    Call<BalanceResponse> balance (
            @Field("kodeplgn") String kodePlg,
            @Field("pin") String pin
    );

    @FormUrlEncoded
    @POST("/historitrx/historyCurrentDate")
    Call<List<HistoryResponse>> listCurrentDateTransaction (
            @Field("kodeplgn") String kodePlg
    );

    @FormUrlEncoded
    @POST("/historitrx/historyByDate")
    Call<List<HistoryResponse>> listRangeDateTransaction (
            @Field("kodeplgn") String kodePlg,
            @Field("startDate") String startDate,
            @Field("endDate") String endDate
    );

    @FormUrlEncoded
    @POST("/pendingtrx/listPending")
    Call<List<PendingResponse>> listPendingTransaction (
            @Field("kodeplgn") String kodePlg
    );

    @FormUrlEncoded
    @POST("/user/login")
    Call<UserLoginResponse> signIn (
            @Field("noTlp") String noTlp,
            @Field("psw") String psw,
            @Field("deviceId") String deviceId,
            @Field("deviceName") String deviceName
    );

    @FormUrlEncoded
    @POST("/user/changePsw")
    Call<ChangePasswordResponse> changePassword(
            @Field("noTlp") String noTlp,
            @Field("pswOld") String pswOld,
            @Field("psw") String psw
    );

    @FormUrlEncoded
    @POST("/tvef/trf")
    Call<TransferSaldoResponse> transfer(
            @Field("kodeplgn") String kodeplgn,
            @Field("noTujuan") String noTujuan,
            @Field("pin") String pin,
            @Field("nominal") String nominal,
            @Field("kodeTrf") String kodeTrf,
            @Field("ulang") String ulang
    );

    @FormUrlEncoded
    @POST("/tvef/topup")
    Call<TopUpResponse> topUp(
            @Field("kodeplgn") String kodeplgn,
            @Field("pin") String pin,
            @Field("nominal") String nominal,
            @Field("bank") String bank
    );

    @POST("/listproduct/home")
    Call<List<ProductPayment>> listHomeProduct();

    @FormUrlEncoded
    @POST("/listproduct/expand")
    Call<List<ProductPaymentExpand>> listProductbyKategori(
            @Field("kategori") String kategori
    );

    @FormUrlEncoded
    @POST("/tvef/daftar")
    Call<CreateAccountResponse> createAccount(
            @Field("noTlp") String noTlp,
            @Field("psw") String psw,
            @Field("namaUsaha") String namaUsaha,
            @Field("namaPemilik") String namaPemilik,
            @Field("alamat") String alamat,
            @Field("deviceId") String deviceId,
            @Field("deviceModel") String deviceModel,
            @Field("kodeplgninduk") String kodeplgninduk,
            @Field("pinBaru") String pinBaru,
            @Field("kodeVerifikasi") String kodeVerifikasi,
            @Field("opsi") String opsi
    );

    @GET("/parameter/customerservicenumber")
    Call<ParamConfigResponse> getCsNumber();

    @GET("/parameter/maintenancestatus")
    Call<ParamConfigResponse> getMaintenanceStatus();

    @GET("/banner/list")
    Call<List<BannerResponse>> listBanner();




}
