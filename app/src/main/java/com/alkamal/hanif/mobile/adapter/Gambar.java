package com.alkamal.hanif.mobile.adapter;

public class Gambar {
    private String Kode;
    private String ImageUrl;
    private String Jenis;
    private String Nama_Server;

    public Gambar(String kode, String imageUrl, String jenis, String nama_Server) {
        Kode = kode;
        ImageUrl = imageUrl;
        Jenis = jenis;
        Nama_Server = nama_Server;
    }

    public Gambar() {
    }

    public Gambar(String kode) {
        Kode = kode;
    }

    public String getKode() {
        return Kode;
    }

    public void setKode(String kode) {
        Kode = kode;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getJenis() {
        return Jenis;
    }

    public void setJenis(String jenis) {
        Jenis = jenis;
    }

    public String getNama_Server() {
        return Nama_Server;
    }

    public void setNama_Server(String nama_Server) {
        Nama_Server = nama_Server;
    }
}
