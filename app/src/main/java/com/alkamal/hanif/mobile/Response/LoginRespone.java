package com.alkamal.hanif.mobile.Response;

/**
 * Created by HanifAlKamal on 08/02/2018.
 */

public class LoginRespone {
    private String no_telp,psw;
    private Boolean success ;

    public LoginRespone(String no_telp, String psw, Boolean success) {
        this.no_telp = no_telp;
        this.psw = psw;
        this.success = success;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
