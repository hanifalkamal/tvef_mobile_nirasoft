package com.alkamal.hanif.mobile.adapter;

public class MultiTrx {
    private int id;
    private String nama_trx;

    public MultiTrx(int id, String nama_trx) {
        this.id = id;
        this.nama_trx = nama_trx;
    }

    public MultiTrx() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_trx() {
        return nama_trx;
    }

    public void setNama_trx(String nama_trx) {
        this.nama_trx = nama_trx;
    }
}
