package com.alkamal.hanif.mobile.utils

object CacheManager {

    @JvmStatic
    var isFirstOpenListBarang: Boolean? = null
    @JvmStatic
    var balance: String? = null
    var pin: String? = null
    var cs: String? = null
    @JvmStatic
    var kodePlgn: String? = null
}