package com.alkamal.hanif.mobile.form.Register

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.utils.CacheManager
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.opsi_downline_layout.*

class ChooseOptionCreateAccountActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.opsi_downline_layout)

        var intent = Intent(this, RegisterFormActivity::class.java)

        bt_opsiKaitkan.setOnClickListener {
            intent.putExtra(Constant.OPSI_REGIS, Constant.OPSI_REGIS_PRAKAITKAN)
            startActivity(intent)
        }

        bt_opsiJoin.setOnClickListener {
            intent.putExtra(Constant.OPSI_REGIS, Constant.OPSI_REGIS_JOIN)
            startActivity(intent)
        }

        bt_opsiDaftarBaru.setOnClickListener {
            intent.putExtra(Constant.OPSI_REGIS, Constant.OPSI_REGIS_NEW)
            startActivity(intent)
        }

        button_back.setOnClickListener {
            onBackPressed()
        }

        text_bantuan.setOnClickListener {
            if (!CacheManager.cs.isNullOrEmpty()) {
                var cs = CacheManager.cs
                startActivity(
                        Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://wa.me/$cs")
                        )
                )
            }
        }
    }
}
