package com.alkamal.hanif.mobile.model

import com.google.gson.annotations.SerializedName

data class ParamConfigResponse (
        @SerializedName("value") var value: String
)