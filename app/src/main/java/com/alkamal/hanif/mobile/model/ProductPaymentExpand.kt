package com.alkamal.hanif.mobile.model

import com.google.gson.annotations.SerializedName

data class ProductPaymentExpand(
        @SerializedName("id") var id :Int,
        @SerializedName("namaProduk") var namaProduk :String,
        @SerializedName("kodeProduk") var kodeProduk :String,
        @SerializedName("hint") var hint :String,
        @SerializedName("logo_produk_new") var logo_produk_new :String
)