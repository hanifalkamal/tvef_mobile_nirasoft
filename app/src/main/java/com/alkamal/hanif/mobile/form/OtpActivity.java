package com.alkamal.hanif.mobile.form;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtpActivity extends BaseActivity {

    private String verificationid;
    private FirebaseAuth mAuth;

    @BindView(R.id.progressBar)
    ProgressBar progressbar;

    @BindView(R.id.editTextCode)
    EditText editText;

    @BindView(R.id.act_code_edit)
    Pinview actCodeEdit;

    @BindView(R.id.tv_resend_sms)
    TextView resend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
//        resend.setVisibility(View.GONE);

        mAuth = FirebaseAuth.getInstance();
        String phonenumber = "+" + getIntent().getStringExtra("NoTelp");
        sendVerificationCode(phonenumber);

        actCodeEdit.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                String code = actCodeEdit.getValue().toString();


                actCodeEdit.setVisibility(View.INVISIBLE);
                progressbar.setVisibility(View.VISIBLE);
                verifyCode(code);


            }
        });


    }

    @OnClick(R.id.buttonSignIn)
    void onVerify() {
        String code = editText.getText().toString().trim();

        if ((code.isEmpty() || code.length() < 6)) {

            editText.setError("Enter code...");
            editText.requestFocus();
            return;
        }
        verifyCode(code);
    }

    @OnClick(R.id.tv_resend_sms)
    void onResend() {
        if (resend.getText().equals("Kirim ulang kode verifikasi!")) {
            Toast.makeText(this, "Kode verifikasi sudah di kirim ulang !", Toast.LENGTH_SHORT).show();

            String phonenumber = "+" + getIntent().getStringExtra("NoTelp");
            sendVerificationCode(phonenumber);
//            resend.setVisibility(View.GONE);
        }
    }

    private void verifyCode(String code) {
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationid, code);
            signInWithCredential(credential);
        } catch (Exception e) {
            Toast.makeText(this, "Silahakan coba lagi", Toast.LENGTH_SHORT).show();
            resend.setVisibility(View.VISIBLE);
//            e.printStackTrace();
//            actCodeEdit.setVisibility(View.VISIBLE);
//            progressbar.setVisibility(View.INVISIBLE);
//
//            String phonenumber = "+" + getIntent().getStringExtra("NoTelp");
//            sendVerificationCode(phonenumber);
        }
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            String myID = "AND" + getIntent().getStringExtra("NoTelp");
                            getSharedPreferences("MY_DATA", MODE_PRIVATE)
                                    .edit()
                                    .putString("MY_USERID", myID)
                                    .putString("MY_NUMBER", getIntent().getStringExtra("NoTelp"))
                                    .apply();

                            getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                                    .edit()
                                    .putBoolean("isFirstRun", false)
                                    .apply();

                            Intent intent = new Intent(OtpActivity.this, SetPinActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                            startActivity(intent);
                            FirebaseAuth.getInstance().signOut();

                        } else {

                            actCodeEdit.setVisibility(View.VISIBLE);
                            progressbar.setVisibility(View.INVISIBLE);

                            Toast.makeText(OtpActivity.this, "Kode invalid !", Toast.LENGTH_LONG).show();
//                            Toast.makeText(OtpActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                });
    }

    private void sendVerificationCode(String number) {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallBack
        );

        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                resend.setText("waktu kirim ulang: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                resend.setText("Kirim ulang kode verifikasi!");
            }
        }.start();
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            verificationid = s;
            Log.d(TAG, "onCodeSent: " + verificationid);
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                progressbar.setVisibility(View.VISIBLE);
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.e(TAG, "onVerificationFailed: " + e.getMessage());

            actCodeEdit.setVisibility(View.VISIBLE);
            progressbar.setVisibility(View.INVISIBLE);
            actCodeEdit.setValue("");
            Toast.makeText(OtpActivity.this, "Ada kesalahan pada server, silahkan coba beberapa saat lagi", Toast.LENGTH_LONG).show();
        }
    };

    private static final String TAG = "OtpActivity";

}



