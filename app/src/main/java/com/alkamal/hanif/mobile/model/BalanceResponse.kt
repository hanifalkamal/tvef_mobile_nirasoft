package com.alkamal.hanif.mobile.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class BalanceResponse(
        @SerializedName("saldo") var saldo: String,
        @SerializedName("nama") var nama: String,
        @SerializedName("kdplgnmaster") var kdplgnmaster: String,
        @SerializedName("kdplgn") var kdplgn: String,
        @SerializedName("hp") var hp: String,
        @SerializedName("tglanggota") var tglanggota: String,
        @SerializedName("tglpoint") var tglpoint: String,
        @SerializedName("statusharga") var statusharga: String,
        @SerializedName("maxpiutang") var maxpiutang: String,
        @SerializedName("statusplgn") var statusplgn: String,
        @SerializedName("uperkodeplgn") var uperkodeplgn: String,
        @SerializedName("kodeplgn_induk") var kodeplgn_induk: String,
        @SerializedName("namaupline") var namaupline: String,
        @SerializedName("bankrekening") var bankrekening: String,
        @SerializedName("banknama") var banknama: String,
        @SerializedName("pointawal") var pointawal: String,
        @SerializedName("komisi") var komisi: String,
        @SerializedName("idutama") var idutama: String,
        @SerializedName("NamaUpline_pelanggan") var NamaUpline_pelanggan: String,
        @SerializedName("cekpoint") var cekpoint: String,
        @SerializedName("pengumuman") var pengumuman: String,
        @SerializedName("respon") var respon: String,
        @SerializedName("success") var success: Boolean
): Parcelable