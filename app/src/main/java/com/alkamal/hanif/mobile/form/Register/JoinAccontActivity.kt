package com.alkamal.hanif.mobile.form.Register

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.model.CreateAccountResponse
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.join_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class JoinAccontActivity : BaseActivity() {

    lateinit var noMember: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.join_layout)

        button_back.setOnClickListener {
            onBackPressed()
        }

        bt_Send_join.setOnClickListener {

            noMember = ed_noMember.text.toString()

            if (noMember.isNullOrEmpty()){
                text_fail_join.visibility = View.VISIBLE
                text_fail_join.setText("No Member Tidak Boleh Kosong !")
            } else {
                submit()
            }

        }

        ed_noMember.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                try {
                    if ("" + ed_noMember.text.toString().get(0) == "0") {
                        ed_noMember.setText(ed_noMember.getText().toString().substring(1))
                        ed_noMember.append("62")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }

    fun submit() {
        text_fail_join.visibility = View.INVISIBLE
        try {
            progressBar.visibility = View.VISIBLE
            var call = api.createAccount(
                    intent.getStringExtra(Constant.REGISTER_NO_TELP),
                    intent.getStringExtra(Constant.REGISTER_PSW),
                    intent.getStringExtra(Constant.REGISTER_NAMA_USAHA),
                    intent.getStringExtra(Constant.REGISTER_NAMA_PEMILIK),
                    intent.getStringExtra(Constant.REGISTER_ALAMAT),
                    encodeAndReverse(androidId),
                    encodeAndReverse(deviceModel),
                    noMember,
                    intent.getStringExtra(Constant.REGISTER_PIN),
                    "",
                    intent.getStringExtra(Constant.OPSI_REGIS))
            call.enqueue(object : Callback<CreateAccountResponse> {
                override fun onFailure(call: Call<CreateAccountResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, "Internal Server Error", Toast.LENGTH_SHORT).show()
                    progressBar.visibility = View.GONE
                }

                override fun onResponse(call: Call<CreateAccountResponse>, response: Response<CreateAccountResponse>) {
                    var result = response.body()
                    progressBar.visibility = View.GONE
                    if (result != null) {
                        if (result.success) {
                            var intent = Intent(applicationContext, RegisterSucceedActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent)

                            finish()
                        } else {
                            progressBar.visibility = View.GONE
                            text_fail_join.visibility = View.VISIBLE
                            text_fail_join.setText(result.keterangan)
//                            throw Exception(result.keterangan)
                        }

                    }
                }
            })


        } catch (e: Exception) {
            progressBar.visibility = View.GONE
            text_fail_join.visibility = View.VISIBLE
            text_fail_join.setText(e.message)
            e.printStackTrace()
        }

    }
}
