package com.alkamal.hanif.mobile.adapter;

/**
 * Created by HanifAlKamal on 04/03/2018.
 */

public class Histori {
    private int id;
    private String isiHistori;

    public Histori(int id, String isiHistori) {
        this.id = id;
        this.isiHistori = isiHistori;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIsiHistori() {
        return isiHistori;
    }

    public void setIsiHistori(String isiHistori) {
        this.isiHistori = isiHistori;
    }
}
