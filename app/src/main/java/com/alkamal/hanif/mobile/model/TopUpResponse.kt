package com.alkamal.hanif.mobile.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TopUpResponse(
        @SerializedName("pesan") val pesan : String,
        @SerializedName("success") val success : Boolean,
        @SerializedName("tiket") val tiket : String,
        @SerializedName("keterangan") val keterangan : String,
        @SerializedName("bank") val bank : String,
        @SerializedName("nominalTrf") val nominalTrf : String
): Parcelable