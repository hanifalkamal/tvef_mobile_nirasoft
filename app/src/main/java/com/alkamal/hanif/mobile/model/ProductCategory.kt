package com.alkamal.hanif.mobile.model

import com.google.gson.annotations.SerializedName

class ProductCategory(
        @SerializedName("id") var id : Int,
        @SerializedName("produk") var produk : String,
        @SerializedName("kode_produk") var kode_produk : String,
        @SerializedName("deskrispi") var deskrispi : String,
        @SerializedName("nama_produk") var nama_produk : String,
        @SerializedName("harga_produk") var harga_produk : String,
        @SerializedName("pic1") var pic1 : String,
        @SerializedName("pic2") var pic2 : String,
        @SerializedName("pic3") var pic3 : String,
        @SerializedName("pic4") var pic4 : String,
        @SerializedName("pic5") var pic5 : String
)
