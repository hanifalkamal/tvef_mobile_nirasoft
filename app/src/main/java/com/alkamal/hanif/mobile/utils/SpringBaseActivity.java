package com.alkamal.hanif.mobile.utils;

import android.util.Log;

import com.alkamal.hanif.mobile.APIService;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SpringBaseActivity {

    APIService api ;

    public SpringBaseActivity() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient().newBuilder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);
        client.addInterceptor(interceptor);
        Retrofit base = new Retrofit.Builder()
                //.baseUrl("http://www.tvef.xyz/tvefKirimPesan/")
                .baseUrl("http://193.168.195.139:8081/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();
        api = base.create(APIService.class);
    }

    public APIService getApi(){
        return api;
    }


    public void log(String log) {
        if (true) {
            Log.d("Hanif" , log);
        }
    }

}
