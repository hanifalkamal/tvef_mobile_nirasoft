package com.alkamal.hanif.mobile.model

import com.google.gson.annotations.SerializedName

data class BannerResponse (
        @SerializedName("url") var url: String
)