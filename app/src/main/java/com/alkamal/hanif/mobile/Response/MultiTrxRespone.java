package com.alkamal.hanif.mobile.Response;

public class MultiTrxRespone {
    private String jenisTrx,id;
    private Boolean success;

    public MultiTrxRespone(String jenisTrx, Boolean success) {
        this.jenisTrx = jenisTrx;
        success = success;
    }

    public MultiTrxRespone(String jenisTrx, String id, Boolean success) {
        this.jenisTrx = jenisTrx;
        this.id = id;
        this.success = success;
    }

    public MultiTrxRespone() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJenisTrx() {
        return jenisTrx;
    }

    public void setJenisTrx(String jenisTrx) {
        this.jenisTrx = jenisTrx;
    }

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        success = success;
    }
}
