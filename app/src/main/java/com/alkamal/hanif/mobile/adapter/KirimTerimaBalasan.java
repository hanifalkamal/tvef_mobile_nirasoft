package com.alkamal.hanif.mobile.adapter;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.form.BalasanActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hanif on 11/05/2018.
 */

public class KirimTerimaBalasan extends BaseActivity {
    NotificationCompat.Builder notification;
    String pesan;
    Calendar calendar;
    SimpleDateFormat dateformat;
    String tanggal,idtrx,Server,pin;
    int waktutimeout,CekCounting;
    ProgressDialog prd;

    private static final int uniqueID = 45612;

    public KirimTerimaBalasan() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

    }

    public void sendReqSal(final int waktutimeout, Call<SendMsgRespone> call, final Call<MsgResponse> call2){



        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);
        setPesan("");

        //Call<SendMsgRespone> call = getApi().SendMSg(myImei, format, idtrx,Server);
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
//                        Toast.makeText(KirimTerimaBalasan.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
//                        Intent i = new Intent(getApplicationContext(),ResponService.class);
//                        startService(i);
                        getBalsan(call2,waktutimeout);

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(KirimTerimaBalasan.this, "Transaksi Gagal Cek Koneksi", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void getBalsan(final Call<MsgResponse> call2, final int waktutimeout){
        //Call<getMsgResponse> call2 = getApi().GetMSg(myImei,idtrx,Server);
        call2.clone().enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call2, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {
                    //    Intent i = new Intent(getApplicationContext(),ResponService.class);
                    //    stopService(i);
                        //Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_LONG).show();
                        //prd.dismiss();
                       // Intent intent = new Intent(KirimTerimaBalasan.this, BalasanActivity.class);
                       // intent.putExtra("CekSal",ges.getPesan());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                // yourMethod();
                            }
                        }, 1000);   //5 seconds*/

                        setPesan(ges.getPesan());
//                        Notif(ges.getPesan());
                        //startActivity(intent);
                        //finish();
                        //tvSaldo.setText(ges.getPesan());

                        //finish();

                        log("Berhasil");

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < waktutimeout){
                            getBalsan(call2,waktutimeout);
                            CekCounting = CekCounting+1;
                        }else{

                     //       Notif("Time Out / Tidak ada balasan masuk");
                     //       Toast.makeText(KirimTerimaBalasan.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call2, Throwable t) {log(t.toString());}
        });
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public String pesan(){
        return pesan;
    }

    public void Notif(String notifBody){
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker(notifBody);
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("TVEF");
        notification.setContentText(notifBody);

      //  Context context = getApplicationContext();
        Intent intent = new Intent(KirimTerimaBalasan.this, BalasanActivity.class);
        intent.putExtra("CekSal",notifBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());

    }
}
