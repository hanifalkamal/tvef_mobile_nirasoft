package com.alkamal.hanif.mobile.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ResponseTrx(
        @SerializedName("success") val success : Boolean,
        @SerializedName("pesan") val pesan : String,
        @SerializedName("response_code") val responseCode : String,
        @SerializedName("response_message") val responseMessage : String
):Parcelable