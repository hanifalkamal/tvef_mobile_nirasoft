package com.alkamal.hanif.mobile.form.Produk

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.model.ProductBanner
import com.alkamal.hanif.mobile.utils.Constant
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_merk.*
import kotlinx.android.synthetic.main.layout_item_merk.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MerkActivity : BaseActivity() {

    lateinit var listProductBanner: List<ProductBanner>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merk)

        button_back.setOnClickListener {
            onBackPressed()
        }

        getListMerk(this)

    }

    fun getListMerk(context: Context){
        var call = api.productCategory
        call.enqueue(object : Callback<List<ProductBanner>>{
            override fun onFailure(call: Call<List<ProductBanner>>, t: Throwable) {
                Toast.makeText(applicationContext, "Internal Server Error", Toast.LENGTH_SHORT).show()
                progressBar.visibility = View.GONE
            }

            override fun onResponse(call: Call<List<ProductBanner>>, response: Response<List<ProductBanner>>) {
                var list = response.body()
                if (list != null){

                    if (list.isEmpty()){
                        layout_image_empty_data.visibility = View.VISIBLE
                    }

                    listProductBanner = list
                    val merkAdapter = RecyclerViewAdapter(listProductBanner, context)
                    recycler_grid_merk.apply {
                        layoutManager = GridLayoutManager(context, 2)
                        adapter = merkAdapter
                    }
                }

                progressBar.visibility = View.GONE
            }
        })
    }

    private class RecyclerViewAdapter(private val list: List<ProductBanner>, private val context: Context) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(){

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
             val imageProduct = view.image_product

            fun bindItem(productBanner: ProductBanner){

            }

        }

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerViewAdapter.ViewHolder {
            return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.layout_item_merk, p0, false))
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(p0: RecyclerViewAdapter.ViewHolder, p1: Int) {

            p0.bindItem(list[p1])

            ShowImage(list.get(p1).banner, p0.imageProduct)

            var kodeProduk = list.get(p1).product_code
            //StartActivity
            var intent = Intent(context , ListBarangByMerkActivity::class.java)
            intent.putExtra(Constant.BUNDLE_PRODUK_KATEGORI, kodeProduk)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            p0.imageProduct.setOnClickListener {
//                Toast.makeText(context, "Internal Server Error", Toast.LENGTH_SHORT).show()
                ContextCompat.startActivity(context, intent, null)
            }


        }

        fun ShowImage(bannerUrl: String, imageView: ImageView) {
            Glide.with(context)
                    .load(Constant.BaseUrlPic + bannerUrl)
                    .apply(RequestOptions().override(145, 102))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
//                            view.progressBar.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
//                            view.progressBar .visibility = View.GONE
                            return false
                        }

                    })
                    .into(imageView)
        }


    }

    var dummy = "[\n" +
            "  {\n" +
            "    \"id\": 40,\n" +
            "    \"product_name\": \"HP OPPO\",\n" +
            "    \"product_code\": \"OPPO\",\n" +
            "    \"urutan\": \"1\",\n" +
            "    \"banner\": \"/uploads/provider/oppo.png\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 59,\n" +
            "    \"product_name\": \"HP VIVO\",\n" +
            "    \"product_code\": \"VIVO\",\n" +
            "    \"urutan\": \"2\",\n" +
            "    \"banner\": \"/uploads/provider/vivo.jpg\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 50,\n" +
            "    \"product_name\": \"HP SAMSUNG\",\n" +
            "    \"product_code\": \"SAMSUNG\",\n" +
            "    \"urutan\": \"3\",\n" +
            "    \"banner\": \"/uploads/provider/samsung.jpg\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 54,\n" +
            "    \"product_name\": \"HP REALME\",\n" +
            "    \"product_code\": \"REALME\",\n" +
            "    \"urutan\": \"4\",\n" +
            "    \"banner\": \"/uploads/provider/Realme.png\"\n" +
            "  }\n" +
            "]"
}
