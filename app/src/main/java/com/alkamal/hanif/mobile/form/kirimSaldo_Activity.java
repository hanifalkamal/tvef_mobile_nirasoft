package com.alkamal.hanif.mobile.form;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 24/02/2018.
 */

public class kirimSaldo_Activity extends BaseActivity {

    String  myID,pin,tanggal,idtrx,Server;
    int CekCounting,waktutimeout;
    NotificationCompat.Builder notification;
    Calendar calendar;
    SimpleDateFormat dateformat;
    private static final int uniqueID = 45612;
    ProgressDialog prd;

    @BindView(R.id.ed_nominalKirimSaldo)
    EditText edNominal;
    @BindView(R.id.ed_Kodeplgn_tujuan) EditText edTujuan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.kirimsaldo_layout);
        ButterKnife.bind(this);
        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);
        //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
        pin = prefs.getString("MY_PIN_TRX", "");
        Server = prefs.getString("MY_SERVER", "");
        myID = prefs.getString("MY_USERID","");

    }

    public void sendReqSal(String format){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;

        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx,Server,"");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(kirimSaldo_Activity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        getBalsan();

                        //finish();
                        log("Berhasil");

                    } else {
                        prd.dismiss();
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(kirimSaldo_Activity.this, "Gagal Load Data Cek Koneksi Internet", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    @OnClick(R.id.bt_KirimSaldo) public void onKirimSaldo(){
        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();

        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
        pin = prefs.getString("MY_PIN_TRX", "");

        String Format = "ts "+pin+" "+edNominal.getText().toString()+" "+edTujuan.getText().toString();
        sendReqSal(Format);


    }

    public void getBalsan(){
        Call<MsgResponse> call = getApi().GetMSg(myID,idtrx, Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {
                        prd.dismiss();
                        Intent intent = new Intent(kirimSaldo_Activity.this, BalasanActivity.class);
                        intent.putExtra("CekSal",ges.getPesan());
                        Notif(ges.getPesan());
                        startActivity(intent);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {

                        log("Berhasil");
                            }
                        }, 1000);

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < waktutimeout){
                            getBalsan();
                            CekCounting = CekCounting+1;
                        }else{
                            prd.dismiss();
                            Notif("Time Out / Tidak ada balasan masuk");
                            Toast.makeText(kirimSaldo_Activity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }

    public void Notif(String notifBody){
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker("This is the ticker");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("TVEF");
        notification.setContentText(notifBody);

        Context context = getApplicationContext();
        Intent intent = new Intent(context, BalasanActivity.class);
        intent.putExtra("CekSal",notifBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());

    }

    private final int PICK_CONTACT = 1;
    @OnClick(R.id.bt_Contacts) public void onContacts(){
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_CONTACT) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                Uri ContactData = data.getData();
                Cursor C = getContentResolver().query(ContactData, null, null, null, null);

                if (C.moveToFirst()) {
                    String name = C.getString(C.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
                    ContentResolver cr = getContentResolver();
                    Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                            "DISPLAY_NAME = '" + name + "'", null, null);
                    if (cursor.moveToFirst()) {
                        String contactId =
                                cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (phones.moveToNext()) {
                            String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                            switch (type) {
                                case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                    // do something with the Mobile number here...
                                    edTujuan.setText(number);
                                    break;
                            }
                        }

                    }
                }
            }
        }


    }
}
