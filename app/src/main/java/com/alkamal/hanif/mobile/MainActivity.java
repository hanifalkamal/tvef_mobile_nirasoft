package com.alkamal.hanif.mobile;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BackgroundService.FirebaseInstanceService;
import com.alkamal.hanif.mobile.BackgroundService.beritaService;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.Response.NamaUsahaRespone;
import com.alkamal.hanif.mobile.Response.SetSessionRespone;
import com.alkamal.hanif.mobile.chat.ChatActivity;
import com.alkamal.hanif.mobile.form.BalasanActivity;
import com.alkamal.hanif.mobile.form.LoginActivity;
import com.alkamal.hanif.mobile.form.MultiTrxActivity;
import com.alkamal.hanif.mobile.form.PilihTanggalHistoryActivity;
import com.alkamal.hanif.mobile.form.PrinterSettingActivity;
import com.alkamal.hanif.mobile.form.ProdukActivity;
import com.alkamal.hanif.mobile.form.SettingActivity;
import com.alkamal.hanif.mobile.form.TambahSaldoActivity;
import com.alkamal.hanif.mobile.form.VersionValidationActivity;
import com.alkamal.hanif.mobile.form.listProviderActivity;
import com.alkamal.hanif.mobile.form.pilihProviderActivity;
import com.alkamal.hanif.mobile.form.trxActivity;
import com.alkamal.hanif.mobile.adapter.BannerAdapter;
import com.alkamal.hanif.mobile.adapter.Gambar;
import com.alkamal.hanif.mobile.adapter.ProdukGridAdapter;
import com.alkamal.hanif.mobile.adapter.ProdukTransaksi;
import com.alkamal.hanif.mobile.adapter.settingFormat;
import com.alkamal.hanif.mobile.model.ProductBanner;
import com.alkamal.hanif.mobile.model.Saldo;
import com.alkamal.hanif.mobile.utils.CekVersionRetrofit;
import com.alkamal.hanif.mobile.utils.Constant;
import com.alkamal.hanif.mobile.utils.SpringBaseActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.itenas.sikpsipil.data.model.VersionValidation.ArrayVersionValidation;
import com.itenas.sikpsipil.data.model.VersionValidation.VersionValidationResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

    private static final String TAG = "MainActivity";
    static String topic;
    String myID, Server, myNumber;
    Calendar calendar;
    SimpleDateFormat dateformat;
    String tanggal, idtrx;
    private int dotscount;
    int dotIndex = 0;
    int pilihtoken = 0, grab = 0, gojek = 0, ovo = 0, etoll = 0, fromListProduk = 0;
    String SelectedLogoUrl;
    String SelectedHint;
    private static int CekCounting, count;
    int KlikMenuIsiUlang, klikTambahSaldo;
    private SparseIntArray mErrorString;
    String format, pin, balasan;

    String SelectedProduk;

    NotificationCompat.Builder notification;
    private static final int uniqueID = 45612;
    //public beritaService bS = new beritaService();
    private BroadcastReceiver broadcastReceiver;
    private static FirebaseInstanceService FIS;
    //    @BindView(R.id.bt_news)
//    Button news;
    private SharedPreferences prefs;
    ProgressDialog prd;
    final Handler handler = new Handler();

    String isi = "Belum ada berita";

    FirebaseUser firebaseUser;
//    private FirebaseAuth mAuth;

    private BannerAdapter adapter;

    private ImageView[] dots;
    public static final ArrayList<String> ImageUrl = new ArrayList<String>();
    //    @BindView(R.id.iv_iklan)
//    ImageView ivIklan;
    @BindView(R.id.tv_jumSaldo)
    TextView tvJumSaldo;
    @BindView(R.id.vp_banner)
    ViewPager viewPager;
    @BindView(R.id.SliderDots)
    LinearLayout sliderDotspanel;
    @BindView(R.id.navbar_bottom)
    RelativeLayout navbarBottom;
    @BindView(R.id.bt_refreshsaldo)
    ImageView btRefreshSaldo;
    @BindView(R.id.progressBar)
    ProgressBar loadingAnim;

    private static int x = 0;

//    int letterIcon[] = {
//            R.drawable.new_isipulsa,
//            R.drawable.new_isidata,
//            R.drawable.new_messanger,
//            R.drawable.new_cekharga,
//            R.drawable.new_pln,
//            R.drawable.gojek_btn,
//            R.drawable.grab_btn,
//            R.drawable.ovo_btn,
//            R.drawable.etoll_btn,
//            R.drawable.new_lainnya
//    };

    public String letterIcon[];
    public String logoProduk[];
    public String hint[];

    public String letterList[];
    int myLastVisiblePos;
//String letterList[] = {
//            "Isi Pulsa",
//            "Paket Data",
//            "Messanger",
//            "Cek Harga",
//            "PLN",
//            "GO-JEK",
//            "GRAB",
//            "OVO",
//            "E-Toll",
//            "Lainnya"
//    };

    public String kodebrg[];
    public String selectedKodebrg;
// String kodebrg[] = {
//            "isi_pulsa",
//            "paket_data",
//            "messanger",
//            "cek_harga",
//            "pln",
//            "GOJEK",
//            "GRB",
//            "OVO",
//            "ETOL",
//            "Lainnya"
//    };

    GridView gridView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mErrorString = new SparseIntArray();
        Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstRun", true);
        // setContentView(R.layout.act_nav);

        cekVersi();

        if (isFirstRun) {

            // Place your dialog code here to display the dialog

            //=========Settingan awal=========
            prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("MY_FORMAT_CEKSALDO", "saldo 1234");
            editor.putString("MY_TRX_TIME_OUT", "120");
            //editor.putString("MY_PIN_TRX", "1234");

            editor.apply();


            //=================================
            // Toast.makeText(MainActivity.this, "First Run", Toast.LENGTH_LONG).show();
            Intent in = new Intent(this, LoginActivity.class);
            startActivity(in);
            runtime_permissions();
            finish();

            // setContentView(R.layout.login_layout);


        } else {
            setContentView(R.layout.mainmenubaru3);
            ButterKnife.bind(this);
            runtime_permissions();

            prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
            String getPinTrx = prefs.getString("MY_PIN_TRX", "1234");
            SharedPreferences.Editor editformat = prefs.edit();
            editformat.putString("MY_FORMAT_CEKSALDO", "saldo " + getPinTrx);


            //editor.putString("MY_PIN_TRX", "1234");

            editformat.apply();
            resetCounter();

            firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

            if (firebaseUser == null) {
//                Intent intent = new Intent(MainActivity.this, SignEmail.class);
//                startActivity(intent);

            }

            Intent i = new Intent(this, FirebaseInstanceService.class);
            startService(i);
            //String Token = FirebaseInstanceService
//            Log.e("FIS",FirebaseInstanceId.getInstance().getToken());
            prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
            myID = prefs.getString("MY_USERID", "");
            myNumber = prefs.getString("MY_NUMBER", "");
            Server = prefs.getString("MY_SERVER", "");
            count = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
            getnamaUsaha();
            //REAL
            String formatCekSaldo = prefs.getString("MY_FORMAT_CEKSALDO", "");
            sendReqSal(formatCekSaldo);
            //DUMMY
//            Gson g = new Gson();
//            Saldo respSaldo = g.fromJson(saldoDummy, Saldo.class);
//            String saldo = respSaldo.getSaldo();
//            tvJumSaldo.setText("Rp. " + saldo);
//            btRefreshSaldo.setVisibility(View.VISIBLE);
//            loadingAnim.setVisibility(View.GONE);

            Server = prefs.getString("MY_SERVER", "");
            notification = new NotificationCompat.Builder(this);
            notification.setAutoCancel(true);

            gridView = (GridView) findViewById(R.id.gridview);
            try {

                getAllProduk();
            } catch (Exception e) {
                e.printStackTrace();
            }

            //if(!runtime_permissions()) {
            // StringBuilder sb = new StringBuilder();

            // TelephonyManager telMan = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            // sb.append(telMan.getDeviceId());
            //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            //Intent i = new Intent(getApplicationContext(),beritaService.class);
            //startService(i);
            //}

            // Intent i = new Intent(getApplicationContext(),ResponService.class);
            // startService(i);
            if (!myID.isEmpty()) {
                topic = "/topics/" + myID;
                FirebaseMessaging.getInstance().subscribeToTopic(topic);
                Log.e("topic ", topic);
                topic = "/topics/free_cell";
                FirebaseMessaging.getInstance().subscribeToTopic(topic);
                Log.e("topic ", topic);
            }

            Toast.makeText(MainActivity.this, "Save Berhasil", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getApplicationContext(), beritaService.class);
            startService(intent);

            SharedPreferences prefs = getSharedPreferences("MY_NEWS", MODE_PRIVATE);
            //news.setText(prefs.getString("MY_NEWS1", ""));

            prefs = getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("NOTIF", 0);
            editor.apply();

            displayIklan();


            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(MainActivity.this, letterList[position], Toast.LENGTH_SHORT).show();
                    if (position < 5) {
                        switch (position) {
                            case 0:
                                Intent intent = new Intent(MainActivity.this, pilihProviderActivity.class);
                                intent.putExtra("pilihan", "pulsa");
                                startActivity(intent);
                                break;
                            case 1:
                                Intent intent2 = new Intent(MainActivity.this, pilihProviderActivity.class);
                                intent2.putExtra("pilihan", "data");
                                startActivity(intent2);
                                break;
                            case 2:
                                Intent intent3 = new Intent(MainActivity.this, ChatActivity.class);
                                startActivity(intent3);
                                break;
                            case 3:
                                Intent intent4 = new Intent(MainActivity.this, listProviderActivity.class);
                                startActivity(intent4);
                                break;
                            case 4:
                                Intent intent5 = new Intent(MainActivity.this, pilihProviderActivity.class);
                                intent5.putExtra("pilihan", "transferpulsa");
                                startActivity(intent5);
                                break;


                        }
                    } else if (position < kodebrg.length - 1) {
                        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
                        pin = prefs.getString("MY_PIN_TRX", "");
                        Server = prefs.getString("MY_SERVER", "");
                        format = "databrg " + pin + " " + kodebrg[position];
                        selectedKodebrg = kodebrg[position];
                        Log.d(TAG, "onItemClick: FORMAT = " + format);
                        SelectedLogoUrl = logoProduk[position];
                        fromListProduk = 1;
                        SelectedProduk = letterList[position];
                        SelectedHint = hint[position];

//                        sendReqSal(format);

//                        prd = new ProgressDialog(MainActivity.this);
//                        prd.setMessage("Mohon Tunggu. . .");
//                        prd.setCancelable(false);
//                        prd.setCanceledOnTouchOutside(false);
//
//                        prd.show();
                        Intent intent = new Intent(MainActivity.this, trxActivity.class);
                        intent.putExtra("judul", "Transaksi Saldo " + SelectedProduk);
                        intent.putExtra("gambarProvider", "fromListProduk");
                        intent.putExtra("logoUrl", SelectedLogoUrl);
                        intent.putExtra("selectedKodebrg", selectedKodebrg);
                        intent.putExtra("VPS", "TRUE");
                        intent.putExtra("hint", SelectedHint);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(MainActivity.this, MultiTrxActivity.class);
                        startActivity(intent);
                    }
                }
            });

            gridView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {

                }
            });

            myLastVisiblePos = gridView.getFirstVisiblePosition();


            gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    int currentFirstVisPos = view.getFirstVisiblePosition();
                    if (currentFirstVisPos > myLastVisiblePos) {
                        //scroll down
                        navbarBottom.animate()
                                .translationY(view.getHeight())
//                                .alpha(0.0f)
                                .setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
//                                        navbarBottom.setVisibility(View.GONE);
                                    }
                                });
//                        navbarBottom.setVisibility(View.GONE);
                    }
                    if (currentFirstVisPos < myLastVisiblePos) {
                        //scroll up
//                        navbarBottom.setVisibility(View.VISIBLE);
                        navbarBottom.animate()
                                .translationY((float) 0)
//                                .alpha(1.0f)
                                .setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
//                                        navbarBottom.setVisibility(View.VISIBLE);
                                    }
                                });

//

                    }
                    myLastVisiblePos = currentFirstVisPos;

                }
            });
            //SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);


        }


        // cekNews();

    }

    private void showBanner() {
        adapter = new BannerAdapter(this, ImageUrl);
        viewPager.setAdapter(adapter);

        dotscount = adapter.getCount();
        dots = new ImageView[dotscount];

        for (int i = 0; i < dotscount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.nonactive_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);

        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dot));

        handler.postDelayed(Banner_runnable, 2000);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    private Runnable Banner_runnable = new Runnable() {
        @Override
        public void run() {
            /* do what you need to do */
            if (viewPager.getCurrentItem() == dotIndex) {
                Log.d(TAG, "run: " + dotIndex);
                //Toast.makeText(getContext(), dotIndex, Toast.LENGTH_SHORT).show();
                if (dotIndex == ImageUrl.size() - 1) {
                    dotIndex = 0;
                    viewPager.setCurrentItem(0);
                } else {
                    viewPager.setCurrentItem(dotIndex + 1);
                    dotIndex++;
                }
            }
            /* and here comes the "trick" */
            handler.postDelayed(this, 2000);
        }
    };

    private void getAllProduk() {
        Log.d(TAG, "getAllProduk: SAMPE SINI");
        prd = new ProgressDialog(MainActivity.this);
//        prd = new ProgressDialog(MainActivity.this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);
        prd.show();
        Call<List<ProdukTransaksi>> call = getApi().getProduk();
        call.enqueue(new Callback<List<ProdukTransaksi>>() {
            @Override
            public void onResponse(Call<List<ProdukTransaksi>> call, Response<List<ProdukTransaksi>> response) {
                try {
                    Log.d(TAG, "onResponse: list Produk berhasil di ambil");
                    List<ProdukTransaksi> listProduk = response.body();
                    int index = 0;
                    letterIcon = new String[listProduk.size()];
                    letterList = new String[listProduk.size()];
                    kodebrg = new String[listProduk.size()];
                    logoProduk = new String[listProduk.size()];
                    hint = new String[listProduk.size()];
                    for (ProdukTransaksi pt : listProduk) {
                        letterList[index] = pt.getNama_produk();
                        letterIcon[index] = pt.getIcon_url();
                        kodebrg[index] = pt.getKode_produk();
                        logoProduk[index] = pt.getLogo_produk();
                        hint[index] = pt.getHint();
                        Log.d(TAG, "onResponse: kodebarang = " + kodebrg[index]);

                        index++;
                    }

                    ProdukGridAdapter adapter = new ProdukGridAdapter(letterIcon, letterList, MainActivity.this);
                    gridView.setAdapter(adapter);
                    prd.hide();
                } catch (Exception e) {
                    prd.hide();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<ProdukTransaksi>> call, Throwable t) {
                prd.hide();
                Log.d(TAG, "onFailure: " + t.getMessage());
                t.printStackTrace();
            }
        });
    }


    private void displayIklan() {
        Query query = FirebaseDatabase.getInstance().getReference("gambar");
        //reference = FirebaseDatabase.getInstance().getReference("gambar").child("B00"+i);
        query.addValueEventListener(new ValueEventListener() {
            int i = 0;

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ImageUrl.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Gambar gambar = snapshot.getValue(Gambar.class);
                    Log.d("cobaGambar", "getData");
                    try {
                        if (gambar.getJenis().equals("Iklan")) {
                            //                        edGetData.append(gambar.getKode() + "\n");
                            //                        edGetData.append(gambar.getJenis() + "\n");
                            //                        edGetData.append(gambar.getNama_Server() + "\n");
                            //                        edGetData.append(gambar.getImageUrl() + "\n");

                            ImageUrl.add(gambar.getImageUrl().toString());
                            Log.d("Task", ImageUrl.get(i) + "");


                        }
                        i++;
                    } catch (Exception e) {
                        Log.d("Task", "Ganti Gambar");
                    } finally {

                    }

                }
                showBanner();
//                final RequestOptions requestOptions = new RequestOptions();
//                requestOptions.skipMemoryCache(true);
//                requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
//
//                final Handler handler = new Handler();
//                final Runnable runnable = new Runnable() {
//                    public void run() {
//
//                        if (x == ImageUrl.size()){
//
//                            x = 0;
//                        }
//                        Glide.with(getApplicationContext())
//                                .load(ImageUrl.get(x))
//                                .transition(DrawableTransitionOptions.withCrossFade(1000))
//                                .apply(requestOptions)
//                                .into(ivIklan);
//                        Log.d("loop", "jalan");
//
//                        if (x++ < ImageUrl.size()) {
//
//                            handler.postDelayed(this, 20000);
//                        }
//
//                    }
//                };
//
//                // trigger first time
//                handler.post(runnable);
//
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("cobaGambar", "Error");
            }
        });

    }

    private void getnamaUsaha() {
        Call<NamaUsahaRespone> call = getApi().GetNamaUsaha(myNumber);
        call.enqueue(new Callback<NamaUsahaRespone>() {
            @Override
            public void onResponse(Call<NamaUsahaRespone> call, Response<NamaUsahaRespone> response) {
                if (response.isSuccessful()) {
                    NamaUsahaRespone res = response.body();
                    if (res.isSuccess()) {
                        //edNamaUsaha.setText(res.getNamaUsaha());
                        getSupportActionBar().setTitle("FREEpay - " + res.getNamaUsaha());
                        Log.d("getNamausaha", "Berhasil");
                    } else {

                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<NamaUsahaRespone> call, Throwable t) {
                prd.dismiss();
                log(t.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_menu, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final Intent intent;
        switch (item.getItemId()) {
            case R.id.m_setting:
                //setContentView(R.layout.setting_format);
                intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);

                return true;
            case R.id.item2:

                getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                        .edit()
                        .putBoolean("isFirstRun", true)
                        .apply();

                try {
                    FirebaseAuth.getInstance().signOut();
                    topic = "/topics/" + myID;
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
                    Log.e("topic ", topic);
                    topic = "/topics/free_cell";
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
                    Log.e("topic ", topic);
                } catch (Exception e) {

                }
                setSession("OUT", myNumber);

                FirebaseAuth.getInstance().signOut();

                intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.m_wa:
//                Uri uri = Uri.parse("smsto:" + "85222396286");
//                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
//                i.putExtra("sms_body", "zz");
//                i.setPackage("com.whatsapp");
//                startActivity(i);
                Intent i = new Intent(MainActivity.this, ChatActivity.class);
                startActivity(i);

            default:
                return super.onOptionsItemSelected(item);

            case R.id.m_settingPrinter:
                intent = new Intent(MainActivity.this, PrinterSettingActivity.class);
                startActivity(intent);
                return true;
        }


    }

    public boolean runtime_permissions() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, Manifest.permission
                .READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 100);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // enable_button();
                StringBuilder sb = new StringBuilder();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, 1);
                //     TelephonyManager telMan = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                //     sb.append(telMan.getDeviceId());
                //     myImei = sb.toString();

            } else {
                runtime_permissions();
            }

        }
    }


//    @OnClick(R.id.bt_news) public void onOpenNews(){
//
//        SharedPreferences prefs = getSharedPreferences("MY_NEWS", MODE_PRIVATE);
//        //news.setText(prefs.getString("MY_NEWS1", ""));
//
//        //getBalsan2();
//    }

    @OnClick(R.id.bt_refreshsaldo)
    public void onRefreshSaldo() {
        btRefreshSaldo.setVisibility(View.GONE);
        loadingAnim.setVisibility(View.VISIBLE);
        prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        String formatCekSaldo = prefs.getString("MY_FORMAT_CEKSALDO", "");
        sendReqSal(formatCekSaldo);

    }

//    @OnClick(R.id.tv_Help) public void onHelp(){
//        Intent i = new Intent(MainActivity.this,ChatActivity.class);
//        startActivity(i);
//    }


    @OnClick(R.id.im_product)
    public void onProduct() {
//        Intent i = new Intent(MainActivity.this, ChatActivity.class);
        Intent i = new Intent(MainActivity.this, ProdukActivity.class);
        startActivity(i);



    }

    @OnClick(R.id.im_profile)
    public void onProfile() {
        Intent i = new Intent(MainActivity.this, ChatActivity.class);
        i.putExtra("getCall", "Profile");
        startActivity(i);

    }

//    @OnClick(R.id.bt_isi_ulang)
//    public void onBtIsiUlangKlik(){
//       // KlikMenuIsiUlang = 1;
//       // sendReqSal("databrg 1234 simpati");
//
//        Intent intent = new Intent(MainActivity.this, pilihProviderActivity.class);
//        intent.putExtra("pilihan","pulsa");
//        startActivity(intent);
//
//    }

//    @OnClick(R.id.bt_paketData)
//    public void onData(){
//        // KlikMenuIsiUlang = 1;
//        // sendReqSal("databrg 1234 simpati");
//
//        Intent intent = new Intent(MainActivity.this, pilihProviderActivity.class);
//        intent.putExtra("pilihan","data");
//        startActivity(intent);
//
//    }

//    @OnClick(R.id.bt_MenukirimSaldo) public void onKirSal(){
//        Intent intent = new Intent(MainActivity.this, kirimSaldo_Activity.class);
//        startActivity(intent);
//    }

    /*
    @OnClick(R.id.bt_join) public void onJoin(){
        Intent intent = new Intent(MainActivity.this, join_actity.class);
        startActivity(intent);
    }*/

    /*
    @OnClick(R.id.bt_info_akun) public void onInfoAkun(){
        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
    */

//    @OnClick(R.id.bt_token) public void onToken(){
//
//        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
//        pin = prefs.getString("MY_PIN_TRX", "");
//        Server = prefs.getString("MY_SERVER", "");
//
//        pilihtoken = 1;
//        format = "databrg "+pin+" pln";
//        sendReqSal(format);
//
//        prd = new ProgressDialog(this);
//        prd.setMessage("Mohon Tunggu. . .");
//        prd.setCancelable(false);
//        prd.setCanceledOnTouchOutside(false);
//
//        prd.show();
//
//
//    }
//
//    @OnClick(R.id.bt_eMoney) public  void onEmoney(){
//        Intent intent = new Intent(MainActivity.this, MultiTrxActivity.class);
//        startActivity(intent);
//    }

    //    @OnClick(R.id.bt_msgr) public void onMsgr(){
//        Intent intent = new Intent(MainActivity.this, SimulasiTrxActivity.class);
//        startActivity(intent);
//    }
//
    @OnClick(R.id.im_history)
    public void onHistoriTrx() {
//        Intent intent = new Intent(MainActivity.this, HistoriTrx_Activity.class);
        Intent intent = new Intent(MainActivity.this, PilihTanggalHistoryActivity.class);
        startActivity(intent);
    }
    /*
    @OnClick(R.id.bt_internet) public void onInternet(){
        Intent intent = new Intent(MainActivity.this, UnderConstructionActivity.class);
        startActivity(intent);
    }*/


//    @OnClick(R.id.bt_cek_harga) public void onCekHarga(){
//        Intent intent = new Intent(MainActivity.this, listProviderActivity.class);
//        startActivity(intent);
//    }


//    @OnClick(R.id.bt_histori_mutasi) public void onHistoriMutasi(){
//        Intent intent = new Intent(MainActivity.this, UnderConstructionActivity.class);
//        startActivity(intent);
//    }

    /*
    @OnClick(R.id.bt_list_downline) public void onListDownline(){
        Intent intent = new Intent(MainActivity.this, UnderConstructionActivity.class);
        startActivity(intent);
    }
    */
     /*
    @OnClick(R.id.bt_trx_downline) public void onTrxDonwline(){
        Intent intent = new Intent(MainActivity.this, kaitkanActivity.class);
        startActivity(intent);
    }*/

//    @OnClick(R.id.bt_gojek) void onGojek(){
//        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
//        pin = prefs.getString("MY_PIN_TRX", "");
//        Server = prefs.getString("MY_SERVER", "");
//        format = "databrg "+pin+" GOJEK";
//
//        gojek = 1;
//        sendReqSal(format);
//
//        prd = new ProgressDialog(this);
//        prd.setMessage("Mohon Tunggu. . .");
//        prd.setCancelable(false);
//        prd.setCanceledOnTouchOutside(false);
//
//        prd.show();
//    }
//
//    @OnClick(R.id.bt_grab) void onGrab(){
//        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
//        pin = prefs.getString("MY_PIN_TRX", "");
//        Server = prefs.getString("MY_SERVER", "");
//        format = "databrg "+pin+" GRB";
//        grab = 1;
//        sendReqSal(format);
//
//        prd = new ProgressDialog(this);
//        prd.setMessage("Mohon Tunggu. . .");
//        prd.setCancelable(false);
//        prd.setCanceledOnTouchOutside(false);
//
//        prd.show();
//
//    }
//
//    @OnClick(R.id.bt_ovo) void onOvo(){
//        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
//        pin = prefs.getString("MY_PIN_TRX", "");
//        Server = prefs.getString("MY_SERVER", "");
//        format = "databrg "+pin+" OVO";
//        ovo = 1;
//        sendReqSal(format);
//
//        prd = new ProgressDialog(this);
//        prd.setMessage("Mohon Tunggu. . .");
//        prd.setCancelable(false);
//        prd.setCanceledOnTouchOutside(false);
//
//        prd.show();
//    }
//
//    @OnClick(R.id.bt_etoll) void onEtoll(){
//        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
//        pin = prefs.getString("MY_PIN_TRX", "");
//        Server = prefs.getString("MY_SERVER", "");
//        format = "databrg "+pin+" ETOL";
//        etoll = 1;
//        sendReqSal(format);
//
//        prd = new ProgressDialog(this);
//        prd.setMessage("Mohon Tunggu. . .");
//        prd.setCancelable(false);
//        prd.setCanceledOnTouchOutside(false);
//
//        prd.show();
//    }

    @OnClick(R.id.bt_tmbh_saldo)
    void onNext() {
        //Toast.makeText(MainActivity.this, myImei, Toast.LENGTH_LONG).show();
        klikTambahSaldo = 1;
        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        String formatCekSaldo = prefs.getString("MY_FORMAT_CEKSALDO", "");
        sendReqSal(formatCekSaldo);
        //count = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT",""));
        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
    }


    public void sendReqSal(String format) {
        settingFormat sf = new settingFormat();


        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;

        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx, Server, "nonotif,JSON");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        //Toast.makeText(MainActivity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());


                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();

                        getBalsan();

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(MainActivity.this, "Gagal Cek Koneksi Internet", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {
                log(t.toString());
                prd.dismiss();
            }
        });
    }

    public void getBalsan() {

        Call<MsgResponse> call = getApi().GetMSg(myID, idtrx, Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {

                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {

                        prefs = getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt("NOTIF", 0);
                        editor.apply();


                        if (pilihtoken == 1) {
                            prd.dismiss();
                            Intent intent = new Intent(MainActivity.this, trxActivity.class);
                            intent.putExtra("CekSal2", ges.getPesan());
                            intent.putExtra("judul", "Transaksi Token PLN");
                            intent.putExtra("gambarProvider", "pln");
                            pilihtoken = 0;


                            startActivity(intent);

                        } else if (KlikMenuIsiUlang == 1) {
                            KlikMenuIsiUlang = 0;

                            Intent i = new Intent(MainActivity.this, trxActivity.class);
                            i.putExtra("CekSal2", ges.getPesan());
                            startActivity(i);
                        } else if (klikTambahSaldo == 1) {

                            try {


                                Gson g = new Gson();
                                Saldo respSaldo = g.fromJson(ges.getPesan().toString(), Saldo.class);
                                Integer saldo = Math.abs(Integer.parseInt(respSaldo.getSaldo()));
                                klikTambahSaldo = 0;
                                prd.dismiss();
                                Intent intent = new Intent(MainActivity.this, TambahSaldoActivity.class);
                                intent.putExtra("CekSal", "Saldo anda Rp. "+currencyFormatter(saldo+""));
                                // Notif(ges.getPesan());
                                startActivity(intent);

                            } catch (Exception e) {
                                e.printStackTrace();
                                klikTambahSaldo = 0;
                                prd.dismiss();
                                Intent intent = new Intent(MainActivity.this, TambahSaldoActivity.class);
                                intent.putExtra("CekSal", ges.getPesan());
                                // Notif(ges.getPesan());
                                startActivity(intent);
                            }


                        } else if (grab == 1) {
                            prd.dismiss();
                            Intent intent = new Intent(MainActivity.this, trxActivity.class);
                            intent.putExtra("CekSal2", ges.getPesan());
                            intent.putExtra("judul", "Transaksi Saldo Grab");
                            intent.putExtra("gambarProvider", "grab");
                            grab = 0;
                            startActivity(intent);
                        } else if (gojek == 1) {
                            prd.dismiss();
                            Intent intent = new Intent(MainActivity.this, trxActivity.class);
                            intent.putExtra("CekSal2", ges.getPesan());
                            intent.putExtra("judul", "Transaksi Saldo Gojek");
                            intent.putExtra("gambarProvider", "gojek");
                            gojek = 0;
                            startActivity(intent);
                        } else if (ovo == 1) {
                            prd.dismiss();
                            Intent intent = new Intent(MainActivity.this, trxActivity.class);
                            intent.putExtra("CekSal2", ges.getPesan());
                            intent.putExtra("judul", "Transaksi Saldo OVO");
                            intent.putExtra("gambarProvider", "ovo");
                            ovo = 0;
                            startActivity(intent);
                        } else if (etoll == 1) {
                            prd.dismiss();
                            Intent intent = new Intent(MainActivity.this, trxActivity.class);
                            intent.putExtra("CekSal2", ges.getPesan());
                            intent.putExtra("judul", "Transaksi Saldo E-Toll");
                            intent.putExtra("gambarProvider", "etoll");
                            etoll = 0;
                            startActivity(intent);
                        } else if (fromListProduk == 1) {
                            prd.dismiss();
                            Intent intent = new Intent(MainActivity.this, trxActivity.class);
                            intent.putExtra("CekSal2", ges.getPesan());
                            intent.putExtra("judul", "Transaksi Saldo " + SelectedProduk);
                            intent.putExtra("gambarProvider", "fromListProduk");
                            intent.putExtra("logoUrl", SelectedLogoUrl);
                            intent.putExtra("hint", SelectedHint);

                            fromListProduk = 0;
                            startActivity(intent);
                        } else {
                            String saldo = "0";
                            try {
                                Gson g = new Gson();
                                Saldo respSaldo = g.fromJson(ges.getPesan().toString(), Saldo.class);
                                saldo = respSaldo.getSaldo();
                                currencyFormatter(saldo,tvJumSaldo);
//                                tvJumSaldo.setText("Rp. " + saldo);
                                btRefreshSaldo.setVisibility(View.VISIBLE);
                                loadingAnim.setVisibility(View.GONE);
                            } catch (Exception e) {
                                e.printStackTrace();
                                saldo = ges.getPesan().substring(ges.getPesan().indexOf("Rp ") + 2);
                                saldo = saldo.substring(0, saldo.indexOf(" pada"));
                                tvJumSaldo.setText("Rp. " + saldo);
                                btRefreshSaldo.setVisibility(View.VISIBLE);
                                loadingAnim.setVisibility(View.GONE);

                            }
                        }
                        //Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_LONG).show();


                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        Handler handler = new Handler();
                         handler.postDelayed(new Runnable() {
                         public void run() {
                         // yourMethod();
                         }
                         }, 5000);   //5 seconds


                        //finish();
                        //tvSaldo.setText(ges.getPesan());

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();

                        log("Gagal");
                        if (CekCounting < count) {
                            getBalsan();
                            CekCounting = CekCounting + 1;
                        } else {
//                                prd.dismiss();
                            btRefreshSaldo.setVisibility(View.VISIBLE);
                            loadingAnim.setVisibility(View.GONE);
                            Notif("Time Out / Tidak ada balasan masuk");
                            Toast.makeText(MainActivity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {
                log(t.toString());
                prd.dismiss();
            }
        });
    }

    public void Notif(String notifBody) {
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker("This is the ticker");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("TVEF");
        notification.setContentText(notifBody);

        Context context = getApplicationContext();
        Intent intent = new Intent(context, BalasanActivity.class);
        intent.putExtra("CekSal", notifBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (broadcastReceiver == null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    //  tv.append("\n"+intent.getExtras().get("coordinates"));
                    //tv.append("\n"+intent.getExtras().get("coordinates"));
                    //SendUrl= (String) intent.getExtras().get("coordinates");
                    //SendUrl = SendUrl.replace(" ","%20");
                    // exeUrl(SendUrl);
                    SharedPreferences prefs = getSharedPreferences("MY_NEWS", MODE_PRIVATE);
                    isi = prefs.getString("MY_NEWS1", "");
                    try {
                        //news.setText(isi);
                    } catch (Exception e) {
                        e.printStackTrace();
                        isi = "Belum ada berita";
                    }

                }
            };
            registerReceiver(broadcastReceiver, new IntentFilter("NewsUpdate"));
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    public void setSession(String status, String notelp) {

        Call<SetSessionRespone> call = getApi().SetSession(notelp, status, "curdate()", "curtime()");
        call.enqueue(new Callback<SetSessionRespone>() {
            @Override
            public void onResponse(Call<SetSessionRespone> call, Response<SetSessionRespone> response) {
                if (response.isSuccessful()) {
                    SetSessionRespone res = response.body();
                    if (res.IsSuccess()) {

                        //finish();
                        log("Berhasil");
                        //SAMPE SINI
                    } else {
                        //Toast.makeText(session.this, "Anda telah terdaftar sebelumnya hubungi admin untuk daftar ulang", Toast.LENGTH_LONG).show();
                        //prd.dismiss();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SetSessionRespone> call, Throwable t) {
                log(t.toString());
                prd.dismiss();
            }
        });
    }


    public void cekNews() {
        int x = 0;
        while (x == 0) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    // yourMethod();
                }
            }, 5000);
            SharedPreferences prefs = getSharedPreferences("MY_NEWS", MODE_PRIVATE);
            //news.setText(prefs.getString("MY_NEWS1", ""));

        }
    }

    public void resetCounter(){
        Date c = Calendar.getInstance().getTime();
//        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);

        String yesterday = getSharedPreferences(Constant.INSTANCE.getPREFERENCES_NAME_IS_YESTERDAY(), MODE_PRIVATE)
                .getString(Constant.INSTANCE.getPREFERENCES_DATE_RESET_COUTER_TRX_ULANG(),"-");

        if (!formattedDate.equals(yesterday)){
            getSharedPreferences(Constant.INSTANCE.getPREFERENCES_NAME_IS_YESTERDAY(), MODE_PRIVATE)
                    .edit()
                    .putString(Constant.INSTANCE.getPREFERENCES_DATE_RESET_COUTER_TRX_ULANG(),formattedDate)
                    .apply();

            getSharedPreferences(Constant.INSTANCE.getPREFERENCES_TRX_ULANG(),MODE_PRIVATE)
                    .edit()
                    .clear()
                    .commit();
        }
    }

    public void cekVersi(){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("dbname","tekniksi_dbglobal");
        jsonObject.addProperty("sqltext","select * from tdaftaraplikasi where nama_app = 'TVEF_ANDROID'");
        jsonObject.addProperty("user","admin");
        jsonObject.addProperty("pass","admin1234");
        CekVersionRetrofit cekVersionRetrofit = new CekVersionRetrofit();
        cekVersionRetrofit.CekVersionRetrofit();
        Call<ArrayVersionValidation> call =  cekVersionRetrofit.getApi().ExecuteQueryCheckVersi(jsonObject);
        call.enqueue(new Callback<ArrayVersionValidation>() {
            @Override
            public void onResponse(Call<ArrayVersionValidation> call, Response<ArrayVersionValidation> response) {
                VersionValidationResponse data = response.body().getData().get(0);
                Log.d(TAG, "onResponse: "+data.getVersi());
                if (!getString(R.string.app_version).equals(data.getVersi())){
                    Intent i = new Intent(MainActivity.this, VersionValidationActivity.class);
                    i.putExtra("info_aktif", data.getInfo_aktif());
                    i.putExtra("link", data.getLink());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();

                }
            }

            @Override
            public void onFailure(Call<ArrayVersionValidation> call, Throwable t) {

            }
        });

    }

    public void GetListProduct(){

        SpringBaseActivity springBaseActivity = new SpringBaseActivity();
        Call<List<ProductBanner>> call =  springBaseActivity.getApi().getProductCategory();
        call.enqueue(new Callback<List<ProductBanner>>() {
            @Override
            public void onResponse(Call<List<ProductBanner>> call, Response<List<ProductBanner>> response) {
                if(response.code() == 200) {
                    List<ProductBanner> listProductBanner = response.body();
                    List<String> listKodeProduk = new ArrayList<String>();
                    List<String> listShortDesc = new ArrayList<String>();
                    List<String> listBanner = new ArrayList<String>();

                    for (int i = 0; i < listProductBanner.size()-1; i++) {
                        listKodeProduk.add(listProductBanner.get(i).getProduct_code());
                        listShortDesc.add(listProductBanner.get(i).getProduct_name());
                        listBanner.add(listProductBanner.get(i).getBanner());
                    }

                    Intent i = new Intent(MainActivity.this, ProdukActivity.class);
                    i.putExtra("listKodeProduk", (ArrayList<String>) listKodeProduk);
                    i.putExtra("listShortDesc", (ArrayList<String>) listShortDesc);
                    i.putExtra("listBanner", (ArrayList<String>) listBanner);
                    startActivity(i);
                } else {
                    Toast.makeText(getApplicationContext(),"Error Code : "+response.code(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<ProductBanner>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Failure",Toast.LENGTH_SHORT).show();
            }
        });

    }

    String saldoDummy = "{\n" +
            "   \"saldo\":\"-1000000\",\n" +
            "   \"nama\":\"HANIF\",\n" +
            "   \"kdplgnmaster\":\"1582365754\",\n" +
            "   \"kdplgn\":\"AND6281313167225\",\n" +
            "   \"hp\":\"AND6281313167225\",\n" +
            "   \"tglanggota\":\"22/02/2020\",\n" +
            "   \"tglpoint\":\"\",\n" +
            "   \"statusharga\":\"6285316619707\",\n" +
            "   \"maxpiutang\":\"0\",\n" +
            "   \"statusplgn\":\"SD02\",\n" +
            "   \"uperkodeplgn\":\"1\",\n" +
            "   \"kodeplgn_induk\":\"AND6281313167225\",\n" +
            "   \"namaupline\":\"\",\n" +
            "   \"bankrekening\":\"\",\n" +
            "   \"banknama\":\"\",\n" +
            "   \"bank_an\":\"\",\n" +
            "   \"blok\":\"False\",\n" +
            "   \"masa_aktif\":\"\",\n" +
            "   \"kodepos\":\"0\",\n" +
            "   \"laba\":\"0\",\n" +
            "   \"pointawal\":\"0\",\n" +
            "   \"komisi\":\"0\",\n" +
            "   \"idutama\":\"1582365754\",\n" +
            "   \"NamaUpline_pelanggan\":\"\",\n" +
            "   \"cekpoint\":\"0\",\n" +
            "   \"pengumuman\":\"0\"\n" +
            "}";




}


