package com.alkamal.hanif.mobile.form;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.adapter.BarangAdapter;
import com.alkamal.hanif.mobile.model.barangresponse.Data;
import com.alkamal.hanif.mobile.model.barangresponse.ListBarang;
import com.alkamal.hanif.mobile.utils.Constant;
import com.alkamal.hanif.mobile.utils.SpringBaseActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 02/02/2018.
 */

public class trxActivity extends BaseActivity{
    private ArrayList<String> array_kodebrg = new ArrayList<String>() ;
    private ArrayList<String> array_kodebrgApply = new ArrayList<String>() ;
    private ArrayList<String> array_keterangan = new ArrayList<String>() ;
    private ArrayList<String> array_harga = new ArrayList<String>() ;
    private ArrayList<String> array_kodebrgApplyTerurut = new ArrayList<String>() ;
    private ArrayList<String> array_keteranganTerurut = new ArrayList<String>() ;
    private ArrayList<String> array_hargaTerurut = new ArrayList<String>() ;
    private ArrayList<String> array_ketHarga = new ArrayList<String>() ;
    Spinner spKodeBrg;
    String myID,Server;
    Calendar calendar;
    SimpleDateFormat dateformat;
    String tanggal,idtrx,pin,noTlp,kodeBrg;
    NotificationCompat.Builder notification;
    private static final int uniqueID = 45612;
    SharedPreferences prefs;
    int CekCounting;
    //@BindView(R.id.ed_pin)
    //EditText edPin;
    @BindView(R.id.ed_noTelp) EditText edNoTlp;
    @BindView(R.id.sp_kodeBrg) Spinner spKode;
    @BindView(R.id.tv_cobacoba)
    TextView coba;
    @BindView(R.id.ed_pin) EditText edPin;

    @BindView(R.id.bt_provider)
    ImageButton provider;

    @BindView(R.id.bt_provider2)
    ImageButton provider2;

    @BindView(R.id.im_logoProduk)
    ImageView imLogoProduk;

    @BindView(R.id.bt_trxKirim) Button btTrxKirim;

    @BindView(R.id.lb_trx_ulang) TextView lbTrxUlang;

    @BindView(R.id.relative_layout_img)
    RelativeLayout rl_image;

    @BindView(R.id.relative_layout_tunggu_trx) RelativeLayout rl_tunggu_trx;
    @BindView(R.id.linear_layout_form_isi_pulsa)
    LinearLayout ll_form_isi_pulsa;
    @BindView(R.id.tv_pilihproduk) TextView textPilihProduk;


    //@BindView(R.id.tv_judul) TextView TvJudul;
    Intent in ;
    int waktutimeout,data,JumArray;
    ProgressDialog prd;
    String gambar1,gambar2 = "";
    Boolean isTrxUlang = false;
    private static final String TAG = "trxActivity";

    private BalasanActivity csa;

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.rightmenu_trx, menu);
//        return super.onCreateOptionsMenu(menu);
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        final Intent intent;
//        switch (item.getItemId()){
//            case R.id.m_ulang:
//                noTlp = edNoTlp.getText().toString();
//
//                int a = 0;
//                while(a < JumArray){
//                    if (spKode.getSelectedItem().toString().matches(array_keterangan.get(a))){
//                        kodeBrg = array_kodebrgApply.get(a);
//                    }
//                    a = a+1;
//                }
//
//                String formatTrx = kodeBrg+" "+noTlp+" "+pin;
//
//                intent = new Intent(trxActivity.this, trxUlangActivity.class);
//                intent.putExtra("format",formatTrx.toString());
//                startActivity(intent);
//
//                return true;
//
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//
//    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trx_layout);
        ButterKnife.bind(this);



        //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
        myID = prefs.getString("MY_USERID","");
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);

       // array_kodebrg.add("SA5");
       // array_kodebrg.add("SA10");
       // array_kodebrg.add("SA2x");
        in = getIntent();

        String isTrxIsiPulsa = "";
        try {
            isTrxIsiPulsa = in.getStringExtra(Constant.INSTANCE.getBUNDLE_TRX_ISI_PULSA());

            if (isTrxIsiPulsa.equals("true")){
                lbTrxUlang.setVisibility(View.VISIBLE);
            } else {
                lbTrxUlang.setVisibility(View.GONE);
            }
        }catch (Exception E){
            E.printStackTrace();

        }

        setHint();

        if (!in.hasExtra("FromProductDetil")) {

            edNoTlp.setText(in.getStringExtra("notelp"));
            String judul = in.getStringExtra("judul");
            try {

                gambar1 = in.getStringExtra("gambarProvider");
                gambar2 = in.getStringExtra("gambarProvider2");
                provider.setImageResource(R.drawable.logolauncher);
//                if (gambar1.equals("telkomsel")) {
//                    provider.setImageResource(R.drawable.telkomsel);
//                    progressBar.setVisibility(View.GONE);
//                } else if (gambar1.equals("three")) {
//                    provider.setImageResource(R.drawable.three);
//                    progressBar.setVisibility(View.GONE);
//                } else if (gambar1.equals("xl")) {
//                    provider.setImageResource(R.drawable.xl);
//                    progressBar.setVisibility(View.GONE);
//                } else if (gambar1.equals("axis")) {
//                    provider.setImageResource(R.drawable.axis);
//                    progressBar.setVisibility(View.GONE);
//                } else if (gambar1.equals("indosat")) {
//                    provider.setImageResource(R.drawable.indosat);
//                    progressBar.setVisibility(View.GONE);
//                } else if (gambar1.equals("smartfren")) {
//                    provider.setImageResource(R.drawable.smartfren);
//                    progressBar.setVisibility(View.GONE);
//                } else if (gambar1.equals("pln")) {
//                    provider.setImageResource(R.drawable.logopln);
//                    progressBar.setVisibility(View.GONE);
//                } else if (gambar1.equals("grab")) {
//                    provider.setImageResource(R.drawable.grab);
//                    progressBar.setVisibility(View.GONE);
//                } else if (gambar1.equals("gojek")) {
//                    provider.setImageResource(R.drawable.gojek);
//                    progressBar.setVisibility(View.GONE);
//                } else if (gambar1.equals("ovo")) {
//                    provider.setImageResource(R.drawable.ovo);
//                    progressBar.setVisibility(View.GONE);
//                } else if (gambar1.equals("etoll")) {
//                    provider.setImageResource(R.drawable.etoll);
//                    progressBar.setVisibility(View.GONE);
//                } else
                if (gambar1.equals("fromListProduk")) {
                    Log.d(TAG, "onCreate: MY LOGO PRODUK URL = " + in.getStringExtra("logoUrl"));
                    provider.setVisibility(View.GONE);
                    provider2.setVisibility(View.GONE);
                    imLogoProduk.setVisibility(View.VISIBLE);
                    Glide.with(this)
                            .load(in.getStringExtra("logoUrl"))
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    progressBar.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    progressBar.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(imLogoProduk);
                } else {
                    provider.setImageResource(R.drawable.logolauncher);
                }

                if (!gambar2.isEmpty()) {
                    if (gambar2.equals("axis")) {
                        provider.setImageResource(R.drawable.xl);
                        provider2.setImageResource(R.drawable.axis);
                    }
                }
            } catch (Exception e) {
                progressBar.setVisibility(View.GONE);
            }

            if (in.hasExtra("VPS")){
                if (in.hasExtra("selectedKodebrg")){
                    getBarangVps("","",in.getStringExtra("selectedKodebrg"));
                } else {
                    getBarangVps(edNoTlp.getText().toString(),in.getStringExtra("jenis"),"");
                }

            }  else {

                Gson g = new Gson();
                ListBarang listBarang = g.fromJson(in.getStringExtra("CekSal2"), ListBarang.class);
                List<Data> listData = listBarang.getData();


                for (int i = 0; i < listData.size(); i++) {
                    array_keterangan.add(listData.get(i).getNamabrg());
                    array_harga.add(listData.get(i).getHargajual());
                    array_kodebrgApply.add(listData.get(i).getKodebrg());
                }
//
                String temp;
                String temp2;
                String temp3;

                for (int i = 0; i < array_harga.size(); i++) {
                    for (int j = i; j > 0; j--) {
                        if (Integer.parseInt(array_harga.get(j)) < Integer.parseInt(array_harga.get(j - 1))) {
                            temp = array_harga.get(j);
                            array_harga.set(j, array_harga.get(j - 1));
                            array_harga.set(j - 1, temp);

                            temp2 = array_keterangan.get(j);
                            array_keterangan.set(j, array_keterangan.get(j - 1));
                            array_keterangan.set(j - 1, temp2);

                            temp3 = array_kodebrgApply.get(j);
                            array_kodebrgApply.set(j, array_kodebrgApply.get(j - 1));
                            array_kodebrgApply.set(j - 1, temp3);
                        }

                    }

                }

                JumArray = listData.size();
                //----
                SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
                waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
                pin = prefs.getString("MY_PIN_TRX", "");
                Server = prefs.getString("MY_SERVER", "");

                spKodeBrg = (Spinner) findViewById(R.id.sp_kodeBrg);
                ArrayAdapter adapter = new ArrayAdapter(this, R.layout.trx_spinner_item, array_keterangan);
                BarangAdapter brgAdapter = new BarangAdapter(this, array_keterangan, array_harga);
                spKodeBrg.setAdapter(brgAdapter);
            }
        } else {
            provider.setVisibility(View.GONE);
            provider2.setVisibility(View.GONE);
            imLogoProduk.setVisibility(View.GONE);
            spKode.setVisibility(View.GONE);
            textPilihProduk.setText(in.getStringExtra("FromProductDetil"));
            rl_image.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }

    }

    private void getBarangVps(String noTlp, String jenis, String kodeBrg){
        SpringBaseActivity springBaseActivity = new SpringBaseActivity();
        Call<List<Data>> call = springBaseActivity.getApi().getDataBarang(myID, noTlp, jenis, kodeBrg, "1234");
        call.enqueue(new Callback<List<Data>>() {
            @Override
            public void onResponse(Call<List<Data>> call, Response<List<Data>> response) {
                List<Data> listData = response.body();

                for (Data data : listData){
                    array_keterangan.add(data.getNamabrg());
                    array_harga.add(data.getHargajual1());
                    array_kodebrgApply.add(data.getKodebrg());
                }

                JumArray = listData.size();
                SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
                waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
                pin = prefs.getString("MY_PIN_TRX", "");
                Server = prefs.getString("MY_SERVER", "");

                spKodeBrg = (Spinner) findViewById(R.id.sp_kodeBrg);
                BarangAdapter brgAdapter = new BarangAdapter(getApplicationContext(), array_keterangan, array_harga);
                spKodeBrg.setAdapter(brgAdapter);
            }

            @Override
            public void onFailure(Call<List<Data>> call, Throwable t) {

            }
        });

    }

    void setHint(){

        if (in.hasExtra("hint")){
            Log.d(TAG, "setHint: "+in.getStringExtra("hint"));
            edNoTlp.setHint(in.getStringExtra("hint"));

        } else {
            Log.d(TAG, "setHint: FALSE");
        }

    }

    @OnClick(R.id.text_kembali) void onKembaliPending(){
        finish();
    }

    @OnClick(R.id.bt_cobacoba) public void onCoba(){


     /*  int a = 0;
        while(a < JumArray){
            if (spKode.getSelectedItem().toString().matches(array_keterangan.get(a))){
                coba.setText(array_kodebrgApply.get(a));
            }
            a = a+1;
        }*/
     Toast.makeText(trxActivity.this, Integer.toString(JumArray) , Toast.LENGTH_LONG).show();

    }

    public void setArray_kodebrg(ArrayList<String> array_kodebrg) {
        this.array_kodebrg = array_kodebrg;
    }

    @OnClick(R.id.lb_trx_ulang) public void onTrxUlang(){

        if (isTrxUlang){

            isTrxUlang = false;
            lbTrxUlang.setText("Klik disini untuk mengulang transaksi");

        } else {

            isTrxUlang = true;
            lbTrxUlang.setText("Klik disini untuk batal transaksi ulang !");
        }

    }

    @OnClick(R.id.bt_trxKirim) public void onSend(){
        try {
            //prd = new ProgressDialog(this);
            //prd.setMessage("Mohon Tunggu. . .");
            //prd.setCancelable(false);
            //prd.setCanceledOnTouchOutside(false);

            //prd.show();
            if (!in.hasExtra("FromProductDetil")) {

                if (edPin.getText().length() < 1) {
                    Toast.makeText(this, "PIN tidak boleh kosong !", Toast.LENGTH_SHORT).show();
                    throw new Exception("PIN tidak boleh kosong");
                } else if (edNoTlp.getText().length() < 1) {
                    Toast.makeText(this, "Nomor telepon tidak boleh kosong !", Toast.LENGTH_SHORT).show();
                    throw new Exception("Nomor telepon tidak boleh kosong !");
                } else if (edPin.getText().toString().equals(pin + "")) {

                    rl_image.setVisibility(View.GONE);
                    ll_form_isi_pulsa.setVisibility(View.GONE);
                    rl_tunggu_trx.setVisibility(View.VISIBLE);

                    int a = 0;
                    Log.e("jumArray", String.valueOf(JumArray));
//            Log.e("TEEST", array_keterangan.get(22123).toString());
                    while (a < JumArray) {
                        try {
                            Log.e("spkode", spKode.getSelectedItem().toString());
                            Log.e("array ket", array_keterangan.get(a));
                            if (spKode.getSelectedItem().toString().equals(array_keterangan.get(a))) {
                                kodeBrg = array_kodebrgApply.get(a);
                                Log.e("array kodebrg", kodeBrg);
                                a = JumArray - 1;
                            }
                            a = a + 1;
                        } catch (Exception E) {
                            E.printStackTrace();
                        }
                    }
                } else {
                    if (edPin.getText().length() < 1) {
                        Toast.makeText(this, "PIN tidak boleh kosong !", Toast.LENGTH_SHORT).show();
                        throw new Exception("PIN tidak boleh kosong");
                    } else if (edNoTlp.getText().length() < 1) {
                        Toast.makeText(this, "Nomor telepon tidak boleh kosong !", Toast.LENGTH_SHORT).show();
                        throw new Exception("Nomor telepon tidak boleh kosong !");
                    } else {
                        kodeBrg = in.getStringExtra("FromProductDetil");//Kode Product
                    }
                }

                noTlp = edNoTlp.getText().toString();
                String formatTrx = "";
                if (!isTrxUlang) {
                    formatTrx = kodeBrg + " " + noTlp + " " + pin;
                } else {

                    String ulangCounter = getSharedPreferences(Constant.INSTANCE.getPREFERENCES_TRX_ULANG(), MODE_PRIVATE)
                            .getString(edNoTlp.getText().toString(), "1");

                    formatTrx = kodeBrg + " " + noTlp + " " + pin + " u" + ulangCounter;

                    int counter = Integer.parseInt(ulangCounter) + 1;
                    getSharedPreferences(Constant.INSTANCE.getPREFERENCES_TRX_ULANG(), MODE_PRIVATE)
                            .edit()
                            .putString(edNoTlp.getText().toString(), counter + "")
                            .apply();
                }

                Log.e("Format TRX : ", formatTrx);
                //SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
                //int waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
                coba.setText(formatTrx);
                if (noTlp.isEmpty()) {
                    Toast.makeText(trxActivity.this, "Nomor Telepon masih kosong !!!", Toast.LENGTH_LONG).show();

                } else {
                    sendReqSal(formatTrx);
                    btTrxKirim.setEnabled(false);
                }
            } else {
                //Toast.makeText(this, pin, Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "PIN anda salah", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public void sendReqSal(String format){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;


        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx,Server,"");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(trxActivity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();

                        getBalsan();

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(trxActivity.this, "Transaksi Gagal Cek Koneksi", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void getBalsan(){
        Call<MsgResponse> call = getApi().GetMSg(myID,idtrx,Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {

                        prefs=getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("LAST_ID",idtrx);
                        editor.putInt("NOTIF", 1);
                        editor.apply();
                        //Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_LONG).show();
                       // prd.dismiss();
                        Intent intent = new Intent(getApplicationContext(), BalasanActivity.class);
                        intent.putExtra("CekSal",ges.getPesan());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                         Handler handler = new Handler();
                         handler.postDelayed(new Runnable() {
                         public void run() {
                         // yourMethod();
                         }
                         }, 1000);   //5 seconds*/

                        //Notif(ges.getPesan());
                        startActivity(intent);

                        finish();
                        //tvSaldo.setText(ges.getPesan());

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < waktutimeout){
                            getBalsan();
                            CekCounting = CekCounting+1;
                        }else{
//                            prd.dismiss();
                            finish();
                            Notif("Cek Koneksi Internet");
//                            Toast.makeText(trxActivity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }

    public void Notif(String notifBody){
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker("This is the ticker");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("TVEF");
        notification.setContentText(notifBody);

        Context context = getApplicationContext();
        Intent intent = new Intent(context, BalasanActivity.class);
        intent.putExtra("CekSal",notifBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());

    }
    private final int PICK_CONTACT = 1;
    @OnClick(R.id.bt_Contacts) public void onContacts(){
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_CONTACT) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                Uri ContactData = data.getData();
                Cursor C = getContentResolver().query(ContactData, null, null, null, null);

                if (C.moveToFirst()) {
                    String name = C.getString(C.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
                    ContentResolver cr = getContentResolver();
                    Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                            "DISPLAY_NAME = '" + name + "'", null, null);
                    if (cursor.moveToFirst()) {
                        String contactId =
                                cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (phones.moveToNext()) {
                            String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                            switch (type) {
                                case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                    // do something with the Mobile number here...
                                    edNoTlp.setText(number);
                                    break;
                            }
                        }

                    }
                }
            }
        }


    }

    String dummySimpati = "{\"Data\":[{\"kodebrg\":\"SA5\",\"namabrg\":\"SIMPATI 5.000\",\"kdspek\":\"SIMPATI\",\"nominalpulsa\":\"5000\",\"hargajual\":\"6150\"},{\"kodebrg\":\"SA10\",\"namabrg\":\"SIMPATI 10.000\",\"kdspek\":\"SIMPATI\",\"nominalpulsa\":\"10000\",\"hargajual\":\"11150\"},{\"kodebrg\":\"SA20\",\"namabrg\":\"SIMPATI 20.000\",\"kdspek\":\"SIMPATI\",\"nominalpulsa\":\"20000\",\"hargajual\":\"21000\"},{\"kodebrg\":\"SA25\",\"namabrg\":\"SIMPATI 25.000\",\"kdspek\":\"SIMPATI\",\"nominalpulsa\":\"25000\",\"hargajual\":\"25950\"},{\"kodebrg\":\"SA50\",\"namabrg\":\"SIMPATI 50.000\",\"kdspek\":\"SIMPATI\",\"nominalpulsa\":\"50000\",\"hargajual\":\"50600\"},{\"kodebrg\":\"SA100\",\"namabrg\":\"SIMPATI 100.000\",\"kdspek\":\"SIMPATI\",\"nominalpulsa\":\"100000\",\"hargajual\":\"99700\"},{},{}]}";
    String dummyBlsnBerhasil = "{\"idtrx\":\"20032020003436\",\"namaserver\":\"free_cell\",\"tag\":\"10\",\"tanggal\":\"20/03/2020\",\"pukulsms_t\":\"00:34:36\",\"pesan\":\"i,1234,081313167225,S5 U1\",\"status\":\"BERHASIL\",\"namaplgn\":\"HANIF\",\"kodebrg\":\"S5\",\"dari_port\":\"ANDRO01\",\"pesan_sms\":\"S5 6281313167225 1234 u1\",\"notelp2trx\":\"AND6281313167225\",\"ktkunci3\":\"081313167225\",\"ktkunci4\":\"S5\",\"namaplgn\":\"HANIF\",\"notelp_upline\":\"1\",\"notujuan\":\"081313167225\",\"kodebrg2\":\"S5\",\"saldo\":\"-8815\",\"nama\":\"HANIF\",\"kdplgnmaster\":\"1555773488\",\"kdplgn\":\"AND6281313167225\",\"hp\":\"AND6281313167225\",\"tglanggota\":\"20/04/2019\",\"tglpoint\":\"\",\"statusharga\":\"2\",\"maxpiutang\":\"0\",\"statusplgn\":\"MASTER\",\"uperkodeplgn\":\"1\",\"kodeplgn_induk\":\"AND6281313167225\",\"namaupline\":\"\",\"bankrekening\":\"\",\"banknama\":\"\",\"bank_an\":\"\",\"blok\":\"n\",\"masa_aktif\":\"\",\"kodepos\":\"0\",\"laba\":\"0\",\"pointawal\":\"0\",\"komisi\":\"0\",\"idutama\":\"1555773488\",\"NamaUpline_pelanggan\":\"FREEKOMUNIKA\",\"cekpoint\":\"0\",\"pengumuman\":\"0\"}";
    String dummyBlsnGagal = "{\"idtrx\":\"20032020004436\",\"namaserver\":\"free_cell\",\"tag\":\"10\",\"tanggal\":\"20/03/2020\",\"pukulsms_t\":\"00:44:36\",\"pesan\":\"i,1234,081300000009,S5 \",\"status\":\"Gagal\",\"namaplgn\":\"HANIF\",\"kodebrg\":\"S5\",\"dari_port\":\"ANDRO01\",\"pesan_sms\":\"S5 6281300000009 1234\",\"notelp2trx\":\"AND6281313167225\",\"ktkunci3\":\"081300000009\",\"ktkunci4\":\"S5\",\"namaplgn\":\"HANIF\",\"notelp_upline\":\"1\",\"notujuan\":\"081300000009\",\"kodebrg2\":\"S5\",\"saldo\":\"-8815\",\"nama\":\"HANIF\",\"kdplgnmaster\":\"1555773488\",\"kdplgn\":\"AND6281313167225\",\"hp\":\"AND6281313167225\",\"tglanggota\":\"20/04/2019\",\"tglpoint\":\"\",\"statusharga\":\"2\",\"maxpiutang\":\"0\",\"statusplgn\":\"MASTER\",\"uperkodeplgn\":\"1\",\"kodeplgn_induk\":\"AND6281313167225\",\"namaupline\":\"\",\"bankrekening\":\"\",\"banknama\":\"\",\"bank_an\":\"\",\"blok\":\"n\",\"masa_aktif\":\"\",\"kodepos\":\"0\",\"laba\":\"0\",\"pointawal\":\"0\",\"komisi\":\"0\",\"idutama\":\"1555773488\",\"NamaUpline_pelanggan\":\"FREEKOMUNIKA\",\"cekpoint\":\"0\",\"pengumuman\":\"0\"}";

}
