package com.alkamal.hanif.mobile.form.Histori

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.BaseFragment
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.form.Transaksi.ReceiptActivity
import com.alkamal.hanif.mobile.model.HistoryResponse
import com.alkamal.hanif.mobile.utils.Constant
import com.alkamal.hanif.mobile.utils.filterEmptyData
import kotlinx.android.synthetic.main.activity_list_produk.*
import kotlinx.android.synthetic.main.fragment_histori_transaction.*
import kotlinx.android.synthetic.main.item_histori_pending.*
import kotlinx.android.synthetic.main.item_histori_pending.view.*
import kotlinx.android.synthetic.main.item_histori_pending.view.layout_item_trx
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
/**
 * A simple [Fragment] subclass.
 * Use the [HistoriTransactionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HistoriTransactionFragment : BaseFragment() {

    lateinit var listData: List<HistoryResponse>
    lateinit var baseActivity: BaseActivity
    lateinit var selectedButtonTgl: TextView
    internal var myCalendar = Calendar.getInstance()
    internal lateinit var date: DatePickerDialog.OnDateSetListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_histori_transaction, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setmContext(context)
        loadTransaction(true)

        date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateText(selectedButtonTgl)

            if (selectedButtonTgl == text_tanggal_sampai) {
                progressBar.visibility = View.VISIBLE
                loadTransaction(false)

            }
        }

        layout_button_tanggal_dari.setOnClickListener {
            selectedButtonTgl = text_tanggal_dari
            context?.let { it1 ->
                DatePickerDialog(
                        it1, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)
                ).show()
            }
        }

        layout_button_tanggal_sampai.setOnClickListener {
            if (text_tanggal_dari.text.equals("Tanggal")) {

                Toast.makeText(context, "Tanggal belum lengkap, mohon untuk memilih tanggal awal", Toast.LENGTH_SHORT).show()

            } else {
                selectedButtonTgl = text_tanggal_sampai
                context?.let { it1 ->
                    DatePickerDialog(
                            it1, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)
                    ).show()
                }
            }
        }

        pullToRefresh.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                println("### REFRESH")
                loadTransaction(true)
            }
        })

        edittext_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (edittext_search.text.isNotBlank()) {

                    var listForListView = ArrayList<HistoryResponse>()

                    for (item in listData.iterator()) {

                        if (item.nomorTujuan != null) {
                            println(item.nomorTujuan + " == " + edittext_search.text.toString())
                            if (item.nomorTujuan.contains(edittext_search.text.toString())) {
                                listForListView.add(item)
                            }
                        }

                    }

                    if (listForListView.isEmpty()){
                        layout_image_empty_data.visibility = View.VISIBLE
                    } else {
                        layout_image_empty_data.visibility = View.GONE
                    }

                    val Adapter = RecyclerViewAdapter(listForListView)

                    view_list_histori_trx.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = Adapter
                    }

                } else {
                    val Adapter = RecyclerViewAdapter(listData.filterEmptyData())

                    view_list_histori_trx.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = Adapter
                    }

                }

            }
        })

    }

    fun updateText(textView: TextView) {
        val myFormat = "yyyy-MM-dd" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)

        textView.setText(sdf.format(myCalendar.time))
    }

    fun loadTransaction(currentDate: Boolean) {

        listData = ArrayList()
        lateinit var call: Call<List<HistoryResponse>>
        if (currentDate) {
            call = api.listCurrentDateTransaction(kodePlgn)
//            call = api.listCurrentDateTransaction("AND6281910673000")
        } else {
//            call = api.listRangeDateTransaction(kodePlgn, text_tanggal_dari.text.toString(), text_tanggal_sampai.text.toString())
            call = api.listRangeDateTransaction(encodeAndReverse("AND6282240867005"), text_tanggal_dari.text.toString(), text_tanggal_sampai.text.toString())
//            call = api.listRangeDateTransaction("AND6281910673000", text_tanggal_dari.text.toString(), text_tanggal_sampai.text.toString())
        }
        call.enqueue(object : Callback<List<HistoryResponse>> {
            override fun onFailure(call: Call<List<HistoryResponse>>, t: Throwable) {
                Toast.makeText(context, "Internal Server Error", Toast.LENGTH_SHORT).show()
                progressBar.visibility = View.GONE
                if (pullToRefresh.isRefreshing) {
                    pullToRefresh.setRefreshing(false)
                }

            }

            override fun onResponse(call: Call<List<HistoryResponse>>, response: Response<List<HistoryResponse>>) {

                var data = response.body()

                if (data != null) {

                    listData = data

                    if (listData.isEmpty()){
                        layout_image_empty_data.visibility = View.VISIBLE
                    } else {
                        layout_image_empty_data.visibility = View.GONE
                    }



                    val Adapter = RecyclerViewAdapter(listData.filterEmptyData())

                    view_list_histori_trx.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = Adapter
                    }
                    progressBar.visibility = View.GONE
                    if (pullToRefresh.isRefreshing) {
                        pullToRefresh.setRefreshing(false)
                    }

                }
            }

        })

    }



    override fun onResume() {
        super.onResume()
        if (listData != null) {

            val Adapter = RecyclerViewAdapter(listData.filterEmptyData())

            view_list_histori_trx.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = Adapter
            }
//            progressBar.visibility = View.GONE

        }
    }

    private class RecyclerViewAdapter(private val ListData: List<HistoryResponse>) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            val textNamaProduk = view.text_nama_produk
            val textTanggalJam = view.text_tanggal_jam
            val textStatus = view.text_status
            val textNoTujuan = view.text_no_tujaun
            val layoutItem = view.layout_item_trx
            val context = view.context

            fun bindItem(Data: HistoryResponse) {
                textNamaProduk.setText(Data.namaProduk)
                textTanggalJam.setText(Data.tanggal + " " + Data.jam)
                textNoTujuan.setText(Data.nomorTujuan)

                if (Data.status != null) {
                    if (Data.status.toUpperCase().equals("BERHASIL")) {
                        textStatus.setTextColor(ContextCompat.getColor(context, R.color.Green))
                    } else if (Data.status.toUpperCase().equals("GAGAL")) {
                        textStatus.setTextColor(ContextCompat.getColor(context, R.color.DarkRed))
                    } else if (Data.status.toUpperCase().equals("RALAT")) {
                        textStatus.setTextColor(ContextCompat.getColor(context, R.color.DarkOrange))
                    } else if (Data.status.toUpperCase().equals("+SALDO")) {
                        textStatus.setTextColor(ContextCompat.getColor(context, R.color.DarkBlue))
                    } else if (Data.status.toUpperCase().equals("RALAT SALDO")) {
                        textStatus.setTextColor(ContextCompat.getColor(context, R.color.DarkGoldenrod))
                    } else if (Data.status.toUpperCase().equals("ULANG") || (Data.status.toUpperCase().equals("LAIN - LAIN"))) {
                        textStatus.setTextColor(ContextCompat.getColor(context, R.color.Black))
                    }
                    textStatus.setText(Data.status)
                } else {
                    textStatus.setText("NULL")
                    textStatus.setTextColor(ContextCompat.getColor(context, R.color.DarkRed))
                }
                layoutItem.setOnClickListener {

                    layoutItem.setBackgroundColor(ContextCompat.getColor(context, R.color.NewUI_Gray))
                    var intent = Intent(context.applicationContext, ReceiptActivity::class.java)
                    intent.putExtra(Constant.RECEIPT_HEADER, Data.status)
                    intent.putExtra(Constant.RECEIPT_PESAN, Data.pesan)
                    intent.putExtra(Constant.RECEIPT_PRODUK, Data.namaProduk)
                    intent.putExtra(Constant.RECEIPT_TANGGAL, Data.tanggal)
                    intent.putExtra(Constant.RECEIPT_WAKTU, Data.jam)
                    intent.putExtra(Constant.RECEIPT_NOMOR_TUJUAN, Data.nomorTujuan)
                    intent.putExtra(Constant.RECEIPT_ID, Data.idTrx)
                    intent.putExtra(Constant.RECEIPT_HARGA, Data.hargaJual)
                    if (Data.status.toUpperCase().equals("BERHASIL") ||
                            Data.status.toUpperCase().equals("RALAT") ||
                            Data.status.toUpperCase().equals("+SALDO")
                    ) {
                        intent.putExtra(Constant.RECEIPT_STATUS, true)

                    } else {
                        intent.putExtra(Constant.RECEIPT_STATUS, false)
                    }

                    context.startActivity(intent)

                }

            }


        }


        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerViewAdapter.ViewHolder {
            return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_histori_pending, p0, false))
        }

        override fun getItemCount(): Int {
            return ListData.size
        }

        override fun onBindViewHolder(p0: RecyclerViewAdapter.ViewHolder, p1: Int) {
            p0.bindItem(ListData.get(p1))


        }


    }


}