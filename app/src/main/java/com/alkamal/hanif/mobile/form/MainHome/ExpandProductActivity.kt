package com.alkamal.hanif.mobile.form.MainHome

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.form.Transaksi.TransaksiActivity
import com.alkamal.hanif.mobile.model.ProductPaymentExpand
import com.alkamal.hanif.mobile.utils.Constant
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_expand_product.*
import kotlinx.android.synthetic.main.layout_expand_product.view.*
import kotlinx.android.synthetic.main.layout_item_expand_produk.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ExpandProductActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expand_product)

        loadExpandProduct(intent.getStringExtra(Constant.HOME_KATEGORI_EXTRA)!!)
    }

    fun loadExpandProduct(kategori : String){
        var call = api.listProductbyKategori(kategori)
        call.enqueue(object : Callback<List<ProductPaymentExpand>> {
            override fun onFailure(call: Call<List<ProductPaymentExpand>>, t: Throwable) {
                Toast.makeText(applicationContext, "Internal Server error", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<List<ProductPaymentExpand>>, response: Response<List<ProductPaymentExpand>>) {
                var data = response.body()

                if (data != null){
                    val productPaymentAdapter = RecyclerViewAdapterExpand(data, applicationContext)

                    list_expand_product.view_expand_product.apply {
                        layoutManager = LinearLayoutManager(applicationContext)
                        adapter = productPaymentAdapter
                    }

                    progressBar.visibility = View.GONE
                }
            }
        })
    }

    private class RecyclerViewAdapterExpand(private val list: List<ProductPaymentExpand>, private val context: Context) : RecyclerView.Adapter<RecyclerViewAdapterExpand.ViewHolder>() {

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            val image = view.img_item
            val text = view.text_title
            val layout = view.layout_item

        }

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerViewAdapterExpand.ViewHolder {
            return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.layout_item_expand_produk, p0, false))
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(p0: RecyclerViewAdapterExpand.ViewHolder, p1: Int) {
            p0.text.text = list.get(p1).namaProduk
            ShowImage(list.get(p1).logo_produk_new, p0.image)

            p0.layout.setOnClickListener {
                var intent = Intent(context, TransaksiActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra(Constant.HOME_IMAGE_EXTRA, list.get(p1).logo_produk_new)
                intent.putExtra(Constant.HOME_KODE_BRG_EXTRA, list.get(p1).kodeProduk)
                intent.putExtra(Constant.HOME_NAMA_PRODUK_EXTRA, list.get(p1).namaProduk)
                intent.putExtra(Constant.HOME_HINT_EXTRA, list.get(p1).hint)
                ContextCompat.startActivity(context, intent, null)

            }

        }

        fun ShowImage(bannerUrl: String, imageView: ImageView) {
            Glide.with(context)
                    .load(Constant.BaseUrlPic + bannerUrl)
                    .apply(RequestOptions().override(100, 100))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
//                            view.progressBar.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
//                            view.progressBar .visibility = View.GONE
                            return false
                        }

                    })
                    .into(imageView)
        }
    }
}
