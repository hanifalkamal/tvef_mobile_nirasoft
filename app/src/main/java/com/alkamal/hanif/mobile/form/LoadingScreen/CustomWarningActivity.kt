package com.alkamal.hanif.mobile.form.LoadingScreen

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.activity_custom_warning.*
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress

class CustomWarningActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_warning)

        text_warning_header.setText(intent.getStringExtra(Constant.WARNING_HEADER))
        text_warning_message.setText(intent.getStringExtra(Constant.WARNING_MESSAGE))

        if (intent.getStringExtra(Constant.WARNING_PIC).equals("MAINTENANCE")){
            image_warning.setBackground(ContextCompat.getDrawable(applicationContext, R.drawable.ic_maintenance))
            button_refresh_koneksi.visibility = View.GONE
        }

        button_refresh_koneksi.setOnClickListener {
            button_refresh_koneksi.visibility = View.GONE
            progressBar.visibility = View.VISIBLE

            val handler = Handler()
            handler.postDelayed({
                if (!isNetworkAvailable()) {
                    text_warning_header.setText("NO INTERNET CONNECTION !")
                    text_warning_message.setText("Periksa kembali koneksi internet anda !")
                    button_refresh_koneksi.visibility = View.VISIBLE
                    progressBar.visibility = View.GONE
                } else if (!isReachable()) {
                    text_warning_header.setText("INTERNAL SERVER ERROR !")
                    text_warning_message.setText("Opps.. Ada kesalahan pada server\nMohon tunggu beberapa saat lagi")
                    button_refresh_koneksi.visibility = View.VISIBLE
                    progressBar.visibility = View.GONE
                } else {
                    finish()
                }
            }, 1500)


        }

    }

    fun isReachable(): Boolean {
        var exists = false
        try {
            val sockaddr: SocketAddress = InetSocketAddress(Constant.ip, Constant.IntBasePort.toInt())
            // Create an unbound socket
            val sock = Socket()

            // This method will block no more than timeoutMs.
            // If the timeout occurs, SocketTimeoutException is thrown.
            val timeoutMs = 2000 // 10 seconds
            sock.connect(sockaddr, timeoutMs)
            exists = true
        } catch (e: IOException) {
            // Handle exception
            e.printStackTrace()
        }
        return exists
    }


    fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo

        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    override fun onBackPressed() {
//        super.onBackPressed()
    }

    fun hideKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}