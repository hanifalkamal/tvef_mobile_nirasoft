package com.alkamal.hanif.mobile.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TransferSaldoResponse(
        @SerializedName("success") var success: Boolean,
        @SerializedName("keterangan") var keterangan: String,
        @SerializedName("pesan") var pesan: String,
        @SerializedName("Brg") var Brg: String,
        @SerializedName("waktu") var waktu: String,
        @SerializedName("idTrx") var idTrx: String,
        @SerializedName("nomorTujuan") var nomorTujuan: String,
        @SerializedName("harga") var harga: String,
        @SerializedName("tanggal") var tanggal: String
):Parcelable