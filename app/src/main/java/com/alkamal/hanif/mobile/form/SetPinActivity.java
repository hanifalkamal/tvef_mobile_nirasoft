package com.alkamal.hanif.mobile.form;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alkamal.hanif.mobile.MainActivity;
import com.alkamal.hanif.mobile.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetPinActivity extends AppCompatActivity {

    @BindView(R.id.ed_setPin_awal) EditText edSetPin;
    @BindView(R.id.bt_simpanPin)
    Button btSimpanPin;
    @BindView(R.id.progressBar)
    ProgressBar prgrsBar;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pin);
        ButterKnife.bind(this);
        prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
    }

    @OnClick(R.id.bt_simpanPin) public void onSimpanPin(){
        prgrsBar.setVisibility(View.VISIBLE);
        btSimpanPin.setVisibility(View.GONE);
        if (String.valueOf(edSetPin.getText()).length() > 0) {
            try {
                SharedPreferences.Editor editor = prefs.edit();

                editor.putString("MY_PIN_TRX", String.valueOf(edSetPin.getText()));
                editor.putString("MY_SERVER", "free_cell");
//                editor.putString("MY_SERVER", "free_cell");
                editor.apply();

                Intent intent = new Intent(SetPinActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

                Toast.makeText(SetPinActivity.this, "PIN Berhasil disimpan", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            prgrsBar.setVisibility(View.GONE);
            btSimpanPin.setVisibility(View.VISIBLE);
            Toast.makeText(SetPinActivity.this, "PIN tidak boleh kosong !", Toast.LENGTH_LONG).show();
        }
    }


}
