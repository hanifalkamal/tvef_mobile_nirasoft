package com.alkamal.hanif.mobile.form.Histori

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.activity_hisotri.*


class HistoriActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hisotri)
        initialTab()

        button_back.setOnClickListener {
            onBackPressed()
        }

        if (intent.hasExtra(Constant.FROM_NOTIF)) {

            var selectedTab = tab_layout.getTabAt(1)
            if (selectedTab != null) {
                selectedTab.select()
            }
        }
    }

    fun initialTab() {
//        tab_layout.addTab(tab_layout.newTab().setText("PENDING"))
//        tab_layout.addTab(tab_layout.newTab().setText("HISTORI"))
//        tab_layout.setTabGravity(TabLayout.GRAVITY_FILL)
        view_pager.adapter = TabsAdapter(supportFragmentManager)
        tab_layout.setupWithViewPager(view_pager)

    }

    class TabsAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        // sebuah list yang menampung objek Fragment
        private val pages = listOf(
                PendingTransactionFragment(),
                HistoriTransactionFragment()
        )

        // menentukan fragment yang akan dibuka pada posisi tertentu
        override fun getItem(position: Int): Fragment {
            return pages[position]
        }

        override fun getCount(): Int {
            return pages.size
        }

        // judul untuk tabs
        override fun getPageTitle(position: Int): CharSequence? {
            return when (position) {
                0 -> "Pending"
                else -> "Histori"
            }
        }
    }
}