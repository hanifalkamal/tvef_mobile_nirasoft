package com.alkamal.hanif.mobile.Response;

public class SetSessionRespone {
    private String notelp,status,tanggal,waktu;
    private Boolean success ;

    public SetSessionRespone(String notelp, String status, String tanggal, String waktu, Boolean success) {
        this.notelp = notelp;
        this.status = status;
        this.tanggal = tanggal;
        this.waktu = waktu;
        this.success = success;
    }

    public SetSessionRespone(String notelp) {
        this.notelp = notelp;
    }

    public String getNotelp() {
        return notelp;
    }

    public void setNotelp(String notelp) {
        this.notelp = notelp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public Boolean IsSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
