package com.itenas.sikpsipil.data.model.VersionValidation

import com.google.gson.annotations.SerializedName

class VersionValidationResponse (
        @SerializedName("aktif") var aktif : String,
        @SerializedName("id_app") var id_app : String,
        @SerializedName("idkategori") var idkategori: String,
        @SerializedName("info_aktif") var info_aktif: String,
        @SerializedName("ket") var ket : String,
        @SerializedName("link") var link : String?,
        @SerializedName("nama_app") var nama_app : String,
        @SerializedName("recompiled") var recompiled : Int,
        @SerializedName("tgl") var tgl : String,
        @SerializedName("versi") var versi : String
)