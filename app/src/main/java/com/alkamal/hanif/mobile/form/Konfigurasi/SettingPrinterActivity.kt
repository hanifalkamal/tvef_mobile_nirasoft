package com.alkamal.hanif.mobile.form.Konfigurasi

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.alkamal.hanif.mobile.BackgroundService.PrinterService
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.activity_setting_printer.*
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.*

class SettingPrinterActivity : BaseActivity() {

    lateinit var bluetoothAdapter: BluetoothAdapter
    lateinit var bluetoothSocket: BluetoothSocket
    lateinit var bluetoothDevice: BluetoothDevice

    lateinit var outputStream: OutputStream
    lateinit var inputStream: InputStream
    lateinit var thread: Thread

    lateinit var readBuffer: ByteArray
    var readBufferPosition = 0

    @Volatile
    var stopWorker = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_printer)

        edittext_printer_type.setText(preferences.getString(Constant.CONFIG_JENIS_PRINTER,""))

        button_back.setOnClickListener {
            onBackPressed()
        }

        button_feed.setOnClickListener {
            FeedPaper()
        }

        button_test_printer.setOnClickListener {
            printData()
        }

        button_connect_printer.setOnClickListener {
            if (edittext_printer_type.text.isNotBlank()) {
                try {
                    progressBar.visibility = View.VISIBLE
                    FindBluetoothDevice(edittext_printer_type.text.toString())
                    openBluetoothPrinter()
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                    text_printer_status.setText("Seri / Jenis Printer Mohon Harus diisi !")
                }
            } else {
                text_printer_status.setText("Seri / Jenis Printer Mohon Harus diisi !")
            }
        }

        button_disconnect_printer.setOnClickListener {
            disconnectBT()
        }

    }

    fun FeedPaper() {
        try {
            val msg = "\n"
            outputStream.write(msg.toByteArray())
            text_printer_status.setText("Printing Text...")
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun FindBluetoothDevice(seriPrinter: String) {
        try {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            if (bluetoothAdapter == null) {
                text_printer_status.setText("Tidak ada Bluetooth Printer yang terkoneksi")
            }
            if (bluetoothAdapter.isEnabled) {
                val enableBT = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBT, 0)
            }
            val pairedDevice = bluetoothAdapter.bondedDevices
            if (pairedDevice.size > 0) {
                for (pairedDev in pairedDevice) {

                    // My Bluetoth printer name is BTP_F09F1A
                    if (pairedDev.name == seriPrinter) {
                        bluetoothDevice = pairedDev
                        text_printer_status.setText("Bluetooth Printer Terkoneksi: " + pairedDev.name)
                        Toast.makeText(this, "Terkoneksi Dengan Pritner", Toast.LENGTH_SHORT).show()
                        //wait(1000);
                        progressBar.visibility = View.GONE
                        break
                    } else {
                        text_printer_status.setText("Printer tidak terdeksi pada seri ini")
                        Toast.makeText(this, "Printer tidak terdeteksi !", Toast.LENGTH_SHORT).show()
                        Log.e("pairDev", "Seri Beda")
                        progressBar.visibility = View.GONE
                    }
                }
            } else {
                text_printer_status.setText("Belum melakukan pairing pada printer ini, koneksikan printer pada menu bluetooth")
                Toast.makeText(this, "Printer belum tersedia !", Toast.LENGTH_SHORT).show()
                Log.e("pairDev", "No Pair")
                progressBar.visibility = View.GONE
            }
            progressBar.visibility = View.GONE
            preferences.edit().putString(Constant.CONFIG_JENIS_PRINTER, seriPrinter).apply()

            //lblPrinterName.setText("Bluetooth Printer Terkoneksi");
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    // Open Bluetooth Printer
    @Throws(IOException::class)
    fun openBluetoothPrinter() {
        try {

            //Standard uuid from string //
            val uuidSting = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb")
            bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(uuidSting)
            bluetoothSocket.connect()
            outputStream = bluetoothSocket.outputStream
            inputStream = bluetoothSocket.inputStream
            val ps = PrinterService(inputStream)
            PrinterService.setOutputStream(outputStream)
            //ps.onStart();
            //beginListenData();
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    // Printing Text to Bluetooth Printer //
    @Throws(IOException::class)
    fun printData() {
        try {
            var msg: String = edittext_printer_test.getText().toString()
            msg += "\n"
            outputStream.write(msg.toByteArray())
            text_printer_status.setText("Printing Text...")
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    // Disconnect Printer //
    @Throws(IOException::class)
    fun disconnectBT() {
        try {
            stopWorker = true
            outputStream.close()
            inputStream.close()
            bluetoothSocket.close()
            text_printer_status.setText("Printer Disconnected.")
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }
}