package com.alkamal.hanif.mobile.adapter;

public class Session {
    private String status;
    private String success;

    public Session(String status, String success) {
        this.status = status;
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
