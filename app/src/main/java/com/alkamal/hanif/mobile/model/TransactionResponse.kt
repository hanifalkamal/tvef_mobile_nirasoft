package com.alkamal.hanif.mobile.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class TransactionResponse (
        @SerializedName("pesan") val pesan : String,
        @SerializedName("keterangan") val keterangan : String,
        @SerializedName("produk") val produk : String,
        @SerializedName("kodeProduk") val kodeProduk : String,
        @SerializedName("success") val success : Boolean,
        @SerializedName("waktu") val waktu : String,
        @SerializedName("nomorTujuan") val nomorTujuan : String,
        @SerializedName("tanggal") val tanggal : String,
        @SerializedName("idTrx") val idTrx : String,
        @SerializedName("harga") val harga : String,
        @SerializedName("response_code") val responseCode : String,
        @SerializedName("response_message") val responseMessage : String
): Parcelable