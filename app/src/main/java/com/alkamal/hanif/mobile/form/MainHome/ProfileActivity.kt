package com.alkamal.hanif.mobile.form.MainHome

import android.os.Bundle
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.utils.CacheManager
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        if (CacheManager.kodePlgn != null) {
            text_kode_plgn.setText(CacheManager.kodePlgn)
        }

        text_copy_kodeplgn.setOnClickListener {
            copyToClipBoard("kodeplgn", text_kode_plgn.text.toString())
            Toast.makeText(applicationContext, "Copy to clipboard", Toast.LENGTH_SHORT).show()
        }

        button_back.setOnClickListener {
            onBackPressed()
        }

    }
}