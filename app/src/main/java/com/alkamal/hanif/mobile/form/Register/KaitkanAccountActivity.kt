package com.alkamal.hanif.mobile.form.Register

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.model.CreateAccountResponse
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.kaitkan_layout.*
import kotlinx.android.synthetic.main.kofirmasi_kaitkan_layout.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class KaitkanAccountActivity : BaseActivity() {

    lateinit var nomorMaster: String;
    lateinit var nomorVerifikasi: String;
    var isAnimatedHeader = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.kaitkan_layout)

        nomorVerifikasi = ""

        layout_konfirmasi.visibility = View.INVISIBLE
        animate("HIDE", layout_konfirmasi)

        text_konfirm_kaitkan.setOnClickListener {
//            layout_konfirmasi.visibility = View.VISIBLE
            animate("SHOW", layout_konfirmasi)
        }

        layout_konfirmasi.text_konfirm_kembali.setOnClickListener {
//            layout_konfirmasi.visibility = View.GONE
            animate("HIDE", layout_konfirmasi)
        }

        button_back.setOnClickListener {
            onBackPressed()
        }

        bt_kaitkan.setOnClickListener {
            nomorMaster = edittext_prefix.text.toString() + ed_kaitkan_nomor.text.toString()

            if (nomorMaster.length < 3) {
                text_error_message.visibility = View.VISIBLE
                text_error_message.setText("Nomor Handphone Utama Tidak Boleh Kosong !")
            } else {
                submit(Constant.OPSI_REGIS_PRAKAITKAN)
            }

        }

        layout_konfirmasi.bt_verifikasi_kaitkan.setOnClickListener {

            nomorVerifikasi = layout_konfirmasi.ed_verifikasi_kaitkan.text.toString()

            if (nomorVerifikasi.isNullOrEmpty()) {
                Toast.makeText(applicationContext, "Verifikasi Harus Diisi", Toast.LENGTH_SHORT).show()
            } else {
                submit(Constant.OPSI_REGIS_KAITKAN)
            }
        }

    }

    fun submit(opsi: String) {
        text_error_message.visibility = View.INVISIBLE
        try {
            progressBar.visibility = View.VISIBLE
            var call = api.createAccount(
                    intent.getStringExtra(Constant.REGISTER_NO_TELP),
                    intent.getStringExtra(Constant.REGISTER_PSW),
                    intent.getStringExtra(Constant.REGISTER_NAMA_USAHA),
                    intent.getStringExtra(Constant.REGISTER_NAMA_PEMILIK),
                    intent.getStringExtra(Constant.REGISTER_ALAMAT),
                    encodeAndReverse(androidId),
                    encodeAndReverse(deviceModel),
                    nomorMaster,
                    intent.getStringExtra(Constant.REGISTER_PIN),
                    nomorVerifikasi,
                    opsi)
            call.enqueue(object : Callback<CreateAccountResponse> {
                override fun onFailure(call: Call<CreateAccountResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, "Internal Server Error", Toast.LENGTH_SHORT).show()
                    progressBar.visibility = View.GONE
                }

                override fun onResponse(call: Call<CreateAccountResponse>, response: Response<CreateAccountResponse>) {
                    try {
                        var result = response.body()
                        progressBar.visibility = View.GONE
                        if (result != null) {
                            if (result.success) {

                                if (opsi.equals(Constant.OPSI_REGIS_KAITKAN)) {

                                    var intent = Intent(applicationContext, RegisterSucceedActivity::class.java)
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent)

                                } else {
                                    Toast.makeText(applicationContext, "Kode Verifikasi Telah Dikirim", Toast.LENGTH_SHORT).show()
//                                layout_konfirmasi.visibility = View.VISIBLE
                                    animate("SHOW", layout_konfirmasi)
                                }

                            } else {
                                throw Exception(result.keterangan)
                            }

                        }
                    } catch (e: Exception) {
                        progressBar.visibility = View.GONE
                        Toast.makeText(applicationContext, e.message, Toast.LENGTH_SHORT).show()
                        e.printStackTrace()
                    }

                }
            })


        } catch (e: Exception) {
            progressBar.visibility = View.GONE
            text_error_message.visibility = View.VISIBLE
            text_error_message.setText(e.message)
//            Toast.makeText(applicationContext, e.message, Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }

    }

    fun animate(arrgument: String, view: View) {
        if (arrgument.equals("SHOW")) {
            if (!isAnimatedHeader) {
                isAnimatedHeader = true
                view.visibility = View.VISIBLE
                view.animate()
                        .translationY(0.toFloat()) //
                        .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                        .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_FADE_TO_SHOW)
                        .setListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator) {
                                super.onAnimationEnd(animation)
                                isAnimatedHeader = false
                            }

                        })
            }
        } else {
            //Animate Header Hide
            view.animate()
                    .translationY(view.height.toFloat()) //
                    .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                    .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_SHOW_TO_FADE)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)
                            isAnimatedHeader = false

                        }
                    })
//                    }
        }
    }
}
