package com.alkamal.hanif.mobile.fungsi;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.Response.SetSessionRespone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class session extends BaseActivity {

    public void setSession(String status,String notelp){
        Session(status,notelp);
    }

    public void Session(String status,String notelp){

        Call<SetSessionRespone> call = getApi().SetSession(notelp, status, "curdate()","curtime()");
        call.enqueue(new Callback<SetSessionRespone>() {
            @Override
            public void onResponse(Call<SetSessionRespone> call, Response<SetSessionRespone> response) {
                if(response.isSuccessful()){
                    SetSessionRespone res = response.body();
                    if(res.IsSuccess()){

                        //finish();
                        log("Berhasil");
                        //SAMPE SINI
                    } else {
                        //Toast.makeText(session.this, "Anda telah terdaftar sebelumnya hubungi admin untuk daftar ulang", Toast.LENGTH_LONG).show();
                        //prd.dismiss();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SetSessionRespone> call, Throwable t) {
                log(t.toString());
            }
        });


    }
}
