package com.alkamal.hanif.mobile.form;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BackgroundService.ResponService;
import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 01/02/2018.
 */

public class SimulasiTrxActivity extends BaseActivity {

    String myID;
    Calendar calendar;
    SimpleDateFormat dateformat;
    String tanggal,idtrx,Server;
    int CekCounting,count;

    @BindView(R.id.tv_respon_pesan)
    TextView respon;

    @BindView(R.id.ed_pesan)
    EditText pesan;

    ResponService Balasan = new ResponService();
    SharedPreferences prefs;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simulasitrx_main);

        ButterKnife.bind(this);

        //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);

        Server = prefs.getString("MY_SERVER", "");
        count = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT",""));
        myID = prefs.getString("MY_USERID","");
       /// Intent i = new Intent(getApplicationContext(),ResponService.class);
        //startService(i);

    }



    @OnClick(R.id.bt_next) public void onSendMsg()
    {
        sendReqSal(pesan.getText().toString());
    }

    public void sendReqSal(String format){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;

        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx,Server,"");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(SimulasiTrxActivity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();

                        //Call<getMsgResponse> call2 = getApi().GetMSg(myID,idtrx,Server);


                       // Toast.makeText(SimulasiTrxActivity.this, idtrx, Toast.LENGTH_LONG).show();
                       // Intent i = new Intent(getApplicationContext(),ResponService.class);
                       // stopService(i);
                       // startService(i);
                        getBalsan();

                        //Intent i = new Intent("KIRIM_FORMAT");
                        // i.putExtra("HotNews",ges.getPengumuman1());
                        //sendBroadcast(i);

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(SimulasiTrxActivity.this, "Gagal", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void getBalsan(){
        Call<MsgResponse> call = getApi().GetMSg(myID,idtrx,Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {
                        prefs=getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("LAST_ID",idtrx);
                        editor.putString("IMEI",myID);
                        editor.putString("ISI_PESAN",ges.getPesan());
                        editor.putInt("NOTIF", 1);
                        editor.apply();
                        respon.setText(ges.getPesan());

                        log("Berhasil");

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < count){
                            getBalsan();
                            CekCounting = CekCounting+1;
                        }else{
                            Toast.makeText(SimulasiTrxActivity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
