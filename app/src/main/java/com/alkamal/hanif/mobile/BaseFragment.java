package com.alkamal.hanif.mobile;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alkamal.hanif.mobile.model.BalanceResponse;
import com.alkamal.hanif.mobile.utils.CacheManager;
import com.alkamal.hanif.mobile.utils.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseFragment extends Fragment {

    APIService api ;
    SharedPreferences preferences;
    Context mContext;

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        preferences = this.getActivity().getSharedPreferences(Constant.PREFERENCES_TRX, Context.MODE_PRIVATE);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient().newBuilder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);
        client.addInterceptor(interceptor);
        Retrofit base = new Retrofit.Builder()
                //.baseUrl("http://www.tvef.xyz/tvefKirimPesan/")
//                .baseUrl("http://193.168.195.139/tvefKirimPesan/")
                .baseUrl(Constant.BaseUrl + Constant.BasePort)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();
        api = base.create(APIService.class);


    }

    public APIService getApi(){
        return api;
    }

    public void makeErrorDialog(String msg, Context context) {
        AlertDialog.Builder alertDBuilder = new AlertDialog.Builder(context);
        alertDBuilder.setMessage(msg);
        alertDBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = alertDBuilder.create();
        alertDialog.show();
    }

    public void log(String log) {
        if (true) {
            Log.d("BaseFragment" , log);
        }
    }

    public void currencyFormatter(String p0, TextView tv){
        try {
            String givenstring = p0.toString();
            Long longval ;
            if (givenstring.contains(",")) {
                givenstring = givenstring.replace(",", "");
            }
            longval = java.lang.Long.parseLong(givenstring);
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String formattedString = formatter.format(longval);
            formattedString = formattedString.replace(",", ".");
            tv.setText("Rp. "+formattedString);
//            tv!!.setSelection(edOmset!!.getText().length)
            // to place the cursor at the end of text
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String currencyFormatter(String p0){

        String formattedString = "" ;
        try {
            String givenstring = p0.toString();
            Long longval ;
            if (givenstring.contains(",")) {
                givenstring = givenstring.replace(",", "");
            }
            longval = java.lang.Long.parseLong(givenstring);
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            formattedString = formatter.format(longval);
            formattedString = formattedString.replace(",", ".");
//            tv!!.setSelection(edOmset!!.getText().length)
            // to place the cursor at the end of text
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedString;
    }

    public String currencyFormatterOnEdiittextListener(String p0, EditText editText){

        String formattedString = "" ;
        try {
            String givenstring = p0.toString();
            Long longval ;
            if (givenstring.contains(",")) {
                givenstring = givenstring.replace(",", "");
            }
            longval = java.lang.Long.parseLong(givenstring);
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            formattedString = formatter.format(longval);
            formattedString = formattedString.replace(",", ".");
            editText.setText(formattedString);
            editText.setSelection(editText.getText().length());
            // to place the cursor at the end of text
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedString;
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showImage(String url, ImageView imageView, Context context){

        Glide.with(context)
                .load(Constant.BaseUrlPic + url)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);

    }

    public String generateIdTrx(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        return dateformat.format(calendar.getTime());
    }



    public BalanceResponse checkBalance(String pin, TextView textView){

        final BalanceResponse[] balanceResponse = {null};
        Call<BalanceResponse> call = api.balance(getKodePlgn(), pin);
        call.enqueue(new Callback<BalanceResponse>() {
            @Override
            public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                balanceResponse[0] = response.body();
                if (balanceResponse[0] != null){
                    if (balanceResponse[0].getSuccess()){
                        textView.setText("Rp. "+currencyFormatter(balanceResponse[0].getSaldo()));
                        CacheManager.setBalance("Rp. "+currencyFormatter(balanceResponse[0].getSaldo()));
                    }
                }


            }

            @Override
            public void onFailure(Call<BalanceResponse> call, Throwable t) {
                log("Fail to check balance, message : "+t.getMessage());
//                makeErrorDialog("Gagal Cek Saldo !");
                balanceResponse[0].setSuccess(false);
                balanceResponse[0].setRespon(t.getMessage());
            }
        });
        return balanceResponse[0];
    }

    public BalanceResponse checkBalance(String pin){

        final BalanceResponse[] balanceResponse = {null};
        Call<BalanceResponse> call = api.balance(getKodePlgn(), pin);
        call.enqueue(new Callback<BalanceResponse>() {
            @Override
            public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                balanceResponse[0] = response.body();
                if (balanceResponse[0] != null){
                    if (balanceResponse[0].getSuccess()){
                        CacheManager.setBalance("Rp. "+currencyFormatter(balanceResponse[0].getSaldo()));
                    }
                }


            }

            @Override
            public void onFailure(Call<BalanceResponse> call, Throwable t) {
                log("Fail to check balance, message : "+t.getMessage());
//                makeErrorDialog("Gagal Cek Saldo !");
                balanceResponse[0].setSuccess(false);
                balanceResponse[0].setRespon(t.getMessage());
            }
        });
        return balanceResponse[0];
    }

    public void openSoftKeyboard(Context context, View view) {
        view.requestFocus();
        // open the soft keyboard
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public String getIdPlgn() {
        return preferences.getString(Constant.ACCOUNT_ID_PLGN,"");
    }
    public String getNamaUsaha() {
        return preferences.getString(Constant.ACCOUNT_NAMA_USAHA,"");
    }
    public String getKodePlgn() {
        return encodeAndReverse(preferences.getString(Constant.ACCOUNT_KODE_PLGN,""));
    }

    public void clearUserInfo(){
        preferences.edit().remove(Constant.ACCOUNT_ID_PLGN).apply();
        preferences.edit().remove(Constant.ACCOUNT_NAMA_USAHA).apply();
        preferences.edit().remove(Constant.ACCOUNT_KODE_PLGN).apply();
    }

    public String reverseString(String str){
        StringBuilder sb = new StringBuilder(str);
        return sb.reverse().toString();
    }

    public String encode(String str){
        try {
            byte[] data = str.getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.NO_WRAP);
            return base64;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String encodeAndReverse(String str){
        return reverseString(encode(str));
    }


}
