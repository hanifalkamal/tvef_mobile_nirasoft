package com.alkamal.hanif.mobile.Response;

/**
 * Created by HanifAlKamal on 09/02/2018.
 */

public class DaftarRespone {
        private String no_telp,psw,namaUsaha,psw_baru;
        private Boolean success ;

    public DaftarRespone(String no_telp, String psw) {
        this.no_telp = no_telp;
        this.psw = psw;
    }

    public DaftarRespone(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getPsw_baru() {
        return psw_baru;
    }

    public void setPsw_baru(String psw_baru) {
        this.psw_baru = psw_baru;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getNamaUsaha() {
        return namaUsaha;
    }

    public void setNamaUsaha(String namaUsaha) {
        this.namaUsaha = namaUsaha;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public Boolean IsSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
