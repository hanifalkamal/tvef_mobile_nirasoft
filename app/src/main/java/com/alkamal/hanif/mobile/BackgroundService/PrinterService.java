package com.alkamal.hanif.mobile.BackgroundService;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.alkamal.hanif.mobile.form.Konfigurasi.SettingPrinterActivity;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PrinterService extends Service {
    BluetoothAdapter bluetoothAdapter;

    Thread thread;

    byte[] readBuffer;
    int readBufferPosition;
    private static OutputStream outputStream;
    volatile boolean stopWorker;

    public static OutputStream getOutputStream() {
        return outputStream;
    }

    public static void setOutputStream(OutputStream outputStream) {
        PrinterService.outputStream = outputStream;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

    }

    public PrinterService() {
    }

    public PrinterService(final InputStream inputStream) {

        try{

            final Handler handler =new Handler();
            final byte delimiter=10;
            stopWorker =false;
            readBufferPosition=0;
            readBuffer = new byte[1024];


            thread=new Thread(new Runnable() {
                @Override
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker){
                        try{
                            int byteAvailable = inputStream.available();
                            if(byteAvailable>0){
                                byte[] packetByte = new byte[byteAvailable];
                                inputStream.read(packetByte);

                                for(int i=0; i<byteAvailable; i++){
                                    byte b = packetByte[i];
                                    if(b==delimiter){
                                        byte[] encodedByte = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer,0,
                                                encodedByte,0,
                                                encodedByte.length
                                        );
                                        final String data = new String(encodedByte,"US-ASCII");
                                        readBufferPosition=0;
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                //lblPrinterName.setText(data);
                                            }
                                        });
                                    }else{
                                        readBuffer[readBufferPosition++]=b;
                                        Log.e("PrinterService","b != delimeter");
                                    }
                                }
                            } else {
                                Log.e("PrinterService","byteAvailable = 0");
                            }
                        }catch(Exception ex){
                            stopWorker=true;
                            Log.e("PrinterService","Gagal koneksi");
                        }
                    }

                }
            });

            thread.start();
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public static void  printData(String pesan, Context context) throws IOException {
        try{
            String msg = pesan;
            msg+="\n";
            try {
                Toast.makeText(context, "Printing. . .", Toast.LENGTH_SHORT).show();
//                getOutputStream().write(msg.getBytes());
                getOutputStream().write( msg.getBytes());
            }catch (Exception ex){
                Log.e("PrinterService","Belum Seting Printer");
                Toast.makeText(context, "Printer Tidak Terkoneksi", Toast.LENGTH_SHORT).show();
//                Intent i = new Intent(context, PrinterSettingActivity.class);
                Intent i = new Intent(context, SettingPrinterActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
            //Toast.makeText(this, "Printing. . .", Toast.LENGTH_SHORT).show();;
        }catch (Exception ex){
            Toast.makeText(context, "Printer Tidak Terkoneksi", Toast.LENGTH_SHORT).show();
            ex.printStackTrace();

        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.

        throw new UnsupportedOperationException("Not yet implemented");
    }


}
