package com.alkamal.hanif.mobile.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CreateAccountResponse(
        @SerializedName("success") var success: Boolean,
        @SerializedName("noTlp") var noTlp: String,
        @SerializedName("namaUsaha") var namaUsaha: String,
        @SerializedName("namaPemilik") var namaPemilik: String,
        @SerializedName("keterangan") var keterangan: String
): Parcelable