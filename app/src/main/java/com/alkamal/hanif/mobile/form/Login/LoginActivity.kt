package com.alkamal.hanif.mobile.form.Login

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.BuildConfig
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.form.Register.ChooseOptionCreateAccountActivity
import com.alkamal.hanif.mobile.model.UserLoginResponse
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.activity_login2.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception


class LoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login2)

        text_version.setText(BuildConfig.VERSION_NAME)

        button_sign_in.setOnClickListener {
            if (edittext_nomor_telepon.text.isEmpty()){
                text_header_sub_login.setTextColor(ContextCompat.getColor(applicationContext, R.color.PaleVioletRed))
                text_header_sub_login.setText("Nomor telepon tidak boleh kosong")
                openSoftKeyboard(applicationContext, edittext_nomor_telepon)
            } else if (edittext_password.text.isEmpty()){
                text_header_sub_login.setTextColor(ContextCompat.getColor(applicationContext, R.color.PaleVioletRed))
                text_header_sub_login.setText("Password tidak boleh kosong")
                openSoftKeyboard(applicationContext, edittext_password)
            } else {
                login()
            }
        }

        edittext_nomor_telepon.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                try {
                    if ("" + edittext_nomor_telepon.getText().get(0) == "0") {
                        edittext_nomor_telepon.setText(edittext_nomor_telepon.getText().toString().substring(1))
                        //edNotelp.append("");
                    } else if ("" + edittext_nomor_telepon.getText().get(0) + "" + edittext_nomor_telepon.getText().get(1) == "62") {
                        edittext_nomor_telepon.setText(edittext_nomor_telepon.getText().toString().substring(2))
                    }
                }catch (e:Exception){
                    e.printStackTrace()
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

        text_ganti_password.setOnClickListener {
            var intent = Intent(applicationContext, ChangePswActivity::class.java)
            startActivity(intent)
        }

        button_daftar_baru.setOnClickListener {
            var intent = Intent(applicationContext, ChooseOptionCreateAccountActivity::class.java)
            startActivity(intent)
        }

    }

    fun login(){
        progressBar.visibility = View.VISIBLE
        var call = api.signIn(
                encodeAndReverse("62"+edittext_nomor_telepon.text.toString()),
                (encodeAndReverse(edittext_password.text.toString())),
                encodeAndReverse(androidId),
                encodeAndReverse(deviceModel))
        call.enqueue(object : Callback<UserLoginResponse>{
            override fun onFailure(call: Call<UserLoginResponse>, t: Throwable) {
                progressBar.visibility = View.GONE
                text_header_sub_login.setTextColor(ContextCompat.getColor(applicationContext, R.color.PaleVioletRed))
                text_header_sub_login.setText("Internal Server Error")
            }

            override fun onResponse(call: Call<UserLoginResponse>, response: Response<UserLoginResponse>) {
                var data = response.body()
                if (data != null){

                    if (data.success){
                        //Save in prefences
                        //Status Login
                        //Kode Plgn AND62...
                        //Nama Usaha
                        //id user
                        var intent = Intent(applicationContext, AktivasiOtpActivity::class.java)
                        intent.putExtra(Constant.ACCOUNT_KODE_PLGN, "AND62"+edittext_nomor_telepon.text.toString())
                        intent.putExtra(Constant.ACCOUNT_NAMA_USAHA, data.namaUsaha)
                        intent.putExtra(Constant.ACCOUNT_ID_PLGN, data.idUser)
                        intent.putExtra(Constant.ACCOUNT_NOMOR_TELEPON, "62"+edittext_nomor_telepon.text.toString())
                        startActivity(intent)
                        finish()
                    } else {
                        progressBar.visibility = View.GONE
                        text_header_sub_login.setTextColor(ContextCompat.getColor(applicationContext, R.color.PaleVioletRed))
                        text_header_sub_login.setText(data.message)
                    }
                }
            }
        })

    }
}