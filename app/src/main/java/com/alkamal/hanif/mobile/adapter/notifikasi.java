package com.alkamal.hanif.mobile.adapter;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;

import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.form.BalasanActivity;

/**
 * Created by HanifAlKamal on 03/02/2018.
 */

public class notifikasi extends BroadcastReceiver {

    NotificationCompat.Builder notification;
    private static final int uniqueID = 45612;

    public notifikasi() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent i = new Intent(context, BalasanActivity.class);
        i.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,100,i,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = (Builder) new Builder(context)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText("ada notif")
                .setContentTitle("Tes Notif")
                .setAutoCancel(true);

        notificationManager.notify(100,builder.build());


    }

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }




}
