package com.alkamal.hanif.mobile.form;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.alkamal.hanif.mobile.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by HanifAlKamal on 14/12/2017.
 */

public class SettingActivity extends AppCompatActivity {

   // @BindView(R.id.tv_blsn_saldo)
  //  TextView tvSaldo;
    private EditText formatCekSaldoInput,timeoutTrx,formatPinTrx,namaServer;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_format);

        ButterKnife.bind(this);

//        settingFormat sf = new settingFormat();
//        sf.setFormatCekSaldo(tvSaldo.getText().toString());

        prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
        String formatCekSaldo = prefs.getString("MY_FORMAT_CEKSALDO","saldo 1234");
        String waktutimeout = prefs.getString("MY_TRX_TIME_OUT", "120");
        String setPinTrx = prefs.getString("MY_PIN_TRX", "1234");
        String server = prefs.getString("MY_SERVER", "free_cell");

        timeoutTrx = (EditText)findViewById(R.id.ed_timeout_trx);
        formatCekSaldoInput = (EditText)findViewById(R.id.ed_format_saldo);
        formatPinTrx = (EditText)findViewById(R.id.ed_setPin_trx);
        namaServer = (EditText)findViewById(R.id.ed_setServer);

        timeoutTrx.setText(waktutimeout);
        formatCekSaldoInput.setText(formatCekSaldo);
        formatPinTrx.setText(setPinTrx);
        namaServer.setText(server);


    }

    @OnClick(R.id.bt_saveSetting) public void onSaveFormatSetting(){
        String formatCekSaldo = formatCekSaldoInput.getText().toString();
        String waktutimeout = timeoutTrx.getText().toString();
        String setPinTrx = formatPinTrx.getText().toString();
        String server = namaServer.getText().toString();

        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("MY_FORMAT_CEKSALDO",formatCekSaldo);
        editor.putString("MY_TRX_TIME_OUT",waktutimeout);
        editor.putString("MY_PIN_TRX", setPinTrx);
        editor.putString("MY_SERVER", server);
        editor.apply();

        Toast.makeText(SettingActivity.this,"Setting berhasil dirubah",Toast.LENGTH_LONG).show();
    }

    public void onSaveFormatSetting(View view){
        String formatCekSaldo = formatCekSaldoInput.getText().toString();
        String waktutimeout = timeoutTrx.getText().toString();
        String setPinTrx = formatPinTrx.getText().toString();
        String server = namaServer.getText().toString();

        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("MY_FORMAT_CEKSALDO",formatCekSaldo);
        editor.putString("MY_TRX_TIME_OUT",waktutimeout);
        editor.putString("MY_PIN_TRX", setPinTrx);
        editor.putString("MY_SERVER", server);
        editor.apply();

        Toast.makeText(SettingActivity.this,"Setting berhasil dirubah",Toast.LENGTH_LONG).show();
    }


}
