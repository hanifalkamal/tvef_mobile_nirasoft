package com.alkamal.hanif.mobile.form;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.widget.EditText;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 19/02/2018.
 */

public class konfirmasiTopUpActivity extends BaseActivity {

    String myID;
    Calendar calendar;
    SimpleDateFormat dateformat;
    String tanggal,idtrx,pin,noTlp,Server;
    NotificationCompat.Builder notification;
    private static final int uniqueID = 45612;
    int CekCounting;
    int waktutimeout;

    @BindView(R.id.ed_noTerimaTiket)
    EditText edTiket;

    ProgressDialog prd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.konfirm_tiket_layout);
        ButterKnife.bind(this);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.6));

        //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);
    }

    @OnClick(R.id.bt_Konf_Tiket) public void onKonfTiket(){
        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
        pin = prefs.getString("MY_PIN_TRX", "");
        Server = prefs.getString("MY_SERVER", "");
        myID = prefs.getString("MY_USERID","");

        String Format = "konftopup "+pin+" "+edTiket.getText().toString();
        sendReqSal(Format);
    }

    public void sendReqSal(String format){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;

        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();

        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx, Server, "");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(konfirmasiTopUpActivity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        getBalsan();

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(res.getMessage());
                        prd.dismiss();
                        Toast.makeText(konfirmasiTopUpActivity.this, "Gagal Load Data Cek Koneksi Internet", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void getBalsan(){
        Call<MsgResponse> call = getApi().GetMSg(myID,idtrx, Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {
                        prd.dismiss();
                        Intent intent = new Intent(konfirmasiTopUpActivity.this, BalasanActivity.class);
                        intent.putExtra("CekSal",ges.getPesan());
                        Notif(ges.getPesan());
                        startActivity(intent);

                        //Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_LONG).show();


                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                // yourMethod();
                            }
                        }, 1000);   //5 seconds


                        //finish();
                        //tvSaldo.setText(ges.getPesan());

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < waktutimeout){
                            getBalsan();
                            CekCounting = CekCounting+1;
                        }else{
                            prd.dismiss();
                            Notif("Time Out / Tidak ada balasan masuk");
                            Toast.makeText(konfirmasiTopUpActivity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }

    public void Notif(String notifBody){
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker("This is the ticker");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("TVEF");
        notification.setContentText(notifBody);

        Context context = getApplicationContext();
        Intent intent = new Intent(context, BalasanActivity.class);
        intent.putExtra("CekSal",notifBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());

    }


}
