package com.alkamal.hanif.mobile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alkamal.hanif.mobile.R;

import java.util.List;

/**
 * Created by HanifAlKamal on 03/03/2018.
 */

public class ProviderAdapter extends BaseAdapter {
    private Context mContext;
    private List<provider> mProvider;

    public ProviderAdapter(Context mContext, List<provider> mProvider) {
        this.mContext = mContext;
        this.mProvider = mProvider;
    }

    @Override
    public int getCount() {
        return mProvider.size();
    }

    @Override
    public Object getItem(int position) {
        return mProvider.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View v = View.inflate(mContext, R.layout.list_provider_item, null);
        TextView tvName = (TextView)v.findViewById(R.id.tv_Name);

        tvName.setText(mProvider.get(position).getNama_provider());

        v.setTag(mProvider.get(position).getId());

        return v;
    }
}
