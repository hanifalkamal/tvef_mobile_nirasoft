package com.alkamal.hanif.mobile.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChangePasswordResponse(
        @SerializedName("success") var success: Boolean,
        @SerializedName("message") var message: String
): Parcelable