package com.alkamal.hanif.mobile.form.Login

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.content.ContextCompat
import android.telephony.PhoneNumberUtils
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import com.alkamal.hanif.mobile.BackgroundService.FirebaseInstanceService
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.form.Transaksi.InputPinActivity
import com.alkamal.hanif.mobile.utils.Constant
import com.goodiebag.pinview.Pinview
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import com.google.firebase.messaging.FirebaseMessaging
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import kotlinx.android.synthetic.main.activity_aktivasi_otp.*
import kotlinx.android.synthetic.main.activity_aktivasi_otp.progressBar
import java.util.concurrent.TimeUnit


class AktivasiOtpActivity : BaseActivity() {

    lateinit var topic: String
    private lateinit var verificationid: String
    private lateinit var mAuth: FirebaseAuth
    private val TAG = "AktivasiOtpActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aktivasi_otp)
        act_code_otp.setTextColor(ContextCompat.getColor(applicationContext, R.color.White))
        act_code_otp.setTextSize(30)

        mAuth = FirebaseAuth.getInstance()
        println("### " + intent.getStringExtra(Constant.ACCOUNT_NOMOR_TELEPON))
        var phoneNumber = intent.getStringExtra(Constant.ACCOUNT_NOMOR_TELEPON)
        sendVerificationCode("+$phoneNumber")

        act_code_otp.setPinViewEventListener(object : Pinview.PinViewEventListener {
            override fun onDataEntered(pinview: Pinview?, fromUser: Boolean) {

//                dummy()
                val code: String = act_code_otp.getValue().toString()

                act_code_otp.setVisibility(View.INVISIBLE)
                progressBar.setVisibility(View.VISIBLE)
                FirebaseAuth.getInstance().signOut();
                verifyCode(code)

            }

        })

        text_resend_otp.setOnClickListener {
            if (text_resend_otp.getText() == "Kirim ulang kode verifikasi!") {
                Toast.makeText(this, "Kode verifikasi sudah di kirim ulang !", Toast.LENGTH_SHORT).show()
                val phonenumber = intent.getStringExtra(Constant.ACCOUNT_NOMOR_TELEPON)
                sendVerificationCode("+$phonenumber")
//            resend.setVisibility(View.GONE);
            }
        }
    }

    private fun sendVerificationCode(number: String) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallBack
        )
        object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                text_resend_otp.setText("waktu kirim ulang: " + millisUntilFinished / 1000)
            }

            override fun onFinish() {
                text_resend_otp.setText("Kirim ulang kode verifikasi!")
            }
        }.start()
    }

    private val mCallBack: OnVerificationStateChangedCallbacks = object : OnVerificationStateChangedCallbacks() {
        override fun onCodeSent(s: String, forceResendingToken: ForceResendingToken) {
            super.onCodeSent(s, forceResendingToken)
            verificationid = s
            Log.d(TAG, "onCodeSent: $verificationid")
        }

        override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
            val code = phoneAuthCredential.smsCode
            if (code != null) {
                progressBar.setVisibility(View.VISIBLE)
                verifyCode(code)
            }
        }

        override fun onVerificationFailed(e: FirebaseException) {
            try {

                Toast.makeText(applicationContext, "Ada kesalahan pada server, silahkan coba beberapa saat lagi", Toast.LENGTH_LONG).show()
                Log.e(TAG, "onVerificationFailed: " + e.message)
                act_code_otp.setVisibility(View.VISIBLE)
                progressBar.setVisibility(View.GONE)
                for (i in 0..5) {
                    act_code_otp.onKey(act_code_otp.getFocusedChild(), KeyEvent.KEYCODE_DEL, KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL))
                }

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun verifyCode(code: String) {
        try {
            val credential = PhoneAuthProvider.getCredential(verificationid, code)
            signInWithCredential(credential)
        } catch (e: Exception) {
            Toast.makeText(this, "Silahakan coba lagi", Toast.LENGTH_SHORT).show()
            text_resend_otp.setVisibility(View.VISIBLE)

        }
    }

    private fun signInWithCredential(credential: PhoneAuthCredential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        preferences.edit().putBoolean(Constant.STATE_HAS_LOGGED_IN, true).apply()
                        preferences.edit().putString(Constant.ACCOUNT_KODE_PLGN, intent.getStringExtra(Constant.ACCOUNT_KODE_PLGN)).apply()
                        preferences.edit().putString(Constant.ACCOUNT_NAMA_USAHA, intent.getStringExtra(Constant.ACCOUNT_NAMA_USAHA)).apply()
                        preferences.edit().putString(Constant.ACCOUNT_ID_PLGN, intent.getStringExtra(Constant.ACCOUNT_ID_PLGN)).apply()
                        //SUBSCRIBE TO FCM

                        val i = Intent(applicationContext, FirebaseInstanceService::class.java)
                        startService(i)

                        var kodeplgn = intent.getStringExtra(Constant.ACCOUNT_KODE_PLGN)
                        topic = "/topics/$kodeplgn"
                        FirebaseMessaging.getInstance().subscribeToTopic(topic)
                        topic = "/topics/free_cell";
                        FirebaseMessaging.getInstance().subscribeToTopic(topic)
                        var intent = Intent(applicationContext, InputPinActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        for (i in 0..5) {
                            act_code_otp.onKey(act_code_otp.getFocusedChild(), KeyEvent.KEYCODE_DEL, KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL))
                        }
                        act_code_otp.setVisibility(View.VISIBLE)
                        progressBar.setVisibility(View.GONE)

                        text_wrong_otp.visibility = View.VISIBLE

//                        Toast.makeText(applicationContext, "Kode invalid !", Toast.LENGTH_LONG).show()
                    }
                }
    }

    fun formatE164Number(countryCode: String?, phNum: String?): String? {
        val e164Number: String?
        e164Number = if (TextUtils.isEmpty(countryCode)) {
            phNum
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                PhoneNumberUtils.formatNumberToE164(phNum, countryCode)
            } else {
                try {
                    val instance = PhoneNumberUtil.getInstance()
                    val phoneNumber = instance.parse(phNum, countryCode)
                    instance.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164)
                } catch (e: NumberParseException) {
                    Log.e(TAG, "Caught: " + e.message, e)
                    phNum
                }
            }
        }
        return e164Number
    }

    fun dummy(){
        preferences.edit().putBoolean(Constant.STATE_HAS_LOGGED_IN, true).apply()
        preferences.edit().putString(Constant.ACCOUNT_KODE_PLGN, intent.getStringExtra(Constant.ACCOUNT_KODE_PLGN)).apply()
        preferences.edit().putString(Constant.ACCOUNT_NAMA_USAHA, intent.getStringExtra(Constant.ACCOUNT_NAMA_USAHA)).apply()
        preferences.edit().putString(Constant.ACCOUNT_ID_PLGN, intent.getStringExtra(Constant.ACCOUNT_ID_PLGN)).apply()
        //SUBSCRIBE TO FCM

        val i = Intent(applicationContext, FirebaseInstanceService::class.java)
        startService(i)

        var kodeplgn = intent.getStringExtra(Constant.ACCOUNT_KODE_PLGN)
        topic = "/topics/$kodeplgn"
        FirebaseMessaging.getInstance().subscribeToTopic(topic)
        topic = "/topics/free_cell";
        FirebaseMessaging.getInstance().subscribeToTopic(topic)
        var intent = Intent(applicationContext, InputPinActivity::class.java)
        startActivity(intent)
        finish()
    }
}