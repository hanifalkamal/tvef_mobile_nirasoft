package com.alkamal.hanif.mobile.model

class Product(
        var kodeProduk: String,
        var shortDesc: String,
        var produkBanner: String
)