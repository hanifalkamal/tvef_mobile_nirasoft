package com.alkamal.hanif.mobile.form;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.widget.TextView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 25/02/2018.
 */

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.tv_nama)
    TextView tvNama;

    @BindView(R.id.tv_alamat_profile) TextView tvAlamat;

    @BindView(R.id.tv_id_plgn) TextView tvId;

    @BindView(R.id.tv_mastertrx) TextView tvMaster;

    private ArrayList<String> array_info = new ArrayList<String>() ;

    String  myID,pin,tanggal,idtrx,Server;
    int CekCounting,waktutimeout;
    NotificationCompat.Builder notification;
    Calendar calendar;
    SimpleDateFormat dateformat;
    private static final int uniqueID = 45612;
    ProgressDialog prd;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infoakun_layout);
        ButterKnife.bind(this);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);
        //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
        pin = prefs.getString("MY_PIN_TRX", "");
        Server = prefs.getString("MY_SERVER", "");
        myID = prefs.getString("MY_USERID","");

        String Format = "akun "+pin ;
        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu ...");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();

        sendReqSal(Format);

    }

    public void sendReqSal(String format){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;

        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx, Server, "");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(ProfileActivity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        getBalsan();

                        //finish();
                        log("Berhasil");

                    } else {
                        prd.dismiss();
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(ProfileActivity.this, "Gagal Load Data Cek Koneksi Internet", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void getBalsan(){
        Call<MsgResponse> call = getApi().GetMSg(myID,idtrx,Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {
                        Pattern p = Pattern.compile("\"(.*?)\" ");
                        Matcher m = p.matcher(ges.getPesan());

                        while (m.find()){
                            //for (int i=1;i<=m.groupCount();i++) {
                            array_info.add(m.group(1).replaceAll("\"",""));

                            // ListData = ListData.replaceAll("\"","");


                            //Toast.makeText(trxActivity.this, ListData.get(data) , Toast.LENGTH_LONG).show();
                            //blsn.setText(m.group(1));
                            //prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
//            SharedPreferences.Editor editor =prefs.edit();



                            //}
                        }

                        tvNama.setText(array_info.get(2));
                        tvAlamat.setText(array_info.get(3));
                        tvId.setText(array_info.get(0));
                        tvMaster.setText(array_info.get(1));

                        prd.dismiss();
                        //Intent intent = new Intent(ProfileActivity.this, BalasanActivity.class);
                        //intent.putExtra("CekSal",ges.getPesan());
                      //  Notif(ges.getPesan());
                        //startActivity(intent);

                        //Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_LONG).show();


                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                // yourMethod();
                                //5 seconds


                                //finish();
                                //tvSaldo.setText(ges.getPesan());

                                //finish();
                                log("Berhasil");
                            }
                        }, 1000);

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < waktutimeout){
                            getBalsan();
                            CekCounting = CekCounting+1;
                        }else{
                            prd.dismiss();
                           // Notif("Time Out / Tidak ada balasan masuk");
                            Toast.makeText(ProfileActivity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }

    public void Notif(String notifBody){
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker("This is the ticker");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("TVEF");
        notification.setContentText(notifBody);

        Context context = getApplicationContext();
        Intent intent = new Intent(context, BalasanActivity.class);
        intent.putExtra("CekSal",notifBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());

    }
}
