package com.alkamal.hanif.mobile.form;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.adapter.Product;
import com.alkamal.hanif.mobile.adapter.ProductListAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 01/03/2018.
 */

public class cekHargaActivity extends BaseActivity{
    private ArrayList<String> balasanCekHarga = new ArrayList<String>() ;
    private ListView lvProduct;
    private ProductListAdapter adapter;
    private List<Product>  mProductList;
    private SharedPreferences prefs;
    String  myID,pin,tanggal,idtrx;
    int CekCounting,waktutimeout;
    NotificationCompat.Builder notification;
    Calendar calendar;
    SimpleDateFormat dateformat;
    private static final int uniqueID = 45612;
    ProgressDialog prd;
    String pilihan;
    String Server;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cek_harga);
        //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
        pin = prefs.getString("MY_PIN_TRX", "");
        Server = prefs.getString("MY_SERVER", "");
        myID = prefs.getString("MY_USERID","");

        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu ...");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();

        Intent intent = getIntent();

        String setProvider = intent.getStringExtra("pilihProvider");
        switch (setProvider){
            case "1":
                pilihan = "xl";
                break;
            case "2":
                pilihan = "s";
                break;
            case "3":
                pilihan = "t";
                break;
            case "4":
                pilihan = "i";
                break;
            case "5":
                pilihan = "sm";
                break;
        }
        String Format = "harga "+pin+" "+pilihan ;
        sendReqSal(Format);


        lvProduct = (ListView)findViewById(R.id.lv_harga);
        lvProduct.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), "Clicked ID = "+view.getTag(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    public void sendReqSal(String format){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;

        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx,Server, "nonotif");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(cekHargaActivity.this, res.getMessage(), Toast.LENGTH_LONG).show();

                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        getBalsan();

                        //finish();
                        log("Berhasil");

                    } else {
                        prd.dismiss();
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(cekHargaActivity.this, "Gagal Load Data Cek Koneksi Internet", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void getBalsan(){
        Call<MsgResponse> call = getApi().GetMSg(myID,idtrx,Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {
                        prefs=getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt("NOTIF", 0);
                        editor.apply();

                        Pattern p = Pattern.compile("\"(.*?)\" ");
                        Matcher m = p.matcher(ges.getPesan());
                        int data = 0;
                        int indexListView = 1;
                        while (m.find()){
                            balasanCekHarga.add(m.group(1));
                        }

                        lvProduct = (ListView)findViewById(R.id.lv_harga);

                        mProductList = new ArrayList<>();

                        while (data != balasanCekHarga.size()){
                         /*   endString = balasanCekHarga.get(data);
                            if (endString.endsWith("5")){
                                keterangan = pilihan +" 5000";
                            } else
                                if (endString.endsWith("10")){
                                    keterangan = pilihan +" 10000";
                                } else
                                    if (endString.endsWith("20")){
                                        keterangan = pilihan +" 20000";
                                    }   else
                                        if (endString.endsWith("50")){
                                            keterangan = pilihan +" 50000";
                                        }   else
                                                if (endString.endsWith("00")){
                                                    keterangan = pilihan +" 100000";
                                                }   else
                                                        if (endString.endsWith("1")){
                                                            keterangan = pilihan +" 1000";
                                                        }*/
                            mProductList.add(new Product(indexListView, pilihan.toUpperCase() , Integer.parseInt(balasanCekHarga.get(data+1)), balasanCekHarga.get(data)));
                            data = data +2;
                            indexListView = indexListView +1;
                        }

                        adapter = new ProductListAdapter(getApplicationContext(), mProductList);
                        lvProduct.setAdapter(adapter);



                        prd.dismiss();
                        //Intent intent = new Intent(ProfileActivity.this, BalasanActivity.class);
                        //intent.putExtra("CekSal",ges.getPesan());
                        //  Notif(ges.getPesan());
                        //startActivity(intent);

                        //Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_LONG).show();


                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                // yourMethod();
                                //5 seconds


                                //finish();
                                //tvSaldo.setText(ges.getPesan());

                                //finish();
                                log("Berhasil");
                            }
                        }, 1000);

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < waktutimeout){
                            getBalsan();
                            CekCounting = CekCounting+1;
                        }else{
                            prd.dismiss();
                            // Notif("Time Out / Tidak ada balasan masuk");
                            Toast.makeText(cekHargaActivity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }
}
