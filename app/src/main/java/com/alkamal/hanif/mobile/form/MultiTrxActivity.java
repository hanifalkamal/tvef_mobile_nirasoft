package com.alkamal.hanif.mobile.form;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.MultiTrxRespone;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.adapter.MultiTrx;
import com.alkamal.hanif.mobile.adapter.multiTrxAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MultiTrxActivity extends BaseActivity {

    private static ListView lvMultiTrx;
    private multiTrxAdapter adapter;
    private static List<MultiTrx> mTrxList;
    private static String  pilihanTrx,myID,pin,tanggal,idtrx,Server,idtrxkirim;
    int count;
    int CekCounting,waktutimeout;
    NotificationCompat.Builder notification;
    Calendar calendar;
    SimpleDateFormat dateformat;
    private ArrayList<String> array_JenisTrx = new ArrayList<String>() ;

    private static final int uniqueID = 45612;
    ProgressDialog prd;
    private SharedPreferences prefs;
    private Boolean SudahReq = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_trx);
        ButterKnife.bind(this);

        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        pin = prefs.getString("MY_PIN_TRX", "");
        Server = prefs.getString("MY_SERVER", "");
        count = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT",""));
        myID = prefs.getString("MY_USERID","");

        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu ...");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();

        lvMultiTrx = (ListView)findViewById(R.id.lv_jenis_trx);
        mTrxList = new ArrayList<>();
        sendReqSal();
        lvMultiTrx.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getApplicationContext(), "Clicked ID = "+view.getTag(), Toast.LENGTH_SHORT).show();
              int pilihan = Integer.parseInt(view.getTag().toString());
              //Log.d("Jum Array",""+array_JenisTrx.get(5));
              String kodeBrg = array_JenisTrx.get(pilihan).toString(); //kode
              pilihanTrx = array_JenisTrx.get(pilihan-1).toString(); //judul

              Log.d("Milih Kode BRG",""+kodeBrg);
              ReqDataBrg(kodeBrg);
            }
        });


    }


    public void sendReqSal(){

        Call<MultiTrxRespone> call = getApi().GetJenisTrx("1");
        call.enqueue(new Callback<MultiTrxRespone>() {
            @Override
            public void onResponse(Call<MultiTrxRespone> call, Response<MultiTrxRespone> response) {
                if (response.isSuccessful()) {
                    MultiTrxRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(MultiTrxActivity.this, "Load Data Berhasil", Toast.LENGTH_LONG).show();

                        Log.d("Multi Trx", "ter Respone");
                        Pattern p = Pattern.compile("\"(.*?)\"");
                        Matcher m = p.matcher(res.getJenisTrx());
                        Log.d("getJenisTrx ",res.getJenisTrx().toString());
                        int indexListView = 1;
                        //lvMultiTrx = (ListView)findViewById(R.id.lv_jenis_trx);


                        while (m.find()){
                            array_JenisTrx.add(m.group(1));
                            Log.d("getData :",m.group(1).toString());
                            if(indexListView%2 != 0) {
                                mTrxList.add(new MultiTrx(indexListView, m.group(1).toString()));
                            }
                            indexListView = indexListView +1;
                        }

                        adapter = new multiTrxAdapter(getApplicationContext(), mTrxList);
                        lvMultiTrx.setAdapter(adapter);
                        prd.dismiss();

                        log("Berhasil");

                    } else {
                        prd.dismiss();
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(MultiTrxActivity.this, "Gagal Load Data Cek Koneksi Internet", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<MultiTrxRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void ReqDataBrg(String kodebrg){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;
        String format = "databrg "+pin+ " "+kodebrg;
        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx, Server, "nonotif");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        prd = new ProgressDialog(MultiTrxActivity.this);
                        prd.setMessage("Mohon Tunggu ...");
                        prd.setCancelable(false);
                        prd.setCanceledOnTouchOutside(false);

                        prd.show();
                        Toast.makeText(MultiTrxActivity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();

                        getBalsan();

                        //finish();
                        log("Berhasil kirim");

                    } else {
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(MultiTrxActivity.this, " Gagal", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {
                log(t.toString());
                log("Failure Send Data");
            }
        });
    }

    public void getBalsan(){
        idtrxkirim = idtrx;
        Call<MsgResponse> call = getApi().GetMSg(myID,idtrxkirim, Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();

                    if (ges.IsSuccess()) {
                        //Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_LONG).show();
                        prd.dismiss();
                        prefs=getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("LAST_ID",idtrxkirim);
                        editor.putInt("NOTIF", 0);
                        editor.apply();
                        // editor.apply();

                        Intent intent = new Intent(MultiTrxActivity.this, trxActivity.class);
                        intent.putExtra("CekSal2",ges.getPesan());
                        intent.putExtra("judul","Transaksi "+pilihanTrx);
                        pilihanTrx = pilihanTrx.toLowerCase();
                        intent.putExtra("gambarProvider",pilihanTrx);
                        prd.hide();
                        startActivity(intent);

                        log("Berhasil terima");

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < count){
                            getBalsan();
                            CekCounting = CekCounting+1;
                            log("loop");
                        }else{
                            prd.dismiss();
                            Toast.makeText(MultiTrxActivity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {
                log(t.toString());
                log("Failure Get Balasan");
            }
        });
    }
}
