package com.alkamal.hanif.mobile.chat.Notification;

import com.alkamal.hanif.mobile.chat.Fragment.APIService;

public class Common {
    public static String currentToken = "";

    private static String baseUrl = "https://fcm.googleapis.com/";

    public static APIService getFCMClient()
    {
        return RetrofitClient.getClient(baseUrl).create(APIService.class);
    }
}
