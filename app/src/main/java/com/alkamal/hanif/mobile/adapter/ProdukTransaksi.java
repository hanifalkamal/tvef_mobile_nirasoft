package com.alkamal.hanif.mobile.adapter;

public class ProdukTransaksi {

    private String id;
    private String nama_produk;
    private String kode_produk;
    private String icon_url;
    private String urutan;
    private String editable;
    private String logo_produk;
    private String hint;


    public ProdukTransaksi(String id, String nama_produk, String kode_produk, String icon_url, String urutan, String editable, String logo_produk) {
        this.id = id;
        this.nama_produk = nama_produk;
        this.kode_produk = kode_produk;
        this.icon_url = icon_url;
        this.urutan = urutan;
        this.editable = editable;
        this.logo_produk = logo_produk;
    }

    public ProdukTransaksi(String id, String nama_produk, String kode_produk, String icon_url, String urutan, String editable, String logo_produk, String hint) {
        this.id = id;
        this.nama_produk = nama_produk;
        this.kode_produk = kode_produk;
        this.icon_url = icon_url;
        this.urutan = urutan;
        this.editable = editable;
        this.logo_produk = logo_produk;
        this.hint = hint;
    }

    public ProdukTransaksi() {
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public ProdukTransaksi(String id) {
        this.id = id;
    }

    public String getLogo_produk() {
        return logo_produk;
    }

    public void setLogo_produk(String logo_produk) {
        this.logo_produk = logo_produk;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_produk() {
        return nama_produk;
    }

    public void setNama_produk(String nama_produk) {
        this.nama_produk = nama_produk;
    }

    public String getKode_produk() {
        return kode_produk;
    }

    public void setKode_produk(String kode_produk) {
        this.kode_produk = kode_produk;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public String getUrutan() {
        return urutan;
    }

    public void setUrutan(String urutan) {
        this.urutan = urutan;
    }

    public String getEditable() {
        return editable;
    }

    public void setEditable(String editable) {
        this.editable = editable;
    }
}
