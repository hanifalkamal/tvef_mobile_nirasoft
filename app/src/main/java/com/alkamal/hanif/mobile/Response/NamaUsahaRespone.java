package com.alkamal.hanif.mobile.Response;

public class NamaUsahaRespone {
    private String an_pengirim,no_telp,namaUsaha;
    private Boolean success;

    public NamaUsahaRespone(String an_pengirim, String no_telp, Boolean success) {
        this.an_pengirim = an_pengirim;
        this.no_telp = no_telp;
        this.success = success;
    }

    public NamaUsahaRespone(String an_pengirim, String no_telp, String namaUsaha, Boolean success) {
        this.an_pengirim = an_pengirim;
        this.no_telp = no_telp;
        this.namaUsaha = namaUsaha;
        this.success = success;
    }

    public NamaUsahaRespone() {
    }

    public String getNamaUsaha() {
        return namaUsaha;
    }

    public void setNamaUsaha(String namaUsaha) {
        this.namaUsaha = namaUsaha;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getAn_pengirim() {
        return an_pengirim;
    }

    public void setAn_pengirim(String an_pengirIm) {
        this.an_pengirim = an_pengirIm;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
