package com.alkamal.hanif.mobile.form.Login

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.model.ChangePasswordResponse
import kotlinx.android.synthetic.main.activity_ganti_psw.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class ChangePswActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ganti_psw)

        button_back.setOnClickListener {
            onBackPressed()
        }

        bt_Send_GantiPsw.setOnClickListener {
            changePSw()
        }

    }

    fun changePSw(){
        progressBar.visibility = View.VISIBLE
        bt_Send_GantiPsw.visibility = View.GONE
        try{
            if (ed_noTelp_ganti.text.isNullOrEmpty()){
                throw Exception("Nomor telepon harus di isi !")
            } else if (ed_psw_lama.text.isNullOrEmpty()) {
                throw Exception("Password lama harus di isi !")
            } else if (ed_psw.text.isNullOrEmpty()) {
                throw Exception("Password baru harus di isi !")
            } else if (ed_psw_conf.text.isNullOrEmpty()) {
                throw Exception("Konfirmasi password baru harus di isi !")
            } else if (!ed_psw_conf.text.toString().equals(ed_psw.text.toString())) {
                throw Exception("Konfirmasi password baru tidak sama !")
            } else if (ed_psw_conf.text.toString().equals(ed_psw_lama.text.toString())) {
                throw Exception("Password baru tidak boleh sama dengan password lama !")
            } else {
                var call = api.changePassword(
                        encodeAndReverse(edittext_prefix.text.toString()+ed_noTelp_ganti.text.toString()),
                        encodeAndReverse(ed_psw_lama.text.toString()),
                        encodeAndReverse(ed_psw.text.toString())
                )
                call.enqueue(object : Callback<ChangePasswordResponse>{
                    override fun onFailure(call: Call<ChangePasswordResponse>, t: Throwable) {
                        Toast.makeText(applicationContext, "Internal Server Error", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<ChangePasswordResponse>, response: Response<ChangePasswordResponse>) {
                        try{
                            var data = response.body();

                            if (data != null){

                                if (data.success){
                                    Toast.makeText(applicationContext, data.message, Toast.LENGTH_SHORT).show()
                                    finish()
                                } else {
                                    throw Exception(data.message)
                                }

                            }
                        }catch (e: Exception){
                            text_warning.visibility = View.VISIBLE
                            bt_Send_GantiPsw.visibility = View.VISIBLE
                            progressBar.visibility = View.GONE
                            text_warning.setText(e.message)
                            e.printStackTrace()
                        }
                    }
                })
            }
        }catch (e : Exception){
            text_warning.visibility = View.VISIBLE
            bt_Send_GantiPsw.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            text_warning.setText(e.message)
            e.printStackTrace()
        }


    }

}