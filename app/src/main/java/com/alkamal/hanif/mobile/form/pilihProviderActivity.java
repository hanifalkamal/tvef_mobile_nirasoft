package com.alkamal.hanif.mobile.form;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.model.Provider;
import com.alkamal.hanif.mobile.utils.Constant;
import com.alkamal.hanif.mobile.utils.SpringBaseActivity;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 15/02/2018.
 */

public class pilihProviderActivity extends BaseActivity {
    Calendar calendar;
    SimpleDateFormat dateformat;
    String tanggal,idtrx,pin,myID,format,Server;
    //SharedPreferences prefs;
    int CekCounting,count;
    ProgressDialog prd;
    String pilihan,idtrxkirim;
    private SharedPreferences prefs;

    NotificationCompat.Builder notification;

    @BindView(R.id.tv_pilihanTrx)
    TextView tvPilihan;
    String provider;
    String provider2 = "";
    @BindView(R.id.ed_noTelpPilihProvider) EditText edNotelp;
    @BindView(R.id.ed_nominal_saldo)
    EditText edNominalSaldo;
    @BindView(R.id.ed_id_transfer)
    EditText edIdTransfer;
    @BindView(R.id.bt_pilihProviderLanjut)
    Button btLanjut;

    @BindView(R.id.chechkBoxTransferSaldo)
    CheckBox cbTransferSaldo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pilih_provider_trx);
        ButterKnife.bind(this);
        //myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);

        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        pin = prefs.getString("MY_PIN_TRX", "");
        Server = prefs.getString("MY_SERVER", "");
        count = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT",""));
        myID = prefs.getString("MY_USERID","");

        Intent intent = getIntent();
        pilihan = intent.getStringExtra("pilihan");

        if (pilihan.equals("pulsa")){
            tvPilihan.setText("ISI PULSA");
        } else if (pilihan.equals("data")){
            tvPilihan.setText("Data Kuota");
        } else if(pilihan.equals("transferpulsa")){
            tvPilihan.setText("Transfer Saldo");
            edNominalSaldo.setVisibility(View.VISIBLE);
            btLanjut.setText("TRANSFER");
//            rbTransferSaldo.setVisibility(View.VISIBLE);
            cbTransferSaldo.setVisibility(View.VISIBLE);


        }

        cbTransferSaldo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (cbTransferSaldo.isChecked() == true){
                    edIdTransfer.setVisibility(View.VISIBLE);
                    btLanjut.setText("RALAT");
                } else {
                    edIdTransfer.setVisibility(View.GONE);
                    btLanjut.setText("TRANSFER");
                }
            }
        });


        edNotelp.addTextChangedListener(new TextWatcher(){

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (("" + edNotelp.getText().charAt(0)).equals("0")) {
                        edNotelp.setText(edNotelp.getText().toString().substring(1));
                        //edNotelp.append("");

                    } else if(("" + edNotelp.getText().charAt(0)+"" + edNotelp.getText().charAt(1)).equals("62")) {
                        edNotelp.setText(edNotelp.getText().toString().substring(2));
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        edNominalSaldo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edNominalSaldo.removeTextChangedListener(this);
                try{

                    String givenstring = s.toString();
                    Long longval = null;
                    if (givenstring.contains(",")) {
                        givenstring = givenstring.replace(",", "");
                    }
                    longval = java.lang.Long.parseLong(givenstring);
                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    String formattedString =  formatter.format(longval);
                    edNominalSaldo.setText(formattedString);
                    edNominalSaldo.setSelection(edNominalSaldo.getText().length());

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                edNominalSaldo.addTextChangedListener(this);
            }

            @Override
            public void afterTextChanged(Editable view) {

            }
        });


    }

    private final int PICK_CONTACT = 1;
    @OnClick(R.id.bt_Contacts) public void onContacts(){
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_CONTACT) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                Uri ContactData = data.getData();
                Cursor C = getContentResolver().query(ContactData, null, null, null, null);

                if (C.moveToFirst()) {
                    String name = C.getString(C.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
                    ContentResolver cr = getContentResolver();
                    Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                            "DISPLAY_NAME = '" + name + "'", null, null);
                    if (cursor.moveToFirst()) {
                        String contactId =
                                cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (phones.moveToNext()) {
                            String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                            switch (type) {
                                case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                    // do something with the Mobile number here...
                                    String plusEmanDua = ""+number.charAt(0)+number.charAt(1)+number.charAt(2);
                                    String EnamDua = ""+number.charAt(0)+number.charAt(1);
                                    String nol = ""+number.charAt(0);

                                    String noTelpTrx = "";
                                    if (nol.equals("0")){
                                        noTelpTrx = number.substring(1);
                                    } else if (EnamDua.equals("62")){
                                        noTelpTrx = number.substring(2);
                                    } else if (plusEmanDua.equals("+62")){
                                        noTelpTrx = number.substring(3);
                                    }
                                    edNotelp.setText(noTelpTrx);
                                    break;
                            }
                        }

                    }
                }
            }
        }
    }

    

    @OnClick (R.id.bt_pilihProviderLanjut) public void onPilihProviderLanjut(){
        if(pilihan.equals("transferpulsa")){
            if (edNotelp.getText().length() < 1){

                Toast.makeText(this, "No Telepon Tidak Boleh kosong", Toast.LENGTH_SHORT).show();

            } else if (edNominalSaldo.getText().length() < 1 ){

                Toast.makeText(this, "Nominal Saldo Tidak Boleh kosong", Toast.LENGTH_SHORT).show();

            } else {
                if (btLanjut.getText().equals("TRANSFER")) {
                    sendReqSal("ts " + pin + " " + edNominalSaldo.getText().toString().replace(",","").replace(".","") + " 62" + edNotelp.getText());
                } else {
                    if (edIdTransfer.getText().length() < 1) {
                        Toast.makeText(this, "ID Transfer Pulsa Tidak Boleh kosong", Toast.LENGTH_SHORT).show();
                    } else {
                        sendReqSal("tr " + pin + " " + edNominalSaldo.getText().toString().replace(",","").replace(".","") + " 62" + edNotelp.getText() +" "+edIdTransfer.getText());
                    }
                }

                prd = new ProgressDialog(this);
                prd.setMessage("Mohon Tunggu. . .");
                prd.setCancelable(false);
                prd.setCanceledOnTouchOutside(false);

                prd.show();
            }
        } else {
            Boolean nomorValid = true;
            String NoAwal = "62" + edNotelp.getText().charAt(0) + edNotelp.getText().charAt(1) + edNotelp.getText().charAt(2);
            //Simpati
            if (NoAwal.equals("62813") || NoAwal.equals("62812") || NoAwal.equals("62811") || NoAwal.equals("62852")
                    || NoAwal.equals("62853") || NoAwal.equals("62821") || NoAwal.equals("62822") || NoAwal.equals("62823")
                    || NoAwal.equals("62826") || NoAwal.equals("62824") || NoAwal.equals("62827") || NoAwal.equals("62828")
                    || NoAwal.equals("62851") || NoAwal.equals("62829")) {

                if (pilihan.equals("pulsa")) {
                    format = "databrg " + pin + " simpati";
                } else if (pilihan.equals("data")) {
                    format = "databrg " + pin + " dasimpati";
                }

                provider = "telkomsel";
                //Three
            } else if (NoAwal.equals("62899") || NoAwal.equals("62898") || NoAwal.equals("62896") || NoAwal.equals("62897")
                    || NoAwal.equals("62895") || NoAwal.equals("62865") || NoAwal.equals("62894") || NoAwal.equals("62893")
                    || NoAwal.equals("62891") || NoAwal.equals("62892") || NoAwal.equals("62899")) {

                if (pilihan.equals("pulsa")) {
                    format = "databrg " + pin + " three";
                } else if (pilihan.equals("data")) {
                    format = "databrg " + pin + " k";
                }

                provider = "three";

                //Indosat
            } else if (NoAwal.equals("62856") || NoAwal.equals("62857") || NoAwal.equals("62858") || NoAwal.equals("62815")
                    || NoAwal.equals("62816") || NoAwal.equals("62814") || NoAwal.equals("62855") || NoAwal.equals("62854")) {

                if (pilihan.equals("pulsa")) {
                    format = "databrg " + pin + " im3";
                } else if (pilihan.equals("data")) {
                    format = "databrg " + pin + " id";
                }
                provider = "indosat";
                //XL / Axis
            } else if (NoAwal.equals("62818") || NoAwal.equals("62817") || NoAwal.equals("62819") || NoAwal.equals("62859")
                    || NoAwal.equals("62878") || NoAwal.equals("62877") || NoAwal.equals("62833") || NoAwal.equals("62872")
                    || NoAwal.equals("62879") || NoAwal.equals("62834") || NoAwal.equals("62838") || NoAwal.equals("62875")
                    || NoAwal.equals("62831") || NoAwal.equals("62832") || NoAwal.equals("62835")) {

                if (pilihan.equals("pulsa")) {
                    format = "databrg " + pin + " xl";
                    provider = "xl";
                    provider2 = "axis";
                } else if (pilihan.equals("data")) {

                    int lengthNomor = edNotelp.length()+2;


//                if (NoAwal.equals("628591") || NoAwal.equals("628592") || NoAwal.equals("628593") || NoAwal.equals("628594")) {
//
//                }
                    if (lengthNomor == 14) {
                        format = "databrg " + pin + " bro";
                        provider = "axis";
                    } else {
                        if (NoAwal.equals("62818") || NoAwal.equals("62817") || NoAwal.equals("62819") || NoAwal.equals("62859") || NoAwal.equals("62872")
                                || NoAwal.equals("62873") || NoAwal.equals("62874") || NoAwal.equals("62875") || NoAwal.equals("62876") || NoAwal.equals("62877")
                                || NoAwal.equals("62878")|| NoAwal.equals("62879")) {

                            format = "databrg " + pin + " ih";
                            provider = "xl";

                        } else if(NoAwal.equals("62831") || NoAwal.equals("62832") || NoAwal.equals("62833") || NoAwal.equals("62834") || NoAwal.equals("62835")
                                || NoAwal.equals("62836") || NoAwal.equals("62837") || NoAwal.equals("62838")) {

                            format = "databrg " + pin + " bro";
                            provider = "axis";

                        }
                    }
                    //else


                }

                //Smart
            } else if (NoAwal.equals("628881") || NoAwal.equals("62888") || NoAwal.equals("62882") || NoAwal.equals("62883")
                    || NoAwal.equals("62889") || NoAwal.equals("62880") || NoAwal.equals("62887") || NoAwal.equals("62884")
                    || NoAwal.equals("62885") || NoAwal.equals("62886") || NoAwal.equals("62881")) {

                if (pilihan.equals("pulsa")) {
                    format = "databrg " + pin + " smart";
                } else if (pilihan.equals("data")) {
                    format = "databrg " + pin + " smd";
                }
                provider = "smartfren";

            } else {
                Toast.makeText(this, "Nomor yang anda masukan salah !", Toast.LENGTH_SHORT).show();
                nomorValid = false;
            }

            if (nomorValid) {
                //REAL
//                sendReqSal(format);
                //DUMMY
                getImageUrl();
//                prefs=getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
//                SharedPreferences.Editor editor = prefs.edit();
//                editor.putString("LAST_ID",idtrxkirim);
//                //editor.putString("NOTELP", edNotelp.getText().toString());
//                editor.putInt("NOTIF", 0);
//                editor.apply();
//                Intent intent = new Intent(pilihProviderActivity.this, trxActivity.class);
//                intent.putExtra("VPS","true");
//                if (pilihan.equals("pulsa")){
//                    intent.putExtra("judul","Transaksi Isi Pulsa");
//                    intent.putExtra("jenis","PULSA");
//                } else if (pilihan.equals("data")){
//                    intent.putExtra("judul","Transaksi Paket Data");
//                    intent.putExtra("jenis","DATA");
//                }
//                if(!pilihan.equals("transferpulsa")) {
//                    intent.putExtra("notelp", "62" + edNotelp.getText().toString());
//                    intent.putExtra("gambarProvider", provider);
//                    intent.putExtra("gambarProvider2", provider2);
//                    intent.putExtra(Constant.INSTANCE.getBUNDLE_TRX_ISI_PULSA(),"true");
//
//                }
//                startActivity(intent);
                //---
//                prd = new ProgressDialog(this);
//                prd.setMessage("Mohon Tunggu. . .");
//                prd.setCancelable(false);
//                prd.setCanceledOnTouchOutside(false);
//
//                prd.show();
            }
        }
    }

    @OnClick (R.id.bt_telkomsel) public void onTelkomsel(){
        if (pilihan.equals("pulsa")){
            format = "databrg "+pin+" simpati";
        } else if (pilihan.equals("data")){
            format = "databrg "+pin+" dasimpati";
        }

        sendReqSal(format);

        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
    }

    @OnClick (R.id.bt_xl) public void onXl(){
        if (pilihan.equals("pulsa")){
            format = "databrg "+pin+" xl";
        } else if (pilihan.equals("data")){
            format = "databrg "+pin+" ih";
        }
        sendReqSal(format);

        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
    }

    @OnClick (R.id.bt_three) public void onThree(){

        if (pilihan.equals("pulsa")){
            format = "databrg "+pin+" three";
        } else if (pilihan.equals("data")){
            format = "databrg "+pin+" k";
        }

        sendReqSal(format);

        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
    }

    @OnClick (R.id.bt_indosat) public void onIndosat(){
        if (pilihan.equals("pulsa")){
            format = "databrg "+pin+" im3";
        } else if (pilihan.equals("data")){
            format = "databrg "+pin+" id";
        }
        sendReqSal(format);

        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
    }

    @OnClick (R.id.bt_axis) public void onAxis(){
        if (pilihan.equals("pulsa")){
            format = "databrg "+pin+" xl";
        } else if (pilihan.equals("data")){
            format = "databrg "+pin+" bro";
        }

        sendReqSal(format);

        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
    }

    @OnClick (R.id.bt_smartfren) public void onSmartfren(){
        if (pilihan.equals("pulsa")){
            format = "databrg "+pin+" smart";
        } else if (pilihan.equals("data")){
            format = "databrg "+pin+" smd";
        }

        sendReqSal(format);

//        prd = new ProgressDialog(this);
//        prd.setMessage("Mohon Tunggu. . .");
//        prd.setCancelable(false);
//        prd.setCanceledOnTouchOutside(false);
//
//        prd.show();
    }

    private static final String TAG = "pilihProviderActivity";

    public void getImageUrl(){

        SpringBaseActivity springBaseActivity = new SpringBaseActivity();
        Call<Provider> call = springBaseActivity.getApi().getImageByPrefix("62" + edNotelp.getText().toString());
        call.enqueue(new Callback<Provider>() {
            @Override
            public void onResponse(Call<Provider> call, Response<Provider> response) {

                Log.d(TAG, "onResponse: " + response.body());

                Provider provider1 = response.body();

                if (provider1.getResponseMessage() != null) {
                    Toast.makeText(pilihProviderActivity.this, provider1.getResponseMessage(), Toast.LENGTH_SHORT).show();
                } else {

                    prefs = getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("LAST_ID", idtrxkirim);
                    //editor.putString("NOTELP", edNotelp.getText().toString());
                    editor.putInt("NOTIF", 0);
                    editor.apply();
                    Intent intent = new Intent(pilihProviderActivity.this, trxActivity.class);
                    intent.putExtra("VPS", "true");
                    intent.putExtra("logoUrl", Constant.BaseUrl + provider1.getImageUrl());
                    if (pilihan.equals("pulsa")) {
                        intent.putExtra("judul", "Transaksi Isi Pulsa");
                        intent.putExtra("jenis", "PULSA");
                    } else if (pilihan.equals("data")) {
                        intent.putExtra("judul", "Transaksi Paket Data");
                        intent.putExtra("jenis", "DATA");
                    }
                    if (!pilihan.equals("transferpulsa")) {
                        intent.putExtra("notelp", "62" + edNotelp.getText().toString());
                        intent.putExtra("gambarProvider", "fromListProduk");
//                    intent.putExtra("gambarProvider", provider);
//                    intent.putExtra("gambarProvider2", provider2);
                        intent.putExtra(Constant.INSTANCE.getBUNDLE_TRX_ISI_PULSA(), "true");

                    }
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<Provider> call, Throwable t) {

            }
        });



    }

    public void sendReqSal(String format){
        Log.d(TAG, "sendReqSal: FORMAT "+format);
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;
        Call<SendMsgRespone> call = null;
        if(pilihan.equals("transferpulsa")){
            call = getApi().SendMSg(myID, format, idtrx, Server, "");
        }else {
            call = getApi().SendMSg(myID, format, idtrx, Server, "nonotif,JSON");
        }
        
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(pilihProviderActivity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();

                        getBalsan();

                        //finish();
                        log("Berhasil kirim");

                    } else {
                        //tvMsg.setText(res.getMessage());
                        Toast.makeText(pilihProviderActivity.this, "Login Gagal", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {
                log(t.toString());
                log("Failure Send Data");
            }
        });
    }

    public void getBalsan(){
        idtrxkirim = idtrx;
        Call<MsgResponse> call = getApi().GetMSg(myID,idtrxkirim, Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();

                    if (ges.IsSuccess()) {
                        //Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_LONG).show();
                        prd.dismiss();
                        prefs=getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("LAST_ID",idtrxkirim);
                        //editor.putString("NOTELP", edNotelp.getText().toString());
                        editor.putInt("NOTIF", 0);
                        editor.apply();
                       // editor.apply();

                        Intent intent = new Intent(pilihProviderActivity.this, trxActivity.class);
                        intent.putExtra("CekSal2",ges.getPesan());

                        if (pilihan.equals("pulsa")){
                            intent.putExtra("judul","Transaksi Isi Pulsa");
                        } else if (pilihan.equals("data")){
                            intent.putExtra("judul","Transaksi Paket Data");
                        }
                        if(!pilihan.equals("transferpulsa")) {
                            intent.putExtra("notelp", "62" + edNotelp.getText().toString());
                            intent.putExtra("gambarProvider", provider);
                            intent.putExtra("gambarProvider2", provider2);
                            intent.putExtra(Constant.INSTANCE.getBUNDLE_TRX_ISI_PULSA(),"true");
                            startActivity(intent);
                        } else {
                            Intent i = new Intent(pilihProviderActivity.this, BalasanActivity.class);
                            i.putExtra("CekSal",ges.getPesan());
                            startActivity(i);
                        }

                        log("Berhasil terima");

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < count){
                            getBalsan();
                            CekCounting = CekCounting+1;
                            log("loop");
                        }else{
                            prd.dismiss();
                            Toast.makeText(pilihProviderActivity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {
                log(t.toString());
                log("Failure Get Balasan");
            }
        });
    }

}
