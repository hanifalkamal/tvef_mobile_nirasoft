package com.alkamal.hanif.mobile.Response;

/**
 * Created by HanifAlKamal on 07/12/2017.
 */

public class SendMsgRespone {
    private String kodeplgn,pesan,idtrx,message,namaserver,tipedata;
    private Boolean success ;

    public String getTipedata() {
        return tipedata;
    }

    public void setTipedata(String tipedata) {
        this.tipedata = tipedata;
    }

    public String getNamaserver() {
        return namaserver;
    }

    public void setNamaserver(String namaserver) {
        this.namaserver = namaserver;
    }

    public String getIdtrx() {
        return idtrx;
    }

    public void setIdtrx(String idtrx) {
        this.idtrx = idtrx;
    }

    public String getKodeplgn() {
        return kodeplgn;
    }

    public void setKodeplgn(String kodeplgn) {
        this.kodeplgn = kodeplgn;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        success = success;
    }
}
