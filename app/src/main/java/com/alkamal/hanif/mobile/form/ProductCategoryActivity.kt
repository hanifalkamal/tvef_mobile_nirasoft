package com.alkamal.hanif.mobile.form

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.adapter.ListProductAdapter
import com.alkamal.hanif.mobile.model.ListProduct
import com.alkamal.hanif.mobile.model.Product
import com.alkamal.hanif.mobile.model.ProductCategory
import com.alkamal.hanif.mobile.model.barangresponse.Data
import com.alkamal.hanif.mobile.utils.Constant
import com.alkamal.hanif.mobile.utils.SpringBaseActivity
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_product_type.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ProductCategoryActivity : BaseActivity() {

    var listProduk : ListProduct? = null
    var adapter : ListProductAdapter? = null
    var calendar : Calendar? = null
    var dateformat : SimpleDateFormat? = null
    var tanggal: String = ""
    var idtrx : String = ""
    var myID : String = ""
    var Server : String = ""
    var myNumber : String = ""
    var CekCounting : Int = 0
    var format = ""
    var count = 0
    var prefs : SharedPreferences? = null
    var baseActivity : SpringBaseActivity? = null


    private var array_data = ArrayList<String>()
    private var array_keterangan = ArrayList<String>()
    private var array_harga = ArrayList<String>()
    private var array_kodebrg = ArrayList<String>()
    private var array_banner = ArrayList<String>()
    var data : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_type)
        prefs = getSharedPreferences("MY_DATA", Context.MODE_PRIVATE)
        var getPinTrx = prefs!!.getString("MY_PIN_TRX", "1234")
        prefs = getSharedPreferences("MY_DATA", Context.MODE_PRIVATE)
        myID = prefs!!.getString("MY_USERID", "").toString()
        myNumber = prefs!!.getString("MY_NUMBER", "").toString()
        Server = prefs!!.getString("MY_SERVER", "").toString()
        format = "databrg " + getPinTrx
        count = prefs!!.getString("MY_TRX_TIME_OUT", "")!!.toInt()



        GetProductPic();

    }

    fun GetProductPic(){
        baseActivity = SpringBaseActivity()
        var jsonObject = JsonObject()
        jsonObject.addProperty("produk", intent.getStringExtra(Constant.BUNDLE_PRODUK_KATEGORI))
        val call: Call<List<ProductCategory>> = baseActivity!!.api.getProductByProductCategory(jsonObject)
        call.enqueue(object : Callback<List<ProductCategory>?>{
            override fun onFailure(call: Call<List<ProductCategory>?>, t: Throwable) {
                Toast.makeText(applicationContext,"Failure",Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<List<ProductCategory>?>, response: Response<List<ProductCategory>?>) {
                if(response.code() == 200) {
                    var listRespProduct = response.body()
                    DecomposeDataBrg(listRespProduct!!)
                } else {
                    Toast.makeText(applicationContext,"Error Code : "+response.code(),Toast.LENGTH_SHORT).show()
                }
            }

        })
    }

    fun DecomposeDataBrg(listPicProduct : List<ProductCategory>){
        var getAllProduct : ArrayList<Product> = ArrayList<Product>()
        array_data = ArrayList()
        array_keterangan = ArrayList()
        array_harga = ArrayList()
        array_kodebrg = ArrayList()
        array_banner = ArrayList()

        val g = Gson()
//        val listBarang: ListBarang = g.fromJson<ListBarang>(intent.getStringExtra(Constant.BUNDLE_KODE_PRODUK), ListBarang::class.java)
//        val listData = listBarang.data
//        println("JSON DATA = "+intent.getStringExtra(Constant.BUNDLE_KODE_PRODUK))
        val collectionType = object : TypeToken<Collection<Data?>?>() {}.type
        val listData : List<Data> =  g.fromJson<List<Data>>(intent.getStringExtra(Constant.BUNDLE_KODE_PRODUK), collectionType)
        println("Jumlah DataBrg = "+listData.size)
        for (i in 0 until listData.size) {
            array_keterangan.add(listData[i].namabrg)
//            array_harga.add(listData[i].hargajual)
            array_harga.add(listData[i].hargajual1)
            array_kodebrg.add(listData[i].kodebrg)
        }

        var i = 0
        for (kodebrg in array_kodebrg) {
//            i = 01
            for (item in listPicProduct) {
                println(item.kode_produk+" = "+kodebrg)

                if (item.kode_produk.equals(kodebrg)) {
                    println(i.toString() +"(i) = "+array_keterangan.get(i).toString() + " \nRp."+ currencyFormatter(array_harga.get(i)))
//                    array_banner.add(item.pic1.toString())
                    var product  = Product(
                            kodebrg,
                            array_keterangan.get(i).toString() + " \nRp."+ currencyFormatter(array_harga.get(i)),
                            item.pic1.toString()

                    )
                    getAllProduct.add(product)
                    //i = 0
                    break
                }

            }
            i++
        }

        listProduk = ListProduct(getAllProduct)

        adapter = applicationContext!!.let { ListProductAdapter(
                it,
                listProduk as ListProduct,
                "ProdukType",
                api,
                myID,
                Server,
                myNumber,
                count,
                format,
                progressBar
        ) }

        progressBar.visibility = View.GONE

        listview_product.setAdapter(adapter)




    }
}
