package com.alkamal.hanif.mobile.form;

import android.content.Intent;
import android.os.Bundle;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by HanifAlKamal on 25/03/2018.
 */

public class opsiActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opsi_downline_layout);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bt_opsiJoin) public void opsiJoin(){
        Intent i = new Intent(opsiActivity.this,DaftarActivity.class);
        i.putExtra("jenisDaftar","join");
        startActivity(i);

    }

    @OnClick(R.id.bt_opsiKaitkan) public void opsiKaitkan(){
        Intent i = new Intent(opsiActivity.this,DaftarActivity.class);
        i.putExtra("jenisDaftar","kaitkan");
        startActivity(i);
    }

    @OnClick(R.id.bt_intruksidaftar) public void opsiintruksidaftar(){
        Intent i = new Intent(opsiActivity.this,IntruksiActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.bt_opsiDaftarBaru) public void opsiDaftarBaru(){
        Intent i = new Intent(opsiActivity.this,DaftarActivity.class);
        i.putExtra("jenisDaftar","daftarbaru");
        startActivity(i);
    }


}
