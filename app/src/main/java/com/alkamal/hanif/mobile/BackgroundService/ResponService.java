package com.alkamal.hanif.mobile.BackgroundService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.alkamal.hanif.mobile.APIService;
import com.alkamal.hanif.mobile.MainActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.MsgResponse;
import com.alkamal.hanif.mobile.form.BalasanActivity;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by HanifAlKamal on 23/02/2018.
 */

public class ResponService extends Service {
    APIService api ;
    String Server,idTrx,myID,Balasan,isi;
    int cekNotif ;
    ProgressDialog prd;
    NotificationCompat.Builder notification;
    private static final int uniqueID = 45612;
    SharedPreferences prefs;
    String idtrx2;

    Uri Sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    @Override
    public void onCreate() {
        Balasan = "Belum ada";
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient().newBuilder();
        client.addInterceptor(interceptor);
        Retrofit base = new Retrofit.Builder()
                .baseUrl("http://www.tvef.xyz/tvefKirimPesan/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();
        api = base.create(APIService.class);

        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        Server = prefs.getString("MY_SERVER", "");
        prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
        myID = prefs.getString("MY_USERID","");
        //myID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        prefs = getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
        idTrx = prefs.getString("LAST_ID", "");
        isi = prefs.getString("ISI_PESAN","");
        idtrx2 = idTrx;

        getBalasan();
        ///Notif(Balasan);


      //  getBalasan();

    }

    public APIService getApi() {
        return api;
    }

    public void log(String log) {
        if (true) {
            Log.d("Hanif" , log);
        }
    }

    public String getIdtrx2() {
        return idtrx2;
    }

    public void setIdtrx2(String idtrx2) {
        this.idtrx2 = idtrx2;
    }

    public void getBalasan(){
        Call<MsgResponse> call = getApi().ServiceGetMSg(myID,Server);
        call.clone().enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {

                        if(idtrx2.equals(ges.getIdtrx().toString())){

                            getBalasan();

                        } else {



                            //idtrx2 = ges.getIdtrx().toString();
                            prefs = getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
                            idTrx = prefs.getString("LAST_ID", "");
                            cekNotif = prefs.getInt("NOTIF", -1);
                            idtrx2 = idTrx;
                            if (cekNotif == 1){
                                Notif(ges.getPesan());
                                prefs=getSharedPreferences("LAST_IDTRX", MODE_PRIVATE);
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putInt("NOTIF", 0);
                                editor.apply();
                                idtrx2 = ges.getIdtrx();

                            }

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {public void run() {
                                getBalasan();
                                // yourMethod();
                            }
                            }, 5000);

                        }
//                        notifikasi notif = new notifikasi();
//                        notif.onNotif(ges.getPesan());

                        log("Berhasil");

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {public void run() {
                            cekNotif = 1;
                            getBalasan();
                            // yourMethod();
                        }
                        }, 5000);
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }

    public void Notif(String notifBody){
    /*
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker("This is the ticker");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("TVEF");
        notification.setContentText(notifBody);

        Context context = getApplicationContext();
        Intent intent = new Intent(context, BalasanActivity.class);
        intent.putExtra("CekSal",notifBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());*/
        Intent intent = new Intent(this, BalasanActivity.class);
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this);
        taskStackBuilder.addParentStack(MainActivity.class);
        taskStackBuilder.addNextIntent(intent.putExtra("CekSal",notifBody));


        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notif = new Notification.Builder(this)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(notifBody)
                .setContentTitle("TVEF")
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setSound(Sound)
                .build();

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(uniqueID,notif);




    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Selamat Datang",Toast.LENGTH_LONG).show();
        return super.onStartCommand(intent, flags, startId);

    }
}
