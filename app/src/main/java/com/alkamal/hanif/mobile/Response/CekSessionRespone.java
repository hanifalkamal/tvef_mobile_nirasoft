package com.alkamal.hanif.mobile.Response;

public class CekSessionRespone {
    private String notelp,status;
    private Boolean success ;

    public CekSessionRespone(String notelp, String status, Boolean success) {
        this.notelp = notelp;
        this.status = status;
        this.success = success;
    }

    public CekSessionRespone(String notelp, Boolean success) {
        this.notelp = notelp;
        this.success = success;
    }

    public CekSessionRespone(String notelp) {
        this.notelp = notelp;
    }

    public String getNotelp() {
        return notelp;
    }

    public void setNotelp(String notelp) {
        this.notelp = notelp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
