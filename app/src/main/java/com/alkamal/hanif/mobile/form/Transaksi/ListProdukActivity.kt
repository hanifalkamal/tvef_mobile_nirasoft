package com.alkamal.hanif.mobile.form.Transaksi

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.model.barangresponse.Data
import com.alkamal.hanif.mobile.utils.CacheManager
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.activity_list_produk.*
import kotlinx.android.synthetic.main.pilih_jenis_produk_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat

class ListProdukActivity : BaseActivity() {

    lateinit var listData: List<Data>
    lateinit var listDataNew: List<Data>
    lateinit var selectedData: Data
    public var hasSelected = 0
    var isAnimating = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_produk)
        init();
        initHeader(header_cari)

        button_back.setOnClickListener {
            onBackPressed()
        }


        ed_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (ed_search.text.isNotBlank()) {
                    animate("SHOW", header_cari)
                } else {
                    animate("HIDE", header_cari)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (ed_search.text.isNotBlank()) {

//                    try {
//                        var givenstring = p0.toString()
//                        val longval: Long
//                        if (givenstring.contains(",")) {
//                            givenstring = givenstring.replace(",", "")
//                        }
//                        longval = givenstring.toLong()
//                        val formatter = DecimalFormat("#,###,###")
//                        var formattedString = formatter.format(longval)
//                        formattedString = formattedString.replace(",", ".")
//                        ed_search.setText(formattedString)
//                        ed_search.setSelection(ed_search.text.length)
//                        // to place the cursor at the end of text
//                    } catch (nfe: java.lang.NumberFormatException) {
//                        nfe.printStackTrace()
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }

                    currencyFormatterOnEdiittextListener(p0.toString(), ed_search, this)

                    CacheManager.isFirstOpenListBarang = true
                    bt_pilih_produk.setBackgroundDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ui_buttonskin_deactive_shape))

                    var listForListView = ArrayList<Data>()

                    for (item in listData.iterator()) {

                        if (item.namabrg.contains(ed_search.text.toString())) {
                            listForListView.add(item)
                        }

                    }

                    val Adapter = RecyclerViewAdapter(listForListView, applicationContext, bt_pilih_produk)

                    view_pilih_produk.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = Adapter
                    }

                } else {
                    val Adapter = RecyclerViewAdapter(listData, applicationContext, bt_pilih_produk)

                    view_pilih_produk.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = Adapter
                    }
                }


            }
        })

        bt_pilih_produk.setOnClickListener {
            //If Button Active
            if (!CacheManager.isFirstOpenListBarang!!) {
                val intent = Intent(this, TransaksiActivity::class.java)
                intent.putExtra(
                        Constant.SELECTED_PRODUK_HARGA_EXTRA,
                        preferences.getString(Constant.PREFERENCES_PRODUK_HARGA, "No Data")
                )

                intent.putExtra(
                        Constant.SELECTED_PRODUK_NAMA_EXTRA,
                        preferences.getString(Constant.PREFERENCES_PRODUK_KETERANGAN, "No Data")
                )

                intent.putExtra(
                        Constant.SELECTED_PRODUK_KODE_EXTRA,
                        preferences.getString(Constant.PREFERENCES_PRODUK_KODE, "No Data")
                )

                setResult(Activity.RESULT_OK, intent)
                onBackPressed()
            } else {
                Toast.makeText(applicationContext, "Silakan pilih barang", Toast.LENGTH_SHORT).show()
            }

        }

        loadProduk()

    }

    fun loadProduk() {
        listData = ArrayList()
//        println("No Telp Tujuan = " + intent.getStringExtra("NoTelp"))
//        println("Jenis = " + intent.getStringExtra("Jenis"))
        var call = api.getDataBarang(kodePlgn, intent.getStringExtra("NoTelp"), intent.getStringExtra("Jenis"), intent.getStringExtra("KodeBrg"), encodeAndReverse(CacheManager.pin))
        call.enqueue(object : Callback<List<Data>> {
            override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                Toast.makeText(applicationContext, "Connection Error, Try again later", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                var data = response.body()

                if (data != null) {

                    listData = data
                    val Adapter = RecyclerViewAdapter(listData, applicationContext, bt_pilih_produk)

                    view_pilih_produk.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = Adapter
                    }

                    progress.visibility = View.GONE

                }

            }
        })

    }

    fun animate(arrgument: String, view: View) {
        if (arrgument.equals("SHOW")) {
            if (!isAnimating) {
                isAnimating = true
                view.visibility = View.VISIBLE
                view.animate()
                        .translationY(0.toFloat()) //
                        .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                        .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_FADE_TO_SHOW)
                        .setListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator) {
                                super.onAnimationEnd(animation)
                                isAnimating = false
                            }

                        })
            }
        } else {
            //Animate Header Hide
            view.animate()
                    .translationY(view.height.toFloat()) //
                    .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                    .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_SHOW_TO_FADE)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)
                            isAnimating = false

                        }
                    })
        }
    }

    fun initHeader(view: View) {
        view.visibility = View.INVISIBLE
        view.animate()
                .translationY(Constant.HEADER_EDITTEXT_TRANSALTION_TO_BOTTOM.toFloat()) //
                .setDuration(Constant.HEADER_EDITTEXT_DURATION.toLong())
                .alpha(Constant.HEADER_EDITTEXT_TRANSALTION_SHOW_TO_FADE)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)

                    }
                })

    }

    fun init() {
        CacheManager.isFirstOpenListBarang = true
        preferences.apply {
            edit().putString(Constant.PREFERENCES_PRODUK_HARGA, "No Data").apply()
            edit().putString(Constant.PREFERENCES_PRODUK_KODE, "No Data").apply()
            edit().putString(Constant.PREFERENCES_PRODUK_KETERANGAN, "No Data").apply()
            edit().putInt(Constant.PREFERENCES_SELECTED_ITEM_POSITION, 9999).apply()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("TO ACTIVITY RESULT")
        if (resultCode == Activity.RESULT_OK) {
            println("PRINT OUT RESULT")
//            selectedData = data!!.getParcelableExtra("selectedData")
//            Toast.makeText(applicationContext, selectedData.kodebrg, Toast.LENGTH_SHORT).show()
            //Button Is Active
            bt_pilih_produk.setBackgroundColor(Color.parseColor("#0466C8"))

        }

    }

    private class RecyclerViewAdapter(private val ListProduk: List<Data>,
                                      private val context: Context,
                                      private val buttonPilih: Button) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val keternnganProduk = view.text_nama_produk
            val hargaProduk = view.text_harga_produk
            val layoutItem = view.layout_pilih_produk_item
            val check = view.ic_check
            val context = view.context

            fun bindItem(data: Data, urutan: Int) {
                keternnganProduk.text = data.namabrg
                currencyFormatter(data.hargajual1, hargaProduk)


                layoutItem.setOnClickListener {



                }
            }

            fun currencyFormatter(p0: String, tv: TextView) {
                try {
                    var givenstring = p0.toString()
                    val longval: Long?
                    if (givenstring.contains(",")) {
                        givenstring = givenstring.replace(",".toRegex(), "")
                    }
                    longval = java.lang.Long.parseLong(givenstring)
                    val formatter = DecimalFormat("#,###,###")
                    var formattedString = formatter.format(longval)
                    formattedString = formattedString.replace(",".toRegex(), ".")
                    tv.setText("Harga " + formattedString)
                } catch (nfe: NumberFormatException) {
                    nfe.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerViewAdapter.ViewHolder {
            return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.pilih_jenis_produk_item, p0, false))
        }

        override fun getItemCount(): Int {
            return ListProduk.size
        }

        override fun onBindViewHolder(p0: RecyclerViewAdapter.ViewHolder, p1: Int) {
            val preferences = context.getSharedPreferences(Constant.PREFERENCES_TRX, Context.MODE_PRIVATE)
            p0.bindItem(ListProduk.get(p1), p1)
            p0.layoutItem.setOnClickListener {
                val selectedData = ListProduk.get(p1)



                preferences.apply {
                    edit().putString(Constant.PREFERENCES_PRODUK_HARGA, selectedData.hargajual1).apply()
                    edit().putString(Constant.PREFERENCES_PRODUK_KODE, selectedData.kodebrg).apply()
                    edit().putString(Constant.PREFERENCES_PRODUK_KETERANGAN, selectedData.namabrg).apply()
                    edit().putInt(Constant.PREFERENCES_SELECTED_ITEM_POSITION, p1).apply()

                }
                CacheManager.isFirstOpenListBarang = false;
//                buttonPilih.setBackgroundColor(Color.parseColor("#0466C8"))
                buttonPilih.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ui_buttonskin_active_shape))
                notifyDataSetChanged()


            }
//            if (CacheManager.isFirstOpenListBarang!!){
            if (!preferences.getInt(Constant.PREFERENCES_SELECTED_ITEM_POSITION, 0).equals(p1)) {

//                p0.layoutItem.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
//                p0.hargaProduk.setTextColor(Color.parseColor("#707070"))
//                p0.keternnganProduk.setTextColor(Color.parseColor("#000000"))
                p0.check.visibility = View.INVISIBLE


            } else {
                p0.check.visibility = View.VISIBLE
//                p0.layoutItem.setBackgroundColor(Color.parseColor("#0466C8"))
//                p0.hargaProduk.setTextColor(Color.parseColor("#FFFFFF"))
//                p0.keternnganProduk.setTextColor(Color.parseColor("#FFFFFF"))

            }
//            }
        }
    }


}
