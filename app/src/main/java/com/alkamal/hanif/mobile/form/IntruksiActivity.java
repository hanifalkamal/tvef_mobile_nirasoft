package com.alkamal.hanif.mobile.form;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.alkamal.hanif.mobile.R;

import butterknife.ButterKnife;

/**
 * Created by Hanif on 02/10/2018.
 */

public class IntruksiActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.intruksi_layout);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
    }
}
