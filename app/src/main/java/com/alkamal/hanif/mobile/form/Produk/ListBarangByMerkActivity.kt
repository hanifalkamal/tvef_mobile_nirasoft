package com.alkamal.hanif.mobile.form.Produk

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.form.ProductDetilActivity
import com.alkamal.hanif.mobile.model.ProductCategory
import com.alkamal.hanif.mobile.utils.Constant
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_list_barang_by_merk.*
import kotlinx.android.synthetic.main.layout_item_produk_by_merk.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat

class ListBarangByMerkActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_barang_by_merk)

        loadProduk()

        button_back.setOnClickListener {
            onBackPressed()
        }



    }

    fun loadProduk(){
        var call = api.loadProdukbyKategori(
                intent.getStringExtra(Constant.BUNDLE_PRODUK_KATEGORI),
                kodePlgn
        )

        call.enqueue(object : Callback<List<ProductCategory>>{
            override fun onFailure(call: Call<List<ProductCategory>>, t: Throwable) {
                progressBar.visibility = View.GONE
                Toast.makeText(applicationContext, "Internal Server Error", Toast.LENGTH_SHORT).show()
                finish()
            }

            override fun onResponse(call: Call<List<ProductCategory>>, response: Response<List<ProductCategory>>) {

                var list = response.body()

                if (list != null){

                    if (list.isEmpty()){
                        layout_image_empty_data.visibility = View.VISIBLE
                    }

                    val productAdapter = RecyclerViewAdapter(list, applicationContext,intent.getStringExtra(Constant.BUNDLE_PRODUK_KATEGORI)!!)

                    recycler_grid_product_by_merk.apply {
                        layoutManager = LinearLayoutManager(applicationContext)
                        adapter = productAdapter
                    }

                }

                progressBar.visibility = View.GONE

            }
        })
    }


    private class RecyclerViewAdapter(private val list: List<ProductCategory>, private val context: Context, private val produkKategori: String) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(){

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val imageProduct = view.image_product_by_merk
            val textNamaProduct = view.text_nama_produk
            val textHargaProduct = view.text_harga_produk
            val buttonDetail = view.button_detail

        }

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerViewAdapter.ViewHolder {
            return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.layout_item_produk_by_merk, p0, false))
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(p0: RecyclerViewAdapter.ViewHolder, p1: Int) {

            ShowImage(list.get(p1).pic1, p0.imageProduct)
            p0.textNamaProduct.setText(list.get(p1).kode_produk)
//            p0.textHargaProduct.setText(list.get(p1).harga_produk)
            currencyFormatter(list.get(p1).harga_produk, p0.textHargaProduct)
            p0.buttonDetail.setOnClickListener {
                var intent = Intent(context, ProductDetilActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra(Constant.BUNDLE_KODE_PRODUK, list.get(p1).kode_produk)
                intent.putExtra(Constant.BUNDLE_PRODUK_KATEGORI, produkKategori)
                intent.putExtra(Constant.BUNDLE_PRODUK_DESKRIPSI, list.get(p1).deskrispi)
                intent.putExtra(Constant.BUNDLE_PRODUK_PIC1, list.get(p1).pic1)
                intent.putExtra(Constant.BUNDLE_PRODUK_PIC2, list.get(p1).pic2)
                intent.putExtra(Constant.BUNDLE_PRODUK_PIC3, list.get(p1).pic3)
                intent.putExtra(Constant.BUNDLE_PRODUK_PIC4, list.get(p1).pic4)
                intent.putExtra(Constant.BUNDLE_PRODUK_PIC5, list.get(p1).pic5)
                intent.putExtra(Constant.BUNDLE_PRODUK_TITLE, list.get(p1).nama_produk +"\n"+
                        currencyFormatter(list.get(p1).harga_produk))
                ContextCompat.startActivity(context, intent, null)
            }

        }
        fun ShowImage(bannerUrl: String, imageView: ImageView) {
            Glide.with(context)
                    .load(Constant.BaseUrlPic + bannerUrl)
                    .apply(RequestOptions().override(116, 116))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
//                            view.progressBar.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
//                            view.progressBar .visibility = View.GONE
                            return false
                        }

                    })
                    .into(imageView)
        }

        fun currencyFormatter(p0: String, tv: TextView) {
            tv.setText(currencyFormatter(p0))
        }

        fun currencyFormatter(p0: String) : String {
            try {
                var givenstring = p0.toString()
                val longval: Long?
                if (givenstring.contains(",")) {
                    givenstring = givenstring.replace(",".toRegex(), "")
                }
                longval = java.lang.Long.parseLong(givenstring)
                val formatter = DecimalFormat("#,###,###")
                var formattedString = formatter.format(longval)
                formattedString = formattedString.replace(",".toRegex(), ".")
                return "Rp. $formattedString"
            } catch (nfe: NumberFormatException) {
                nfe.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return ""
        }

    }
}
