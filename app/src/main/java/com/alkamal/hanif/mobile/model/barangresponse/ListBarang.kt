package com.alkamal.hanif.mobile.model.barangresponse

import com.google.gson.annotations.SerializedName

class ListBarang(
        @SerializedName("Data") val data: List<Data>

)