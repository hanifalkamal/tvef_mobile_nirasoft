package com.alkamal.hanif.mobile.form

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.alkamal.hanif.mobile.R
import kotlinx.android.synthetic.main.activity_version_validation.*

class VersionValidationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_version_validation)

        val info_aktif = intent.getStringExtra("info_aktif")
        val link = intent.getStringExtra("link")

        text_info.setText(info_aktif)

        text_link_ps.setOnClickListener {
            startActivity(
                    Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(link.toString())
                    )
            )
        }
    }

    override fun onBackPressed() {
//        false
    }
}
