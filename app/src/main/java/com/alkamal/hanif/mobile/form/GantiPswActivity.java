package com.alkamal.hanif.mobile.form;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

//import com.alkamal.hanif.kirimdatalisensi.R;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.DaftarRespone;
import com.alkamal.hanif.mobile.Response.LoginRespone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GantiPswActivity extends BaseActivity {

    ProgressDialog prd;
    @BindView(R.id.ed_noTelp_ganti)
    EditText edNotelp;
    @BindView(R.id.ed_psw_lama)
    EditText edPswLama;
    @BindView(R.id.ed_psw)
    EditText edPsw;
    @BindView(R.id.ed_psw_conf)
    EditText edPsw_conf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganti_psw);
        ButterKnife.bind(this);

        edNotelp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (("" + edNotelp.getText().charAt(0)).equals("0")) {
                        edNotelp.setText(edNotelp.getText().toString().substring(1));
                        //edNotelp.append("");

                    } else if(("" + edNotelp.getText().charAt(0)+"" + edNotelp.getText().charAt(1)).equals("62")) {
                        edNotelp.setText(edNotelp.getText().toString().substring(2));
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }


    @OnClick(R.id.bt_Send_GantiPsw)
    public void onGanti() {

        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();
        String myTelp = edNotelp.getText().toString();
        String myPswLama = edPswLama.getText().toString();
        final String myPsw = edPsw.getText().toString();
        String myPswConf = edPsw_conf.getText().toString();


        if (myTelp.matches("")||myPswLama.matches("")||myPsw.matches("")||myPswConf.matches("")){
            Toast.makeText(GantiPswActivity.this, "Form wajib di isi semua", Toast.LENGTH_LONG).show();
            prd.dismiss();
        } else {

            if (myPsw.matches(myPswConf)){

                Call<LoginRespone> call = getApi().GetMSgLogin("62"+myTelp , myPswLama);
                call.enqueue(new Callback<LoginRespone>() {
                    @Override
                    public void onResponse(Call<LoginRespone> call, Response<LoginRespone> response) {
                        if(response.isSuccessful()){
                            LoginRespone res = response.body();
                            if(res.isSuccess()){
                                if (myPsw.length() > 5) {
                                    GantiPsw();
                                } else {
                                    prd.dismiss();
                                    Toast.makeText(GantiPswActivity.this, "Password minimal memiliki 5 karakter", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(GantiPswActivity.this, "No Telepon atau Password tidak dikenal  ", Toast.LENGTH_LONG).show();
                                prd.dismiss();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginRespone> call, Throwable t) {
                        log(t.toString());
                        Toast.makeText(GantiPswActivity.this, "Password atau No. Telepon Salah", Toast.LENGTH_SHORT).show();
                        prd.dismiss();
                    }
                });

            } else {
                Toast.makeText(GantiPswActivity.this, "Password tidak sama", Toast.LENGTH_LONG).show();
                prd.dismiss();
            }

        }


    }

    public void GantiPsw(){

        Call<DaftarRespone> call = getApi().GetGantiPsw("62"+edNotelp.getText().toString() , edPsw.getText().toString());
        call.enqueue(new Callback<DaftarRespone>() {
            @Override
            public void onResponse(Call<DaftarRespone> call, Response<DaftarRespone> response) {
                if(response.isSuccessful()){
                    DaftarRespone res = response.body();
                    if(res.IsSuccess()){
                        Toast.makeText(GantiPswActivity.this, "Ganti Password Berhasil !", Toast.LENGTH_SHORT).show();

                        finish();//here
                    } else {
                        Toast.makeText(GantiPswActivity.this, "Ada kesalahan ", Toast.LENGTH_LONG).show();
                        prd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<DaftarRespone> call, Throwable t) {
                log(t.toString());
            }
        });
    }
}
