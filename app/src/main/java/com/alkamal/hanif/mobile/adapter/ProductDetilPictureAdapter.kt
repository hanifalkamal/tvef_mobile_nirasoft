package com.alkamal.hanif.mobile.adapter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.utils.Constant
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import java.io.ByteArrayOutputStream


class ProductDetilPictureAdapter(
     var mContext : Context,
     var mListPicBase64 : MutableList<String>
) : RecyclerView.Adapter<ProductDetilPictureAdapter.ViewHolder>() {

    var options: RequestOptions = RequestOptions()
            .centerCrop() //            .placeholder(R.drawable.ic_load_black_24dp)
            .error(R.mipmap.ic_no_image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)


    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

        var image : ImageView = itemView.findViewById(R.id.imageProductDetil)
        var progressBar : ProgressBar = itemView.findViewById(R.id.progress)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        var view = LayoutInflater.from(p0.context).inflate(R.layout.productdetil_pic_item, p0, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mListPicBase64.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
//        base64ToImage(mListPicBase64.get(p1), p0.image, p0)
        ShowImage(mListPicBase64.get(p1), p0.image, p0)
    }

    fun base64ToImage(encodedImage: String?, ib: ImageView, view : ViewHolder) {
        try {
//            var resizeBase64 = resizeBase64Image(encodedImage!!)
            var decodedString = Base64.decode(encodedImage, Base64.DEFAULT)

            Glide.with(mContext!!)
                    .load(decodedString)
                    .listener(object : RequestListener<Drawable>{
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            view.progressBar.setVisibility(View.GONE)
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            view.progressBar.setVisibility(View.GONE)
                            return false
                        }

                    })
                    .apply(options)
                    .into(ib)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun resizeBase64Image(base64image: String): String? {
        val encodeByte = Base64.decode(base64image.toByteArray(), Base64.DEFAULT)
        val options = BitmapFactory.Options()
        options.inPurgeable = true
        var image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size, options)
        if (image.height <= 400 && image.width <= 400) {
            return base64image
        }
        image = Bitmap.createScaledBitmap(image, image.width, image.height, false)
        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 100, baos)
        val b: ByteArray = baos.toByteArray()
        System.gc()
        return Base64.encodeToString(b, Base64.NO_WRAP)
    }

    fun ShowImage(bannerUrl: String, imageView: ImageView, view : ViewHolder) {
        Glide.with(mContext!!)
                .load(Constant.BaseUrlPic + bannerUrl)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        view.progressBar.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        view.progressBar .visibility = View.GONE
                        return false
                    }

                })
                .into(imageView)
    }
}

