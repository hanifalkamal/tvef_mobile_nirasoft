package com.alkamal.hanif.mobile.chat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.LoginRespone;
import com.alkamal.hanif.mobile.Response.NamaUsahaRespone;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignEmail extends BaseActivity {

    private SharedPreferences prefs;
    private static String myNumber, myID;
    FirebaseAuth auth;
    @BindView(R.id.ed_namaUsaha) EditText edNamaUsaha;
    @BindView(R.id.email) EditText edEmail;
    @BindView(R.id.password) EditText edPsw;
    DatabaseReference reference ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_email);
        ButterKnife.bind(this);

        prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
        myNumber = prefs.getString("MY_NUMBER","");
        myID = prefs.getString("MY_USERID","");
        getnamaUsaha();

        auth = FirebaseAuth.getInstance();
    }

    @OnClick(R.id.btn_lewati) public  void onLewati(){
        finish();
    }
    @OnClick(R.id.btn_register) public void onEmailRegis(){
        String txt_email = edEmail.getText().toString();
        String txt_password = edPsw.getText().toString();

        if(TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)){
            Toast.makeText(this,"Tidak boleh ada data yang kososng", Toast.LENGTH_SHORT).show();
        } else {
            checkPassword(txt_email,txt_password, edNamaUsaha.getText().toString());

        }
    }

    @OnClick(R.id.btn_loginEmail) public void onEmailLogin(){
        Intent i = new Intent(SignEmail.this, LoginEmailActivity.class);
        startActivity(i);
        finish();
    }

    private void checkPassword(final String email, final String password, final String username){

        Call<LoginRespone> call = getApi().GetMSgLogin(myNumber , edPsw.getText().toString());
        call.enqueue(new Callback<LoginRespone>() {
            @Override
            public void onResponse(Call<LoginRespone> call, Response<LoginRespone> response) {
                if(response.isSuccessful()){
                    LoginRespone res = response.body();
                    if(res.isSuccess()){
                        auth.createUserWithEmailAndPassword(email,password).
                                addOnCompleteListener(new OnCompleteListener<AuthResult>(){
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()){
                                            FirebaseUser firebaseuser = auth.getCurrentUser();
                                            assert firebaseuser != null;
                                            String userid = firebaseuser.getUid();

                                            reference = FirebaseDatabase.getInstance().getReference("Users").child(userid);

                                            HashMap<String, String> hashMap = new HashMap<>();
                                            hashMap.put("id", userid);
                                            hashMap.put("username", username);
                                            hashMap.put("imageURL", "default");
                                            hashMap.put("status", "offline");
                                            hashMap.put("search", username.toLowerCase());
                                            hashMap.put("akun", "downline");

                                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()){
                                                        auth.signInWithEmailAndPassword(email, password)
                                                                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                                                        if(task.isSuccessful()){
                                                                            String topic = "/topics/AND6287654321";
                                                                            FirebaseMessaging.getInstance().subscribeToTopic(topic);
                                                                            finish();
                                                                            Toast.makeText(SignEmail.this, "Register Email Berhasil", Toast.LENGTH_SHORT).show();
                                                                        } else {
                                                                            Toast.makeText(SignEmail.this, "Auth Failed", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                });

                                                        finish();
                                                    }
                                                }
                                            });


                                        } else {
                                            Toast.makeText(SignEmail.this, "Tidak bisa registrasi dengan email ini",Toast.LENGTH_LONG).show();
                                        }
                                    }


                                });


                    } else {
                        Toast.makeText(SignEmail.this, "Password Salah", Toast.LENGTH_LONG).show();

                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginRespone> call, Throwable t) {
                log(t.toString());
            }
        });

    }

    private void getnamaUsaha(){
        Call<NamaUsahaRespone> call = getApi().GetNamaUsaha(myNumber);
        call.enqueue(new Callback<NamaUsahaRespone>() {
            @Override
            public void onResponse(Call<NamaUsahaRespone> call, Response<NamaUsahaRespone> response) {
                if(response.isSuccessful()){
                    NamaUsahaRespone res = response.body();
                    if(res.isSuccess()){
                        edNamaUsaha.setText(res.getNamaUsaha());
                        Log.d("getNamausaha","Berhasil");
                    } else {

                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<NamaUsahaRespone> call, Throwable t) {
                log(t.toString());
            }
        });
    }

}
