package com.alkamal.hanif.mobile.BackgroundService;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.form.Histori.HistoriActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Random;

public class FirebaseInstanceService extends FirebaseMessagingService {
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("MyToken ",s);
        Log.d("MyToken ",s);
        getSharedPreferences("_", MODE_PRIVATE).edit().putString("fb", s).apply();
    }

    public static String getToken(Context context) {
        return context.getSharedPreferences("_", MODE_PRIVATE).getString("fb", "empty");
    }
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e("OnMessageRecived ", "Message recived");
        if(remoteMessage.getData().isEmpty()){
            Log.e("notif","empty");
            showNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());


        }else {
            Log.e("notif","not empty");
            try {
                showNotification(remoteMessage.getData());
            }catch (Exception e){
                Log.e("FirebaseInstanceService", ""+e);
            }

        }


    }

    private void showNotification(Map<String,String> data){
        String title = data.get("title").toString();
        //String body = data.get("body").toString();
        String body = data.get("message").toString();

        NotificationManager notificationmanager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "com.alkamal.hanif.kirimdatalisensi.test";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            try {
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification",
                        NotificationManager.IMPORTANCE_DEFAULT);

                notificationChannel.setDescription("Notif");
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.BLUE);
                notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                notificationChannel.enableLights(true);
                notificationmanager.createNotificationChannel(notificationChannel);
            }catch (Exception e){
                Log.e("FirebaseInstaceService",""+e);
            }
        }

        NotificationCompat.Builder notificationbuilder = new NotificationCompat.Builder(this,NOTIFICATION_CHANNEL_ID);

        notificationbuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setContentInfo("Info");

        Context context = getApplicationContext();
//        Intent intent = new Intent(context, BalasanActivity.class);
        Intent intent = new Intent(context, HistoriActivity.class);
        intent.putExtra("CekSal",body);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationbuilder.setContentIntent(pendingIntent);

        notificationmanager.notify(new Random().nextInt(),notificationbuilder.build());
        Log.e("notif","1");
    }

    private void showNotification(String title, String body){
        NotificationManager notificationmanager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "com.alkamal.hanif.kirimdatalisensi.test";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification",
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("Hanif Coba FCM");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setVibrationPattern(new long[]{0,1000,500,1000});
            notificationChannel.enableLights(true);
            notificationmanager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationbuilder = new NotificationCompat.Builder(this,NOTIFICATION_CHANNEL_ID);

        notificationbuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setContentInfo("Info");

        Context context = getApplicationContext();
//        Intent intent = new Intent(context, BalasanActivity.class);
        Intent intent = new Intent(context, HistoriActivity.class);
        intent.putExtra("CekSal",body);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationbuilder.setContentIntent(pendingIntent);

        notificationmanager.notify(new Random().nextInt(),notificationbuilder.build());
        Log.e("notif","2");
    }

}
