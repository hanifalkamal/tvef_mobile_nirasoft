package com.alkamal.hanif.mobile.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PendingResponse(
        @SerializedName("idRequest") var idRequest : String,
        @SerializedName("tanggalJam") var tanggalJam : String,
        @SerializedName("status") var status : String,
        @SerializedName("kodeProduk") var kodeProduk : String,
        @SerializedName("namaProduk") var namaProduk : String,
        @SerializedName("nomorTujuan") var nomorTujuan : String,
        @SerializedName("pesan") var pesan : String,
        @SerializedName("hargaJual") var hargaJual : String
):Parcelable