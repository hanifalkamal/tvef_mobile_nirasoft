package com.alkamal.hanif.mobile.model.barangresponse

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Data (
        @SerializedName("kodebrg") val kodebrg : String,
        @SerializedName("namabrg") val namabrg : String,
        @SerializedName("kdspek") val kdspek : String,
        @SerializedName("nominalpulsa") val nominalpulsa : String,
        @SerializedName("hargajual") val hargajual : String,
        @SerializedName("hargajual1") val hargajual1 : String
): Parcelable