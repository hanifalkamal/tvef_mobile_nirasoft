package com.alkamal.hanif.mobile.form;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BackgroundService.PrinterService;
import com.alkamal.hanif.mobile.R;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class PrinterSettingActivity extends AppCompatActivity {
    BluetoothAdapter bluetoothAdapter;
    BluetoothSocket bluetoothSocket;
    BluetoothDevice bluetoothDevice;

    OutputStream outputStream;
    InputStream inputStream;
    Thread thread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    TextView lblPrinterName;
    EditText textBox,edSeriPrinter;

    private SharedPreferences prefs;
    ProgressDialog prd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer_setting);

        // Create object of controls
        Button btnConnect = (Button) findViewById(R.id.btnConnect);
        Button btnDisconnect = (Button) findViewById(R.id.btnDisconnect);
        Button btnPrint = (Button) findViewById(R.id.btnPrint);
        Button Feed = (Button) findViewById(R.id.btnFeed);

        textBox = (EditText) findViewById(R.id.txtText);
        edSeriPrinter = (EditText) findViewById(R.id.ed_seriPrinter);

        lblPrinterName = (TextView) findViewById(R.id.lblPrinterName);

        prefs=getSharedPreferences("MY_DATA", MODE_PRIVATE);
        String jenisPrinter = prefs.getString("JENIS_PRINTER","");

        edSeriPrinter.setText(jenisPrinter);

        Feed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    FeedPaper();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(PrinterSettingActivity.this, "Ada Kesalahan !!! Tidak dapat melakukan feed ", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edSeriPrinter.getText().toString().equals("")) {
                    try {
                        prd = new ProgressDialog(PrinterSettingActivity.this);
                        prd.setMessage("Mohon Tunggu. . .");
                        prd.setCancelable(false);
                        prd.setCanceledOnTouchOutside(false);

                        prd.show();
                        FindBluetoothDevice(edSeriPrinter.getText().toString());
                        openBluetoothPrinter();

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        Toast.makeText(PrinterSettingActivity.this, "Tidak dapat terhubung dengan printer", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PrinterSettingActivity.this, "Seri / Jenis Printer Mohon Harus diisi !", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    disconnectBT();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(PrinterSettingActivity.this, "Tidak dapat memutus hubungan dengan printer", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    printData();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(PrinterSettingActivity.this, "Tidak bisa Print data !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void FeedPaper(){
        try{
            String msg = "\n";
            outputStream.write(msg.getBytes());
            lblPrinterName.setText("Printing Text...");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    void FindBluetoothDevice(String seriPrinter){

        try{

            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if(bluetoothAdapter==null){
                lblPrinterName.setText("Tidak ada Bluetooth Printer yang terkoneksi");
            }
            if(bluetoothAdapter.isEnabled()){
                Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBT,0);
            }

            Set<BluetoothDevice> pairedDevice = bluetoothAdapter.getBondedDevices();

            if(pairedDevice.size()>0){
                for(BluetoothDevice pairedDev:pairedDevice){

                    // My Bluetoth printer name is BTP_F09F1A
                    if(pairedDev.getName().equals(seriPrinter)){
                        bluetoothDevice=pairedDev;
                        lblPrinterName.setText("Bluetooth Printer Terkoneksi: "+pairedDev.getName());
                        Toast.makeText(this, "Terkoneksi Dengan Pritner", Toast.LENGTH_SHORT).show();
                        //wait(1000);
                        prd.hide();

                        break;
                    } else {
                        lblPrinterName.setText("Printer tidak terdeksi pada seri ini");
                        Toast.makeText(this, "Printer tidak terdeteksi !", Toast.LENGTH_SHORT).show();
                        Log.e("pairDev","Seri Beda");
                        prd.hide();
                    }

                }
            } else {
                lblPrinterName.setText("Belum melakukan pairing pada printer ini, koneksikan printer pada menu bluetooth");
                Toast.makeText(this, "Printer belum tersedia !", Toast.LENGTH_SHORT).show();
                Log.e("pairDev","No Pair");
                prd.hide();
            }

            prd.hide();

            SharedPreferences.Editor editor = prefs.edit();

            editor.putString("JENIS_PRINTER",seriPrinter);
            editor.apply();

            //lblPrinterName.setText("Bluetooth Printer Terkoneksi");

        }catch(Exception ex){
            ex.printStackTrace();
        }

    }

    // Open Bluetooth Printer

    void openBluetoothPrinter() throws IOException {
        try{

            //Standard uuid from string //
            UUID uuidSting = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            bluetoothSocket=bluetoothDevice.createRfcommSocketToServiceRecord(uuidSting);
            bluetoothSocket.connect();
            outputStream=bluetoothSocket.getOutputStream();
            inputStream=bluetoothSocket.getInputStream();

            PrinterService ps = new PrinterService(inputStream);
            ps.setOutputStream(outputStream);
            //ps.onStart();
            //beginListenData();

        }catch (Exception ex){

        }
    }

    void beginListenData(){
        try{

            final Handler handler =new Handler();
            final byte delimiter=10;
            stopWorker =false;
            readBufferPosition=0;
            readBuffer = new byte[1024];

            thread=new Thread(new Runnable() {
                @Override
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker){
                        try{
                            int byteAvailable = inputStream.available();
                            if(byteAvailable>0){
                                byte[] packetByte = new byte[byteAvailable];
                                inputStream.read(packetByte);

                                for(int i=0; i<byteAvailable; i++){
                                    byte b = packetByte[i];
                                    if(b==delimiter){
                                        byte[] encodedByte = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer,0,
                                                encodedByte,0,
                                                encodedByte.length
                                        );
                                        final String data = new String(encodedByte,"US-ASCII");
                                        readBufferPosition=0;
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                lblPrinterName.setText(data);
                                            }
                                        });
                                    }else{
                                        readBuffer[readBufferPosition++]=b;
                                    }
                                }
                            }
                        }catch(Exception ex){
                            stopWorker=true;
                        }
                    }

                }
            });

            thread.start();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    // Printing Text to Bluetooth Printer //
    void printData() throws  IOException{
        try{
            String msg = textBox.getText().toString();
            msg+="\n";
            outputStream.write(msg.getBytes());
            lblPrinterName.setText("Printing Text...");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    // Disconnect Printer //
    void disconnectBT() throws IOException{
        try {
            stopWorker=true;
            outputStream.close();
            inputStream.close();
            bluetoothSocket.close();
            lblPrinterName.setText("Printer Disconnected.");
            Toast.makeText(this, "Koneksi dengan Printer telah diputus", Toast.LENGTH_SHORT).show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
