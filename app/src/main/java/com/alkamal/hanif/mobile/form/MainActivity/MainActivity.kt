package com.alkamal.hanif.mobile.form.MainActivity

import android.content.Intent
import android.os.Bundle
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.form.LoadingScreen.SplashScreenActivity

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var intent = Intent(applicationContext, SplashScreenActivity::class.java)
        startActivity(intent)
        finish()

//        if (preferences.getBoolean(Constant.STATE_HAS_LOGGED_IN, false)){
//            var intent = Intent(applicationContext, InputPinActivity::class.java)
//            startActivity(intent)
//            finish()
//        } else {
//            var intent = Intent(applicationContext, LoginActivity::class.java)
//            startActivity(intent)
//            finish()
//        }



    }
}