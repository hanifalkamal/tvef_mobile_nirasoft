package com.alkamal.hanif.mobile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alkamal.hanif.mobile.R;

import java.util.List;

/**
 * Created by HanifAlKamal on 01/03/2018.
 */

public class ProductListAdapter extends BaseAdapter{
    private Context mContext;
    private List<Product> mProduction;

    public ProductListAdapter(Context mContext, List<Product> mProduction) {
        this.mContext = mContext;
        this.mProduction = mProduction;
    }

    @Override
    public int getCount() {
        return mProduction.size();
    }

    @Override
    public Object getItem(int position) {
        return mProduction.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View v = View.inflate(mContext, R.layout.cek_harga_item, null);
        TextView tvName = (TextView)v.findViewById(R.id.tv_Name);
        TextView tvPrice = (TextView)v.findViewById(R.id.tv_Price);
        TextView tvDesc = (TextView)v.findViewById(R.id.tv_desc);

        tvName.setText(mProduction.get(position).getName());
        tvPrice.setText(String.valueOf("Rp "+mProduction.get(position).getPrice()));
        tvDesc.setText(mProduction.get(position).getDesc());

        v.setTag(mProduction.get(position).getId());

        return v;
    }
}
