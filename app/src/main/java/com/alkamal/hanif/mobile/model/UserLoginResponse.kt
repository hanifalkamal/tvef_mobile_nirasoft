package com.alkamal.hanif.mobile.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserLoginResponse(
        @SerializedName("success") var success : Boolean,
        @SerializedName("idUser") var idUser : String,
        @SerializedName("namaUsaha") var namaUsaha : String,
        @SerializedName("message") var message : String
):Parcelable