package com.alkamal.hanif.mobile.form.Transaksi

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.form.Histori.HistoriActivity
import com.alkamal.hanif.mobile.form.MainHome.HomeActivity
import com.alkamal.hanif.mobile.model.BalanceResponse
import com.alkamal.hanif.mobile.model.TopUpResponse
import com.alkamal.hanif.mobile.model.TransactionResponse
import com.alkamal.hanif.mobile.model.TransferSaldoResponse
import com.alkamal.hanif.mobile.utils.CacheManager
import com.alkamal.hanif.mobile.utils.Constant
import com.alkamal.hanif.mobile.utils.Constant.PREFERENCES_TRX_ULANG
import com.goodiebag.pinview.Pinview
import kotlinx.android.synthetic.main.activity_input_pin.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class InputPinActivity : BaseActivity() {

    var serviceError = false;
    var serviceTimeOut = false;

//    fun openSoftKeyboard(context: Context, view: View) {
//        view.requestFocus()
//        // open the soft keyboard
//        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
//    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_pin)

        openSoftKeyboard(applicationContext, act_code_edit)

        button_home.setOnClickListener {
            onBackPressed()
        }
        button_history.setOnClickListener {
            var intent = Intent(applicationContext, HistoriActivity::class.java)
            startActivity(intent)
            finish()
        }
        act_code_edit.setTextColor(ContextCompat.getColor(applicationContext, R.color.White))
        act_code_edit.setTextSize(50)
        act_code_edit.setPinViewEventListener(object : Pinview.PinViewEventListener {
            override fun onDataEntered(pinview: Pinview?, fromUser: Boolean) {
                hideKeyboard(this@InputPinActivity)

                var pin = act_code_edit.value.toString()
                if (intent.hasExtra(Constant.SELECTED_PRODUK_KODE_EXTRA)) {
                    //this is trx
                    //Kirim Trx

                    if (!CacheManager.pin.isNullOrEmpty()) {
                        if (CacheManager.pin.equals(pin)) {
                            waitState(Constant.STATE_TRX)
                            Transaction(encodeAndReverse(pin), 0)


                        } else {
                            wrongPin()
                        }
                    }


                } else if (intent.hasExtra(Constant.HOME_TRANSFER_SALDO)){
                    if (!CacheManager.pin.isNullOrEmpty()) {
                        if (CacheManager.pin.equals(pin)) {
                            waitState(Constant.STATE_TRX)
                            var tujuan = intent.getStringExtra("nomorHp")
                            var nominal = intent.getStringExtra("nominal")
                            if (intent.hasExtra("kodeTrf")){
                                // RALAT
                                var kodeTrf = intent.getStringExtra("kodeTrf")
                                trfSaldo(kodePlgn, tujuan.toString(), nominal.toString(), kodeTrf.toString(), "", encodeAndReverse(pin))
                            } else {
                                // ORDINARY TRF
                                trfSaldo(kodePlgn, tujuan.toString(), nominal.toString(), "", "", encodeAndReverse(pin))
                            }

                        }
                    } else {
                        inputState()
                        wrongPin()
                    }

                } else if (intent.hasExtra(Constant.HOME_TAMBAH_SALDO)){
                    if (!CacheManager.pin.isNullOrEmpty()) {
                        if (CacheManager.pin.equals(pin)) {

                            waitState(Constant.STATE_TRX)
                            var nominal = intent.getStringExtra("nominal")
                            var bank = intent.getStringExtra("bank")
                            topup(kodePlgn, encodeAndReverse(pin), nominal.toString(), bank.toString())

                        }
                    } else {
                        wrongPin()
                    }
                } else {
                    //this is non trx
                    waitState(Constant.STATE_NON_TRX)
                    cekSaldo(encodeAndReverse(pin))
                }


            }
        })

    }

    fun wrongPin(){
        for (i in 0..3) {
            act_code_edit.onKey(act_code_edit.getFocusedChild(), KeyEvent.KEYCODE_DEL, KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL))
        }
        header_input_pin.setText(R.string.wrong_pin_message)
        header_input_pin.setTextColor(ContextCompat.getColor(applicationContext, R.color.PaleVioletRed))
        openSoftKeyboard(applicationContext, act_code_edit)
    }

    fun Transaction(pin: String, repeatCountTrx: Int) {
        var call = api.transaction(
                kodePlgn,
                intent.getStringExtra(Constant.SELECTED_PRODUK_KODE_EXTRA),
                intent.getStringExtra("nomorHp"),
                pin,
                repeatCountTrx,
                intent.getStringExtra(Constant.SELECTED_PRODUK_HARGA_EXTRA),
                intent.getStringExtra(Constant.SELECTED_PRODUK_NAMA_EXTRA)
        )
        call.enqueue(object : retrofit2.Callback<TransactionResponse> {
            override fun onFailure(call: Call<TransactionResponse>, t: Throwable) {
                Toast.makeText(applicationContext, "Internal Server Error", Toast.LENGTH_SHORT).show()
                finish()
            }

            override fun onResponse(call: Call<TransactionResponse>, response: Response<TransactionResponse>) {
                checkBalance(pin)
                var data = response.body()
                println("### responseCode = " + response.code())
                if (response.code() != 200) {

                    Toast.makeText(applicationContext, "Time Out", Toast.LENGTH_SHORT).show()
                    finish()

                }
//                var data = response.body()
                if (data != null) {

                    var intent = Intent(applicationContext, ReceiptActivity::class.java)
                    println("success = "+data.success)
                    println(Constant.RECEIPT_HEADER+ data.keterangan)
                    println(Constant.RECEIPT_PESAN+ data.pesan)
                    println(Constant.RECEIPT_PRODUK+ data.produk)
                    println(Constant.RECEIPT_TANGGAL+ data.tanggal)
                    println(Constant.RECEIPT_WAKTU+ data.waktu)
                    println(Constant.RECEIPT_NOMOR_TUJUAN+ data.nomorTujuan)
                    println(Constant.RECEIPT_ID+ data.idTrx)
                    println(Constant.RECEIPT_HARGA+ data.harga)
                    if (data.success == true) {
                        showNotif(data.keterangan, data.pesan)
                        // show response data
                        if (!isFinishing) {
                            intent.putExtra(Constant.RECEIPT_HEADER, data.keterangan)
                            intent.putExtra(Constant.RECEIPT_PESAN, data.pesan)
                            intent.putExtra(Constant.RECEIPT_PRODUK, data.produk)
                            intent.putExtra(Constant.RECEIPT_TANGGAL, data.tanggal)
                            intent.putExtra(Constant.RECEIPT_WAKTU, data.waktu)
                            intent.putExtra(Constant.RECEIPT_NOMOR_TUJUAN, data.nomorTujuan)
                            intent.putExtra(Constant.RECEIPT_STATUS, true)
                            intent.putExtra(Constant.RECEIPT_ID, data.idTrx)
                            intent.putExtra(Constant.RECEIPT_HARGA, data.harga)
                            startActivity(intent)
                            finish()
                        }
                        // Notif

                    } else {

                        if (!isFinishing) {
                            showNotif(data.keterangan, data.pesan)
                            text_trx_keterangan.setText(data.keterangan)
                            intent.putExtra(Constant.RECEIPT_HEADER, data.keterangan)
                            if (data.keterangan.toUpperCase().equals("GAGAL")) {

                                intent.putExtra(Constant.RECEIPT_PESAN, data.pesan)
                                intent.putExtra(Constant.RECEIPT_PRODUK, data.produk)
                                intent.putExtra(Constant.RECEIPT_TANGGAL, data.tanggal)
                                intent.putExtra(Constant.RECEIPT_WAKTU, data.waktu)
                                intent.putExtra(Constant.RECEIPT_NOMOR_TUJUAN, data.nomorTujuan)
                                intent.putExtra(Constant.RECEIPT_STATUS, false)
                                intent.putExtra(Constant.RECEIPT_ID, data.idTrx)
                                intent.putExtra(Constant.RECEIPT_HARGA, data.harga)

                                startActivity(intent)
                                finish()
                            } else if (data.keterangan.toUpperCase().contains("PERNAH DILAKUKAN")) {

                                val builder = AlertDialog.Builder(ContextThemeWrapper(this@InputPinActivity, R.style.Theme_AppCompat_Light_Dialog))
                                builder.setTitle("Perhatian")
                                builder.setMessage(R.string.retransaction_confirm_message)

                                builder.setPositiveButton("LANJUT") { dialog, which ->
                                    waitState(Constant.STATE_TRX)
                                    val ulangCounter = getSharedPreferences(PREFERENCES_TRX_ULANG, Context.MODE_PRIVATE)
                                            .getString(intent.getStringExtra("nomorHp"), "1")

                                    Transaction(pin, ulangCounter!!.toInt())

                                    val counter = ulangCounter.toInt() + 1
                                    getSharedPreferences(PREFERENCES_TRX_ULANG, Context.MODE_PRIVATE)
                                            .edit()
                                            .putString(intent.getStringExtra("nomorHp"), counter.toString() + "")
                                            .apply()
                                }

                                builder.setNegativeButton("TIDAK") { dialog, which ->
                                    finish()
                                }

                                builder.show()
                            }

                            //IF PIN SALAH -> GAUSH ke receipt, munculin input pin nya lat
                        }
                        // Notif

//                        Log.e("### Error ###", data.responseMessage)
//                        Log.e("### Error Code ###", data.responseCode)
                    }


                } else {
                    inputState()
                    Log.e("### Error ###", "Response is null")
                }
            }
        })
    }

    fun cekSaldo(pin: String) {
        val balanceResponse = arrayOf<BalanceResponse?>(null)
        val call = api.balance(kodePlgn, pin)
        call.enqueue(object : Callback<BalanceResponse?> {
            override fun onResponse(call: Call<BalanceResponse?>, response: Response<BalanceResponse?>) {
                balanceResponse[0] = response.body()
                if (balanceResponse[0] != null) {
                    if (balanceResponse[0]!!.success) {

                        CacheManager.balance = "Rp. " + currencyFormatter(balanceResponse[0]!!.saldo)
                        CacheManager.pin = act_code_edit.value.toString()
                        CacheManager.kodePlgn = balanceResponse[0]!!.kdplgnmaster
                        var intent = Intent(applicationContext, HomeActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        //CLEAR PIN VIEW
                        for (i in 0..3) {
                            act_code_edit.onKey(act_code_edit.getFocusedChild(), KeyEvent.KEYCODE_DEL, KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL))
                        }
                        header_input_pin.setText(R.string.wrong_pin_message)
                        header_input_pin.setTextColor(ContextCompat.getColor(applicationContext, R.color.PaleVioletRed))
                        inputState()
                        openSoftKeyboard(applicationContext, act_code_edit)
                    }
                }
            }

            override fun onFailure(call: Call<BalanceResponse?>, t: Throwable) {
                inputState()
                log("Fail to check balance, message : " + t.message)
                makeErrorDialog("Tidak ada jaringan")
                balanceResponse[0]!!.success = false
                balanceResponse[0]!!.respon = t.message!!
            }
        })
    }

    fun trfSaldo(kodeplgn: String, nomorTujuan: String, nominal: String, kodeTrf: String, ulang: String, pin: String){
        val call = api.transfer(
                kodeplgn,
                nomorTujuan,
                pin,
                nominal,
                kodeTrf,
                ulang
        )
        call.enqueue(object :Callback<TransferSaldoResponse?>{
            override fun onFailure(call: Call<TransferSaldoResponse?>, t: Throwable) {
                Toast.makeText(applicationContext, "Internal Server Error", Toast.LENGTH_SHORT).show()
                finish()
            }

            override fun onResponse(call: Call<TransferSaldoResponse?>, response: Response<TransferSaldoResponse?>) {
                checkBalance(pin)
                var data = response.body()
                println("### responseCode = " + response.code())
                if (response.code() != 200) {

                    Toast.makeText(applicationContext, "Time Out", Toast.LENGTH_SHORT).show()
                    finish()

                }

                if (data != null){

                    var intent = Intent(applicationContext, ReceiptActivity::class.java)
                    if (data.success){

                        showNotif(data.keterangan, data.pesan)
                        if (!isFinishing){
                            intent.putExtra(Constant.RECEIPT_HEADER, data.keterangan)
                            intent.putExtra(Constant.RECEIPT_PESAN, data.pesan)
                            intent.putExtra(Constant.RECEIPT_PRODUK, data.Brg)
                            intent.putExtra(Constant.RECEIPT_TANGGAL, data.tanggal)
                            intent.putExtra(Constant.RECEIPT_WAKTU, data.waktu)
                            intent.putExtra(Constant.RECEIPT_NOMOR_TUJUAN, data.nomorTujuan)
                            intent.putExtra(Constant.RECEIPT_STATUS, true)
                            intent.putExtra(Constant.RECEIPT_ID, data.idTrx)
                            intent.putExtra(Constant.RECEIPT_HARGA, data.harga)
                            startActivity(intent)
                            finish()
                        }

                    } else {

                        if (data.keterangan.contains("Pernah dilkaukan")) {

                            val builder = AlertDialog.Builder(ContextThemeWrapper(this@InputPinActivity, R.style.Theme_AppCompat_Light_Dialog))
                            builder.setTitle("Perhatian")
                            builder.setMessage(R.string.retransfer_confirm_message)
                            builder.setPositiveButton("LANJUT") { dialog, which ->

                                waitState(Constant.STATE_TRX)
                                trfSaldo(kodeplgn,nomorTujuan,nominal, kodeTrf, "YA", pin)

                            }

                            builder.setNegativeButton("TIDAK") { dialog, which ->
                                finish()
                            }

                            builder.show()
                        } else {

                            // GANTI ICON GAGAl
                            image_pending.setBackground(ContextCompat.getDrawable(applicationContext, R.drawable.ic_fail))
                            text_trx_keterangan.setText(data.keterangan)
                            text_trx_keterangan.setTextColor(ContextCompat.getColor(applicationContext, R.color.PaleVioletRed))

                        }

                    }

                }
            }
        })
    }

    fun topup(kodeplgn: String, pin: String, nominal: String, bank: String ){

        val call = api.topUp(kodeplgn, pin, nominal, bank)
        call.enqueue(object :Callback<TopUpResponse?>{
            override fun onFailure(call: Call<TopUpResponse?>, t: Throwable) {
                Toast.makeText(applicationContext, "Internal Server Error", Toast.LENGTH_SHORT).show()
                finish()
            }

            override fun onResponse(call: Call<TopUpResponse?>, response: Response<TopUpResponse?>) {
                var data = response.body()
                println("### responseCode = " + response.code())
                if (response.code() != 200) {

                    Toast.makeText(applicationContext, "Time Out", Toast.LENGTH_SHORT).show()
                    finish()

                }

                if (data != null){
                    if (data.success){
                        if (!isFinishing){

                            var intent = Intent(applicationContext, TopUpInstructionActivity::class.java)
                            preferences.edit().apply {
                                putString(Constant.TOPUP_TIKET, data.tiket)
                                putString(Constant.TOPUP_NO_REK, data.bank)
                                putString(Constant.TOPUP_NOMINAL, data.nominalTrf)
                                apply()
                            }
                            startActivity(intent)
                            finish()

                        }
                    } else {
                        if (data.keterangan.toUpperCase().contains("MEMILIKI")){

                            Toast.makeText(applicationContext, data.keterangan, Toast.LENGTH_SHORT).show()
                            var intent = Intent(applicationContext, TopUpInstructionActivity::class.java)
                            startActivity(intent)
                            finish()

                        } else {
                            image_pending.setBackground(ContextCompat.getDrawable(applicationContext, R.drawable.ic_fail))
                            text_trx_keterangan.setText(data.keterangan)
                            text_trx_keterangan.setTextColor(ContextCompat.getColor(applicationContext, R.color.PaleVioletRed))
                        }
                    }

                }
            }
        })
    }

    fun inputState() {
        act_code_edit.visibility = View.VISIBLE
        header_input_pin.visibility = View.VISIBLE
        text_trx_keterangan.visibility = View.GONE
        button_home.visibility = View.GONE
        image_pending.visibility = View.GONE
        button_history.visibility = View.GONE
    }

    fun waitState(tag: String) {
        act_code_edit.visibility = View.GONE
        header_input_pin.visibility = View.GONE
        text_trx_keterangan.visibility = View.VISIBLE
        image_pending.visibility = View.VISIBLE

        if (tag.equals(Constant.STATE_TRX)) {
            text_trx_keterangan.setText(R.string.trx_wait_message)
            button_home.visibility = View.VISIBLE
            button_history.visibility = View.VISIBLE
        } else if (tag.equals(Constant.STATE_NON_TRX)) {
            text_trx_keterangan.setText(R.string.universal_wait_message)
            button_home.visibility = View.GONE
            button_history.visibility = View.GONE
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        finish()
    }
}