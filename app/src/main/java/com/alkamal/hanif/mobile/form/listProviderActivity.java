package com.alkamal.hanif.mobile.form;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.adapter.ProviderAdapter;
import com.alkamal.hanif.mobile.adapter.provider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HanifAlKamal on 03/03/2018.
 */

public class listProviderActivity extends BaseActivity {
    private ListView lvProvider;
    private ProviderAdapter adapter;
    private List<provider> mProviderList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_provider);

        lvProvider = (ListView)findViewById(R.id.lv_provider);

        mProviderList = new ArrayList<>();

        mProviderList.add(new provider(1, "XL"));
        mProviderList.add(new provider(2, "SIMPATI"));
        mProviderList.add(new provider(3, "THREE"));
        mProviderList.add(new provider(4, "INDOSAT"));
        mProviderList.add(new provider(5, "SMARTFREN"));


        adapter = new ProviderAdapter(getApplicationContext(), mProviderList);
        lvProvider.setAdapter(adapter);

        lvProvider.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), "Clicked ID = "+view.getTag(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(listProviderActivity.this, cekHargaActivity.class);
                i.putExtra("pilihProvider",view.getTag().toString());
                startActivity(i);

            }
        });
    }
}
