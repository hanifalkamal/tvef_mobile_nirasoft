package com.alkamal.hanif.mobile.form.Konfigurasi

import android.os.Bundle
import com.alkamal.hanif.mobile.BackgroundService.PrinterService
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.activity_setting_receipt_format.*
import java.lang.Exception

class SettingReceiptFormat : BaseActivity() {

    lateinit var body :String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_receipt_format)

        initial()
        parsingData()

        button_back.setOnClickListener {
            onBackPressed()
        }

        button_print_preview.setOnClickListener {
            try {
                print()
            }catch (E:Exception){
                E.printStackTrace()
            }

        }


    }

    fun parsingData(){
        body =
                "ID Transaksi : " + intent.getStringExtra(Constant.RECEIPT_ID) + "\n" +
                "Tanggal : " + intent.getStringExtra(Constant.RECEIPT_TANGGAL) + "\n" +
                "Jam : " + intent.getStringExtra(Constant.RECEIPT_WAKTU) + "\n" +
                "Produk : " + intent.getStringExtra(Constant.RECEIPT_PRODUK) + "\n" +
                "Harga : " + intent.getStringExtra(Constant.RECEIPT_HARGA) + "\n" +
                "Nomor Tujuan : " + intent.getStringExtra(Constant.RECEIPT_NOMOR_TUJUAN) + "\n"
        edittext_body.setText(body)
    }

    fun print(){

        preferences.edit().putString(Constant.CONFIG_HEADER_PRINTER, edittext_header.text.toString()).apply()
        preferences.edit().putString(Constant.CONFIG_FOOTER_PRINTER, edittext_footer.text.toString()).apply()
        var dataToPrint = edittext_header.text.toString() +"\n\n" + edittext_body.text.toString() + "\n\n" + edittext_footer.text.toString()
        PrinterService.printData(dataToPrint,applicationContext)

    }

    fun initial(){
        edittext_header.setText(preferences.getString(Constant.CONFIG_HEADER_PRINTER,""))
        edittext_footer.setText(preferences.getString(Constant.CONFIG_FOOTER_PRINTER,""))
    }
}