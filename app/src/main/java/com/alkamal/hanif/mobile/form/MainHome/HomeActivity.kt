package com.alkamal.hanif.mobile.form.MainHome

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.BuildConfig
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.adapter.HomeHorizontalBannerAdapter
import com.alkamal.hanif.mobile.form.Histori.HistoriActivity
import com.alkamal.hanif.mobile.form.Konfigurasi.SettingPrinterActivity
import com.alkamal.hanif.mobile.form.Login.ChangePswActivity
import com.alkamal.hanif.mobile.form.Login.LoginActivity
import com.alkamal.hanif.mobile.form.Produk.MerkActivity
import com.alkamal.hanif.mobile.form.Transaksi.InputPinActivity
import com.alkamal.hanif.mobile.form.Transaksi.TransaksiActivity
import com.alkamal.hanif.mobile.model.BannerResponse
import com.alkamal.hanif.mobile.model.ParamConfigResponse
import com.alkamal.hanif.mobile.model.ProductPayment
import com.alkamal.hanif.mobile.utils.CacheManager
import com.alkamal.hanif.mobile.utils.Constant
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.view.*
import kotlinx.android.synthetic.main.payment_product_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomeActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var listProductPayment: List<ProductPayment>
    lateinit var listBanner: List<BannerResponse>
    private val EXPAND = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        getInitComponent()
        setupNavDrawer()
        //Balance Inquiry
//        checkBalance("1234", text_saldo)
//        text_saldo.setText("Rp. 10.000.000")

        //SET no color if api < 21
        if(Build.VERSION.SDK_INT < 21){
            layout_trx_utama.setBackgroundColor(Color.TRANSPARENT)
        }


        layout_trx_utama

        text_version.setText(BuildConfig.VERSION_NAME)

        text_nama_usaha.setText(namaUsaha)

        pullToRefresh.setOnRefreshListener {
            try {
                checkBalance(encodeAndReverse(CacheManager.pin), text_saldo, pullToRefresh)
            }catch (e: Exception){
                var intent = Intent(applicationContext, InputPinActivity::class.java)
                startActivity(intent);
                finish()
            }
        }

        layout_button_tambah_saldo.setOnClickListener {
            var intent = Intent(applicationContext, TransaksiActivity::class.java)
            intent.putExtra(Constant.HOME_TAMBAH_SALDO, true)
            startActivity(intent)
        }

        layout_button_tf_saldo.setOnClickListener {
            var intent = Intent(applicationContext, TransaksiActivity::class.java)
            intent.putExtra(Constant.HOME_TRANSFER_SALDO, true)
            startActivity(intent)
        }

        if (!CacheManager.balance.isNullOrEmpty()) {
            text_saldo.text = CacheManager.balance //Bugs here
        }
        layout_button_isipulsa.setOnClickListener {
            var intent = Intent(this, TransaksiActivity::class.java)
            intent.putExtra(Constant.HOME_TRX_PULSA, "PULSA")
            startActivity(intent)
        }

        layout_button_paketdata.setOnClickListener {
            var intent = Intent(this, TransaksiActivity::class.java)
            intent.putExtra(Constant.HOME_TRX_PULSA, "DATA")
            startActivity(intent)
        }

        layout_button_pln.setOnClickListener {
            var intent = Intent(this, TransaksiActivity::class.java)
//            intent.putExtra(Constant.HOME_IMAGE_EXTRA, listProductPayment.get(p1).img_item1)
            intent.putExtra(Constant.HOME_KODE_BRG_EXTRA, "PLN")
            intent.putExtra(Constant.HOME_NAMA_PRODUK_EXTRA, "PLN - Listrik")
            intent.putExtra(Constant.HOME_HINT_EXTRA, "Nomor Listrik")
            startActivity(intent)
        }

        nav_view.text_logout.setOnClickListener {
            var topic = "/topics/"+preferences.getString(Constant.ACCOUNT_KODE_PLGN, "")
            FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)
            Log.e("topic ", topic)
            topic = "/topics/free_cell"
            FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)

            FirebaseAuth.getInstance().signOut()

            clearUserInfo()
            preferences.edit().putBoolean(Constant.STATE_HAS_LOGGED_IN, false).apply()
            var intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
            finish()
            true
        }

//        val productPaymentAdapter = RecyclerViewAdapter(listProductPayment, this)
//
//        view_product_item.apply {
//            layoutManager = LinearLayoutManager(context)
//            adapter = productPaymentAdapter
//        }



        layout_button_history.setOnClickListener {
            var intent = Intent(applicationContext, HistoriActivity::class.java)
            startActivity(intent)
        }

        view_product_item.layoutManager = object : LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false){
            override fun canScrollHorizontally(): Boolean {
                return false
            }

            override fun canScrollVertically(): Boolean {
                return false
            }
        }

        button_cs.setOnClickListener {

            if (!CacheManager.cs.isNullOrEmpty()) {
                var cs = CacheManager.cs
                startActivity(
                        Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://wa.me/$cs")
                        )
                )
            }


        }


    }

    fun getInitComponent() {
        loadHomeProduct()
        loadHomeBanner()
        getCustomerServiceNumber()
    }

    override fun onResume() {
        super.onResume()
        if (!CacheManager.balance.isNullOrEmpty()) {
            text_saldo.text = CacheManager.balance //Bugs here
        }
        layout_drawer.closeDrawer(GravityCompat.START)
    }

    private class RecyclerViewAdapter(private val listProductPayment: List<ProductPayment>, private val context: Context) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            private val textKategori = view.text_title_product_payment
            private val item1 = view.text_item1
            private val item2 = view.text_item2
            private val item3 = view.text_item3
            private val item4 = view.text_item4
            val Image1 = view.img_item1
            val Image2 = view.img_item2
            val Image3 = view.img_item3
            val Image4 = view.img_item4
            val buttonItem1 = view.button_item1
            val buttonItem2 = view.button_item2
            val buttonItem3 = view.button_item3
            val buttonItem4 = view.button_item4
            val expand = view.text_showall
            val getView = view


            fun bindItem(productPayment: ProductPayment) {

                textKategori.text = productPayment.kategori
                item1.text = productPayment.item1
                item2.text = productPayment.item2
                item3.text = productPayment.item3
                item4.text = productPayment.item4

            }


        }


        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerViewAdapter.ViewHolder {
            return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.payment_product_item, p0, false))
        }

        override fun getItemCount(): Int {
            return listProductPayment.size
        }

        override fun onBindViewHolder(p0: RecyclerViewAdapter.ViewHolder, p1: Int) {

            p0.bindItem(listProductPayment[p1])

            ShowImage(listProductPayment.get(p1).img_item1, p0.Image1, p0.buttonItem1)
            ShowImage(listProductPayment.get(p1).img_item2, p0.Image2, p0.buttonItem2)
            ShowImage(listProductPayment.get(p1).img_item3, p0.Image3, p0.buttonItem3)
            ShowImage(listProductPayment.get(p1).img_item4, p0.Image4, p0.buttonItem4)


            // Cek kategori is tagihan
            var intent = Intent(context, TransaksiActivity::class.java)
            intent.putExtra(Constant.HOME_KATEGORI_EXTRA, listProductPayment.get(p1).kategori)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            p0.buttonItem1.setOnClickListener {
                intent.putExtra(Constant.HOME_IMAGE_EXTRA, listProductPayment.get(p1).img_item1)
                intent.putExtra(Constant.HOME_KODE_BRG_EXTRA, listProductPayment.get(p1).kode_item1)
                intent.putExtra(Constant.HOME_NAMA_PRODUK_EXTRA, listProductPayment.get(p1).item1)
                intent.putExtra(Constant.HOME_HINT_EXTRA, listProductPayment.get(p1).hint1)
                ContextCompat.startActivity(context, intent, null)
            }

            p0.buttonItem2.setOnClickListener {
                intent.putExtra(Constant.HOME_IMAGE_EXTRA, listProductPayment.get(p1).img_item2)
                intent.putExtra(Constant.HOME_KODE_BRG_EXTRA, listProductPayment.get(p1).kode_item2)
                intent.putExtra(Constant.HOME_NAMA_PRODUK_EXTRA, listProductPayment.get(p1).item2)
                intent.putExtra(Constant.HOME_HINT_EXTRA, listProductPayment.get(p1).hint2)
                ContextCompat.startActivity(context, intent, null)
            }

            p0.buttonItem3.setOnClickListener {
                intent.putExtra(Constant.HOME_IMAGE_EXTRA, listProductPayment.get(p1).img_item3)
                intent.putExtra(Constant.HOME_KODE_BRG_EXTRA, listProductPayment.get(p1).kode_item3)
                intent.putExtra(Constant.HOME_NAMA_PRODUK_EXTRA, listProductPayment.get(p1).item3)
                intent.putExtra(Constant.HOME_HINT_EXTRA, listProductPayment.get(p1).hint3)
                ContextCompat.startActivity(context, intent, null)
            }

            p0.buttonItem4.setOnClickListener {
                intent.putExtra(Constant.HOME_IMAGE_EXTRA, listProductPayment.get(p1).img_item4)
                intent.putExtra(Constant.HOME_KODE_BRG_EXTRA, listProductPayment.get(p1).kode_item4)
                intent.putExtra(Constant.HOME_NAMA_PRODUK_EXTRA, listProductPayment.get(p1).item4)
                intent.putExtra(Constant.HOME_HINT_EXTRA, listProductPayment.get(p1).hint4)
                ContextCompat.startActivity(context, intent, null)
            }

            p0.expand.setOnClickListener {
                var i = Intent(context, ExpandProductActivity::class.java)
                i.putExtra(Constant.HOME_KATEGORI_EXTRA, listProductPayment.get(p1).kategori)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                ContextCompat.startActivity(context, i, null)
            }
        }

        fun ShowImage(bannerUrl: String, imageView: ImageView, layout: RelativeLayout) {

            if (bannerUrl.isEmpty()){
                layout.visibility = View.INVISIBLE
            } else {

                Glide.with(context)
                        .load(Constant.BaseUrlPic + bannerUrl)
                        .apply(RequestOptions().override(100, 100))
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
//                            view.progressBar.visibility = View.GONE
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
//                            view.progressBar .visibility = View.GONE
                                return false
                            }

                        })
                        .into(imageView)
            }
        }

    }



    fun loadHomeProduct(){
        var call = api.listHomeProduct()
        call.enqueue(object : Callback<List<ProductPayment>> {
            override fun onFailure(call: Call<List<ProductPayment>>, t: Throwable) {
                Toast.makeText(applicationContext, "Internal Server error", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<List<ProductPayment>>, response: Response<List<ProductPayment>>) {
                var data = response.body()

                if (data != null) {
                    val productPaymentAdapter = RecyclerViewAdapter(data, applicationContext)

                    view_product_item.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = productPaymentAdapter
                    }
                }
            }
        })
    }

    fun loadHomeBanner(){
        var call = api.listBanner()
        call.enqueue(object : Callback<List<BannerResponse>> {
            override fun onFailure(call: Call<List<BannerResponse>>, t: Throwable) {
                Toast.makeText(applicationContext, "Internal Server error", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<List<BannerResponse>>, response: Response<List<BannerResponse>>) {
                var data = response.body()

                if (data != null) {
                    if (!data.isEmpty()) {
                        listBanner = data
                        val infoProductAdapter = HomeHorizontalBannerAdapter(applicationContext, listBanner)

                        view_info_produk.apply {
                            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                            adapter = infoProductAdapter
                        }
                    }
                }
            }
        })
    }

    fun getCustomerServiceNumber(){
        var call = api.csNumber
        call.enqueue(object : Callback<ParamConfigResponse> {
            override fun onFailure(call: Call<ParamConfigResponse>, t: Throwable) {
                Toast.makeText(applicationContext, "Internal Server error", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<ParamConfigResponse>, response: Response<ParamConfigResponse>) {
                var data = response.body()

                if (data != null) {
                    CacheManager.cs = data.value
                }
            }
        })
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == EXPAND) {
            Toast.makeText(applicationContext, "aASD", Toast.LENGTH_SHORT).show()
        }
    }

    fun setupNavDrawer() {
//        val toggle = ActionBarDrawerToggle(
//                this, layout_drawer, 1, 2)
//        layout_drawer.addDrawerListener(toggle)
//        toggle.syncState()
        image_icon_logo.setOnClickListener({
            layout_drawer.openDrawer(GravityCompat.START)
        })
        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_setting_printer -> {

                var intent = Intent(applicationContext, SettingPrinterActivity::class.java)
                startActivity(intent)

                true
            }

            R.id.nav_hp -> {
                var intent = Intent(applicationContext, MerkActivity::class.java)
                startActivity(intent)
                true
            }

            R.id.nav_profile -> {
                var intent = Intent(applicationContext, ProfileActivity::class.java)
                startActivity(intent)
                true
            }

            R.id.nav_ganti_psw -> {
                var intent = Intent(applicationContext, ChangePswActivity::class.java)
                startActivity(intent)
                true
            }

        }
        return true
    }


    var itemDummy = "[\n" +
            "  {\n" +
            "    \"kategori\": \"e-wallet\",\n" +
            "    \"item1\": \"OVO\",\n" +
            "    \"kode_item1\": \"OVO\",\n" +
            "    \"img_item1\": \"/uploads/product/ic_ovo.jpg\",\n" +
            "    \"hint1\": \"Nomor Telepon / OVO ID\",\n" +
            "    \"item2\": \"DANA\",\n" +
            "    \"kode_item2\": \"DANA\",\n" +
            "    \"img_item2\": \"/uploads/product/ic_dana.png\",\n" +
            "    \"hint2\": \"Nomor Telepon / DANA ID\",\n" +
            "    \"item3\": \"GoPay\",\n" +
            "    \"kode_item3\": \"GOJEK\",\n" +
            "    \"img_item3\": \"/uploads/product/ic_gopay.jpg\",\n" +
            "    \"hint3\": \"Nomor Telepon / Gojek ID\",\n" +
            "    \"item4\": \"LinkAja!\",\n" +
            "    \"kode_item4\": \"LINKAJA\",\n" +
            "    \"img_item4\": \"/uploads/product/ic_link.png\",\n" +
            "    \"hint4\": \"Nomor Telepon / LinkAja ID\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"kategori\": \"Games\",\n" +
            "    \"item1\": \"MOBILELEGENDS\",\n" +
            "    \"kode_item1\": \"ML\",\n" +
            "    \"img_item1\": \"/uploads/product/ic_ml.jpg\",\n" +
            "    \"hint1\": \"ML ID\",\n" +
            "    \"item2\": \"FREE FIRE\",\n" +
            "    \"kode_item2\": \"FIRE\",\n" +
            "    \"img_item2\": \"/uploads/product/ic_freefire.jpg\",\n" +
            "    \"hint2\": \"FF ID\",\n" +
            "    \"item3\": \"PUBG\",\n" +
            "    \"kode_item3\": \"PUBG\",\n" +
            "    \"img_item3\": \"/uploads/product/ic_pubgm.jpg\",\n" +
            "    \"hint3\": \"PUBG ID\",\n" +
            "    \"item4\": \"GARENA\",\n" +
            "    \"kode_item4\": \"GARENA\",\n" +
            "    \"img_item4\": \"/uploads/product/ic_garena.jpg\",\n" +
            "    \"hint4\": \"GARENA ID\"\n" +
            "  }" +
            "]"


}
