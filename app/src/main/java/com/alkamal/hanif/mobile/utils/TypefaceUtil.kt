package com.alkamal.hanif.mobile.utils

import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.util.Log
import java.lang.reflect.Field


public class TypefaceUtil {

    fun overrideFont(context: Context, defaultFontNameToOverride: String, customFontFileNameInAssets: String) {
        val customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val newMap: MutableMap<String, Typeface> = HashMap()
            newMap["serif"] = customFontTypeface
            try {
                val staticField: Field = Typeface::class.java
                        .getDeclaredField("sSystemFontMap")
                staticField.setAccessible(true)
                staticField.set(null, newMap)
            } catch (e: NoSuchFieldException) {
                e.printStackTrace()
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            }
        } else {
            try {
                val defaultFontTypefaceField: Field = Typeface::class.java.getDeclaredField(defaultFontNameToOverride)
                defaultFontTypefaceField.setAccessible(true)
                defaultFontTypefaceField.set(null, customFontTypeface)
            } catch (e: Exception) {
                Log.e(TypefaceUtil::class.java.simpleName, "Can not set custom font $customFontFileNameInAssets instead of $defaultFontNameToOverride")
            }
        }
    }
}