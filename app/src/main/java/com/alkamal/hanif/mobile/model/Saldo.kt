package com.alkamal.hanif.mobile.model

import com.google.gson.annotations.SerializedName

class Saldo (
        @SerializedName("saldo") val saldo : String,
        @SerializedName("nama") val nama : String,
        @SerializedName("kdplgnmaster") val kdplgnmaster : String,
        @SerializedName("kdplgn") val kdplgn : String,
        @SerializedName("hp") val hp : String,
        @SerializedName("tglanggota") val tglanggota : String,
        @SerializedName("tglpoint") val tglpoint : String,
        @SerializedName("statusharga") val statusharga : String,
        @SerializedName("maxpiutang") val maxpiutang : String,
        @SerializedName("statusplgn") val statusplgn : String,
        @SerializedName("uperkodeplgn") val uperkodeplgn : String,
        @SerializedName("kodeplgn_induk") val kodeplgn_induk : String,
        @SerializedName("namaupline") val namaupline : String,
        @SerializedName("bankrekening") val bankrekening : String,
        @SerializedName("banknama") val banknama : String,
        @SerializedName("bank_an") val bank_an : String,
        @SerializedName("blok") val blok : Boolean,
        @SerializedName("masa_aktif") val masa_aktif : String,
        @SerializedName("kodepos") val kodepos : String,
        @SerializedName("laba") val laba : String,
        @SerializedName("pointawal") val pointawal : String,
        @SerializedName("komisi") val komisi : String,
        @SerializedName("idutama") val idutama : String,
        @SerializedName("NamaUpline_pelanggan") val namaUpline_pelanggan : String,
        @SerializedName("cekpoint") val cekpoint : String,
        @SerializedName("pengumuman") val pengumuman : String

)