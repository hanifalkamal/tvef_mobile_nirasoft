package com.alkamal.hanif.mobile.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.alkamal.hanif.mobile.R
import kotlinx.android.synthetic.main.trx_spinner_item.view.*
import java.lang.Exception
import java.text.DecimalFormat

class BarangAdapter(
        val context : Context,
        val barang : ArrayList<String>,
        val hargaBarang : ArrayList<String>

):BaseAdapter(){

    override fun getView(position: Int, view: View?, viewGroup: ViewGroup?): View {
        var v = View.inflate(context, R.layout.trx_spinner_item , null)
        try {
            v.lb_listBarang.setText(barang.get(position).toString())
            currencyFormatter(hargaBarang.get(position).toString(), v.lb_listHargaBarang)
//        v.lb_listHargaBarang.setText("Rp. "+hargaBarang.get(position).toString())
        }catch (E:Exception){
            E.printStackTrace()
        }
            return v
    }

    override fun getItem(p0: Int): Any {
        return barang.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return barang.size
    }

    fun currencyFormatter(p0: String, tv :  TextView){
        try {
            var givenstring = p0.toString()
            val longval: Long?
            if (givenstring.contains(",")) {
                givenstring = givenstring.replace(",".toRegex(), "")
            }
            longval = java.lang.Long.parseLong(givenstring)
            val formatter = DecimalFormat("#,###,###")
            var formattedString = formatter.format(longval)
            formattedString = formattedString.replace(",".toRegex(), ".")
            tv!!.setText("Rp. "+formattedString)
//            tv!!.setSelection(edOmset!!.getText().length)
            // to place the cursor at the end of text
        } catch (nfe: NumberFormatException) {
            nfe.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}