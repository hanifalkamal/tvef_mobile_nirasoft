package com.alkamal.hanif.mobile.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.alkamal.hanif.mobile.R;

import java.util.List;

/**
 * Created by HanifAlKamal on 03/03/2018.
 */

public class HistoriTrxListAdapter extends BaseAdapter {
    private Context mContext;
    private List<Histori> mHistori;

    public HistoriTrxListAdapter(Context mContext, List<Histori> mHistori) {
        this.mContext = mContext;
        this.mHistori = mHistori;
    }

    @Override
    public int getCount() {
        return mHistori.size();
    }

    @Override
    public Object getItem(int position) {
        return mHistori.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View v = View.inflate(mContext, R.layout.listhistoritrx_item_layout, null);
//        TextView tvHisTrx = (TextView)v.findViewById(R.id.tv_ListhistoriTrx);
//
//        tvHisTrx.setText(mHistori.get(position).getIsiHistori());

        v.setTag(mHistori.get(position).getId());

        return v;
    }
}
