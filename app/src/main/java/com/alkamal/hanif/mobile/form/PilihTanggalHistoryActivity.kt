package com.alkamal.hanif.mobile.form

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import android.widget.Toast
import com.alkamal.hanif.mobile.R
import kotlinx.android.synthetic.main.activity_pilih_tanggal_history.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class PilihTanggalHistoryActivity : AppCompatActivity() {

    internal var myCalendar = Calendar.getInstance()
    internal lateinit var date: DatePickerDialog.OnDateSetListener
    var selectedEditTextTgl : EditText? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pilih_tanggal_history)

        rb_hari_ini.isChecked = true;
        rb_hari_ini.setOnClickListener {

            if (rb_history.isChecked == true){
                rb_history.isChecked = false;
            }

        }

        rb_history.setOnClickListener {

            if (rb_hari_ini.isChecked == true){
                rb_hari_ini.isChecked = false;
            }

        }

        ed_tanggal1.setOnClickListener {
            selectedEditTextTgl = ed_tanggal1;
            DatePickerDialog(
                    this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        ed_tanggal2.setOnClickListener {
            selectedEditTextTgl = ed_tanggal2;
            DatePickerDialog(
                    this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel(selectedEditTextTgl!!)
        }

        bt_next.setOnClickListener {
//            println("RB HISTORI = "+rb_history.isChecked)
            try {
                val intent = Intent(this, HistoriTrx_Activity::class.java)
                if (rb_history.isChecked == true) {
//                println("Tanngal 1 length = "+ed_tanggal1.text.length)
//                println("Tanngal 2 length = "+ed_tanggal2.text.length)
                    if ((ed_tanggal1.text.length < 1) || (ed_tanggal2.text.length < 1)) {

                        Toast.makeText(this, "Tanggal tidak boleh kosong !", Toast.LENGTH_SHORT).show()
                        throw Exception("Tanggal Kososng")

                    } else {

                        intent.putExtra("tanggal1", ed_tanggal1.text.toString())
                        intent.putExtra("tanggal2", ed_tanggal2.text.toString())

                    }

                }

                startActivity(intent)
            }catch (E:Exception){
                E.printStackTrace()
            }
        }

    }

    fun updateLabel(editText: EditText){
        val myFormat = "yyyy-MM-dd" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)

        editText.setText(sdf.format(myCalendar.time))
    }


}
