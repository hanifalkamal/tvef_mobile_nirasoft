package com.alkamal.hanif.mobile.adapter;

/**
 * Created by HanifAlKamal on 03/03/2018.
 */

public class provider {
    private int id;
    private String nama_provider;

    public provider(int id, String nama_provider) {
        this.id = id;
        this.nama_provider = nama_provider;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_provider() {
        return nama_provider;
    }

    public void setNama_provider(String nama_provider) {
        this.nama_provider = nama_provider;
    }
}
