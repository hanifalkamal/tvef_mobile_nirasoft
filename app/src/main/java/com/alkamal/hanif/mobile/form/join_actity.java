package com.alkamal.hanif.mobile.form;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.widget.EditText;
import android.widget.Toast;

import com.alkamal.hanif.mobile.BaseActivity;
import com.alkamal.hanif.mobile.MainActivity;
import com.alkamal.hanif.mobile.R;
import com.alkamal.hanif.mobile.Response.SendMsgRespone;
import com.alkamal.hanif.mobile.Response.MsgResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HanifAlKamal on 19/02/2018.
 */

public class join_actity extends BaseActivity {
    static String myID;
    Calendar calendar;
    SimpleDateFormat dateformat;
    String tanggal,idtrx,pin,Server,kodeBrg;
    NotificationCompat.Builder notification;
    private static final int uniqueID = 45612;
    int CekCounting,waktutimeout;

    @BindView(R.id.ed_noMember)
    EditText edNoMember;
    @BindView(R.id.ed_join_nama) EditText edNama;
    @BindView(R.id.ed_join_alamat) EditText edAlamat;
    @BindView(R.id.ed_join_telp) EditText edTelp;
    String Nama,Alamat,Notelp;
    ProgressDialog prd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_layout);
        ButterKnife.bind(this);

        ////myImei = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        SharedPreferences prefs = getSharedPreferences("MY_DATA", MODE_PRIVATE);
        waktutimeout = Integer.parseInt(prefs.getString("MY_TRX_TIME_OUT", ""));
        Server = prefs.getString("MY_SERVER", "");

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);
        Intent intent = getIntent();
        Nama = intent.getStringExtra("nama");
        Alamat = intent.getStringExtra("alamat");
        Notelp = intent.getStringExtra("notelp");


    }

    @OnClick(R.id.bt_Send_join) public void onSendJoin(){
        String NoMember = edNoMember.getText().toString();
        String Format = "join " +
                        NoMember + " " +
                "\""+Nama+"\"" + " " +
                "\""+Alamat+"\"" + " "+
                "\" 62"+Notelp+"\"" + " " ;
        Toast.makeText(this, Format,Toast.LENGTH_LONG).show();
        myID = "AND62"+Notelp;
        prd = new ProgressDialog(this);
        prd.setMessage("Mohon Tunggu. . .");
        prd.setCancelable(false);
        prd.setCanceledOnTouchOutside(false);

        prd.show();

        sendReqSal(Format);
    }

    public void sendReqSal(String format){
        calendar = Calendar.getInstance();
        dateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        tanggal = dateformat.format(calendar.getTime());
        idtrx = tanggal;

        Call<SendMsgRespone> call = getApi().SendMSg(myID, format, idtrx, Server, "");
        call.enqueue(new Callback<SendMsgRespone>() {
            @Override
            public void onResponse(Call<SendMsgRespone> call, Response<SendMsgRespone> response) {
                if (response.isSuccessful()) {
                    SendMsgRespone res = response.body();
                    if (res.isSuccess()) {
                        Toast.makeText(join_actity.this, res.getMessage(), Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(MainActivity.this, loginActivity.class);
                        //intent.putExtra("NRP",res.getNRP());
                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());
                        //startActivity(intent);
                        //tvMsg.setText(res.getMessage());
                        CekCounting = 0;
                        //Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();

                        getBalsan();

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(res.getMessage());
                        //prd.dismiss();
                        Toast.makeText(join_actity.this, "Cek Koneksi Internet", Toast.LENGTH_LONG).show();
                        log("Gagal");
                    }
                }
            }

            @Override
            public void onFailure(Call<SendMsgRespone> call, Throwable t) {log(t.toString());}
        });
    }

    public void getBalsan(){
        Call<MsgResponse> call = getApi().GetMSg(myID,idtrx, Server);
        call.enqueue(new Callback<MsgResponse>() {

            @Override
            public void onResponse(Call<MsgResponse> call, Response<MsgResponse> response) {
                if (response.isSuccessful()) {
                    MsgResponse ges = response.body();
                    if (ges.IsSuccess()) {
                        //Toast.makeText(join_actity.this, ges.getPesan(), Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(join_actity.this, BalasanActivity.class);
                        intent.putExtra("CekSal",ges.getPesan());

                        //intent.putExtra("password",res.getPassword());
                        //intent.putExtra("email",res.getEmail());

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                // yourMethod();
                            }
                        }, 1000);   //5 seconds*/
                        prd.dismiss();
                        Notif(ges.getPesan());
                        startActivity(intent);
                        finish();
                        //tvSaldo.setText(ges.getPesan());

                        //finish();
                        log("Berhasil");

                    } else {
                        //tvMsg.setText(ges.getPesan());
                        // Toast.makeText(MainActivity.this, "Tunggu Balasan Masuk", Toast.LENGTH_LONG).show();
                        log("Gagal");
                        if (CekCounting < waktutimeout){
                            getBalsan();
                            CekCounting = CekCounting+1;
                        }else{
                            prd.dismiss();
                            Notif("Time Out / Tidak ada balasan masuk");
                            Toast.makeText(join_actity.this, "Time Out", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MsgResponse> call, Throwable t) {log(t.toString());}
        });
    }

    public void Notif(String notifBody){
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker("This is the ticker");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("TVEF");
        notification.setContentText(notifBody);

        Context context = getApplicationContext();
        Intent intent = new Intent(context, BalasanActivity.class);
        intent.putExtra("CekSal",notifBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());

    }

    @OnClick(R.id.tv_BacktoLogin) public void onBacktoLogin(){
        Intent intent = new Intent(join_actity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
