package com.alkamal.hanif.mobile.form.Register

import android.content.Intent
import android.os.Bundle
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.form.Login.LoginActivity
import kotlinx.android.synthetic.main.activity_register_succeed.*

class RegisterSucceedActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_succeed)

        button_shortcut_signin.setOnClickListener {
            var intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

    }


}
