package com.alkamal.hanif.mobile.form.Register

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.model.CreateAccountResponse
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.daftar_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class RegisterFormActivity : BaseActivity() {

    lateinit var namaPemilik: String
    lateinit var RegisnamaUsaha: String
    lateinit var noTlp: String
    lateinit var provinsi: String
    lateinit var kota: String
    lateinit var kecamatan: String
    lateinit var kelurahan: String
    lateinit var alamat: String
    lateinit var alamatLengkap: String
    lateinit var password: String
    lateinit var confPassword: String
    lateinit var pin: String
    lateinit var confPin: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.daftar_layout)

        var opsi = intent.getStringExtra(Constant.OPSI_REGIS)

        button_back.setOnClickListener {
            onBackPressed()
        }

        ed_noTelp_daftar.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                try {
                    if ("" + ed_noTelp_daftar.getText().get(0) == "0") {
                        ed_noTelp_daftar.setText(ed_noTelp_daftar.getText().toString().substring(1))
                    } else if ("" + ed_noTelp_daftar.getText().get(0) + "" + ed_noTelp_daftar.getText().get(1) == "62") {
                        ed_noTelp_daftar.setText(ed_noTelp_daftar.getText().toString().substring(2))
                    }
                } catch (E: Exception) {
                    E.printStackTrace()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        bt_Send_Daftar.setOnClickListener {

            hideEmptyMessage()

            namaPemilik = ed_nama_pemilik.text.toString()
            noTlp = edittext_prefix.text.toString() + ed_noTelp_daftar.text.toString()
            RegisnamaUsaha = ed_nama_usaha.text.toString()
            provinsi = ed_provinsi.text.toString()
            kota = ed_kota.text.toString()
            kecamatan = ed_kecamatan.text.toString()
            kelurahan = ed_kelurahan.text.toString()
            alamat = ed_alamat.text.toString()
            password = ed_psw.text.toString()
            confPassword = ed_psw_conf.text.toString()
            pin = ed_pin.text.toString()
            confPin = ed_pin_conf.text.toString()

            alamatLengkap = "$alamat, $kecamatan, $kelurahan, $kota, $provinsi"

            if (namaPemilik.isNullOrEmpty()){
                text_empty_nama.visibility = View.VISIBLE
            } else if (noTlp.length < 3){
                text_empty_no_telp.visibility = View.VISIBLE
            } else if (RegisnamaUsaha.isNullOrEmpty()){
                text_empty_nama_usaha.visibility = View.VISIBLE
            } else if (provinsi.isNullOrEmpty()){
                text_empty_pronvinsi.visibility = View.VISIBLE
            } else if (kota.isNullOrEmpty()){
                text_empty_kota.visibility = View.VISIBLE
            } else if (kelurahan.isNullOrEmpty()){
                text_empty_kelurahan.visibility = View.VISIBLE
            } else if (kecamatan.isNullOrEmpty()){
                text_empty_kecamatan.visibility = View.VISIBLE
            } else if (alamat.isNullOrEmpty()){
                text_empty_alamat.visibility = View.VISIBLE
            } else if (password.isNullOrEmpty()){
                text_empty_password.visibility = View.VISIBLE
            } else if (confPassword.isNullOrEmpty()){
                text_empty_password.visibility = View.VISIBLE
            } else if (!confPassword.equals(password)){
                text_empty_password.setText("Password dan Konfirmasi Password Tidak Sama !")
                text_empty_password.visibility = View.VISIBLE
            } else if (pin.isNullOrEmpty()){
                text_empty_pin.visibility = View.VISIBLE
            } else if (pin.length < 4){
                text_empty_pin.visibility = View.VISIBLE
                text_empty_pin.setText("Pin harus 4 digit")
            } else if (confPin.isNullOrEmpty()){
                text_empty_pin.visibility = View.VISIBLE
            } else if (!confPin.equals(pin)){
                text_empty_pin.setText("Pin dan Konfirmasi Pin Tidak Sama !")
                text_empty_pin.visibility = View.VISIBLE
            } else {

                if (opsi.equals(Constant.OPSI_REGIS_PRAKAITKAN)) {

                    var intent = Intent(this, KaitkanAccountActivity::class.java)
                    intent.putExtra(Constant.OPSI_REGIS, Constant.OPSI_REGIS_PRAKAITKAN)
                    intent.putExtra(Constant.REGISTER_NAMA_PEMILIK, namaPemilik)
                    intent.putExtra(Constant.REGISTER_NO_TELP, encodeAndReverse(noTlp))
                    intent.putExtra(Constant.REGISTER_NAMA_USAHA, RegisnamaUsaha)
                    intent.putExtra(Constant.REGISTER_ALAMAT, alamatLengkap)
                    intent.putExtra(Constant.REGISTER_PSW, encodeAndReverse(password))
                    intent.putExtra(Constant.REGISTER_PIN, encodeAndReverse(pin))
                    startActivity(intent)

                } else if (opsi.equals(Constant.OPSI_REGIS_JOIN)) {

                    var intent = Intent(this, JoinAccontActivity::class.java)
                    intent.putExtra(Constant.OPSI_REGIS, Constant.OPSI_REGIS_JOIN)
                    intent.putExtra(Constant.REGISTER_NAMA_PEMILIK, namaPemilik)
                    intent.putExtra(Constant.REGISTER_NO_TELP, encodeAndReverse(noTlp))
                    intent.putExtra(Constant.REGISTER_NAMA_USAHA, RegisnamaUsaha)
                    intent.putExtra(Constant.REGISTER_ALAMAT, alamatLengkap)
                    intent.putExtra(Constant.REGISTER_PSW, encodeAndReverse(password))
                    intent.putExtra(Constant.REGISTER_PIN, encodeAndReverse(pin))
                    startActivity(intent)

                } else if (opsi.equals(Constant.OPSI_REGIS_NEW)) {
                    submit()
                }
            }
        }
    }

    fun hideEmptyMessage() {
        text_empty_nama.visibility = View.GONE
        text_empty_no_telp.visibility = View.GONE
        text_empty_nama_usaha.visibility = View.GONE
        text_empty_pronvinsi.visibility = View.GONE
        text_empty_kota.visibility = View.GONE
        text_empty_kelurahan.visibility = View.GONE
        text_empty_kecamatan.visibility = View.GONE
        text_empty_alamat.visibility = View.GONE
        text_empty_password.visibility = View.GONE
        text_empty_password.setText("Password Tidak Boleh Kosong !")
        text_empty_pin.visibility = View.GONE
        text_empty_pin.setText("PIN Tidak Boleh Kosong !")
        text_fail_register.visibility = View.GONE
    }

    fun submit() {

        try {
            progressBar.visibility = View.VISIBLE
            var call = api.createAccount(encodeAndReverse(noTlp), encodeAndReverse(password), RegisnamaUsaha, namaPemilik, alamatLengkap, encodeAndReverse(androidId), encodeAndReverse(deviceModel), "", encodeAndReverse(pin), "", "NEW")
            call.enqueue(object : Callback<CreateAccountResponse> {
                override fun onFailure(call: Call<CreateAccountResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, "Internal Server Error", Toast.LENGTH_SHORT).show()
                    progressBar.visibility = View.GONE
                }

                override fun onResponse(call: Call<CreateAccountResponse>, response: Response<CreateAccountResponse>) {
                    var result = response.body()
                    progressBar.visibility = View.GONE
                    if (result != null) {
                        if (result.success) {
                            var intent = Intent(applicationContext, RegisterSucceedActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent)

                        } else {
                            throw Exception(result.keterangan)
                        }

                    }
                }
            })


        } catch (e: Exception) {
            progressBar.visibility = View.GONE
            text_fail_register.visibility = View.VISIBLE
            text_fail_register.setText(e.message)
            e.printStackTrace()
        }

    }
}
