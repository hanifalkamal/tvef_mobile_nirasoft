package com.alkamal.hanif.mobile.form.Histori

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseFragment
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.model.PendingResponse
import kotlinx.android.synthetic.main.fragment_histori_transaction.*
import kotlinx.android.synthetic.main.fragment_histori_transaction.progressBar
import kotlinx.android.synthetic.main.fragment_pending_transaction.*
import kotlinx.android.synthetic.main.item_histori_pending.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * A simple [Fragment] subclass.
 * Use the [PendingTransactionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PendingTransactionFragment : BaseFragment() {

    lateinit var listData: List<PendingResponse>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pending_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setmContext(context)
        progressBar.visibility = View.VISIBLE
        loadPending()

        pullToRefreshPending.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener{
            override fun onRefresh() {
                println("### REFRESH")
                loadPending()
            }
        })
    }

    fun loadPending(){

        listData = ArrayList()
        var  call = api.listPendingTransaction(kodePlgn)
        call.enqueue(object : Callback<List<PendingResponse>> {
            override fun onFailure(call: Call<List<PendingResponse>>, t: Throwable) {
                Toast.makeText(context, "Internal Server Error", Toast.LENGTH_SHORT).show()
                progressBar.visibility = View.GONE
                if (pullToRefreshPending.isRefreshing()){
                    pullToRefreshPending.setRefreshing(false)
                }

            }

            override fun onResponse(call: Call<List<PendingResponse>>, response: Response<List<PendingResponse>>) {

                var data = response.body()

                if (data != null) {

                    listData = data

                    if (listData.isEmpty()){
                        layout_image_empty_pending_data.visibility = View.VISIBLE
                    } else {
                        layout_image_empty_pending_data.visibility = View.GONE
                    }

                    val Adapter = RecyclerViewAdapter(listData)

                    view_list_pending_trx.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = Adapter
                    }
                    progressBar.visibility = View.GONE
                    if (pullToRefreshPending.isRefreshing()){
                        pullToRefreshPending.setRefreshing(false)
                    }

                }
            }

        })

    }

    private class RecyclerViewAdapter(private val ListData: List<PendingResponse>) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(){

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            val textNamaProduk = view.text_nama_produk
            val textTanggalJam = view.text_tanggal_jam
            val textStatus = view.text_status
            val textNoTujuan = view.text_no_tujaun
            val layoutItem = view.layout_item_trx
            val context = view.context

            fun bindItem(Data: PendingResponse){
                textNamaProduk.setText(Data.namaProduk)
                textTanggalJam.setText(Data.tanggalJam)
                textNoTujuan.setText(Data.nomorTujuan)

                if (Data.status.toUpperCase().equals("PENDING")) {
                    textStatus.setTextColor(ContextCompat.getColor(context, R.color.DarkOrange))
                } else if (Data.status.toUpperCase().equals("TIME OUT")) {
                    textStatus.setTextColor(ContextCompat.getColor(context, R.color.DarkRed))
                }
                textStatus.setText(Data.status)

                layoutItem.setOnClickListener {

//                    layoutItem.setBackgroundColor(ContextCompat.getColor(context, R.color.NewUI_Gray))
//                    var intent = Intent(context.applicationContext, ReceiptActivity::class.java)
//                    intent.putExtra(Constant.RECEIPT_HEADER, Data.status)
//                    intent.putExtra(Constant.RECEIPT_PESAN, Data.pesan)
//                    intent.putExtra(Constant.RECEIPT_PRODUK, Data.namaProduk)
//                    intent.putExtra(Constant.RECEIPT_TANGGAL, Data.tanggal)
//                    intent.putExtra(Constant.RECEIPT_WAKTU, Data.jam)
//                    intent.putExtra(Constant.RECEIPT_NOMOR_TUJUAN, Data.nomorTujuan)
//                    intent.putExtra(Constant.RECEIPT_ID, Data.idTrx)
//                    intent.putExtra(Constant.RECEIPT_HARGA, Data.hargaJual)
//                    if (Data.status.toUpperCase().equals("BERHASIL") ||
//                            Data.status.toUpperCase().equals("RALAT") ||
//                            Data.status.toUpperCase().equals("+SALDO")
//                    ) {
//                        intent.putExtra(Constant.RECEIPT_STATUS, true)
//
//                    } else {
//                        intent.putExtra(Constant.RECEIPT_STATUS, false)
//                    }
//
//                    context.startActivity(intent)

                }

            }




        }



        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerViewAdapter.ViewHolder {
            return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_histori_pending, p0, false))
        }

        override fun getItemCount(): Int {
            return ListData.size
        }

        override fun onBindViewHolder(p0: RecyclerViewAdapter.ViewHolder, p1: Int) {
            p0.bindItem(ListData.get(p1))


        }


    }
}