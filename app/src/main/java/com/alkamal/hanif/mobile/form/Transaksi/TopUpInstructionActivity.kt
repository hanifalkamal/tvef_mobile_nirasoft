package com.alkamal.hanif.mobile.form.Transaksi

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Toast
import com.alkamal.hanif.mobile.BaseActivity
import com.alkamal.hanif.mobile.R
import com.alkamal.hanif.mobile.utils.CacheManager
import com.alkamal.hanif.mobile.utils.Constant
import kotlinx.android.synthetic.main.activity_top_up_instruction.*

class TopUpInstructionActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_up_instruction)

        init()

        button_back.setOnClickListener {
            onBackPressed()
        }

        text_copy_bank.setOnClickListener {
            var arr = text_value_bank.text.split(" ").toTypedArray()
            var noRek = arr[1]
            copyToClipBoard("noRekening", noRek)
            Toast.makeText(applicationContext, "Copy to clipboard", Toast.LENGTH_SHORT).show()
        }

        text_copy_nominal.setOnClickListener {
            copyToClipBoard("nominal", preferences.getString(Constant.TOPUP_NOMINAL, "-"))
            Toast.makeText(applicationContext, "Copy to clipboard", Toast.LENGTH_SHORT).show()
        }

        text_copy_no_cs.setOnClickListener {
            copyToClipBoard("cs", text_no_cs.text.toString())
            Toast.makeText(applicationContext, "Copy to clipboard", Toast.LENGTH_SHORT).show()
        }

        if (!CacheManager.cs.isNullOrEmpty()) {
            var csdisplay = CacheManager.cs
            text_no_cs.setText(csdisplay)
        }


        button_wa.setOnClickListener {
            if (!CacheManager.cs.isNullOrEmpty()) {
                var cs = CacheManager.cs
                startActivity(
                        Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://wa.me/$cs")
                        )
                )
            }

        }



    }

    fun init(){
        text_value_tiket.setText(preferences.getString(Constant.TOPUP_TIKET, "-"))
        text_value_bank.setText(preferences.getString(Constant.TOPUP_NO_REK, "-"))
        text_value_nominal.setText("Rp. "+preferences.getString(Constant.TOPUP_NOMINAL, "-"))

        if (text_value_bank.text.contains(getString(R.string.TOPUP_SELECTED_BCA))){
            image_logo_bank.background = ContextCompat.getDrawable(applicationContext, R.drawable.ic_bca)
        } else if (text_value_bank.text.contains(getString(R.string.TOPUP_SELECTED_BRI))){
            image_logo_bank.background = ContextCompat.getDrawable(applicationContext, R.drawable.ic_bri)
        } else {
            image_logo_bank.visibility = View.GONE
        }
    }
}
